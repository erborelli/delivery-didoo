<?php

include_once('../includes.php');

if(!isset($_POST['dataHoraPedidos']) || !isset($_POST['acao']) || empty($_POST['acao'])) return false;

$date = new DateTime($_POST['dataHoraPedidos']);
$tosub = new DateInterval('PT3H');
$date->sub($tosub);
$dataIni = $date->format('Y-m-d H:i:s');
$dataFim = $_POST['dataHoraPedidos'];

if($_POST['acao'] == 'cancelar'){
  Pedido::cancelarVariosPedidos($dataIni,$dataFim);
}else {
  Pedido::aprovarVariosPedidos($dataIni,$dataFim);
}
