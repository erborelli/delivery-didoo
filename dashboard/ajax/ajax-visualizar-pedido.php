<?php

include_once('../includes.php');

if(!isset($_POST['idPedido']) || !is_numeric($_POST['idPedido'])) return false;

Pedido::visualizarPedido($_POST['idPedido']);
