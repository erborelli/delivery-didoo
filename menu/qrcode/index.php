
<?php require_once '../../includes.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comanda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="icon" href="<?=CAMINHO_IMAGEM?>/favicon/<?=FAVICON?>"/>
</head>
<body>
    <br>
    <div style="text-align: center;">
        <img id="imgLogo" style="height: 110px;" src="">
    </div>
    <br>

    <div style="text-align: center; margin-top: 15px; margin-bottom: 20px;">
        <h3>Selecione a categoria</h3>
    </div>
    <table class="table">
        <tbody id="tbodyCategorias">
        </tbody>
    </table>

    <!-- modal carrinho -->
    <div class="modal fade" id="carrinhoModal" tabindex="-1" role="dialog" aria-labelledby="carrinhoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="carrinhoModalLabel"></h5>
                </div>
                <div class="modal-body" style="padding: 0px !important;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Produto</th>
                                <th style="padding-left: 0px; padding-right: 0px; margin: 0px;">Qtd</th>
                                <th>Adicionais</th>
                                <th>R$</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tbodyCarrinho">
                        </tbody>
                    </table>
                    <br>
                    <div class="row" style="margin-right: 10px;">
                        <div class="col" style="text-align: right;">
                            <label>Valor Total R$</label>
                            <label id="lblValorTotalCarrinhoModal">0</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar
                        <i class="fas fa-times-circle"></i>
                    </button>
                    <button type="button" style="background-color: #00793d" class="btn btn-success" onclick="finalizarPedidoCarrinho()">
                        Confirmar compra<i style="margin-left: 5px;" class="fas fa-plus-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal finalizar pedido -->
    <div class="modal fade" id="finalizarModal" tabindex="-1" role="dialog" aria-labelledby="finalizarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="finalizarModalLabel"></h5>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="padding: 5px;">
                        <div class="row">
                            <label>Seu local</label>
                            <input id="txtMesaCliente" readonly="true" class="form-control">
                        </div>
                        <div class="row">
                            <label>Seu nome</label>
                            <input id="txtNomeCliente" class="form-control">
                        </div>
                        <div class="row">
                            <label>Seu Wathsapp</label>
                            <input id="txtWathsappCliente" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancelar <i class="fas fa-times-circle"></i>
                    </button>
                    <button type="button" class="btn btn-success" onclick="finalizarPedido()">
                        Finalizar Pedido <i class="fas fa-plus-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal da imagem do produto -->
    <div class="modal fade" id="imagemModal" tabindex="-1" role="dialog" aria-labelledby="imagemModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="imagemModalLabel"></h5>
                </div>
                <div class="modal-body">
                    <div>
                        <p id="imagemModalP"></p>
                    </div>
                    <div>
                        <img id="imagemModalImg" style="height: 300px; width: 300px; border-radius:7px;"></img>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Fechar <i class="fas fa-times-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal escolher sabores e adicionais -->
    <div class="modal fade" id="pedidoModal" tabindex="-1" role="dialog" aria-labelledby="pedidoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="pedidoModalLabel"></h5>
                </div>
                <div class="modal-body" style="padding: 2px;">
                    <div style="text-align: center;">
                        <label style="font-size: 25px; font-weight: bold;" id="txtNomeProduto"></label>
                        <p id="pedidoModalP"></p>
                    </div>
                    <input id="txtIdProduto" style="display: none;" value="">
                    <input id="txtTipo" style="display: none;" value="">
                    <div style="text-align: center;">
                        <img id="imgProduto" style="height: 300px; width: 300px; border-radius:7px;">
                    </div>
                    <div style="text-align: right; margin-top: 15px; margin-right: 20px;">
                        <label id="lblDescricaoValor">Valor R$</label>
                        <label id="lblValor">0.0</label>
                    </div>
                    <br>
                    <div style="text-align: center;">
                        <h5>Quantidade</h5>
                    </div>
                    <div class="row" style="text-align: center; width: 100%; margin-left: 1px;">
                        <div class="col-md-4 col-4">
                            <button onclick="diminuirQtd()" style="width: 90px;" class="btn btn-primary">-</button>
                        </div>
                        <div class="col-md-4 col-4">
                            <input id="txtQuantidade" style="width: 90px; text-align: center;" type="number" min="1" max="99" class="form-control" value="1"><br>
                        </div>
                        <div class="col-md-4 col-4">
                            <button onclick="aumentarQtd()" style="width: 90px;" class="btn btn-primary">+</button>
                        </div>
                    </div>
                    <div id='dvPedido'></div><br>
                    <div class="row form-group col-12 col-md-12">
                        <label>Observações</label>
                        <textarea id="txtObservacoes" style="width: 100%; margin: 0px; margin-left: 25px;" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancelar <i class="fas fa-times-circle"></i>
                    </button>
                    <button type="button" class="btn btn-success" onclick="adicionaProdutoPedido()">
                        Confirmar <i class="fas fa-plus-circle"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>

    <!-- carrinho -->
    <div id="dvCarrinho" class="container row justify-content-center"
    style="position: fixed;bottom: 0px; background-color: #00793d; height: 40px;
    vertical-align: middle; width: 110%; margin-right: 0px;
    border-radius: 40px 45px 0px 0px;"
    onclick="abrirCarrinho()">
        <i style="color: white; margin-top: 10px;" class="fas fa-shopping-cart"></i>
        <span style="color: white; margin-left: 5px; margin-top: 7px">Carrinho</span>
        <div style="margin-top: 7px; margin-left: 15px;">
            <span style="color: white;">R$</span>
            <span id="spnValorCarrrinho" style="color: white;">0</span>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/45c8cc9125.js"></script>
    <script src="scripts/pedido.js?v=<?=strtotime('now')?>"></script>
    <script src="scripts/carrinho.js?v=<?=strtotime('now')?>"></script>
    <script src="scripts/produtos.js?v=<?=strtotime('now')?>"></script>
    <script src="scripts/categorias.js?v=<?=strtotime('now')?>"></script>
    <script src="scripts/index.js?v=<?=strtotime('now')?>"></script>
</body>
</html>
