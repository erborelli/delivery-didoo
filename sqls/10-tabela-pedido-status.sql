-- Copiando estrutura para tabela mirie_coxinhas.pedido_status
CREATE TABLE IF NOT EXISTS `pedido_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `obs` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela mirie_coxinhas.pedido_status: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_status` DISABLE KEYS */;
INSERT INTO `pedido_status` (`id`, `status`, `obs`) VALUES
	(1, 'pendente', 'aguardando o estabelecimento aceitar o pedido'),
	(2, 'aceito', 'aceito pelo estabelecimento'),
	(3, 'negado', 'negado pelo estabelecimento'),
	(4, 'cancelado', 'cancelado pelo cliente');
/*!40000 ALTER TABLE `pedido_status` ENABLE KEYS */;