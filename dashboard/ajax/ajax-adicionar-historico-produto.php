<?php

include_once('../includes.php');

if(!isset($_POST['idProduto']) || !is_numeric($_POST['idProduto'])){
  $_SESSION['historicoFinalizacao'] = true;
}else {
  // adiciona na session o historico e o id do produto
  $_SESSION['historico'] = $_POST['idProduto'];
}
