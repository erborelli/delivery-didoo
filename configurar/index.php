<?php
    require_once '../dashboard/classes/AtualizaConfiguracoes.class.php';
    $atualizaConfiguracoes = new AtualizaConfiguracoes();

    // se configuração existir, redireciona
    if($atualizaConfiguracoes->verificarConfiguracao()){
        Header('Location: '.CAMINHO);
    }

    // cria arquivo de configuracao.php
    if(isset($_POST['salvar'])){
        $atualizaConfiguracoes->setParametros([
            'NOME_BANCO'    => $_POST['nome_banco'],
            'USUARIO_BANCO' => $_POST['usuario_banco'],
            'SENHA_BANCO'   => $_POST['senha_banco'],
            'CAMINHO'       => $_POST['caminho'],
            'PASTA_SISTEMA' => $_POST['pasta_sistema']
        ]);
        $atualizaConfiguracoes->definirConfiguracoes();
        $atualizaConfiguracoes->carregarConfiguracoes();
        Header('Location: '.CAMINHO);
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Configurar Sistema</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
    <br>
    <div style="text-align: center;">
        <h4>Configurações do Sistema</h4>
    </div>
    <br>
    <form method="POST">
        <div class="form-group" style="margin-left: 40px;">
            <div class="row">
                <div class="col-md-2">
                    <label>Nome Banco</label>
                    <input name="nome_banco" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>Usuário do Banco</label>
                    <input name="usuario_banco" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>Senha do Banco</label>
                    <input name="senha_banco" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <label>URL</label>
                    <input name="caminho" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <label>Pasta do Sistema</label>
                    <input name="pasta_sistema" class="form-control">
                </div>
            </div>
            <br>
            <input value="salvar" name="salvar" type="submit" class="btn btn-primary">
        </div>
    </form>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>