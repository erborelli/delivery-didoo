<?php

include_once('../includes.php');

$dados      = $_POST['dados'];
$dadosFrete = Frete::getValorEntregaBairro($dados['bairro']);
$retorno    = ['endereco' => '', 'valor' => 0];

if(empty($dadosFrete)) return false;

foreach ($dados as $key => $value) {
  $_SESSION['venda_interna']['frete'][$key] = $value;
}

$dadosFrete = $dadosFrete[0];
$retorno['valor'] = $dadosFrete->valor;
// adiciona na sessao da venda interna
$_SESSION['venda_interna']['frete']['valor'] = $dadosFrete->valor;
if(isset($_POST['semCep']) && !$_POST['semCep']){
  $_SESSION['venda_interna']['frete']['cep']   = $_POST['cep'];
  $retorno['endereco'] = $_POST['dados']['logradouro'].',
  '.$_POST['dados']['bairro'].',
  '.$_POST['dados']['localidade'].',
  '.$_POST['dados']['uf'].',
  '.$_POST['dados']['cep'];
  $_SESSION['venda_interna']['frete']['endereco'] = $retorno['endereco'];
}

echo json_encode($retorno);
