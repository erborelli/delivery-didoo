<?php

class CategoriaAdicional {

  public static function excluirVinculorPorIdCategoria($idCategoria){
    if(!is_numeric($idCategoria)) return false;
    $link = getConnection();
    $sql = "DELETE FROM categoria_adicional WHERE id_categoria=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$idCategoria]);
  }

  public static function excluirVinculoAdicionalPorIdCategoria($idCategoria,$idAdicional){
    if(!is_numeric($idCategoria)) return false;
    $link = getConnection();
    $sql = "DELETE FROM categoria_adicional WHERE id_categoria=? AND id_adicional=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$idCategoria,$idAdicional]);
  }

  public static function excluirVinculoAdicionalGrupoPorIdCategoria($idCategoria,$idAdicionalGrupo){
    if(!is_numeric($idCategoria)) return false;
    $link = getConnection();
    $sql = "DELETE FROM categoria_adicional WHERE id_categoria=? AND id_adicional_grupo=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$idCategoria,$idAdicionalGrupo]);
  }

  /**
   * vincula 'categoria' a um 'adicional'
   */
  public static function vincularAdicionalCategoria($idCategoria,$idAdicional){
    if(!is_numeric($idCategoria) || !is_numeric($idAdicional)) return false;
    
    $sql  = "INSERT INTO categoria_adicional(id_categoria,id_adicional) VALUES(?,?)";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idCategoria,$idAdicional]);
  }

  /**
   * vincula 'categoria' a um 'adicional grupo'
   */
  public static function vincularAdicionalGrupoAoCategoria($idCategoria,$idAdicionalGrupo){
    if(!is_numeric($idCategoria) || !is_numeric($idAdicionalGrupo)) return false;
    
    $sql  = "INSERT INTO categoria_adicional(id_categoria,id_adicional_grupo) VALUES(?,?)";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    return $stmt->execute([$idCategoria,$idAdicionalGrupo]);
  }

  /**
   * verifica vinculo entre 'categoria' e 'adicional' / 'adicional grupo'
   */
  public static function verificarVinculo($idCategoria,$idAdicional,$idAdicionalGrupo=0){
    $sql = "SELECT * FROM categoria_adicional WHERE id_categoria=? AND ";
    $id = 0;

    // adicional
    if($idAdicionalGrupo==0){
      $sql .= "id_adicional=?";
      $id   = $idAdicional;
    }
    
    // adicional grupo
    else{
      $sql .= "id_adicional_grupo=?";
      $id   = $idAdicionalGrupo;
    }

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idCategoria,$id]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
