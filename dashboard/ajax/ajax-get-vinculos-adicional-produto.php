<?php

include_once('../includes.php');

$idProduto          = $_POST['idProduto'];
$adicionais         = '';
$produtosAdicionais = Adicional::getAdicionaisProduto($idProduto);

foreach ($produtosAdicionais as $key => $value) {
  $vinculado   = is_numeric($value->id) ? 'checked' : '';
  $adicionais .= '<tr class="containerSelectLabelAdicionais">
                  <td><input id-adicional="'.$value->idAdicional.'" type="checkbox" '.$vinculado.'></td>
                  <td class="labelAdicionalProdutoListagem">'.$value->nome.'</td>
                </tr>';
}

$retorno = '
<table class="table tabelaVinculoAdicionais" id-produto="'.$idProduto.'">
  <thead>
    <tr>
      <th style="width:50%;" scope="col">Vincular</th>
      <th style="width:50%" scope="col">Adicional</th>
    </tr>
  </thead>
  <tbody>
    '.$adicionais.'
  </tbody>
</table>';

echo json_encode($retorno);
