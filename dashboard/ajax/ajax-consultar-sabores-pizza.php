<?php
//retorna sabores da pizza

include_once('../includes.php');

//caso não receba o id do produto, retorna erro
if(!isset($_GET['idProduto'])){
    echo('erro');
    return;
}

echo json_encode(PizzaSabores::getSaboresPizzaPorIdProduto($_GET['idProduto']),true);