<div class="col-xs-12 about-area-entrega listaToggle entrega">
  <div>
    <?php
      $fretes = Frete::getFretes();
    ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Bairro</th>
          <th scope="col">Taxa Entrega</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($fretes as $key => $value) {
            echo '<tr>
                    <td>'.$value->bairro.'</td>
                    <td>'.$value->valor.'</td>
                  </tr>
                  ';
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
