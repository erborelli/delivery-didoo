<div data-id-produto="" id="modalAdicionarPedidoCarrinho" class="modal w920 fade modalMontagemProduto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; justify-content: center; padding-right: 16px;">
  <div class="modal-dialog w920" role="document" data-cy="modal-dialog">
    <div class="modal-content w920" style="top: 0px;">
      <div class="modal-close-content">
        <i class="material-icons close-button js-modal-close" data-dismiss="modal" data-ga="modal-close" data-cy="modal-dialog__close">close</i>
      </div>
      <div class="modal-header is-hidden">
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-header-mobile">
        <button class="modal-header-content-back js-modal-close" data-dismiss="modal">
          <i class="material-icons icon">keyboard_arrow_left</i>
          <span class="text"> Voltar</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="properties-modal-body w920">
          <div class="scroll-container containerInfosProduto">
            <div class="left-side contanierEsquerdoInfosProd">
              <div class="properties-list" data-ga="property-list">
                <div id="item-properties-616724" class="item-properties">
                  <!-- propriedades do item -->
                </div>
              </div>
              <div style="position-item-qty-observations">
                <span class="labelSabores paddingAdicionais" nome-grupo="extras">Selecione a quantidade:</span>
                <span class="item-qty">
                  <div class="qty-label">Quantidade: </div>
                  <div class="item-actions js-item-amount" data-ga-action="item__change_amount" data-ga-category="item">
                    <a href="javascript:void(0)" class="js-decrease decrease pos-relative text-decoration-none btn-remove-item" data-ga="property-qty_decrease" data-cy="item__decrease">
                      <i class="material-icons pos-absolute" data-modern="remove">remove_circle</i>
                    </a>
                    <input value="1" class="input-amount js-input-amount font-size-16" type="text" data-price="" disabled="">
                    <a href="javascript:void(0)" class="js-increase increase pos-relative text-decoration-none btn-add-item" data-ga="property-qty_increase" data-cy="item__increase">
                      <i class="material-icons pos-absolute" data-modern="add">add_circle</i>
                    </a>
                  </div>
                </span>
                <div class="observations-textfield-container">
                  <div class="observations-textfield mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-upgraded is-focused" data-upgraded=",MaterialTextfield">
                    <input class="mdl-textfield__input js-item-comments" type="text" id="observacaoProduto" data-ga-action="item__add_note" data-ga-category="item">
                    <label class="mdl-textfield__label" for="observations">Observações (opcional)</label>
                  </div>
                </div>
              </div>
              <div class="containerAdicionais">
                <!-- conteudo que será substituido no JS-->
              </div>
              <span class="item-delete js-item-delete is-hidden">
                <a href="javascript:void(0)" class="js-delete">
                  <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn-delete border-r-10" data-ga="property-delete" data-upgraded=",MaterialButton,MaterialRipple">
                    <i class="material-icons btn-delete-color">delete</i>
                    <span class="btn-delete-color-text">Remover do carrinho</span>
                    <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
                </a>
              </span>
              <div class="price-container">
                <!-- preco -->
                <span class="js-item-price price" data-cy="item__price"></span>
                <button class="add-to-cart-btn adicionarCarrinhoProdutoDefault btn js-add-item" data-ref="items-list-item-240796" data-ga-action="item__add_to_cart" data-ga-category="item" data-cy="item__button--add-240796" type="button">
                  <i class="material-icons">add</i>Adicionar </button>
              </div>
            </div>
            <div class="right-side js-right-side">
              <!-- container de imagens do produto -->
              <div id="carouselImagensProdutosPadrao" data-interval="false" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#carouselImagensProdutosPadrao" data-slide="prev">
                  <span class="material-icons">keyboard_arrow_left</span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carouselImagensProdutosPadrao" data-slide="next">
                  <span class="material-icons">keyboard_arrow_right</span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <!-- item nome -->
              <p class="item-name"></p>
              <div class="item-description-container">
                <p class="js-item-description item-description"></p>
              </div>
              <div class="item-description-show-more-container">
                <span class="js-item-description-show-more item-description-show-more color-settings-secondary is-hidden">Ver mais</span>
              </div>
            </div>
          </div>
          <div class="item-mobile-footer">
            <div class="shadow-indicator-scroll"></div>

            <div class="price-container">
              <!-- preco -->
              <span class="js-item-price price" data-cy="item__price"></span>
              <button class="add-to-cart-btn adicionarCarrinhoProdutoDefault btn js-add-item" data-ref="items-list-item-240796" data-ga-action="item__add_to_cart" data-ga-category="item" data-cy="item__button--add-240796" type="button">
                <i class="material-icons">add</i> Adicionar </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer is-hidden">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>
