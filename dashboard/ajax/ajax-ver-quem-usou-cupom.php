<?php

include_once('../includes.php');

$retorno = '';
$hashCupom = $_POST['hashCupom'];
$clientesQueUsaram = Cupom::getClientesUsouCupom($hashCupom);

if(empty($clientesQueUsaram)) {
  echo 'Ninguém utilizou o cupom até o momento';exit;
}

foreach ($clientesQueUsaram as $key => $value) {
  $dataFormatada = date("d/m/Y  H:i:s", strtotime($value->data_utilizacao));
  $retorno .= '<span>O cliente '.$value->nome.' utlizou o cupom no dia '.$dataFormatada.' <small>('.$value->utilizacao.')</small></span><br>';
}

echo $retorno;
