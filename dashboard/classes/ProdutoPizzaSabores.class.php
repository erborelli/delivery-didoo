<?php

class ProdutoPizzaSabores {

  public static function excluirVinculosPorIdProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql = "DELETE FROM produto_pizza_sabores WHERE id_produto=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$idProduto]);
  }

  public static function vincularSaboresProduto($idProduto,$dadosVinculo){
    if(!is_numeric($idProduto) || empty($dadosVinculo)) return false;
    $link = getConnection();
    foreach ($dadosVinculo as $key => $value) {
      // CASO FOR MARCADO TRUE NO CHECKED PARA VINCULAR
      if($value == 'true'){
        $sql  = "INSERT INTO produto_pizza_sabores (id_produto,id_sabor) VALUES (?,?)";
        $stmt = $link->prepare($sql);
        $stmt->execute([$idProduto,$key]);
      }
    }
  }

}
