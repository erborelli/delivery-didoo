<?php

class PizzaTamanhos {

  public static function getTamanhosPizza(){
    $link = getConnection();
    $sql  = "SELECT * FROM pizza_tamanhos";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaTamanhos');
  }

  public static function getSelectTamanhosPizza($idTamanho = 0){
    $tamanhosPizza = self::getTamanhosPizza();
    $retorno       = '';
    foreach ($tamanhosPizza as $key => $value) {
      $selected = $value->id == $idTamanho ? 'selected' : '';
      $retorno .= '<option '.$selected.' value="'.$value->id.'">'.$value->label.'</option>';
    }
    return $retorno;
  }

  public static function getTamanho($idTamanho){
    if(!is_numeric($idTamanho)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM pizza_tamanhos WHERE id = ".$idTamanho;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaTamanhos');
  }

  public static function updateTamanho($dadosPost){
    if(!isset($dadosPost) || empty($dadosPost)) return false;
    $link = getConnection();
    $sql = "UPDATE pizza_tamanhos SET label=? WHERE id=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['label'],$_GET['id']]);
    return true;
  }

  public static function gravarTamanho($dadosPost){
    if(!isset($dadosPost) || empty($dadosPost)) return false;
    $link = getConnection();
    $sql = "INSERT INTO pizza_tamanhos (label) VALUES (?)";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['label']]);
    return true;
  }

}
