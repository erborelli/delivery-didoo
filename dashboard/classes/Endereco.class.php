<?php

class Endereco {

    public static function getEnderecoPorCep($cep){
      return isset($_SESSION['usuario']) && isset($_SESSION['usuario']['hash']) ? true : '';
    }

    public static function formatarEnderecoClienteSession(){
      return self::formatarEnderecoCliente(
          $_SESSION['endereco']['logradouro'],
          $_SESSION['endereco']['numero'],
          $_SESSION['endereco']['bairro'],
          $_SESSION['endereco']['localidade'],
          $_SESSION['endereco']['uf']
      );
    }

    /**
     * transforma session da venda interna em string formatada
     */
    public static function formatarEnderecoClienteSessionVendaInterna(){

      //se endereço tiver número
      if(!isset($_SESSION['venda_interna']['frete']['endereco']) && isset($_SESSION['venda_interna']['frete']['numero'])){
        //verifica cep
        $cep = isset($_SESSION['venda_interna']['frete']['cep']) ? $_SESSION['venda_interna']['frete']['cep'] : null;

        //retorna string formatada
        return self::formatarEnderecoCliente(
          $_SESSION['venda_interna']['frete']['logradouro'],
          $_SESSION['venda_interna']['frete']['numero'],
          $_SESSION['venda_interna']['frete']['bairro'],
          $_SESSION['venda_interna']['frete']['localidade'],
          $_SESSION['venda_interna']['frete']['uf'],
          $cep
        );

      // se endereço não tiver número
      }elseif (isset($_SESSION['venda_interna']['frete']['endereco'])) {
        return $_SESSION['venda_interna']['frete']['endereco'];
      }
      return '';
    }

    public static function formatarEnderecoClienteArray(array $enderecoCliente){
      return self::formatarEnderecoCliente(
        $enderecoCliente['logradouro'],
        $enderecoCliente['numero'],
        $enderecoCliente['bairro'],
        $enderecoCliente['localidade'],
        $enderecoCliente['uf']
      );
    }

    /**
     * formata os valores recebidos em uma string
     */
    private static function formatarEnderecoCliente($logradouro, $numero,$bairro,$localidade,$uf,$cep=null){
      //caso receba o cep, formata em string
      if($cep!=null){
        $cep = ' ,'.$cep;
      }

      //retorna string formatada
      return          $logradouro
        .', n'.       $numero
        .', bairro: '.$bairro
        .', Cidade:'. $localidade
        .' - '.       $uf
        . $cep;
    }
}
