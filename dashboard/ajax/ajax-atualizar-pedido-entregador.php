<?php

include_once('../includes.php');

if(!isset($_POST['idEntregador']) || !is_numeric($_POST['idEntregador'])) return false;

$entregas = Entregador::updatePedidoEntregador($_POST['idEntregador'],$_POST['idPedido']);
