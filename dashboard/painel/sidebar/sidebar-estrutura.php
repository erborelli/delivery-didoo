<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="<?=CAMINHO_DASHBOARD?>" class="brand-link">
    <img src="<?=CAMINHO?>/dashboard/images/logoDashboard/<?=LOGODASHBOARD?>" alt="logo" class="brand-image"
        style="opacity: .8">
    <span class="brand-text font-weight-light"><?=NOMELOJA?></span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
        <img src="<?=CAMINHO?>/img/dashboard/user.png" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
        <a href="#" class="d-block"><?=$_SESSION['usuario']['dados']['nome']?></a>
    </div>
    </div>

    <!-- Sidebar Menu -->
    <?php include_once(dirname(__FILE__).'../../menu/menu-estrutura.php'); ?>

</div>
<!-- /.sidebar -->
</aside>
