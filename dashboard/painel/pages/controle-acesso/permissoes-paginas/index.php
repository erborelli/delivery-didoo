<?php
  include_once('../../../../includes.php');
  $grupos = UsuarioGrupo::getGrupos();
?>

<!DOCTYPE html>
<html>
<?php include_once('../../../head/head-estrutura.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Permissões Páginas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Permissões Páginas</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <br>
          <div class="row">
            <div class="col-md-2" style="text-align: right;">
              <label>Selecione o Grupo:</label>
            </div>
            <div class="col-md-3">
              <select class="form-control">
                <?php foreach($grupos as $grupo){ ?>
                  <option value="<?=$grupo->id?>"><?=$grupo->nome?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br>
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Módulo</th>
                <th>Páginas</th>
                <th>Submodulos</th>
                <th>Páginas do Submodulo</th>
              </tr>
            </thead>
            <tbody>
                <?php
                  $path = CAMINHO_SISTEMA.'dashboard/painel/pages/';
                  $diretorio = dir($path);
                  while($arquivo = $diretorio -> read()){
                    if(strlen($arquivo)>2){
                      echo '<tr>';

                        // MODULO
                        echo '<td style="width:60px">'.$arquivo.'</td>';


                        // PAGINAS
                        echo '<td style="width:400px">';
                        $subModulos = [];
                        $diretorioPage = dir($path.$arquivo.'/');
                        while($arquivoPage = $diretorioPage->read()){
                          if(strlen($arquivoPage)>2){
                            if(substr($arquivoPage,-4)=='.php' || substr($arquivoPage,-5)=='.html' || substr($arquivoPage,-3)=='.js'){
                              echo '<input type="checkbox"> '.$arquivoPage.'<br>';
                            }else{
                              $subModulos[] = $arquivoPage;
                            }
                          }
                        }
                        $diretorioPage -> close();
                        echo '</td>';


                        // SUBMODULOS
                        echo '<td style="width:300px">';
                        $subs='';
                        foreach($subModulos as $sub){
                          $subs .= $sub.'<br>';
                        }
                        echo $subs;
                        echo '</td>';


                        // PAGINAS SUBMODULOS
                        echo '<td>';
                        $subs='';
                        foreach($subModulos as $sub){
                          $diretorioSubPage = dir($path.$arquivo.'/'.$sub.'/');
                          while($arquivoSubPage = $diretorioSubPage->read()){
                            if(strlen($arquivoSubPage)>2){
                              echo '<input type="checkbox"> '.$arquivoSubPage.'<br>';
                            }
                          }
                          $diretorioSubPage->close();
                          echo '<br>';
                        }
                        unset($subModulos);
                        echo $subs;
                        echo '</td>';

                      echo '</tr>';
                    }
                  }
                  $diretorio -> close();
                ?>
            </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/controle-acesso/grupo-usuario/criar.php">
        <i class="fas fa-plus"></i>Adicionar Grupo
      </a>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>

<script src="../../../plugins/jquery/jquery.min.js"></script>
<script src="../../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../../dist/js/adminlte.min.js"></script>
<script src="../../../dist/js/demo.js"></script>
<script src="../../../plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
