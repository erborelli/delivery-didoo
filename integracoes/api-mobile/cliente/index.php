<?php

require_once('../include.php');

//coxinha/integracoes/api-mobile/cliente?logar

//retorna cliente por cpf e senha
if(isset($_GET['logar'])){
    $login = Cliente::logar($_POST);

    //verifica login
    if($login==null){
        echo(json_encode(['mensagem'=>'cpf ou senha incorretos'],true));
        http_response_code(401);
        return;
    }

    //retorna dados cliente
    echo(json_encode($login,true));
    http_response_code(200);
    return;
}

//atualiza cliente
if(count($_PUT)){
    $retornoPedido = Cliente::updateClienteApp($_PUT);

    if($retornoPedido==false){
        echo(json_encode(['mensagem'=>'campos obrigatorios ausentes'],true));
        http_response_code(400);
        return;
    }

    http_response_code(200);
    return;
}

//insere cliente
if(count($_POST)){
    $retornoPedido = Cliente::insertClienteApp($_POST);

    if($retornoPedido==false){
        echo(json_encode(['mensagem'=>'campos obrigatorios ausentes'],true));
        http_response_code(400);
        return;
    }

    echo(json_encode(['mensagem'=>'cliente cadastrado com sucesso'],true));
    http_response_code(200);
    return;
}

echo(json_encode(['mensagem'=>'campos obrigatorios ausentes'],true));
http_response_code(401);
return;