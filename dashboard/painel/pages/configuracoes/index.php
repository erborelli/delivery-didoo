<?php
include_once('../../../includes.php');

$link = getConnection();
if(isset($_POST) && isset($_POST['btnEnviar'])){
  foreach ($_POST as $key => $value) {
    $query = 'UPDATE config SET valor = "'.$value.'" WHERE hash = "'.$key.'"';
    $sql = $link->prepare($query);
    $sql->execute();
  }
  $dadosSemFormatar = Config::getVariaveisConfig($link);
  $dados            = [];
  foreach ($dadosSemFormatar as $key => $value) {
    $dados[$dadosSemFormatar[$key]->hash] = $dadosSemFormatar[$key]->valor;
  }
}else{
  $dadosSemFormatar = Config::getVariaveisConfig($link);
  $dados            = [];
  foreach ($dadosSemFormatar as $key => $value) {
    $dados[$dadosSemFormatar[$key]->hash] = $dadosSemFormatar[$key]->valor;
  }
}

// UPLOAD DOS ARQUIVOS
foreach ($_FILES as $key => $value) {
  if(!empty($_FILES) && isset($_FILES[$key]) && $_FILES[$key]['error'] == 0){
    $destinoArquivo = CAMINHO_UPLOAD . $key.'/' . basename($_FILES[$key]['name']);
    // Caso já existir um arquivo com o mesmo nome
    if(file_exists($destinoArquivo)) {
      chmod($destinoArquivo,0755);
      unlink($destinoArquivo);
    }
    move_uploaded_file($_FILES[$key]['tmp_name'], $destinoArquivo);

    $_POST[$key] = $_FILES[$key]['name'];

    // atualiza no banco
    $query = 'UPDATE config SET valor = "'.$_FILES[$key]['name'].'" WHERE hash = "'.$key.'"';
    $sql = $link->prepare($query);
    $sql->execute();

    // apos atualizar ele atualiza o array de dados qeu vai ser utilizado abaixo
    $dados[$key] =  $_FILES[$key]['name'];
  }
}

$selectConfigCep = '';
$opcoesConfigCep = ['antes' => 'Antes de escolher o produto','depois' => 'Depois (finalização da compra)'];
foreach ($opcoesConfigCep as $key => $value) {
  $selected = isset($dados['pedirCep']) && $dados['pedirCep'] == $key ? 'selected' : '';
  $selectConfigCep .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectCategoriasHome   = '';
$opcoesConfigCategorias = ['aberta' => 'Abertas','fechada' => 'Fechadas'];
foreach ($opcoesConfigCategorias as $key => $value) {
  $selected = isset($dados['categoriasHome']) && $dados['categoriasHome'] == $key ? 'selected' : '';
  $selectCategoriasHome .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectNovosPedidos   = '';
$opcoesConfigPedidos = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigPedidos as $key => $value) {
  $selected = isset($dados['aceitarPedidosAutomatico']) && $dados['aceitarPedidosAutomatico'] == $key ? 'selected' : '';
  $selectNovosPedidos .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigEndereco = '';
$opcoesConfigEndereco = ['cep' => 'Por CEP','digitando' => 'Digitando (Manual)'];
foreach ($opcoesConfigEndereco as $key => $value) {
  $selected = isset($dados['informarEndereco']) && $dados['informarEndereco'] == $key ? 'selected' : '';
  $selectConfigEndereco .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectOferecerRetirada       = '';
$opcoesConfigOferecerRetirada = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigOferecerRetirada as $key => $value) {
  $selected = isset($dados['oferecerRetirada']) && $dados['oferecerRetirada'] == $key ? 'selected' : '';
  $selectOferecerRetirada .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigPizza      = '';
$opcoesConfigValorPizza = ['media' => 'Média dos valores','maior' => 'Maior valor'];
foreach ($opcoesConfigValorPizza as $key => $value) {
  $selected = isset($dados['configValorPizza']) && $dados['configValorPizza'] == $key ? 'selected' : '';
  $selectConfigPizza .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigObservacao = '';
$opcoesConfigObservacao = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigObservacao as $key => $value) {
  $selected = isset($dados['mostrarObservacaoPedido']) && $dados['mostrarObservacaoPedido'] == $key ? 'selected' : '';
  $selectConfigObservacao .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigComprarPelaVendaCelular = '';
$opcoesConfigComprarPelaVendaCelular = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigComprarPelaVendaCelular as $key => $value) {
  $selected = isset($dados['comprarPelaVendaCelular']) && $dados['comprarPelaVendaCelular'] == $key ? 'selected' : '';
  $selectConfigComprarPelaVendaCelular .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigArquivoImpressao = '';
$opcoesConfigArquivoImpressao = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigArquivoImpressao as $key => $value) {
  $selected = isset($dados['gerarArquivoImpressao']) && $dados['gerarArquivoImpressao'] == $key ? 'selected' : '';
  $selectConfigArquivoImpressao .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectConfigMostrarLogoEtiqueta = '';
$opcoesConfigMostrarLogoEtiqueta = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigMostrarLogoEtiqueta as $key => $value) {
  $selected = isset($dados['mostrarLogoEtiqueta']) && $dados['mostrarLogoEtiqueta'] == $key ? 'selected' : '';
  $selectConfigMostrarLogoEtiqueta .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectRedirecionarWhatsapp       = '';
$opcoesConfigRedirecionarwhatsapp = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesConfigRedirecionarwhatsapp as $key => $value) {
  $selected = isset($dados['redirecionarWhatsapp']) && $dados['redirecionarWhatsapp'] == $key ? 'selected' : '';
  $selectRedirecionarWhatsapp .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

$selectExibirBoxEnderecoMobile = '';
$opcoesExibirBoxEnderecoMobile = ['s' => 'Sim','n' => 'Não'];
foreach ($opcoesExibirBoxEnderecoMobile as $key => $value) {
  $selected = isset($dados['mostrarEnderecoMobile']) && $dados['mostrarEnderecoMobile'] == $key ? 'selected' : '';
  $selectExibirBoxEnderecoMobile .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
}

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Configurações</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Configurações</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post" enctype="multipart/form-data">

          <div class="col-md-6">

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Geral</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nomeLoja">Nome Loja</label>
                  <input type="text" name="nomeLoja" id="nomeLoja" class="form-control" value="<?=$dados['nomeLoja']?>">
                </div>
                <div class="form-group">
                  <label style="margin-bottom: 0px;" for="whatsappEstabelecimento">WhatsApp do Estabelecimento</label>
                  <br><span>DDD + Número, SEM PONTUAÇÃO | Exemplo: 1112345678</span>
                  <input type="text" name="whatsappEstabelecimento" id="whatsappEstabelecimento" class="form-control" value="<?=$dados['whatsappEstabelecimento']?>">
                </div>
                <div class="form-group">
                  <label style="margin-bottom: 0px;" for="whatsappFormatado">WhatsApp Formatado</label>
                  <br><span>DDD + Número, FORMATADO | Exemplo: (11) 9 1234-5678</span>
                  <input type="text" name="whatsappFormatado" id="whatsappFormatado" class="form-control" value="<?=$dados['whatsappFormatado']?>">
                </div>
                <div class="form-group">
                  <label for="tempoEspera">Tempo Espera</label>
                  <input type="text" name="tempoEspera" value="<?=$dados['tempoEspera']?>" id="tempoEspera" class="form-control">
                </div>
                <div class="form-group">
                  <label for="taxaEntrega">Taxa Mínima de Entrega</label>
                  <input type="text" name="taxaEntrega" id="taxaEntrega" value="<?=$dados['taxaEntrega']?>" class="form-control">
                </div>
                <div class="form-group">
                  <label for="cepLoja">Cep Loja</label>
                  <input type="text" name="cepLoja" id="cepLoja" class="form-control" value="<?=$dados['cepLoja']?>">
                </div>
                <div class="form-group">
                  <label for="destaques1">ID's dos 2 primeiros produtos em destaque(separados por vírgula)</label>
                  <input type="text" name="destaques1" id="destaques1" class="form-control" value="<?=$dados['destaques1']?>">
                </div>
                <div class="form-group">
                  <label for="destaques2">ID's dos 2 últimos produtos em destaque(separados por vírgula)</label>
                  <input type="text" name="destaques2" id="destaques2" class="form-control" value="<?=$dados['destaques2']?>">
                </div>
                <div class="form-group">
                  <label for="valorMinimoPedido">Valor mínimo do pedido</label>
                  <input type="text" name="valorMinimoPedido" id="valorMinimoPedido" class="form-control" value="<?=$dados['valorMinimoPedido']?>">
                </div>
                <div class="form-group">
                  <label for="sobreNos">Requisitar o usuário à digitar o cep Antes/Depois de escolher o produto</label>
                  <select class="form-control" name="pedirCep">
                    <?=$selectConfigCep?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="mostrarObservacaoPedido">Exibir opção de observação para (Aceitar / Negar) pedido?</label>
                  <select class="form-control" name="mostrarObservacaoPedido">
                    <?=$selectConfigObservacao?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="informarEndereco">Como o usuário do site deverá informar o endereço?</label>
                  <select class="form-control" name="informarEndereco">
                    <?=$selectConfigEndereco?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="categoriasHome">Trazer categorias home (aberta/fechada)</label>
                  <select class="form-control" name="categoriasHome">
                    <?=$selectCategoriasHome?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="oferecerRetirada">Oferecer retirada no local?</label>
                  <select class="form-control" name="oferecerRetirada">
                    <?=$selectOferecerRetirada?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="mostrarEnderecoMobile">Exibir box mobile, solicitando Endereço?</label>
                  <select class="form-control" name="mostrarEnderecoMobile">
                    <?=$selectExibirBoxEnderecoMobile?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="aceitarPedidosAutomatico">Aceitar novos pedidos automáticamente?</label>
                  <select class="form-control" name="aceitarPedidosAutomatico">
                    <?=$selectNovosPedidos?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="mostrarLogoEtiqueta">Exibir logo na etiqueta?</label>
                  <select class="form-control" name="mostrarLogoEtiqueta">
                    <?=$selectConfigMostrarLogoEtiqueta?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="sobreNos">Sobre nós</label>
                  <textarea name="sobreNos" id="sobreNos" class="form-control" rows="4"><?=$dados['sobreNos']?></textarea>
                </div>
              </div>
            </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Produtos</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Calculo valores pizza</label>
                  <select class="form-control" name="configValorPizza">
                    <?=$selectConfigPizza?>
                  </select>
                </div>
              </div>
            </div>

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Venda Celular / Comanda</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="logo">Permitir venda</label>
                  <select class="form-control" name="comprarPelaVendaCelular">
                    <?=$selectConfigComprarPelaVendaCelular?>
                  </select>
                </div>
              </div>
            </div>

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Impressão</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Gerar Arquivo de Impressão</label>
                  <select class="form-control" name="gerarArquivoImpressao">
                    <?=$selectConfigArquivoImpressao?>
                  </select>
                </div>
              </div>
            </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Cores</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="corFaixaPrincipalMobile">Cor Faixa Principal Menu Mobile</label>
                  <input type="color" id="corFaixaPrincipalMobile" name="corFaixaPrincipalMobile" value="<?=$dados['corFaixaPrincipalMobile']?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Endereço Loja</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                <div class="form-group">
                  <label for="rua">Rua</label>
                  <input type="text" name="rua" id="rua" value="<?=$dados['rua']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="numero">Número do endereço (nº)</label>
                  <input type="text" name="numero" id="numero" value="<?=$dados['numero']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="bairro">Bairro</label>
                  <input type="text" name="bairro" id="bairro" value="<?=$dados['bairro']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="cidade">Cidade</label>
                  <input type="text" name="cidade" id="cidade" value="<?=$dados['cidade']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="estado">Estado (por extenso, ex: São Paulo)</label>
                  <input type="text" name="estado" id="estado" value="<?=$dados['estado']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="uf">UF (ex: SP)</label>
                  <input type="text" name="uf" id="uf" value="<?=$dados['uf']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="cep">CEP</label>
                  <input type="text" name="cep" id="cep" value="<?=$dados['cep']?>" class="form-control"></input>
                </div>

              </div>
            </div>

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Imagens</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group" style="border: 1px solid #c6c6c6;padding: 10px;border-radius: 10px;">
                  <label for="logo">Logo (500x500)</label>
                  <li class="list-inline-item" style="float:left;height: 50px;">
                      <img alt="Logo" style="width:65px;" class="table-avatar" src="<?=CAMINHO_DASHBOARD?>/images/logo/<?=$dados['logo']?>">
                  </li><br>
                  <!-- <input type="image" id="inputEstimatedBudget" class="form-control"> -->
                  <input type="file" name="logo" id="logo">
                  <!-- <input type="submit" value="Upload Image" name="submit"> -->
                </div>
                <div class="form-group" style="border: 1px solid #c6c6c6;padding: 10px;border-radius: 10px;">
                  <li class="list-inline-item" style="float:left;height: 50px;">
                      <img alt="Banner home" style="width:65px;" class="table-avatar" src="<?=CAMINHO_DASHBOARD?>/images/bannerHome/<?=$dados['bannerHome']?>">
                  </li>
                  <label for="bannerHome">Banner Home (1920x504)</label>
                  <br>
                  <!-- <input type="image" id="inputSpentBudget" class="form-control"> -->
                  <input type="file" name="bannerHome" id="bannerHome">
                </div>
                <div class="form-group" style="border: 1px solid #c6c6c6;padding: 10px;border-radius: 10px;">
                  <li class="list-inline-item" style="float:left;height: 50px;">
                      <img alt="Logo Etiqueta / Dashboard" style="width:65px;" class="table-avatar" src="<?=CAMINHO_DASHBOARD?>/images/logoDashboard/<?=$dados['logoDashboard']?>">
                  </li>
                  <label for="logoDashboard">Logo Etiqueta / Dashboard</label>
                  <br>
                  <!-- <input type="image" id="inputSpentBudget" class="form-control"> -->
                  <input type="file" name="logoDashboard" id="logoDashboard">
                </div>
                <div class="form-group" style="border: 1px solid #c6c6c6;padding: 10px;border-radius: 10px;">
                  <li class="list-inline-item" style="float:left;height: 50px;">
                      <img alt="Favicon" style="width:65px;" class="table-avatar" src="<?=CAMINHO_DASHBOARD?>/images/favicon/<?=$dados['favicon']?>">
                  </li>
                  <label for="logoDashboard">Favicon [Extensões: .ico / .png] [512x512]</label>
                  <br>
                  <input type="file" name="favicon" id="favicon">
                </div>
                <!-- <div class="form-group">
                  <label for="inputEstimatedDuration">Estimated project duration</label>
                  <input type="number" id="inputEstimatedDuration" class="form-control">
                </div> -->
              </div>
            </div>

            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Promocional</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="tituloPromocional">Titulo Promocional</label>
                  <input type="text" name="tituloPromocional" id="tituloPromocional" value="<?=$dados['tituloPromocional']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="textoPromocional">Texto Promocional</label>
                  <textarea name="textoPromocional" id="textoPromocional" class="form-control" rows="4"><?=$dados['textoPromocional']?></textarea>
                </div>
              </div>
            </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Bot Telegram</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="botToken">Token Bot</label>
                  <input type="text" name="botToken" id="botToken" value="<?=$dados['botToken']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="botChatId">Chat Id</label>
                  <input type="text" name="botChatId" id="botChatId" value="<?=$dados['botChatId']?>" class="form-control"></input>
                </div>
              </div>
            </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Agendamento</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="intervaloTempoAgendamento">Intervalo entre os Tempos do Agendamento (Em Minutos)</label>
                  <input type="text" name="intervaloTempoAgendamento" id="intervaloTempoAgendamento" max="60" value="<?=$dados['intervaloTempoAgendamento']?>" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="quantidadeDeAgendamentosPorIntervalo">Quantidade de agendamentos permitidos para cada horário:</label>
                  <input type="text" name="quantidadeDeAgendamentosPorIntervalo" id="quantidadeDeAgendamentosPorIntervalo" max="60" value="<?=$dados['quantidadeDeAgendamentosPorIntervalo']?>" class="form-control"></input>
                </div>
              </div>
            </div>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Pedidos WhatsApp</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="redirecionarWhatsapp">Redirecionar para o Whatsapp?</label>
                  <select class="form-control" id="redirecionarWhatsapp" name="redirecionarWhatsapp">
                    <?=$selectRedirecionarWhatsapp?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="tempoRedirecionarWhatsapp">Tempo de redirecionamento para o WhatsApp</label>
                  <input type="text" name="tempoRedirecionarWhatsapp" id="tempoRedirecionarWhatsapp" max="60" value="<?=$dados['tempoRedirecionarWhatsapp']?>" class="form-control"></input>
                </div>
              </div>
            </div>

            <?php if(defined('VERSAOBANCO')){ ?>
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Atualizar Banco de Dados</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <button id="btnAtualizarBanco" type="button" class="btn btn-primary">Atualizar</button>
                </div>
              </div>
            </div>
            <?php } ?>

          </div>
          <div class="row">
            <div class="col-12">
              <!-- <a href="#" class="btn btn-secondary">Cancel</a> -->
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>
<script>
  $(document).on('click','#btnAtualizarBanco',function(){
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/configuracao/ajax-atualizar-banco.php',
      type    : 'POST',
      dataType: 'JSON',
      success: function(data){
        console.log(data);
        mensagem(data.status,data.mensagem)
      },error:function(data){
        console.log(data)
        mensagem(data.status,data.mensagem)
      }
    })
  })

  function mensagem(status, mensagem){
    if(status){
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar   = false;
      toastr.success(mensagem)
    }else{
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar   = false;
      toastr.error(mensagem)
    }
  }
</script>

</body>
</html>
