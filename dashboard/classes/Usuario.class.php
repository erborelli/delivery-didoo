<?php

class Usuario {

    public static function getDadosUsuario($email){
        $link = getConnection();
        $sql  = "SELECT * FROM usuarios WHERE email = :email";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function criarUsuario($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "INSERT INTO usuarios(nome,email,senha,id_usuario_grupo) VALUES (?,?,?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['email'],$dadosPost['senha'],$dadosPost['id_usuario_grupo']]);
      return true;
    }

    public static function getUsuarios(){
        $link = getConnection();
        $sql  = "SELECT * FROM usuarios";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Usuario');
    }

    public static function updateUsuario($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE usuarios SET nome=?,email=?,senha=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['email'],$dadosPost['senha'],$_GET['id']]);
      return true;
    }

    public static function getUsuario($idUsuario){
      $link = getConnection();
      $sql  = "SELECT * FROM usuarios WHERE id = :idUsuario";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Usuario');
    }

}
