function deletarCustom(btn){
    btn.parentElement.parentElement.remove()
}

function ocultarDivCustom(){
    document.getElementById('dvProdutoCustom').style.display = 'none'
}

//botão mostrar/ocultar produto custom
function mostrarProdutoCustom(botao){
    let dvProdutoCustom = document.getElementById('dvProdutoCustom')

    if(dvProdutoCustom.style.display == 'none'){
        dvProdutoCustom.style.display = ''
    }

    //se div amostra
    else{ 
        let div = document.createElement('div')
        div.style.marginTop = '3px'
        div.className = 'row'

        //criando divs
        let divNome  = document.createElement('div')
        let divValor = document.createElement('div')
        let divQtd   = document.createElement('div')
        let divBtn   = document.createElement('div')

        //criando inputs
        let txtNome  = document.createElement('input')
        let txtValor = document.createElement('input')
        let txtQtd   = document.createElement('input')

        //icone
        let iDel = document.createElement('i')
        iDel.className = 'fa fa-ban'
        iDel.style.color = 'white'

        //button
        let btnDel       = document.createElement('button')
        btnDel.type      = 'button'
        btnDel.className = 'btn'
        btnDel.style.backgroundColor = '#fe2b3e'
        btnDel.setAttribute('onclick','deletarCustom(this)')

        divNome.className  = 'col-md-5'
        divValor.className = 'col-md-2'
        divQtd.className   = 'col-md-2'
        divBtn.className   = 'col-md-1'

        //input nome
        txtNome.className  = 'form-control'
        txtNome.name       = 'nomeProdutoCustom'

        //input valor
        txtValor.className = 'form-control'
        txtValor.name      = 'valorProdutoCustom'
        txtValor.type      = 'number'

        //input quantidade
        txtQtd.className   = 'form-control'
        txtQtd.name        = 'quantidadeProdutoCustom'
        txtQtd.min         = '1'
        txtQtd.value       = '1'
        txtQtd.type        = 'number'

        divNome.append(txtNome)
        divValor.append(txtValor)
        divQtd.append(txtQtd)
        btnDel.append(iDel)
        divBtn.append(btnDel)

        div.append(divNome)
        div.append(divValor)
        div.append(divQtd)
        div.append(divBtn)

        dvProdutoCustom.append(div)
    }
}