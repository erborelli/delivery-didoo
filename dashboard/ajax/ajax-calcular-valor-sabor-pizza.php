<?php

include_once('../includes.php');

if(!is_numeric($_POST['idVinculoSaborValorPizza']) || !is_numeric($_POST['idProduto'])) exit;

$valorPizza = PizzaSabores::getValorPizzaAPartirDoIdSaborValor($_POST['idVinculoSaborValorPizza'])->preco;

// CASO AINDA NÃO FOI SETADO NA SESSION VALORES DA PIZZA EU SETO
if(!isset($_SESSION['valoresProduto']) || !isset($_SESSION['valoresProduto']['valoresPizza'])){
  $_SESSION['valoresProduto']['valoresPizza'] = [];
}

// CASO HOUVER OUTROS INDICES SALVOS NA SESSAO REFERENTES A OUTROS IDS DE PIZZA, LIMPA ELES
if(!empty($_SESSION['valoresProduto']['valoresPizza']) &&
   !isset($_SESSION['valoresProduto']['valoresPizza'][$_POST['idProduto']])) $_SESSION['valoresProduto']['valoresPizza'] = [];

if($_POST['acao'] == 'add'){
  $_SESSION['valoresProduto']['valoresPizza'][$_POST['idProduto']][$_POST['idVinculoSaborValorPizza']] = $valorPizza;
}else {
  unset($_SESSION['valoresProduto']['valoresPizza'][$_POST['idProduto']][$_POST['idVinculoSaborValorPizza']]);
}

$valorPizza = Produto::calcularValorProduto($_POST['idProduto']);

echo json_encode(number_format($valorPizza,2));
