<?php

include_once('../includes.php');

$retorno = ['sucesso'       => false,
            'valorAbaixo'   => false,
            'carrinhoVazio' => true];

// valor minimo da sessao
$valorMinimoPedido = $_SESSION['valorMinimoPedido'];

// faço uma série de verificações para confirmar que o cliente tem produtos e o valor necessário no carrinho
if(isset($_SESSION['carrinho']) && isset($_SESSION['carrinho']['itens']) && !empty($_SESSION['carrinho']['itens'])){

  if(isset($_SESSION['carrinho']['total']) && $_SESSION['carrinho']['total'] >= $valorMinimoPedido){
    $retorno['sucesso'] = true;
  }else {
    // caso cair aqui o valor nao atingiu o minimo exigido
    $retorno['valorAbaixo'] = true;
    $retorno['valor']       = $valorMinimoPedido;
  }

}else {
  // caso cair aqui o carrinho está vazio
  $retorno['carrinhoVazio'] = true;
}

echo json_encode($retorno);
