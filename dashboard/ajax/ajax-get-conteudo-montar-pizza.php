<?php

include_once('../includes.php');

// Report all PHP errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$idProduto = $_POST['idProduto'];
$idTamanho = $_POST['idTamanho'];
$retorno = ['sucesso' => false, 'layoutSabores' => '', 'layoutAdicionais' => '', 'qtdSabores' => 1];

// caso nao vier um id de produto
if(!is_numeric($idProduto) || $idTamanho == 0){
  echo json_encode($retorno);exit;
}

$saboresPizza = PizzaSabores::getSaboresPizzaHome($idProduto,$idTamanho);
$adicionais   = Adicional::getAdicionaisPorProdutoFormatados($idProduto);

// caso não houver vinculos
if(empty($saboresPizza)){
  echo json_encode($retorno);exit;
}

$retorno['layoutSabores']    = Index::montarLayoutSaboresPizza($saboresPizza);
$retorno['layoutAdicionais'] = Index::montarLayoutAdicionais($adicionais, $idProduto);
$retorno['sucesso']          = true;
$retorno['qtdSabores']       = $saboresPizza[0]->qtdSabores;

echo json_encode($retorno);
