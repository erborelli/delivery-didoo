<?php
include_once('../../../includes.php');

$totalRegistros = 10;
$pagina         = isset($_GET['pagina']) ? $_GET['pagina'] : 0;
$pagianAtual    = $pagina == 0 ? 1 : $pagina;
$inicio         = $pagianAtual - 1;
$inicio         = $inicio * $totalRegistros;

$botaoAnterior   = '';
$botaoProximo    = '';
$paginaAtual     = '';
$qtdTotalPedidos = Pedido::getTotalPedidosAgendados();

$paginaAtual    = '<span class="pagAtual">Pag. Atual: '.$pagianAtual.'</span>';
// botoes anterior / proximo
$totalPaginas   = $qtdTotalPedidos / $totalRegistros; // verifica o número total de páginas
// Botões "Anterior e próximo"
$anterior       = $pagianAtual -1;
$proximo        = $pagianAtual +1;
if ($pagianAtual > 1) {
  $botaoAnterior = "<a href='?pagina=$anterior'><button class='botoesPaginacao'><< Página Anterior</button></a>";
}
if ($pagianAtual < $totalPaginas) {
  $botaoProximo  = "<a href='?pagina=$proximo'><button class='botoesPaginacao'>Próxima Página >></button></a>";
}

$pedidos = Pedido::getAgendamentos($inicio,$totalRegistros);

$selectStatusPedido = '';

function retornarStatusPedidoPorId($idStatus){
  $possiveisStatus = [1 => 'pendente',
                      2 => 'aceito',
                      3 => 'negado',
                      4 => 'cancelado'];
  return $possiveisStatus[$idStatus];
}

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<style>
  .tr-pedido-aceito{
    background-color: #9aff9a !important;
    font-weight: bold !important;
  }
  tr small{
    font-weight: bold !important;
  }
  .tr-pedido-pendente{
    background-color: #ffd17c !important;
    font-weight: bold !important;
  }
  .tr-pedido-negado{
    background-color: #ff9f9f !important;
    font-weight: bold !important;
  }
  .tr-pedido-cancelado{
    background-color: #ff9f9f !important;
    font-weight: bold !important;
  }
</style>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><b>Agendamento</b> de Pedidos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pedidos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Modal -->
    <div class="modal fade" id="modalEtiqueta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:345px;margin:0 auto;">
          <div class="modal-header">
            <!-- <h5 class="modal-title" id="exampleModalLabel">Etiqueta</h5> -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center" id="conteudoEtiqueta">

            <style>
            /* css etiqueta */
            html,body{
              margin:0px;
              padding:0px;
            }
            .etiqueta{
              width: 289px;
              height: auto;
              margin:0 7px;
              overflow: hidden;
              z-index: 1;
              float:left;
              font-family: arial;
            }
            .etiqueta .bloco:first-child{
              margin-top:0;
            }
            .etiqueta .bloco{
              width: 96%;
              font-size:15px;
              color:#C00;
              border: 1px solid #000;
              padding: 5px;
              margin: 5px 0 0 0;
              position:relative;
            }
            .etiqueta .bloco .txtEtiqueta{
              color:#000;
              margin:3px 0 0 0;
            }
            .etiqueta .bloco .imgEtiqueta span.inativo{
              display:none;
            }
            .etiqueta .bloco .imgEtiqueta span.ativo{
              display: table-cell;
            }
            .etiqueta .bloco .imgEtiqueta span{
              vertical-align:middle;
              display:table-cell;
            }
            .etiqueta .bloco .imgEtiqueta{
              width: 100%;
              height:110px;
              text-align: center;
              display:table;
              vertical-align:middle;
            }
            .etiqueta .bloco .imgEtiqueta img{
              vertical-align:middle;
            }
            .etiqueta .tracejado{
              width: 100%;
              border-bottom: 2px dashed #000;
              margin: 5px 0 0 0;
            }
            .impressora{
              float: right;
              cursor: pointer;
            }
            .selos{
              text-align: left;
              float: left;
            }
            @media print {
              .selos, .impressora {
                visibility: hidden;
              }
            }

            .fancyVerEtiquetaMultiestoque{
              padding-left: 30px;
              font-size: 11px;
              color: blue;
              cursor: pointer;
            }
            .fancybox-slide--iframe .fancybox-content{
              overflow: hidden !important;
              border-radius: 10px !important;
            }
            </style>

            <!-- conteudo etiqueta -->
            <div style="width: 260px;max-width:100%;max-height: 900px;height: auto;" class=" pace-done">
              <div class="pace  pace-inactive">
                <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                  <div class="pace-progress-inner"></div>
                </div>
                <div class="pace-activity"></div>
              </div>
              <div id="fancyTamanhoEtiqueta" style="text-align:left;">
                <div class="etiqueta" style="width: 260px;">
                  <div class="bloco">
                    <div class="txtEtiqueta"><b>Pedido:</b> <span class="idPedido"></span></div>
                    <div class="txtEtiqueta"><b>Data:</b> <span class="dataHoraPedido"></span></div>
                  </div>
                  <div class="tracejado"></div>
                  <div class="bloco">
                    <div class="imgEtiqueta">
                      <span>
                        <img src="<?=CAMINHO?>dashboard/images/logoDashboard/<?=LOGODASHBOARD?>" width="150">
                      </span>
                    </div>
                  </div>
                  <div class="bloco">
                    <div class="txtEtiqueta text-center"><b>Dados Cliente:</b></div>
                    <div class="txtEtiqueta" style="padding-top: 5px;"><b>Nome:</b> <span class="nomeCliPedido"></span></div>
                    <div class="txtEtiqueta"><b>Contato:</b> <span class="telCliPedido"> {{whats_cliente}}</span></div>
                    <div class="txtEtiqueta"><b>Endereco:</b> <span class="endCliPedido"></span></div>
                    <div class="txtEtiqueta"><b>Itens Pedido:</b>
                      <div class="itensPedido"></div>
                    </div>
                    <div class="txtEtiqueta"><b>Pagamento: </b><span class="pagamentoCliPedido"></span></div>
                    <div class="txtEtiqueta"><b>Total: </b>R$ <span class="valorCliPedido"></span></div>
                    <div class="txtEtiqueta"><b>Troco: </b><span class="trocoCliPedido"></span></div>
                  </div>
                </div>

                <div style="width: 250px; float:left; margin: 10px;">
                  <div class="impressora">
                    <i class="fas fa-print"></i>
                  </div>
                </div>
              </div>
              <div style="clear:both"></div>
            </div>
            <!-- fim conteudo etiqueta -->
          </div>
        </div>
      </div>
    </div>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Pedidos Agendados</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                    <th style="width: 2%">
                      #
                    </th>
                    <th style="width: 5%">
                        Nº Pedido
                    </th>
                    <th style="width: 20%">
                        Endereço
                    </th>
                    <th style="width: 15%">
                        Nome Cliente
                    </th>
                    <th style="width: 15%">
                        Itens Pedido
                    </th>
                    <th style="width: 10%">
                        Agendado
                    </th>
                    <th style="width: 10%">
                        Total Pedido
                    </th>
                    <th style="width: 10%">
                        Status
                    </th>
                  </tr>
              </thead>
              <tbody>


                  <?php
                    // entregadores cadastrados
                    $entregadores = Entregador::getEntregadores();

                    foreach ($pedidos as $key => $value) {
                      // box entregadores
                      $value->troco = $value->troco == 0 ? 'Não' : $value->troco;
                      $idEntregador    = $value->id_entregador;
                      $boxEntregadores = '<option value="0">Entregadores</option>';
                      foreach ($entregadores as $chave => $valor) {
                        $selectedEntregador = $valor->id == $idEntregador ? 'selected' : '';
                        $boxEntregadores .= '<option '.$selectedEntregador.' value="'.$valor->id.'">'.$valor->nome.'</option>';
                      }

                      $selectStatusPedido = Pedido::getSelectStatusPedidoPorId($value->id_status);

                      $itensPedido       = PedidoItem::getItensPedido($value->id);
                      $boxItensPedido    = '';
                      $saboresFormatados = '';

                      foreach ($itensPedido as $chave => $item) {
                        $boxItensPedido .= '<small>'.$item->qtd.' - '.$item->nome_produto.'</small><br>';
                        //adicionais do produto
                        $adicionaisProduto = PedidoItemAdicional::getAdicionaisItemPedidoArray($item->id);

                        if(count($adicionaisProduto)>0){
                          $adicionaisStr = '';
                          foreach($adicionaisProduto as $adicional){
                            $adicionaisStr .= $adicional['nome'].', ';
                          }
                          $adicionaisStr   = substr($adicionaisStr,0,-2);
                          $boxItensPedido .= '<small><li><b>Adicionais:</b> '.$adicionaisStr.'</li></small>';
                        }

                        //verificar sabores em caso de pizza
                        if(Produto::verificarProdutoPizza($item->id_produto)){
                          $sabores = PedidoItemSabor::getSaboresItemPedido($item->id);
                          foreach($sabores as $sabor){
                            $saboresFormatados.=$sabor['sabor'].', ';
                          }
                          $saboresFormatados = '<small><li><b>Sabores:</b> '.substr($saboresFormatados,0,-2).'</li></small>';
                        }

                        // verificar observacções
                        $observacao = '';
                        if(isset($item->observacoes) && !empty($item->observacoes)){
                          $observacao = '<small><li><b>Observação:</b> '.$item->observacoes.'</li></small><br>';
                        }
                      }
                      $dataPedido = date("d/m/Y H:m:s", strtotime($value->data_pedido));
                      $agendamento        = $value->agendamento == 's' ? 'AGENDADO PARA: <br>' : 'não';
                      $dataFormatada      = date("d/m/Y H:i:s", strtotime($value->data_agendamento));
                      $horarioAgendamento = $value->agendamento == 's' ? $dataFormatada : '';
                      echo '<tr id-pedido="'.$value->id.'" class="tr-container-linha-pedido tr-pedido-'.retornarStatusPedidoPorId($value->id_status).'">
                              <td data-id-pedido="'.$value->id.'" style="cursor:pointer" class="slideDown">
                                <i class="fas fa-angle-down"></i>
                              </td>
                              <td>'.$value->id.'<br></td>
                              <td><small>'.$value->endereco.'</small><br></td>
                              <td class="project-state text-left">
                                  <span class="">'.$value->nome_cliente.'</span>
                              </td>
                              <td class="project-actions">
                                '.$boxItensPedido.
                                  $saboresFormatados.
                                  $observacao.'
                              </td>
                              <td class="project-actions">
                                <span class="text-left">'.$agendamento.$horarioAgendamento.'</span>
                              </td>
                              <td class="project-actions">
                                <span class="">'.$value->total_pedido.'</span>
                              </td>
                              <td class="project-actions">
                                <select data-id-status="'.$value->id_status.'" class="selectStatusPedido">
                                  <span class="">'.$selectStatusPedido.'</span>
                                </select>
                              </td>
                          </tr>
                          <tr style="display:none;" class="containerInfosExtras'.$value->id.'">
                            <td colspan="8" style="padding:unset;">
                              <table style="width:100%;">
                                <tbody>
                                  <tr style="background: white;">
                                    <th style="width:16%">Entregador</th>
                                    <th style="width:12%">Taxa Entrega</th>
                                    <th style="width:18%">Contato</th>
                                    <th style="width:14%">Etiqueta</th>
                                    <th style="width:10%">Cupom</th>
                                    <th style="width:8%">Troco</th>
                                    <th style="width:20%">Data do Pedido</th>
                                  </tr>
                                  <tr style="background: white;">
                                    <td>
                                      <select data-id-pedido="'.$value->id.'" class="selectEntregadores">
                                        '.$boxEntregadores.'
                                      </select>
                                    </td>
                                    <td>'.$value->taxa_entrega.'</td>
                                    <td class="project_progress">
                                      <small>'.$value->whats_cliente.'</small>
                                    </td>
                                    <td>
                                      <span data-id-pedido="'.$value->id.'" data-toggle="modal" data-target="#modalEtiqueta" class="etiquetaAncora">etiqueta</span>
                                    </td>
                                    <td>'.$value->cupom_utilizado.'</td>
                                    <td>'.$value->troco.'</td>
                                    <td>'.$value->data_pedido.'</td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>';
                      }
                    ?>

              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <div class="col-sm-12 d-flex flex-row-reverse containerPaginacao">
      <div class="col-sm-8">
        <div>
          <?=$botaoAnterior?>
          <?=$paginaAtual?>
          <?=$botaoProximo?>
        </div>
      </div>
    </div>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
