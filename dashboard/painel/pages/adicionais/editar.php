<?php
include_once('../../../includes.php');

//post de salvar edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(Adicional::updateAdicional($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/');
  }
}

//se id do adicional não recebido, redireciona para listagem
if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/');
}

$adicionais = Adicional::getAdicionalPorId($_GET['id']);

//caso tenha retornado false
if(!$adicionais){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/');
}

if(isset($adicionais[0])){
    $adicionais = $adicionais[0];
}

$itensRelacionamentos = Adicional::getRelacionamentoPorAdicional($_GET['id']);

?>

<!DOCTYPE html>
<html>
  <style>
    #div{
      border: 1px solid;
      padding: 5px;
      border-radius: 10px;
      border-color: #d2d4d7
    }
  </style>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Adicionais</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cliente</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Editar Adicional</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="<?=$adicionais->nome?>">
                </div>
                <div class="form-group">
                  <label for="preco">Preço</label>
                  <input type="text" name="preco" id="preco" class="form-control" value="<?=$adicionais->preco?>">
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>      
      <br>
      
      <div id='div' style="background-color: white;">
        <div style="background-color: white;">
          <div style="text-align: center;">
            <h4>Produtos e Categorias com o Adicional</h4>
          </div>
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Tipo</th>
                <th>Id</th>
                <th>Nome</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($itensRelacionamentos as $item){ ?>
                <tr>
                  <td><?=$item['tipo']?></td>
                  <td><?=$item['id']?></td>
                  <td><?=$item['nome']?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>

    </section>
  </div>
  

  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
