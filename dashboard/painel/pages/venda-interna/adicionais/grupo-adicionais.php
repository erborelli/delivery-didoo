
<div id='modaisJS'>

</div>

<!-- dv primaria -->
<div class="modal fade" id="grupoAdicionaisModal" tabindex="-1" role="dialog" aria-labelledby="grupoAdicionaisModalLabel" aria-hidden="true">
  <!-- dv secundaria -->
  <div class="modal-dialog" role="document">
    <!-- dv terciaria -->
    <div class="modal-content">

      <!-- dv titulo -->
      <div class="modal-header">
        <h5 class="modal-title" id="grupoAdicionaisModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input style="display: none;" id="grupoAdicionaisQtdMin">
      <input style="display: none;" id="grupoAdicionaisQtdMax">

      <!-- dv corpo -->
      <div class="modal-body">
          <label>Valor total dos grupoAdicionais: R$</label>
          <label id="lblValorTotalgrupoAdicionais">0</label>
          <table class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th style="text-align: center;">Adicional</th>
                      <th style="text-align: center;">Valor</th>
                      <th style="text-align: center;">Selecionar</th>
                  </tr>
              </thead>
              <tbody id="tbodyGrupoAdicionais">
              </tbody>
          </table>
      </div>

      <!-- footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnConfirmarGrupoAdicionais" class="btn btn-primary">Continuar</button>
      </div>
    </div>
  </div>
</div>