<?php

class Pedido {

  public static function getSelectStatusPedidoPorId($idStatusPedido){
    $boxSelectStatus = '';
    $possiveisStatus = [1 => 'pendente',
                        2 => 'aceito',
                        3 => 'negado',
                        4 => 'cancelado'];

    foreach ($possiveisStatus as $key => $value) {
      $selectedStatus   = $key == $idStatusPedido ? 'selected' : '';
      $boxSelectStatus .= '<option '.$selectedStatus.' value="'.$key.'">'.$value.'</option>';
    }
    return $boxSelectStatus;
  }

  public static function gravarPedido(){
    //informações do pedido
    $produtos    = Produto::getProdutosSession();
    $endereco    = Endereco::formatarEnderecoClienteSession();
    $agendamento = Agendamento::formatarAgendamentoSession();
    $cupom       = Cupom::temCupomNaSessao() ? $_SESSION['carrinho']['cupom'] : '';
    $troco       = $_SESSION['troco'];

    $cliente  = [
      'nome'          =>$_SESSION['cliente']['nome'],
      'contato'       =>$_SESSION['cliente']['contato'],
      'formaPagamento'=>$_SESSION['cliente']['formaPagamento']
    ];

    //montar carrinho
    $produtos = Funcoes::unserializeArray($produtos);
    $descontoCupom = Cupom::temCupomNaSessao() ? $_SESSION['carrinho']['totalDescontoCupom'] : 0;
    $subtotal = PedidoItem::calcularSubtotalPedido($produtos) - $descontoCupom;
    // frete gratis (retirada)
    $retirada = 'n';
    if(isset($_SESSION['endereco'])        &&
       isset($_SESSION['endereco']['cep']) &&
       !empty($_SESSION['endereco'])       &&
       $_SESSION['endereco']['cep'] == CEP){
      $frete    = 0;
      $retirada = 's';
    }else { // frete padrao
      $frete    = Frete::getValorEntregaBairro($_SESSION['endereco']['bairro'])[0]->valor;
    }
    if(!is_numeric($frete)) $frete = TAXAENTREGA;
    $total    = $subtotal + $frete;
    $carrinho = [
      'total'         => $total,
      'frete'         => $frete,
      'subtotal'      => $subtotal,
      'descontoCupom' => $descontoCupom
    ];

    //insere pedido
    $idPedidoInserido = self::insertPedido($endereco,$cliente,$carrinho,$agendamento,$cupom,$troco,$retirada);
    //insere itens do pedido
    PedidoItem::gravarPedidoItens($produtos,$idPedidoInserido);
    Impressao::imprimirPedido($idPedidoInserido);
    return $idPedidoInserido;
  }

  public static function gravarPedidoMobile(array $pedido){
    $endereco    = 'mesa '.$pedido['mesa'];
    $agendamento = ['agendado'=>'n','data-hora'=>date('Y-m-d H:i:s')];
    $cliente     = ['nome'=>$pedido['clienteNome'], 'contato'=>$pedido['clienteWathsapp'], 'formaPagamento'=>'dinheiro'];
    $cupom       = '';
    $troco       = 0;
    $subtotal    = PedidoItem::calcularTotalVendaCelular($pedido['itens']);
    $frete       = 0;
    $total       = $subtotal + $frete;
    $carrinho    = ['total'=>$total, 'frete'=>$frete, 'subtotal'=>$subtotal];

    //insere pedido
    $idPedidoInserido = self::insertPedido($endereco,$cliente,$carrinho,$agendamento,$cupom,$troco);

    //insere itens do pedido
    PedidoItem::gravarPedidoItensVendaCelular($pedido['itens'],$idPedidoInserido);

    $produtosNomes = '';
    foreach($pedido['itens'] as $item){
      $produtosNomes .= $item['produto']['quantidade'] .'un- '.$item['produto']['nome'].', ';

      if(isset($item['sabores'])){
        $produtosNomes .= '| Sabores: ';
        foreach($item['sabores'] as $sabores){
          $produtosNomes .= $sabores['nome'].', ';
        }
      }

      if(isset($item['adicionais'])){
        $produtosNomes .= '| Adicionais: ';
        foreach($item['adicionais'] as $adicional){
          $produtosNomes .= $adicional['nome'].', ';
        }
      }
    }

    $msg = '### NOVO PEDIDO ### |Olá '.NOMELOJA.', |'
    .'Cliente: '.$pedido['clienteNome'].', |'
    .'Origem: mesa '.$pedido['mesa'].', | |'
    .'### VALORES ### |'
    .'Valor Total: R$'.$total.' | |'
    .'### PRODUTOS PEDIDO ### |'
    .$produtosNomes;

    $bot = new  BotAlerta();
    $bot->avisarPedido($msg);

    Impressao::imprimirPedido($idPedidoInserido);
    return ['idpedido'=>$idPedidoInserido];
  }


  private static function insertPedido($endereco, array $cliente,array $carrinho, array $agendamento, $cupom, $troco, $retirada = 'n'){

    $idStatus = 1;
    if(defined('ACEITARPEDIDOSAUTOMATICO')){
      $idStatus = ACEITARPEDIDOSAUTOMATICO == 's' ? 2 : 1;
    }

    $pontoReferencia = isset($_SESSION['endereco']['referencia']) ? $_SESSION['endereco']['referencia'] : '';
    $sql  = "INSERT INTO pedido
              (id_status,endereco,whats_cliente,nome_cliente,
              total_pedido,subtotal,
              agendamento,data_agendamento,cupom_utilizado,
              taxa_entrega,desconto_cupom,tipo_pagamento,
              ponto_referencia,troco,retirada)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idStatus,$endereco,
                    $cliente['contato'],$cliente['nome'],
                    $carrinho['total'],$carrinho['subtotal'],
                    $agendamento['agendado'],$agendamento['data-hora'],$cupom,
                    $carrinho['frete'],$carrinho['descontoCupom'],
                    $cliente['formaPagamento'],$pontoReferencia,$troco,$retirada]);

    // id do pedido inserido acima
    $idPedidoInserido = $link->lastInsertId();

    // aviso pedido telegram
    if(isset($_SESSION['mensagemPedidoTelegram'])){
      $bot = new  BotAlerta();
      $bot->avisarPedido($_SESSION['mensagemPedidoTelegram']);
    }

    // id do pedido inserido acima
    return $idPedidoInserido;
  }

  public static function verificarPedidosNaSessao(){
    // caso nao houver pedidos na session
    if(!isset($_SESSION['pedidos']) || empty($_SESSION['pedidos'])){
      return false;
    }

    // pega o id do pedido pendente na sessao do cliente
    $idPedido     = array_keys($_SESSION['pedidos'])[0];
    $dadosPedido  = Pedido::getStatusObservacaoPedido($idPedido);
    $statusPedido = $dadosPedido->id_status;

    // chegou aqui, quer dizer que há pedidos na sessao
    $retorno['sucesso'] = true;
    if($statusPedido == 1){
      return true;
    }
    return false;
  }

  public static function gravarPedidoVendaInterna($dadosClientePost){
    $dados = [
      Endereco::formatarEnderecoClienteSessionVendaInterna(),
      isset($dadosClientePost['whatsCliente']) && !empty($dadosClientePost['whatsCliente']) ? $dadosClientePost['whatsCliente'] : '',
      $dadosClientePost['nomeCliente'],
      $_SESSION['venda_interna']['valores']['total'],
      $_SESSION['venda_interna']['valores']['subtotal'],
      'n',
      $_SESSION['venda_interna']['valores']['frete'],
      $dadosClientePost['pagamento'],
      isset($dadosClientePost['trocoClinte'])  && !empty($dadosClientePost['trocoClinte'])  ? $dadosClientePost['trocoClinte']  : 0.00,
      $dadosClientePost['origemVenda']
    ];

    $link = getConnection();
    $sql  = "INSERT INTO pedido
              (endereco,whats_cliente,nome_cliente,
              total_pedido,subtotal,agendamento,
              taxa_entrega,tipo_pagamento,troco,origem)
            VALUES (?,?,?,?,?,?,?,?,?,?)";

    $stmt = $link->prepare($sql);
    $stmt->execute($dados);

    // id do pedido inserido acima
    $idPedidoInserido = $link->lastInsertId();

    //inserir os produtos do pedido
    $produtos = Produto::getProdutosSessionVendaInterna();
    foreach($produtos as $produto){
      //inserir produto no pedido, tabela 'pedido_item'
      $idPedidoItem = PedidoItem::gravarPedidoItemArray($produto[0],$idPedidoInserido);

      //inserir adicional no produto do pedido, tabela 'pedido_item_adicional'
      if(isset($produto[0]['adicionais'])){
        foreach($produto[0]['adicionais'] as $adicional){
          PedidoItemAdicional::gravarPedidoItemAdicional(
            $idPedidoItem,$adicional['id'],$adicional['valor']
          );
        }
      }

      //inserir sabores do produto na tabela 'pedido_item_sabor'
      if(isset($produto[0]['sabores'])){
        foreach($produto[0]['sabores'] as $sabores){
          PedidoItemSabor::gravarPedidoItemSabor($idPedidoItem,$sabores['idSaborValor'],$sabores['valor']);
        }
      }
    }
    Impressao::imprimirPedido($idPedidoInserido);
    // id do pedido inserido acima
    return $idPedidoInserido;
  }


  public static function getPedidos(){
    $link = getConnection();
    $sql  = "SELECT * FROM pedido WHERE agendamento = 'n' ORDER BY id DESC";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getPedidosPorCliente(int $idCliente){
    $link = getConnection();
    $sql  = "SELECT * FROM pedido
            WHERE id_cliente=:ID_CLIENTE
            ORDER BY id DESC";
    $stmt = $link->prepare($sql);
    $stmt->bindParam(':ID_CLIENTE', $idCliente);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getPedidosLimitados($inicio,$total){
    $link = getConnection();
    $sql  = "SELECT * FROM pedido WHERE agendamento = 'n' ORDER BY id DESC LIMIT " . $inicio . "," . $total;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getPedidosLimitadosPorData($inicio,$total,$data){
    date_default_timezone_set('America/Sao_Paulo');
    $link = getConnection();
    $dataPieces = explode(' - ',$data);
    foreach ($dataPieces as $key => $value) {
      $dataEntregaFormatada = date("Y-m-d", strtotime($value));
      if($key == 0){
        $dataIni    = $dataEntregaFormatada. ' 00:00:00';
      }else {
        $dataFim    = $dataEntregaFormatada. ' 23:59:59';
      }
    }
    $sql  = "SELECT * FROM pedido
             WHERE agendamento = 'n'
             AND data_pedido
             BETWEEN '".$dataIni."' AND '".$dataFim."'
             ORDER BY id DESC LIMIT " . $inicio . "," . $total;

    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getDadosPedido($idPedido){
    if(empty($idPedido) || !is_numeric($idPedido)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM pedido WHERE id = :idPedido";
    $stmt = $link->prepare($sql);
    $stmt->bindParam(':idPedido', $idPedido);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function montarSaboresEnviarWhatsapp($obProduto){
    if(!isset($obProduto->sabores) || empty($obProduto->sabores) ||
       !$obProduto instanceof Produto || empty($obProduto)) return '';

    $keysArray = array_keys((array)$obProduto->sabores);
    $retorno = 'Qtd sabores: '.count((array)$obProduto->sabores).' |';
    $sabores = PizzaSabores::getLabelSaboresPorIds($keysArray);
    foreach ($sabores as $key => $value) {
      $retorno .= ($key+1).' - '.$value->label.' |';
    }
    return $retorno;
  }

  public static function montarAdicionaisEnviarWhatsapp($obProduto){
    if(!isset($obProduto->adicionais) || empty($obProduto->adicionais) ||
       !$obProduto instanceof Produto || empty($obProduto)) return '';

    $keysArray  = array_keys((array)$obProduto->adicionais);
    $retorno    = 'Qtd adicionais: '.count((array)$obProduto->adicionais).' |';
    $adicionais = Adicional::getLabelAdicionaisPorIds($keysArray);
    foreach ($adicionais as $key => $value) {
      $retorno .= ($key+1).' - '.$value->nome.' |';
    }
    return $retorno;
  }

  public static function getDadosPedidoCliente($idPedido,$idCliente){
    if(empty($idPedido)  || !is_numeric($idPedido))  return false;
    if(empty($idCliente) || !is_numeric($idCliente)) return false;

    $link = getConnection();
    $sql  = "SELECT * FROM pedido
             WHERE id=:ID_PEDIDO
             AND id_cliente=:ID_CLIENTE";

    $stmt = $link->prepare($sql);
    $stmt->execute([':ID_PEDIDO'=>$idPedido,':ID_CLIENTE'=>$idCliente]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getTotalPedidos(){
    $link = getConnection();
    $sql  = "SELECT count(id) as total FROM pedido WHERE agendamento = 'n'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC)['total'];
  }

  public static function getTotalPedidosAgendados(){
    $link = getConnection();
    $sql  = "SELECT count(id) as total FROM pedido WHERE agendamento = 's'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC)['total'];
  }

  public static function getAgendamentos($inicio = null, $total = null){
    $link = getConnection();
    $sql  = "SELECT * FROM pedido WHERE agendamento = 's' ORDER BY id DESC";
    if(!is_null($inicio) && !is_null($total)){
      $sql  = "SELECT * FROM pedido WHERE agendamento = 's' ORDER BY id DESC LIMIT " . $inicio . "," . $total;
    }
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getQtdPedidosHoje(){
    date_default_timezone_set('America/Sao_Paulo');
    $horarioAtual     = date('Y-m-d H:i:s');
    $horarioInicioDia = date('Y-m-d 00:00:00');
    $link = getConnection();
    $sql  = "SELECT count(id) as id FROM pedido WHERE data_pedido BETWEEN '".$horarioInicioDia."' AND '".$horarioAtual."'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public static function getPedidosPorStatus($statusPedido){
    if(!is_numeric($statusPedido)) return false;
    $link = getConnection();
    $sql  = "SELECT count(id) as qtd FROM pedido WHERE id_status = ".$statusPedido." OR visualizado = 'n'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  /**
   * método responsável por cancelar todos os pedidos que estão com o status pendentes
   * a partir dos parametros de dataInicial passado e do dataFinal passado no método
   */
  public static function cancelarVariosPedidos($dataHoraPedidosIni,$dataHoraPedidosFim){
    if(empty($dataHoraPedidosIni) || empty($dataHoraPedidosFim)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido SET id_status = 3, visualizado = 's'
            WHERE data_pedido BETWEEN ? AND ?
            AND id_status = 1";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dataHoraPedidosIni,$dataHoraPedidosFim]);
    return true;
  }

  /**
   * método responsável por cancelar todos os pedidos que estão com o status pendentes
   * a partir dos parametros de dataInicial passado e do dataFinal passado no método
   */
  public static function alterarStatusPedido($idPedido, $novoStatusPedido){
    if(empty($idPedido) || empty($novoStatusPedido)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido
            SET id_status = ?
            WHERE id = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$novoStatusPedido, $idPedido]);
    return true;
  }

  /**
   * método responsável por aprovar todos os pedidos que estão com o status pendentes
   * a partir dos parametros de dataInicial passado e do dataFinal passado no método
   */
  public static function aprovarVariosPedidos($dataHoraPedidosIni,$dataHoraPedidosFim){
    if(empty($dataHoraPedidosIni) || empty($dataHoraPedidosFim)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido SET id_status = 2, visualizado = 's'
            WHERE data_pedido BETWEEN ? AND ?
            AND id_status = 1";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dataHoraPedidosIni,$dataHoraPedidosFim]);
    return true;
  }

  public static function cancelarPedido($idPedido, $obsPedido = ''){
    if(!is_numeric($idPedido)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido
            SET id_status = 3, visualizado = 's', observacao = ?
            WHERE id = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$obsPedido,$idPedido]);
    return true;
  }

  public static function aprovarPedido($idPedido, $obsPedido = ''){
    if(!is_numeric($idPedido)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido
            SET id_status = 2, visualizado = 's', observacao = ?
            WHERE id = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$obsPedido,$idPedido]);
    return true;
  }

  public static function visualizarPedido($idPedido){
    if(!is_numeric($idPedido)) return false;
    $link = getConnection();
    $sql = "UPDATE pedido
            SET visualizado = 's'
            WHERE id = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$idPedido]);
    return true;
  }

  public static function getStatusObservacaoPedido($idPedido){
    $link = getConnection();
    $sql  = "SELECT id_status, observacao, agendamento, data_agendamento
             FROM pedido
             WHERE id = ".$idPedido;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getPedido($idPedido, $array=false){
    $sql  = "SELECT * FROM pedido WHERE id=?";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedido]);
    return $array == true ? $stmt->fetch(PDO::FETCH_ASSOC) : $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getPedidoComItens($idPedido){
    $sql  = "SELECT * FROM pedido WHERE id=?";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedido]);
    $pedido = $stmt->fetch(PDO::FETCH_ASSOC);
    $pedido['produtos'] = PedidoItem::getItensPedidoArray($idPedido);
    return $pedido;
  }

  public static function getLayoutItensEtiqueta($idPedido){
    if(!is_numeric($idPedido)) return '';
    $itensPedido = PedidoItem::getItensPedidoArray($idPedido);
    $layout      = '';
    foreach ($itensPedido as $key => $value) {
      ##### PRODUTO #####
      $layout .= '<div class="produto">';
      $layout .= '<span class="nomeProduto">'.$value["nome_produto"].' - Qtd: '.$value['qtd'].'</span>';

      ##### SABORES #####
      if(isset($value['sabores']) && !empty($value['sabores'])){
        $layout .= '<div class="sabores">Sabores:</div>';
        $layout .= '<div class="itensSabores">';
        foreach ($value['sabores'] as $chave => $sabor) {
          $layout .= '<span>'.$sabor['sabor'].'</span>';
        }
        $layout .= '</div>';
      }

      ##### ADICIONAIS #####
      if(isset($value['adicionais']) && !empty($value['adicionais'])){
        $layout .= '<div class="adicionais">Adicionais:</div>';
        $layout .= '<div class="itensAdicionais">';
        foreach ($value['adicionais'] as $chave => $adicional) {
          $layout .= '<span>'.$adicional['nome'].'</span>';
        }
        $layout .= '</div>';
      }

      ##### OBSERVACAO #####
      if(isset($value['observacoes']) && !empty($value['observacoes'])){
        $layout .= '<div class="adicionais">Observação:</div>';
        $layout .= '<div class="itensAdicionais">';
        $layout .= '<span>'.$value['observacoes'].'</span>';
        $layout .= '</div>';
      }

      // fecha div do produto
      $layout .= '</div>';
    }
    return $layout;
  }

  /**
   * responsável por retornar os pedidos pendentes com id status = 1
   */
  public static function getPedidosPendentes(){
    $link = getConnection();
    $sql  = "SELECT * FROM pedido WHERE id_status = 1 OR visualizado = 'n'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Pedido');
  }

  public static function getAgendamentosHoje(){
    date_default_timezone_set('America/Sao_Paulo');
    $horarioAtual     = date('Y-m-d H:i:s');
    $horarioInicioDia = date('Y-m-d 00:00:00');
    $link = getConnection();
    $sql  = "SELECT count(id) as id FROM pedido WHERE data_pedido BETWEEN '".$horarioInicioDia."' AND '".$horarioAtual."' AND agendamento = 's'";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public static function getVendasHoje(){
    date_default_timezone_set('America/Sao_Paulo');
    $totalVendas = 0;
    $horarioAtual     = date('Y-m-d H:i:s');
    $horarioInicioDia = date('Y-m-d 00:00:00');
    $link = getConnection();
    $sql  = "SELECT b.* FROM pedido a
             INNER JOIN pedido_item b ON a.id = b.id_pedido
             WHERE a.data_pedido BETWEEN '".$horarioInicioDia."' AND '".$horarioAtual."'
             AND a.id_status = 2";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    $vendasHoje = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach ($vendasHoje as $key => $value) {
      $totalVendas += $value->valor * $value->qtd;
    }
    return number_format($totalVendas,2);
  }

}
