<?php
include_once('../../../includes.php');

$categorias = Categoria::getCategorias();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Categorias</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Categorias</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/categorias/criar.php">
        <i class="fas fa-plus"></i>Nova Categoria
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Produtos</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 5%">
                          #
                      </th>
                      <th style="width: 35%">
                          Nome categoria
                      </th>
                      <th style="width: 35%">
                          Ordem categoria
                      </th>
                      <th>
                          Data criação
                      </th>
                  </tr>
              </thead>
              <tbody>

                <?php foreach ($categorias as $key => $value) { ?>
                    <tr>
                      <td>#</td>
                      <td><a><?=$value->nome?></a><br></td>
                      <td><a><?=$value->ordem?></a><br></td>
                      <td class="project_progress">
                          <small><?=$value->data?></small>
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="<?=CAMINHO_DASHBOARD.'painel/pages/categorias/editar.php?id='.$value->id?>">
                              <i class="fas fa-pencil-alt"></i>Editar
                          </a>
                          <a data-id="<?=$value->id?>" data-modulo="categoria" class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"></i>Excluir
                          </a>
                      </td>
                  </tr>
                <?php } ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/categorias/criar.php">
          <i class="fas fa-plus"></i>Nova Categoria
      </a>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>

<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
