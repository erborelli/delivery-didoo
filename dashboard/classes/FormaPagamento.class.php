<?php

class FormaPagamento{
    public static function getTodos($array=false){
        $link = getConnection();
        $sql  = "SELECT * FROM forma_pagamento";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        return ($array==false ? $stmt->fetchAll(PDO::FETCH_CLASS,'FormaPagamento') : $stmt->fetchAll(PDO::FETCH_ASSOC));
      }
  
      public static function gravar($dados){
        if(empty($dados)) return false;
        $sql  = "INSERT INTO forma_pagamento (nome,troco) VALUES (?,?)";
        $link = getConnection();
        $stmt = $link->prepare($sql);
        $stmt->execute([trim($dados['nome']),$dados['troco']]);
      }
  
      /**
       * update utilizado no post
       * @var $dadosPost array $_POST
       */
      public static function update($dadosPost){
        if(!isset($dadosPost) || empty($dadosPost)) return false;
        $link = getConnection();
        $sql = "UPDATE forma_pagamento SET nome=?,troco=? WHERE id=?";
        $stmt= $link->prepare($sql);
        $stmt->execute([trim($dadosPost['nome']),$dadosPost['troco'],$_GET['id']]);
        return true;
      }
  
      public static function getPorId($id){
          if(!isset($id) || !is_numeric($id) || empty($id)){
            return false;
          }
          $sql  = "SELECT * FROM forma_pagamento WHERE id=?";
          $link = getConnection();
          $stmt = $link->prepare($sql);
          $stmt->execute([$id]);
          return $stmt->fetchAll(PDO::FETCH_CLASS,'FormaPagamento');
      }
}