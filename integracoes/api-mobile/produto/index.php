<?php

require_once('../include.php');

//retorna produto por id
if(isset($_GET['id'])){
    $produto = Produto::getProduto($_GET['id']);
    echo(json_encode($produto,true));
    http_response_code(200);
    return;
}

// retorna os sabores de um produto
if(isset($_GET['sabores-produto'])){
    $sabores = PizzaSabores::getSaboresPizzaPorIdProduto($_GET['sabores-produto']);
    echo(json_encode($sabores,true));
    http_response_code(200);
    return;
}

// retorna os adicionais de um produto
if(isset($_GET['adicionais-produto'])){
    $adicionais = Adicional::getTodosAdicionaisPorProdutoAdicionaisEmGrupo($_GET['adicionais-produto']);
    echo(json_encode($adicionais,true));
    http_response_code(200);
    return;
}

//retorna todos os produtos ativos
$retorno = Categoria::getCategoriasArray();
forEach($retorno as $chave => $categoria){
    if(isset($categoria['id'])){
        $retorno[$chave]['produtos'] = Produto::getProdutosPorIdCategoria($categoria['id']);
    }
}

echo(json_encode($retorno,true));
http_response_code(200);
return;