<?php

include_once('includes.php');

$link            = getConnection();
$variaveisConfig = Config::getVariaveisConfig($link);
$dadosHome       = [];

// validação caso houver valores de sabores na sessao ao recarregar/iniciar a página inicial
if(isset($_SESSION['valoresProduto'])){
  unset($_SESSION['valoresProduto']);
}

// VARIAVEIS CONFIGS
foreach ($variaveisConfig as $key => $value) {
  $dadosHome[$value->hash] = $value->valor;
}

// ATENDIMENTO
$atendimentoLoja = Atendimento::getAtendimentoLoja();
// label aberto / fechado
$labelEstadoLoja   = 'Fechado';
$iconEstadoLoja    = 'off';
$date              = new DateTime($atendimentoLoja['horarioFechamento']);
$horarioFechamento = '<span class="verHorarioDeAtendimento">Horários de funcionamento</span>';
$visibilidadeLojaAberta  = 'is-hidden';
$visibilidadeLojaFechada = '';
if($atendimentoLoja['aberta']){
  $visibilidadeLojaAberta = '';
  $visibilidadeLojaFechada = 'is-hidden';
  $horarioFechamento = 'Aberto até '.$date->format('H:i').'h';
  $labelEstadoLoja = 'Aberto';
  $iconEstadoLoja  = 'on';
}
if(Agendamento::verificarAgendamentoSessao()){
  $visibilidadeLojaAberta = '';
  $visibilidadeLojaFechada = 'is-hidden';
}

// CATEGORIAS
$categorias = Categoria::getCategorias();

// verifica se é um cliente recorrente analisando o cookie
$clienteRecorrente = isset($_COOKIE['clienteRecorrente']) ? unserialize($_COOKIE['clienteRecorrente']) : false;

$endereco        = '';
$mostrarEndereco = false;
$valorFrete      = 'A partir de R$'.$dadosHome['taxaEntrega'];

// caso não houver alguns indices do carrinho inicia
if(!isset($_SESSION['carrinho']['itens']) || !isset($_SESSION['carrinho']['total']) || !isset($_SESSION['carrinho']['subtotal'])){
  $_SESSION['carrinho']['itens']    = [];
  $_SESSION['carrinho']['total']    = 0;
  $_SESSION['carrinho']['subtotal'] = 0;
}

if($clienteRecorrente){
  // seta na sessao os dados
  $_SESSION['endereco'] = $clienteRecorrente['endereco'];
  $_SESSION['cliente']  = $clienteRecorrente['cliente'];
}

// sempre atualiza o valor do frete
$_SESSION['carrinho']['frete'] = 0;
if(isset($_SESSION['endereco']['bairro']) && !empty($_SESSION['endereco']['bairro'])){
  $dadosFrete = Frete::getValorEntregaBairro($_SESSION['endereco']['bairro']);
  $valor = isset($dadosFrete[0]) && !empty($dadosFrete[0]) ? $dadosFrete[0]->valor : 0;
  $_SESSION['carrinho']['frete'] = $valor;
}

$nomeCliente = isset($_SESSION['cliente']['nome']) ? ucwords(explode(' ',$_SESSION['cliente']['nome'])[0]) : '';
$labelRetirada = 'ENTREGA';

// caso houver um endereco na sessao ou for um cliente recorrente
if(isset($_SESSION['endereco']) && !empty($_SESSION['endereco'])){

  $mensagemSaudacao = '';
  if(isset($_SESSION['cliente'])){
    $saudacao    = Tema::getSaudacao();
    $mensagemSaudacao = '<b>'.$saudacao.' '.$nomeCliente.'! &#128525;</b><br>';
  }

  $cep = $_SESSION['endereco']['cep'];
  if($cep == '00000-000'){
    $cep = '';
  }
  $endereco        =  $mensagemSaudacao.
                    '<span class="js-address-box-description">
                    '.$_SESSION['endereco']['logradouro'].',
                    '.$_SESSION['endereco']['numero'].',
                    '.$_SESSION['endereco']['bairro'].',
                    '.$_SESSION['endereco']['localidade'].',
                    '.$_SESSION['endereco']['uf'].',
                    '.$cep.'</span>';
  $mostrarEndereco = true;
  $valorFrete      = 'R$'.number_format($_SESSION['carrinho']['frete'],2);
  if($valorFrete == 0 && $_SESSION['endereco']['cep'] == CEP) {
    $valorFrete = 'Grátis (Buscar)';
    $labelRetirada = 'RETIRADA';
  }
}
$carrinhoVazio       = isset($_SESSION['carrinho']) && isset($_SESSION['carrinho']['itens']) && !empty($_SESSION['carrinho']['itens']) ? false : true;
$subtotalCarrinho    = $carrinhoVazio ? 0 : $_SESSION['carrinho']['subtotal'];
$subtotalCarrinho    = number_format($subtotalCarrinho,2);
$totalCarrinho       = $carrinhoVazio ? 0 : $_SESSION['carrinho']['total'];
$totalCarrinho       = number_format($totalCarrinho,2);
$classeCarrinhoVazio = $carrinhoVazio ? 'empty' : '';
if($carrinhoVazio){
  $labelCarrinhoMobile = 'Seu carrinho está vazio.';
}else{
  $qtdItensCarrinho = count($_SESSION['carrinho']['itens']);
  $labelCarrinhoMobile = $qtdItensCarrinho > 1 ? $qtdItensCarrinho.' ITENS NO SEU CARRINHO' : $qtdItensCarrinho.' ITEM NO SEU CARRINHO';
}

// classes agendamento
$entregaAgendada        = false;
$classeEntregaAgendada  = 'is-hidden';
$labelAgendamento       = 'Agendar Pedido';
$labelAgendamentoCustom = 'Agendar Pedido';
if(isset($_SESSION['carrinho']['agendamento'])){
  $entregaAgendada        = true;
  $classeEntregaAgendada  = '';
  $dataFormatada          = date("d/m/Y", strtotime($_SESSION['carrinho']['agendamento']['dataAgendamento']));
  $labelAgendamento       = '<span class="hidden-md hidden-lg">Pedido agendado: </span>'.$dataFormatada.' ÀS '.$_SESSION['carrinho']['agendamento']['horario'];
  $labelAgendamentoCustom = '<span class="hidden-md hidden-lg"></span>'.$dataFormatada.' ÀS '.$_SESSION['carrinho']['agendamento']['horario'];
}

$labelCupomAplicado  = '';
$classeCupomAplicado = 'is-hidden';
if(isset($_SESSION['carrinho']['cupom'])){
  $labelCupomAplicado  = $_SESSION['carrinho']['cupom'];
  $classeCupomAplicado = '';
}

// box de categorias para filtros
$boxFiltroCategoria       = '<li><a data-id="0" class="js-all-categories-select" href="javascript:void(0)" rel="nofollow">Ver todas categorias</a></li>';
$boxFiltroCategoriaMobile = '<option class="js-all-categories-select" data-id="0" value="0">Ver todas categorias</option>';
foreach ($categorias as $key => $value) {
  $boxFiltroCategoria       .= '<li><a data-id="'.$value->id.'" href="javascript:void(0)" rel="nofollow">'.$value->nome.'</a></li>';
  $boxFiltroCategoriaMobile .= '<option value="'.$value->id.'" data-id="'.$value->id.'">'.$value->nome.'</option>';
}

$mostrarValorMinimo            = false;
$valorMinimoPedido             = $dadosHome['valorMinimoPedido'];
$_SESSION['valorMinimoPedido'] = $valorMinimoPedido;
if($valorMinimoPedido > 0){
  $mostrarValorMinimo = true;
}

// numero aleatorio get script / nao deixa cachear
$getScript = strtotime("now");

// LISTAGEM DOS PRODUTOS NA HOME
$boxProdutos = Index::getProdutosHome($categorias);

// rua / numero / bairro
$enderecoRuaNumeroBairro       = $dadosHome['rua'].', '.$dadosHome['numero'].' - '.$dadosHome['bairro'];
$tipoInformacaoConfigEndereco  = INFORMARENDERECO == 'digitando' ? 'Endereço' : 'CEP';

$dadosLayout = ['mostrarEndereco' => '',
                'endereco'        => '<a class="js-use-zipcode use-zipcode">Informe seu '.$tipoInformacaoConfigEndereco.' para entrega:</a>',
                'visibilidadeCep' => '',
                'labelRetirada'   => $labelRetirada
               ];
if($mostrarEndereco){
  $dadosLayout['mostrarEndereco'] = 'is-hidden';
  $dadosLayout['endereco']        = '<a class="js-use-zipcode use-zipcode">'.$endereco.':</a>';
}
$dadosLayout['visibilidadeCep'] = MOSTRARENDERECOMOBILE == 's' ? '' : 'display:none;';
$layoutEnderecoDesktop = Sistema::getLayout($dadosLayout,'layout/home','layout-home-endereco-desktop-cep.html');
$layoutEnderecoMobile  = Sistema::getLayout($dadosLayout,'layout/home','layout-home-endereco-mobile-cep.html');
if(INFORMARENDERECO == 'digitando'){
  $layoutEnderecoDesktop = Sistema::getLayout($dadosLayout,'layout/home','layout-home-endereco-desktop-digitando.html');
  $layoutEnderecoMobile  = Sistema::getLayout($dadosLayout,'layout/home','layout-home-endereco-mobile-digitando.html');
}

$produtosDestaqueHome = Index::getProdutosDestaqueHome(DESTAQUES1,DESTAQUES2);

$visibilidadePedidoPendenteFlutuante = 'none';
if(Pedido::verificarPedidosNaSessao()){
  $visibilidadePedidoPendenteFlutuante = 'block';
}

// verifica se já está setado o endereço na sessão
if(!isset($_COOKIE['enderecoSessao'])){
  setcookie('enderecoSessao', 0, time() + (86400 * 30 * 7), "/");
}
// caso houver endereço ja na session passa para o cookie
if(isset($_SESSION['endereco']) && !empty($_SESSION['endereco'])){
  $_COOKIE['enderecoSessao'] = $_SESSION['endereco'];
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />

  <title>
    <?=NOMELOJA?>
  </title>

  <meta name="country" content="br">
  <meta name="language" content="pt-BR">
  <link rel="icon" type="image/png" href="<?=CAMINHO_IMAGEM?>/favicon/<?=FAVICON?>" />
  <link rel="canonical" href="<?=CAMINHO?>">

  <meta name="apple-mobile-web-app-title" content="<?=NOMELOJA?>">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta property="og:title" content="<?=NOMELOJA?>" />
  <meta property="og:url" content="<?=CAMINHO?>" />
  <meta property="og:type" content="website" />
  <meta property="og:description" content="Faça pedidos online no delivery OFICIAL <?=NOMELOJA?>, <?=$dadosHome['cidade']?>, <?=ESTADO?>. Veja preços dos pratos no catálogo online do <?=NOMELOJA?> para entrega." />
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimum-scale=1, user-scalable=1" />
  <meta name="description" content="Faça pedidos online no delivery OFICIAL <?=NOMELOJA?>, <?=$dadosHome['cidade']?>, <?=ESTADO?>. Veja preços dos pratos no catálogo online do <?=NOMELOJA?> para entrega.">
  <meta name="keywords" content="<?=NOMELOJA?> delivery OFICIAL, <?=NOMELOJA?> delivery, <?=NOMELOJA?> entrega, <?=NOMELOJA?> <?=$dadosHome['cidade']?>, <?=NOMELOJA?> <?=$dadosHome['cidade']?>, cardapio <?=NOMELOJA?>, preços <?=NOMELOJA?>">

  <style media="screen">
    :root {
      --cor-faixa-principal-mobile: <?=CORFAIXAPRINCIPALMOBILE?>;
    }
  </style>

  <?php
    $css  = '';
    // $css .= file_get_contents('https://fonts.googleapis.com/icon?family=Material+Icons');
    // $css .= file_get_contents('https://fonts.googleapis.com/css?family=Lato:400,400italic,700,300');
    // $css .= file_get_contents(CAMINHO_SISTEMA.'/css/store.css');
    // $css .= file_get_contents(CAMINHO_SISTEMA.'/css/sp.css');
    // $css .= file_get_contents(CAMINHO_SISTEMA.'/css/custom.css');
    // $css .= file_get_contents(CAMINHO_SISTEMA.'/css/sweetalert.min.css');
    // $css .= file_get_contents('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css');

    echo $varsScript;

    echo '<style>
            .header-cover-photo{
              background-image: url("'.CAMINHO_IMAGEM.'bannerHome/'.$dadosHome['bannerHome'].'");
            }'.$css.'</style>';
   ?>
  <link rel="stylesheet" href="<?=CAMINHO?>/css/fonts.css" />
  <link rel="stylesheet" href="<?=CAMINHO?>/css/store.css" />
  <link rel="stylesheet" href="<?=CAMINHO?>/css/sp.css" />
  <link rel="stylesheet" href="<?=CAMINHO?>/css/custom.css?<?=$getScript?>" />
  <link rel="stylesheet" href="<?=CAMINHO?>/css/sweetalert.min.css"/>
  <link rel="stylesheet" href="<?=CAMINHO?>/css/select2.min.css" />
  <link rel="stylesheet" href="<?=CAMINHO?>/css/carousel-bootstrap.css">

  <script type="text/javascript">
    var _cio = {
      identify: function() {},
      track: function() {}
    }
    var ruaLoja    = '<?=RUA?>';
    var numeroLoja = '<?=NUMERO?>';
    var bairroLoja = '<?=BAIRRO?>';
    var cidadeLoja = '<?=CIDADE?>';
    var estadoLoja = '<?=ESTADO?>';
    var ufLoja     = '<?=UF?>';
    var cepLoja    = '<?=CEP?>';
  </script>
  <script id="googlePlaceActionsScript" type="application/ld+json" async>
    Array
  </script>
</head>

<body>

  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary botaoAbrirModalCep" style="display:none" data-toggle="modal" data-target="#modalCep"></button>

  <!-- Button trigger modal -->
  <button type="button" class="btn botaoAbrirModalCarrinho" style="display:none" data-toggle="modal" data-target="#modalCarrinho"></button>

  <!-- Button trigger modal -->
  <button type="button" class="btn abrirBoxSelecaoQuantidade" style="display:none" data-toggle="modal" data-target="#modalAdicionarPedidoCarrinho"></button>

  <!-- cabecalho mobile -->
  <?php include(CAMINHO_COMPONENTES.'cabecalho.php') ?>

  <div id="body-store" class="wrap container store-top-padding">

    <!-- botao modaol finalizar pedido -->
    <button type="button" class="botaoAbrirBoxFinalizarCompra" style="display: none;" data-toggle="modal" data-target="#modal-pedido"></button>

    <div class="row">
      <div class="mobile-storeinfo col-xs-12 col-sm-12 hidden-md hidden-lg js-storeinfo-content">
        <div class="container-logo-nome-mobile">
          <div class="mobile-storeinfo-logo">
            <img class="border-r-10" src="<?=CAMINHO_IMAGEM?>logo/<?=$dadosHome['logo']?>" alt="<?=NOMELOJA?>" />
          </div>
          <h1 itemprop="name"><a href="<?=CAMINHO?>"><?=NOMELOJA?></a></h1>
        </div>
        <h2 class="is-hidden" itemscope itemprop="address" itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress"><?=$enderecoRuaNumeroBairro?></span>,
          <span itemprop="addressLocality"><?=$dadosHome['cidade']?></span>,
          <span itemprop="addressRegion"><?=$dadosHome['uf']?></span>
          <span itemprop="postalCode" content="telephone=no"><?=$dadosHome['cep']?></span>
        </h2>

        <div class="container-horario-taxa-mobile">
          <!-- aberto/fechado mobile -->
          <div class="mobile-storeinfo-business_hour-box">
            <div class="status">
              <a data-ga="business-hour-header" rel="nofollow">
                <?php if($atendimentoLoja['aberta']){ ?>
                  <i class="material-icons">check_circle_outline</i>
                  <span><?=$horarioFechamento?></span>
                <?php }else{ ?>
                  <i class="material-icons closed">highlight_off</i>
                  <span class="closed">Fechado</span>
                <?php } ?>
              </a>
            </div>
          </div>

          <!-- tempo espera -->
          <div class="mobile-storeinfo-business_hour-box">
            <div class="status">
              <a class="js-business-hours" data-ga="business-hour-header" rel="nofollow">
                <span class="material-icons">
                  query_builder
                </span>
                <span>Tempo de Espera: </span>
                <span class="js-range-waiting-time">
                  <?=$dadosHome['tempoEspera']?>
                </span>
              </a>
            </div>
          </div>

          <!-- valor frete -->
          <div class="mobile-storeinfo-business_hour-box">
            <div class="status">
              <a class="js-business-hours" data-ga="business-hour-header" rel="nofollow">
                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><g><rect fill="none" height="24" width="24"/></g><g><path d="M12,2C6.48,2,2,6.48,2,12s4.48,10,10,10s10-4.48,10-10S17.52,2,12,2z M12,20c-4.41,0-8-3.59-8-8c0-4.41,3.59-8,8-8 s8,3.59,8,8C20,16.41,16.41,20,12,20z M12.89,11.1c-1.78-0.59-2.64-0.96-2.64-1.9c0-1.02,1.11-1.39,1.81-1.39 c1.31,0,1.79,0.99,1.9,1.34l1.58-0.67c-0.15-0.44-0.82-1.91-2.66-2.23V5h-1.75v1.26c-2.6,0.56-2.62,2.85-2.62,2.96 c0,2.27,2.25,2.91,3.35,3.31c1.58,0.56,2.28,1.07,2.28,2.03c0,1.13-1.05,1.61-1.98,1.61c-1.82,0-2.34-1.87-2.4-2.09L8.1,14.75 c0.63,2.19,2.28,2.78,3.02,2.96V19h1.75v-1.24c0.52-0.09,3.02-0.59,3.02-3.22C15.9,13.15,15.29,11.93,12.89,11.1z"/></g></svg>
                <span>Taxa de Entrega: </span>
                <label data-frete="" class="js-delivery-price js-delivery-fee-value">
                  <?=$valorFrete?>
                </label>
              </a>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="row storeinfo">
      <div class="hidden-xs hidden-sm col-md-12 col-lg-12 padding-0 js-header-storeinfo js-storeinfo-content storeinfo-header">
        <div class="header-content">
          <a href="<?=CAMINHO?>" data-ga="logo-header">
            <img src="<?=CAMINHO_IMAGEM?>logo/<?=$dadosHome['logo']?>" alt="<?=NOMELOJA?>" data-ga="header-store_logo" class="img-responsive border-r-10" data-cy="store-info__logo" />
          </a>
          <div class="header-content-info">
            <h1 itemprop="name"><a data-ga="header-store_name" data-cy="header-store__name" href="<?=CAMINHO?>"><?=NOMELOJA?></a></h1>
            <h2 itemscope itemprop="address" itemtype="http://schema.org/PostalAddress">
              <span itemprop="streetAddress"><?=$enderecoRuaNumeroBairro?></span>,
              <span itemprop="addressLocality"><?=$dadosHome['cidade']?></span>,
              <span itemprop="addressRegion"><?=$dadosHome['uf']?></span>
              <span itemprop="postalCode" content="telephone=no"><?=$dadosHome['cep']?></span>
            </h2>
            <div class="header-content-info-wrapper">
              <div class="header-content-info-bottom header-content-info-waiting_time" data-ga="header-info_waiting_time">
                <span>Tempo de espera</span>
                <label>
                  <span class="js-range-waiting-time">
                    <?=$dadosHome['tempoEspera']?> </span>
                </label>
              </div>
              <div class="header-content-info-bottom header-content-mouse-pointer header-content-info-delivery_fee" data-ga="header-info_delivery_fee" title="Para mudar o endereço, clique aqui">
                <span class="header-content-mouse-pointer">Taxa de entrega</span>
                <label class="js-delivery-price js-delivery-fee-value header-content-mouse-pointer" data-cy="store-info__delivery-fee">
                  <?=$valorFrete?> </label>
              </div>
              <div class="header-content-info-bottom header-content-mouse-pointer header-content-info-business_hours js-header-content-info-business_hours" title="Para ver os horários de funcionamento, clique aqui" data-is_open_now="1"
                data-accept_scheduled_orders="1" data-ga-action="menu__view_open_hours" data-ga-category="menu">
                <span class="header-content-mouse-pointer">
                  <?=$labelEstadoLoja?> <i class="material-icons icon-status js-icon-status <?=$iconEstadoLoja?>">lens</i>
                </span>
                <label class="header-content-mouse-pointer js-business-hours" data-modal_title="" data-cy="store-info__business-hours">
                  <?=$horarioFechamento?> </label>
              </div>
            </div>
          </div>
          <div class="header-loyalty-separator"></div>
          <div class="header-loyaltyprogram">
            <!-- texto acima endereço -->
            <?php include(CAMINHO_COMPONENTES.'estrutura-texto-acima-endereco.php') ?>
            <?php if($mostrarValorMinimo){ ?>
              <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent checkout-button botaoPedidoMinimo" style="background-color: rgb(153, 153, 153);">
                <span class="text js-go-to-checkout-text" style="color: white;">Pedido mínimo de R$<?=$valorMinimoPedido?></span>
              </button>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row store-items-div">

      <?=$layoutEnderecoMobile?>

      <?php include(CAMINHO_COMPONENTES.'menu-home.php') ?>

      <?php include(CAMINHO_COMPONENTES.'barra-pesquisa-home.php') ?>

      <div class="replaceEnderecoPreenchido"></div>
      <!-- condicao caso a loja não esteja aberta -->
      <?php //if(Atendimento::ALojaEstaFechadaESemAgendamento()){ ?>
        <div class="store-cart-box col-desktop-3 col-mobile-12 js-cart-box boxLojaFechada <?=$visibilidadeLojaFechada?>">
          <div class="store-cart-content store-unavailable-content js-cart-content-closed">
            <div class="status">
              <i class="material-icons icon" data-modern="access_time"></i>
              <span class="text">Fechado</span>
            </div>
          <span class="description">Estamos indisponíveis no momento, mas você pode agendar seu pedido</span>
          </div>
          <button data-toggle="modal" data-target="#modalAgendarEntrega" class="btnAgendamentoLojaFechada mdl-button mdl-js-button mdl-js-ripple-effect btn-to-schedule action js-schedule-order <?php if(!$entregaAgendada)'is-hidden'?>">
            <i class="material-icons action-color">event</i>
            <span class="text">Agendar pedido</span>
          </button>
          <?php if(!$entregaAgendada){ ?>
          <div class="cart-scheduled-order js-scheduled-content is-hidden text-center botaoEntregaAgendadaDesktop">
            <span class="title-schedule-order js-scheduled-order-title">Entrega agendada:</span>
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect action action-color btn-to-reschedule js-change-schedule-order">
              <i class="material-icons">mode_edit</i>
              <span class="description js-scheduled-order-description"></span>
            </button>
          </div>
          <?php }else{?>
            <div class="cart-scheduled-order js-scheduled-content text-center botaoEntregaAgendadaDesktop">
              <span class="title-schedule-order js-scheduled-order-title">Entrega agendada:</span>
              <button data-toggle="modal" data-target="#modalAgendarEntrega" class="mdl-button mdl-js-button mdl-js-ripple-effect action action-color btn-to-reschedule js-change-schedule-order">
                <i class="material-icons">mode_edit</i>
                <span class="description js-scheduled-order-description"><?= $labelAgendamento ?></span>
              </button>
            </div>
          <?php }?>
        </div>
      <?php // } ?>

        <div class="store-cart-box col-desktop-3 col-mobile-12 js-cart-box boxLojaAberta <?=$visibilidadeLojaAberta?>">
          <div class="store-cart-content js-cart-content-open containerCarrinho1" data-has_free_delivery=>

            <div class="replaceEnderecoPreenchido"></div>
            <div class="hidden-mobile js-desktop-cart" data-ga="cart-address-box">
              <div class="address-box js-address-box">

                <!-- caso já existir o endereço na sessao -->
                <div class="address-box-content js-address-content hidden-mobile <?php if(!$mostrarEndereco) echo 'is-hidden' ?> containerCarrinho2">
                  <span class="title">Endereço de <span class="labelEnderecoDe"><?=$labelRetirada?>:</span> </span>
                  <div class="description-container">
                    <div class="edit-button">
                      <button class="mdl-button mdl-js-button mdl-js-ripple-effect action action-color js-change-address" data-upgraded=",MaterialButton,MaterialRipple" data-ga-action="cart__change_address" data-ga-category="cart" data-cy="cart__button--change-address">
                        <i class="material-icons">mode_edit</i>
                        <span class="mdl-button__ripple-container">
                          <span class="mdl-ripple"></span>
                        </span>
                      </button>
                    </div>
                    <div class="description-box">
                      <span class="description js-address-box-description js-change-address" data-ga-action="cart__change_address" data-ga-category="cart" data-cy="cart__address-description"><?=$endereco?></span>
                    </div>
                  </div>
                </div>
                <?=$layoutEnderecoDesktop?>
              </div>
            </div>

            <div class="hidden-mobile-when-cart-empty containerCarrinho2">
              <div class="checkout-content">
                <div class="title hidden-mobile" data-ga="my-cart-title">
                  <i class="material-icons action-color">shopping_cart</i>
                  <span class="text">Meu carrinho</span>
                </div>

                <ul class="cart-list <?=$classeCarrinhoVazio?> hidden-mobile visible-desktop-block js-cart-list" data-placeholder="Seu pedido está vazio." data-cy="cart__items-list">
                  <?php

                    if(!$carrinhoVazio){

                      foreach ($_SESSION['carrinho']['itens'] as $key => $value) {
                        $produto        = unserialize($value);
                        $produto->valor = number_format($produto->valor,2);
                        $total          = $produto->valor *  $produto->quantidade;
                        $total          = number_format($total,2);
                        $boxSabores     = Carrinho::montarBoxSabores($produto);
                        $boxAdicionais  = Carrinho::montarBoxAdicionais($produto);
                        $labelObservacao = Carrinho::getLabelObservacao($produto->observacao);
                        echo '
                        <li class="row list-item js-checkout-details-items-list js-checkout-details-items-list-'.$produto->id.'" data-id="'.$produto->id.'" data-iid="1060098676" data-ga="newcheckout-edit-item">
                          <div class="col-xs-1 list-item-content edit-content js-edit-checkout-details-items-list" data-ga-action="cart__edit_item" data-ga-category="cart" data-id="'.$produto->id.'" data-iid="1060098676" data-cy="cart__edit-item--'.$produto->id.'">
                            <i class="material-icons icon action-color">mode_edit</i>
                          </div>
                          <div class="col-xs-6 list-item-content description-content" data-ga-action="cart__edit_item" data-ga-category="cart" data-cy="cart__item-description">
                            <span class="title">'.$produto->nome.'</span><br>
                            '.$boxSabores.'
                            '.$boxAdicionais.'
                            '.$labelObservacao.'
                          </div>
                          <div class="col-xs-2 list-item-content amount-content">
                            <span class="amount js-amount">'.$produto->quantidade.'</span>
                          </div>
                          <div class="col-xs-3 list-item-content price-content">
                            <span class="text js-price" data-price="'.$produto->valor.'" data-total="'.$total.'">R$'.$total.'</span>
                            <span class="delete js-delete-item" data-id-sess="'.$produto->idSessaoCarrinho.'" data-id="'.$produto->id.'" data-iid="1060098676">
                              <i class="material-icons list-item-delete" data-ga-action="cart__remove_item" data-ga-category="cart">highlight_off</i>
                            </span>
                          </div>
                          <div class="col-xs-12 list-item-observations js-observations"></div>
                        </li>';
                      }
                    }

                  ?>
                </ul>

                <div class="cart-info js-cart-info hidden-mobile" data-ga="cart-info">

                  <div class="subtotal cupomAplicado <?=$classeCupomAplicado?>">
                    <span class="text">Cupom aplicado</span>
                    <span class="value labelCupomAplicado"><?=$labelCupomAplicado?></span>
                  </div>

                  <div class="subtotal js-subtotal-content">
                    <span class="text">Subtotal</span>
                    <span class="value js-subtotal-value">R$<?=$subtotalCarrinho?></span>
                  </div>

                  <div class="delivery-fee js-delivery-fee-content">
                    <span class="text delivery-text">Taxa de entrega </span>
                    <span class="value js-delivery-fee-value" data-cy="cart__delivery-fee">
                      <?=$valorFrete?> </span>
                    <i class="material-icons trending-up js-trending-up cursor-pointer font-size-18 float-right is-hidden" data-toggle="tooltip" title="Taxa de entrega mais alta">trending_up</i>
                  </div>

                  <div class="total js-total-content">
                    <span class="text font-weight-bold">Total</span>
                    <span class="value js-total-value" data-cy="cart__total">R$<?=$totalCarrinho?></span>
                  </div>
                </div>
                <div class="cart-divider"></div>

                <div class="containerCarrinhoMobile">
                  <div class="itemCarrinhoMobile" data-toggle="modal" data-target="#modalAgendarEntrega">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="30px" height="30px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V9h14v10zm0-12H5V5h14v2zM7 11h5v5H7z"/></svg>
                    <span>Agendar Pedido</span>
                  </div>
                  <div class="itemCarrinhoMobile itemHomeMenuMobile">
                      <svg width="30px" height="30px" id="Layer_1" style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path stroke="#b2b2b2" stroke-width="1" d="M21.146,8.576l-7.55-6.135c-0.925-0.751-2.267-0.751-3.192,0c0,0,0,0,0,0L2.855,8.575C2.59,8.79,2.439,9.108,2.439,9.448  v11.543c0,0.62,0.505,1.13,1.125,1.13h5.062c0.62,0,1.125-0.51,1.125-1.13v-7.306h4.499v7.306c0,0.62,0.505,1.13,1.125,1.13h5.062  c0.62,0,1.125-0.51,1.125-1.13V9.448C21.561,9.108,21.41,8.79,21.146,8.576z M20.436,20.997h-5.062V13.68  c0-0.62-0.505-1.119-1.125-1.119H9.75c-0.62,0-1.125,0.499-1.125,1.119v7.317H3.564V9.448l7.55-6.134  c0.513-0.418,1.26-0.417,1.773,0l7.55,6.134V20.997z"/></svg>
                    <span>Home</span>
                  </div>
                  <div class="itemCarrinhoMobile js-cart-info-mobile">
                    <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" viewBox="0 0 24 24" fill="black" width="32px" height="32px"><g><rect fill="none" height="24" width="24"/><path d="M18,6h-2c0-2.21-1.79-4-4-4S8,3.79,8,6H6C4.9,6,4,6.9,4,8v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V8C20,6.9,19.1,6,18,6z M12,4c1.1,0,2,0.9,2,2h-4C10,4.9,10.9,4,12,4z M18,20H6V8h2v2c0,0.55,0.45,1,1,1s1-0.45,1-1V8h4v2c0,0.55,0.45,1,1,1s1-0.45,1-1V8 h2V20z"/></g></svg>
                    <span>Finalizar Compra</span>
                  </div>
                </div>

                <!-- <div class="cart-info-mobile js-cart-info-mobile">
                  <i class="material-icons cart-icon">shopping_cart</i>
                  <span class="cart-description js-cart-description">
                    <?=$labelCarrinhoMobile?> </span>
                  <div class="total js-total-content">
                    <span class="value js-total-value">R$<?=$totalCarrinho?></span>
                  </div>
                </div> -->

                <!-- Botoes mobile abaixo do carrinho -->
                <!-- <div class="containerBotoesRodapeMobile">
                  <button data-toggle="modal" class="col-mobile-6 btnFecharPedido mdl-button mdl-js-button mdl-js-ripple-effect action js-schedule-order <?php if(!$entregaAgendada)'is-hidden'?>">
                    <span class="text">FECHAR PEDIDO</span>
                  </button>
                  <button data-toggle="modal" data-target="#modalAgendarEntrega" class="col-mobile-6 btnAgendarPedido mdl-button mdl-js-button mdl-js-ripple-effect action js-schedule-order <?php if(!$entregaAgendada)'is-hidden'?>">
                    <i class="material-icons action-color white pr-10">event</i>
                    <?php if(!$entregaAgendada){ ?>
                      <span class="text">AGENDAR PEDIDO</span>
                    <?php }else{ ?>
                      <span class="text"><?=$labelAgendamentoCustom?></span>
                    <?php } ?>
                  </button>
                </div> -->

                <div class="logoQuandoMobile">
                  <a class="logoRodape" target="_blank" href="https://www.didoo.com.br">
                    <div>Criado com:
                      <img class="rodapeLogo" src="<?=CAMINHO?>/img/home/logo-didoo.png">
                    </div>
                  </a>
                </div>

              </div>
            </div>
          </div>

          <div class="branco card-content box-fechamento hidden-mobile">

            <a data-target="#modal-pedido" class="btn fechar-pedido modal-trigger">Fechar Pedido</a>

            <button type="submit" class="btn btn-cupom roxo">Aplicar Cupom</button>
            <div class="input-field is-hidden campoCupom">
              <input placeholder="Código do Cupom" type="text" class="codigo_cupom desktop roxo-texto" name="codigo_cupom" value="">
              <button class="aplicarCupomDesconto">aplicar</button>
            </div>

            <!-- AGENDAR -->
            <div class="cart-schedule-order hidden-mobile js-order-schedule-content" data-ga="cart-schedule" data-toggle="modal" data-target="#modalAgendarEntrega">
              <?php if(!$entregaAgendada){ ?>
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-to-schedule action js-schedule-order">
                  <i class="material-icons action-color">event</i>
                  <span class="text">Agendar pedido</span>
                </button>
                <div class="cart-scheduled-order js-scheduled-content is-hidden">
                  <span class="title-schedule-order js-scheduled-order-title">Entrega agendada:</span>
                  <button class="mdl-button mdl-js-button mdl-js-ripple-effect action action-color btn-to-reschedule js-change-schedule-order">
                    <i class="material-icons">mode_edit</i>
                    <span class="description js-scheduled-order-description"></span>
                  </button>
                </div>
              <?php }else{ ?>
                <div class="cart-scheduled-order js-scheduled-content">
                  <span class="title-schedule-order js-scheduled-order-title">Entrega agendada:</span>
                  <button class="mdl-button mdl-js-button mdl-js-ripple-effect action action-color btn-to-reschedule js-change-schedule-order">
                    <i class="material-icons">mode_edit</i>
                    <span class="description js-scheduled-order-description"><?= $labelAgendamento ?></span>
                  </button>
                </div>
              <?php } ?>
            </div>

            <div class="row">
              <div class="col s12">
                <p class="center-align">Pague na entrega com dinheiro</p>
                <p class="center-align"><strong>OU</strong></p>
                <img src="<?=CAMINHO?>/img/home/bandeiras.png" class="responsive-img center-block imgBandeirasCartoes">
              </div>
            </div>
          </div>

        </div>
      <?php //} /* fim do else da verificação de loja aberta */ ?>

      <!-- INICIO PRODUTOS DESTAQUE -->
        <?=$produtosDestaqueHome?>
      <!-- FIM PRODUTOS DESTAQUE -->

      <!-- listagem de produtos na home -->
      <div class="js-item-block items-block col-mobile-12 col-desktop-9 medium-transition">

        <?=$boxProdutos?>

        <?php
          include(CAMINHO_COMPONENTES.'estrutura-area-entrega.php');
          include(CAMINHO_COMPONENTES.'estrutura-contato.php');
          include(CAMINHO_COMPONENTES.'estrutura-sobre.php');
        ?>

      </div>
    </div>
  </div>

  <div class="containerPedidoPendenteFlutuante" style="display: <?=$visibilidadePedidoPendenteFlutuante?>">
    <span class="text-center">
      <span>Aguardando</span>
      <br>
      <span>Confirmação...</span>
    </span>
    <div class="divPedidoPendenteFlutuante">
      <img src="<?=CAMINHO?>/img/home/preload.gif">
    </div>
  </div>

  <!-- modal montar pizza -->
  <?php include(CAMINHO_COMPONENTES.'modal-montar-pizza.php'); ?>
  <!-- modal informe endereço -->
  <?php include(CAMINHO_COMPONENTES.'modal-informe-endereco.php'); ?>
  <!-- modal finalizar pedido -->
  <?php include(CAMINHO_COMPONENTES.'modal-finalizar-pedido.php'); ?>
  <!-- modal finalizar pedido -->
  <?php include(CAMINHO_COMPONENTES.'modal-adicionar-pedido.php'); ?>
  <!-- modal carrinho -->
  <?php include(CAMINHO_COMPONENTES.'modal-carrinho.php'); ?>
  <!-- modal agendamento -->
  <?php include(CAMINHO_COMPONENTES.'modal-agendar-entrega.php'); ?>
  <!-- modal pedido pendente -->
  <?php include(CAMINHO_COMPONENTES.'modal-pedido-pendente.php'); ?>

  <footer class="footer js-footer-container container-fluid ">
    <div class="row footer-copyright">
      <div class="origin col-xs-12">
        <?=date("Y")?> - Todos os direitos reservados | <span class="copyright-separator"><?=NOMELOJA?></span>
        <br>
        <div class="containerLogoRodapeDesktop hidden-sm hidden-xs">
          <a target="_blank" href="https://www.didoo.com.br">
            Criado com: <img class="rodapeLogo" src="<?=CAMINHO?>/img/home/logo-didoo.png">
          </a>
        </div>
      </div>
    </div>
    <div class="row footer-stretch"></div>
  </footer>

  <div class="box-btWhats">
    <a class="link-whats" href="https://api.whatsapp.com/send?phone=55<?=WHATSAPPESTABELECIMENTO?>" target="_blank">
      <img width="50px" src="<?=CAMINHO?>/img/home/whatsapp-logo-icone.png" alt="whatsapp">
    </a>
  </div>

  <div id="popover" class="popover">
    <h3 class="popover-title is-hidden"></h3>
    <div class="popover-content"></div>
  </div>

  <div id="environment-name" data-name="BETA"></div>

  <!-- <script src="<?//=CAMINHO?>/js/global-app.bundle.js?1587149901"></script> -->
  <script>
    var STORE_NAME = 'sp';
    var BRAND_NAME = '<?=NOMELOJA?>';
    var LOCALE = 'pt_BR'
    var TITLE = "<?=NOMELOJA?> - Delivery OFICIAL";
  </script>
  <!-- jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- bootstrap js -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <!-- select -->
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <!-- sweet alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- notification -->
  <script src="<?=CAMINHO?>/js/notification.min.js?<?=$getScript?>"></script>
  <!-- mask plugin -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
  <!-- Bootstrap is required -->
  <script src="<?=CAMINHO?>/js/jquery.bcSwipe.js"></script>
  <!-- xzoom -->
  <script src="<?=CAMINHO?>/js/jquery.zoom.min.js"></script>
  <!-- custom -->
  <script src="<?=CAMINHO?>/js/custom.js?<?=$getScript?>"></script>

  <input id='dd_web_version' type='hidden' value="2.11.137" />

  <script type="text/javascript">
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
      $('.js-example-basic-single').select2({
        "language": {
          "noResults": function(){
            return "Nenhum resultado";
          }
        },
        escapeMarkup: function (markup) {
          return markup;
        }
      });
    });

    $('#valorTroco').mask("#.##0.00", {reverse: true});
    $('.carousel').bcSwipe({ threshold: 50 });
  </script>

</body>

</html>
