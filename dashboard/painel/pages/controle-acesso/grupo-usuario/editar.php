<?php
include_once('../../../../includes.php');

if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(UsuarioGrupo::updateGrupo($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/controle-acesso/grupo-usuario');
  }
}

if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/controle-acesso/grupo-usuario');
}

$idGrupo = $_GET['id'];
$grupo  = UsuarioGrupo::getGrupoPorId($idGrupo);

// caso tenha retornado false
if(!$grupo){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/controle-acesso/grupo-usuario');
}

$grupo = $grupo[0];

?>

<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Grupo de Usuários</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Grupos Usuários</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
        <form action="" autocomplete="off" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Grupo</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="<?=$grupo->nome?>">
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>
<?php include_once('../../../includes-scripts/includes-scripts-estrutura.php'); ?>
</body>
</html>
