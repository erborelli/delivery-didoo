<?php
  include_once('../../../../includes.php');
  $grupos = UsuarioGrupo::getGrupos();
?>

<!DOCTYPE html>
<html>
<?php include_once('../../../head/head-estrutura.php'); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Grupos de Usuários</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Grupos de Usuários</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 15%">
                          Nome
                      </th>
                      <th style="width: 10%">
                          Data Cadastro
                      </th>
                      <th style="width: 15%">
                      </th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($grupos as $grupo){ ?>
                  <tr>
                    <td><?=$grupo->nome?></td>
                    <td><?=$grupo->data_cadastro?></td>
                    <td class="project-actions text-right">
                        <a class="btn btn-info btn-sm" href="<?=CAMINHO_DASHBOARD.'/painel/pages/controle-acesso/grupo-usuario/editar.php?id='.$grupo->id?>">
                            <i class="fas fa-pencil-alt"></i>Edit
                        </a>
                        <a data-id="<?=$grupo->id?>" data-modulo="usuario_grupo" class="btn btn-danger btn-sm" href="#">
                            <i class="fas fa-trash"></i>Delete
                        </a>
                      </td>
                  </tr>
                <?php } ?>
              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/controle-acesso/grupo-usuario/criar.php">
        <i class="fas fa-plus"></i>Adicionar Grupo
      </a>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>

<script src="../../../plugins/jquery/jquery.min.js"></script>
<script src="../../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../../dist/js/adminlte.min.js"></script>
<script src="../../../dist/js/demo.js"></script>
<script src="../../../plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
