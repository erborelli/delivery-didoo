<?php

class PedidoItemSabor{

  public static function gravarPedidoItemSabor($id_pedido_item,$idVinculoSaborValor,$valor){
    if(!is_numeric($id_pedido_item) || empty($idVinculoSaborValor)) return '';
    $idELabelPizzaSabor = PizzaSabores::getSaborPizzaAPartirDoIdSaborValor($idVinculoSaborValor);
    $link = getConnection();
    $sql  = "INSERT INTO pedido_item_sabor(id_pedido_item,id_sabor,sabor,valor)
             VALUES(?,?,?,?)";
    $stmt = $link->prepare($sql);
    $stmt->execute([$id_pedido_item,$idELabelPizzaSabor->id,$idELabelPizzaSabor->label,$valor]);
  }

  public static function getSaboresItemPedido($idPedidoItem){
    if(empty($idPedidoItem) || !is_numeric($idPedidoItem)) return false;
    $link = getConnection();

    $sql  = "SELECT * FROM pedido_item_sabor
             WHERE id_pedido_item = ?";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedidoItem]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
