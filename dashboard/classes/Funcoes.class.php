<?php

class Funcoes {

  public static function unserializeArray($array){
    if(empty($array)) return [];
    $retorno = [];
    foreach ($array as $key => $value) {
      $retorno[] = unserialize($value);
    }
    return $retorno;
  }

}
