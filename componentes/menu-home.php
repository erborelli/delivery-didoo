<div class="col-mobile-12 col-desktop-9 store-custom-menu js-store-custom-menu" data-is_empty="false">
  <div class="containerAbasConteudo">
    <div class="col-mobile-12 hidden-desktop store-custom-menu-itens-mobile">
      <ul class="store-custom-menu-itens mobile">
        <li class="menu-item selected item-ponta-esquerda" data-item-menu="item1" data-alvo="cardapio">
          <a href="javascript:void(0)">Catálogo </a>
        </li>
        <li class="menu-item sub-ponta-esquerda" data-item-menu="item2" data-alvo="entrega">
          <a href="javascript:void(0)">
            Área de entrega </a>
        </li>
        <li class="menu-item sub-ponta-direita" data-item-menu="item3" data-alvo="contato">
          <a href="javascript:void(0)">
            Contato </a>
        </li>
        <li class="menu-item item-ponta-direita" data-item-menu="item4" data-alvo="sobre">
          <a href="javascript:void(0)">
            Sobre nós </a>
        </li>
      </ul>
    </div>
    <div class="hidden-mobile col-desktop-12 padding-0">
      <ul class="store-custom-menu-itens">
        <li class="menu-item selected" data-alvo="cardapio">
          <a href="javascript:void(0)" data-ga-action="custommenu__" data-ga-category="menu" data-cy="custom-menu__">Catálogo </a>
          <div class="custom-border-bottom"></div>
        </li>
        <li class="menu-item" data-alvo="entrega">
          <a href="javascript:void(0)" data-ga-action="custommenu__delivery-area" data-ga-category="menu" data-cy="custom-menu__delivery-area">
            Área de entrega </a>
          <div class="custom-border-bottom"></div>
        </li>
        <li class="menu-item" data-alvo="contato">
          <a href="javascript:void(0)" data-ga-action="custommenu__contact-us" data-ga-category="menu" data-cy="custom-menu__contact-us">
            Contato </a>
          <div class="custom-border-bottom"></div>
        </li>
        <li class="menu-item" data-alvo="sobre">
          <a href="javascript:void(0)" data-ga-action="custommenu__about" data-ga-category="menu" data-cy="custom-menu__about">
            Sobre nós </a>
          <div class="custom-border-bottom"></div>
        </li>
      </ul>
    </div>
  </div>
</div>
