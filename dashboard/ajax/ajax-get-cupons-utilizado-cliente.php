<?php

include_once('../includes.php');

$retorno = '';
$idCliente = $_POST['idCliente'];
$cuponsCliente = Cupom::getUsosCupomCliente($idCliente);

if(empty($cuponsCliente)) {
  echo 'Nenhum cupom utilizado até o momento';exit;
}

foreach ($cuponsCliente as $key => $value) {
  $dataFormatada = date("d/m/Y  H:i:s", strtotime($value->data_utilizacao));
  $retorno .= '<span>Utilizou o cupom "'.$value->nome_cupom.'" no dia '.$dataFormatada.'</span><br>';
}

echo $retorno;
