<?php
include_once('../../../includes.php');

if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/formas-pagamento/');
}

// caso houver o post da ação da edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  FormaPagamento::update($_POST);
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/formas-pagamento/');
}
$formaPagamento = FormaPagamento::getPorId($_GET['id']);
$formaPagamento = $formaPagamento[0];

// caso tenha retornado false
if(!$formaPagamento){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/formas-pagamento/');
}

?>
<!DOCTYPE html>
<html>
<?php include_once('../../head/head-estrutura.php'); ?>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
    <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>
    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Forma de Pagamento</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Forma de Pagamento</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
        <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Editar</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group col-md-4" style="float:left;">
                <label for="nome">Nome</label>
                <input type="text" name="nome" id="nome" class="form-control" value="<?=$formaPagamento->nome?>">
              </div>
              <div class="form-group col-md-4" style="float:left;">
                <label for="troco">Pedir Troco</label>
                <select id="slcTroco" name="troco" class="form-control">
                  <option value="s">Sim</option>
                  <option value="n">Não</option>
                </select>
                <input id="txtTroco" style="display: none;" value="<?=$formaPagamento->troco?>">
              </div>
              <div class="form-group col-md-4" style="float:left;top:20px;">
                <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <footer class="main-footer">
      <strong><?=NOMELOJA?></strong>
    </footer>
    <aside class="control-sidebar control-sidebar-dark"></aside>
  </div>
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../dist/js/adminlte.min.js"></script>
  <script src="../../dist/js/demo.js"></script>
  <script src="../../plugins/summernote/summernote-bs4.min.js"></script>
  <script>
    $(function () {
      $('#compose-textarea').summernote()
    })
    let pedirTroco = document.getElementById('txtTroco').value
    document.getElementById('slcTroco').value = pedirTroco
  </script>
</body>
</html>
