<?php
include_once('../../../../includes.php');

$tamanhosPizza = PizzaTamanhos::getTamanhosPizza();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tamanhos (Pizza / Esfiha)</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tamanhos (Pizza / Esfiha)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/tamanhos-pizza/criar.php">
        <i class="fas fa-plus"></i>Novo tamanho
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Produtos - Tamanhos (Pizza / Esfiha)</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 5%">
                          #
                      </th>
                      <th style="width: 35%">
                          Label
                      </th>
                      <th>
                          Data criação
                      </th>
                  </tr>
              </thead>
              <tbody>

                <?php foreach ($tamanhosPizza as $key => $value) {

             echo '<tr>
                      <td>#</td>
                      <td><a>'.$value->label.'</a><br></td>
                      <td class="project_progress">
                          <small>'.$value->data_criacao.'</small>
                      </td>

                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'painel/pages/produtos/tamanhos-pizza/editar.php?id='.$value->id.'">
                              <i class="fas fa-pencil-alt"></i>Editar
                          </a>
                          <a data-id="'.$value->id.'" data-modulo="pizza_tamanhos" class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"></i>Excluir
                          </a>
                      </td>
                  </tr>';
                }
                  ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/tamanhos-pizza/criar.php">
          <i class="fas fa-plus"></i>Novo tamanho
      </a>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>

<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
