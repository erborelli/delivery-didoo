<?php

require_once('../include.php');

//deleta registro
if(count($_DELETE)){
    echo('delete');
    http_response_code(200);
    return;
}

//atualiza registro
if(count($_PUT)){
    $pedido = json_decode($_PUT);
    //Pedido::
    http_response_code(200);
    return;
}

//insere registro
if(count($_POST)){
    $pedido = json_decode($_POST);
    //Pedido::
    http_response_code(200);
    return;
}

//retorna registro por id
if(isset($_GET['id'])){
    //$pedido = Pedido::getAgendamento($_GET['id']);
    //echo(json_encode($pedido,true));
    http_response_code(200);
    return;
}

if(isset($_GET['agendamentos-hoje'])){
    $pedidos = Pedido::getAgendamentosHoje();
    echo(json_encode($pedidos,true));
    http_response_code(200);
    return;
}

//retorna todos os registro ativos
$pedidos = Pedido::getAgendamentos();
echo(json_encode($pedidos,true));
http_response_code(200);
return;