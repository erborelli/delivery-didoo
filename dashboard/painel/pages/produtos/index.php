<?php
include_once('../../../includes.php');

$produtos   = Produto::getProdutosAtivosEInativos();

// BUSCAR DE PRODUTO
$busca = '';
if(isset($_GET['buscaNome'])){
  $busca          = $_GET['buscaNome'];
  $resultadoBusca = Busca::buscarNoModulo('produto','nome',$busca);
}

// FILTRO DE CATEGORIA
if(isset($_GET['filtrarCategoria'])){
  $idCategoria    = $_GET['filtrarCategoria'];
  $resultadoBusca = Busca::buscarNoModulo('produto','id_categoria',$idCategoria);
}

$boxCategorias = '<option value="">Ver todas</option>';
$categorias    = Categoria::getCategorias();
foreach ($categorias as $key => $value) {
  $selected = isset($idCategoria) && $value->id == $idCategoria ? 'selected' : '';
  $boxCategorias .= '<option '.$selected.' value="'.$value->id.'">'.$value->nome.'</option>';
}

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<style media="screen">
  .table-avatar{
    margin-left: 5px;
  }
</style>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Produtos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produtos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <?php include_once(CAMINHO_SISTEMA.'/dashboard/painel/pages/produtos/adicionais/modal-adicionais.php'); ?>

    <!-- busca / filtros -->
    <div class="col-sm-12 d-flex flex-row-reverse">
      <div class="col-sm-3" >
        <div class="form-group">
          <label>Filtrar por categoria:</label>
          <select class="form-control filtrarCategoria">
            <?=$boxCategorias?>
          </select>
        </div>
      </div>
      <div class="col-sm-3">
        <form class="" method="GET">
          <div class="form-group">
            <label>Nome Produto:</label>
            <input value="<?=$busca?>" type="text" name="buscaNome" class="form-control" placeholder="Buscar ..." >
          </div>
        </form>
      </div>
    </div>

    <section class="content">
      <!-- Default box -->

      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/criar.php">
        <i class="fas fa-plus">
        </i>Novo Produto
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Produtos</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          ID
                      </th>
                      <th style="width: 20%">
                          Nome
                      </th>
                      <th style="width: 13%">
                          Imagem
                      </th>
                      <th style="width: 22%">
                          Descrição
                      </th>
                      <th style="width: 9%" class="text-left">
                          Adicionais
                      </th>
                      <th style="width: 8%" class="text-center">
                          Preço
                      </th>
                      <th style="width: 8%" class="text-center">
                          Inativo
                      </th>
                      <th style="width: 15%">
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <?php

                    if(isset($resultadoBusca)){
                      $produtos = $resultadoBusca;
                    }

                    foreach ($produtos as $key => $value) {
                      $inativo        = $value->inativo == 's' ? 'checked' : '';
                      $preco          = $value->tipo == 'pizza' ? 'Variável' : 'R$ '.$value->valor;
                      $imagensProduto = '';
                      foreach (explode(',',$value->foto_produto) as $chave => $valor) {
                        $imagensProduto .= '<img alt="Produto" class="table-avatar" src="'.CAMINHO_DASHBOARD.'/images/produtos/'.$valor.'">';
                      }
                      echo '<tr>
                              <td>'.$value->id.'</td>
                              <td><a>'.$value->nome.'</a><br></td>
                              <td>
                                  <ul class="list-inline">
                                      <li class="list-inline-item">
                                        '.$imagensProduto.'
                                      </li>
                                  </ul>
                              </td>
                              <td class="project_progress">
                                  <small>'.$value->descricao.'</small>
                              </td>
                              <td class="text-left project-state">
                                <a id="aModalAdicionaisProduto" onclick="setarDadosModalAdicionais(this)" data-id-produto="'.$value->id.'" class="modalAdicionaisProduto" href="javascript:void(0)">
                                  <span data-toggle="modal" data-target="#modalAdicionais">Adicionais</span>
                                </a>
                              </td>
                              <td class="project-state">
                                  <span class="badge badge-success">'.$preco.'</span>
                              </td>
                              <td class="project-state">
                                  <input data-id="'.$value->id.'" class="checkInativarProduto" type="checkbox" '.$inativo.'>
                              </td>
                              <td class="project-actions text-right">
                                  <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'/painel/pages/produtos/editar.php?id='.$value->id.'">
                                      <i class="fas fa-pencil-alt"></i>Editar
                                  </a>
                                  <a data-id="'.$value->id.'" data-modulo="produto" class="btn btn-danger btn-sm" href="#">
                                      <i class="fas fa-trash"></i>Excluir
                                  </a>
                              </td>
                          </tr>';
                      }
                    ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/criar.php">
        <i class="fas fa-plus">
        </i>Novo Produto
      </a>
    </section>


  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>

<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/jquery/jquery.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>

<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })

  function setarDadosModalAdicionais(e){
    document.getElementById('txtIdProdutoModalAdicionais').value = e.getAttribute('data-id-produto')
  }

</script>
</body>
</html>
