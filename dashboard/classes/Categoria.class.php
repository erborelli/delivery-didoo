<?php

class Categoria {

    public static function getCategorias(){
        $link = getConnection();
        $sql  = "SELECT *,DATE_FORMAT(data_criacao,'%d/%m/%Y %H:%i:%s') as data FROM categoria ORDER BY ordem ASC;";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Categoria');
    }

    /**
     * utilizado no endpoint 'produtos' da 'api-mobile'
     */
    public static function getCategoriasArray(){
      $link = getConnection();
      $sql  = "SELECT id,nome,ordem FROM categoria ORDER BY ordem ASC";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getCategoriasArrayPorNome($nome=''){
      $link = getConnection();
      $sql  = "SELECT * FROM categoria WHERE nome LIKE CONCAT('%',?,'%') ORDER BY ordem ASC";
      $stmt = $link->prepare($sql);
      $stmt->execute([$nome]);
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getCategoriasArrayPorNomeVerificaAdicional($idAdicional,$tipoAdicional,$nome=''){
      $link = getConnection();

      $tipo = '';
      if($tipoAdicional=='adicionais'){
        $tipo = 'a.id_adicional';
      }else{
        $tipo = 'a.id_adicional_grupo';
      }

      $sql  = "SELECT c.*,
                    (
                      IF(
                        (SELECT a.id FROM categoria_adicional as a WHERE $tipo=? AND a.id_categoria=c.id LIMIT 1)
                        IS NOT NULL,'s','n'
                      )
                    ) as adicional
                FROM categoria as c
                WHERE c.nome LIKE CONCAT('%',?,'%') ORDER BY c.ordem ASC;";

      $stmt = $link->prepare($sql);
      $stmt->execute([$idAdicional,$nome]);
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function gravarCategoria($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      // trata o nome da categoria
      $dadosCategoria = strip_tags($dadosPost['nome']);

      $link = getConnection();
      $sql = "INSERT INTO categoria (nome,ordem) VALUES (?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosCategoria,$dadosPost['ordem']]);
      return true;
    }

    public static function updateCategoria($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      // trata o nome da categoria
      $dadosCategoria = strip_tags($dadosPost['nome']);

      $link = getConnection();
      $sql = "UPDATE categoria SET nome=?,ordem=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosCategoria,$dadosPost['ordem'],$_GET['id']]);
      return true;
    }

    public static function getCategoria($idCategoria){
        if(!isset($idCategoria) || !is_numeric($idCategoria) || empty($idCategoria)){
          return false;
        }
        $link = getConnection();
        $sql  = "SELECT * FROM categoria WHERE id = :idCategoria";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':idCategoria', $idCategoria);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Categoria');
    }

}
