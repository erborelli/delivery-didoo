<?php
include_once('../../../../includes.php');

$grupos = UsuarioGrupo::getGrupos();

if(isset($_POST) && isset($_POST['btnEnviar'])){
  $_POST['senha'] = md5($_POST['senha']);
  if(Usuario::criarUsuario($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/controle-acesso/usuarios/');
  }
}

?>

<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Criar Usuário</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Usuário</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="row">
        <form action="" autocomplete="off" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Criar Usuário</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome Usuário</label>
                  <input type="text" name="nome" id="nome" class="form-control">
                </div>
                <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="text" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                  <label for="id_usuario_grupo">Grupo de Usuário</label>
                  <select class="form-control" name="id_usuario_grupo">
                    <option></option>
                    <?php foreach($grupos as $grupo){ ?>
                      <option value="<?=$grupo->id?>"><?=$grupo->nome?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="senha">Senha</label>
                  <input  onfocus="this.removeAttribute('readonly');" type="password" name="senha" id="senha" class="form-control passwordUser" autocomplete="off"  readonly>
                  <!-- <span class="verSenha"><small>ver senha</small></span> -->
                </div>
              </div>

              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>

        </form>
      </div>
    </section>

  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>
<?php include_once('../../../includes-scripts/includes-scripts-estrutura.php'); ?>
</body>
</html>
