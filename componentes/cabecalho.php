<div class="container-fluid padding-0">
  <div id="header" class="row header-cover-photo js-header-cover-photo-mobile mobile header-cover-photo-mobile-padding" style="height: auto;">
    <div class="header-gradient-mobile"></div>
    <div class="col-xs-12 col-sm-12 hidden-md hidden-lg ">
    </div>
  </div>
  <div id="header-div-top" class="background-header-container">
    <div class="background-header-container js-background-header-cover-photo">
      <div class="header-cover-photo desktop js-header-cover-photo"></div>
    </div>
    <div class="row">
      <div class="header-gradient"></div>
      <div class="hidden-xs hidden-sm col-md-12 col-lg-12">
        <div class="header-fixed js-header-fixed" data-cy="store__header-fixed">
          <div class="wrap container">
            <img class="js-link-to-top img-header-fixed img-responsive" src="<?=CAMINHO_IMAGEM?>logo/<?=$dadosHome['logo']?>" />

            <div class="items-search-bar items-search-bar-header" data-component="ItemsSearchBar">
              <div class="header-fixed js-header-fixed hidden-md hidden-lg without-promo-box-mobile">
                <div class="mobile-header js-mobile-header">
                  <div id="CategoriesFilter" class="categories-filter js-categories-hidden dropdown float-left">
                    <select name="" id="" class="mobile-dropdown-menu js-list-mobile form-control">
                      <?=$boxFiltroCategoriaMobile?>
                    </select>
                  </div>
                  <button class="js-return-button return-button btn hidden"><i class="icon material-icons color-settings-secondary">arrow_back</i></button>
                  <div class="hidden js-search-hidden items-search">
                    <input type="text" class="buscarProdutoNome" data-ga="cardapio-item-search" placeholder="Busque por um produto">
                    <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
                    <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
                  </div>
                  <button class="js-search-button search-button btn">
                    <i class="icon material-icons js-show-search color-settings-secondary">search</i>
                  </button>
                </div>
              </div>

              <div id="CategoriesFilter" class="categories-filter dropdown float-left js-categories-filter-body" data-facebooktab=":eq(1)">
                <select name="" id=""  class="font-size-16 mobile-dropdown-menu js-list-mobile  hidden-md hidden-lg form-control" data-ga-action="menu__filter_category" data-ga-category="menu">
                  <?=$boxFiltroCategoriaMobile?>
                </select>
                <button class="dropdown-toggle js-dropdown hidden-sm hidden-xs " type="button" data-toggle="dropdown" data-ga-action="menu__filter_category" data-ga-category="menu" aria-haspopup="true" aria-expanded="true" value="Filtrar catálogo">
                  <span class="js-selected">Filtrar catálogo</span>
                  <span class="caret color-settings-secondary"></span>
                </button>
                <ul class="js-list dropdown-menu categories-filter__dropdown-menu hidden-sm hidden-xs" aria-labelledby="CategoriesFilter" data-ga-action="menu__filter_category_select" data-ga-category="menu">
                  <?=$boxFiltroCategoria?>
                </ul>
              </div>
              <div class="js-items-search-bar-fixed items-search-bar-fixed hidden-md hidden-lg">
                <button class="js-return-button-fixed return-button btn hidden"><i class="icon material-icons color-settings-secondary">arrow_back</i></button>
                <div id="SearchBar" class="hidden js-search-hidden-fixed items-search">
                  <input type="text" class="buscarProdutoNome" data-ga="cardapio-item-search" placeholder="Busque por um produto">
                  <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
                  <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
                </div>
                <button class="js-search-button-fixed search-button btn">
                  <i class="icon material-icons js-show-search color-settings-secondary">search</i>
                </button>
              </div>

              <div class="hidden-sm hidden-xs items-search">
                <input type="text" class="buscarProdutoNome" data-ga-action="menu__search_submit" data-ga-category="menu" placeholder="Busque por um produto">
                <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
                <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
              </div>
              <div class="items-search-result-ga-click js-items-search-result-click" data-ga-action="menu__search_select_result" data-ga-category="menu"></div>
            </div>
            <?php if($mostrarValorMinimo){ ?>
              <ul class="header-menu js-header-menu">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent checkout-button botaoPedidoMinimoFlutuante" style="background-color: rgb(153, 153, 153);">
                  <span class="text js-go-to-checkout-text" style="color: white;">Pedido mínimo de R$<?=$valorMinimoPedido?></span>
                  <span class="mdl-button__ripple-container">
                    <span class="mdl-ripple"></span>
                  </span>
                </button>
              </ul>
            <?php } ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
