<?php

class Tema {

    public static function getSaudacao(){
      date_default_timezone_set('America/Sao_Paulo');
      $saudacao = '';
      $time     = date("H");
      if ($time < 12) {
        $saudacao = "Bom dia";
      }elseif ($time >= 12 && $time < 18) {
        $saudacao = "Boa tarde";
      } else{
        $saudacao = "Boa noite";
      }
      return $saudacao;
    }

}
