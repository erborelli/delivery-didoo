<?php

class Config {

  public static function getVariaveisConfig($link){
    $sql  = "SELECT * FROM config";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Config');
  }

  public static function getConfigEspecifico($hash){
    $link = getConnection();
    $sql  = 'SELECT valor FROM config WHERE hash = "'.$hash.'"';
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ)->valor;
  }

  public static function setConfigGlobais(){
    $link   = getConnection();
    $config = self::getVariaveisConfig($link);
    foreach ($config as $key => $value) {
      DEFINE(strtoupper($value->hash),$value->valor);
    }
  }


    public static function getVariavelConfigHash($hash){
        $link = getConnection();
        $sql  = "SELECT * FROM config
                WHERE hash = :HASH";

        $stmt = $link->prepare($sql);
        $stmt->execute([':HASH'=>$hash]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
