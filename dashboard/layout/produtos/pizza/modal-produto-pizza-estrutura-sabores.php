<?php
  include_once('../../../../../includes.php');
?>

<!-- Modal Sabores -->
<div class="modal fade" id="modalSabores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div style="max-width:600px;" class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Vincular possíveis sabores:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="conteudoEntregas" style="width: 90%;">
        <div class="row">
          <div class="col-md-4 col-4">
            <div style="margin-top: 5px; margin-left: 10px;">
              <label>Selecionar Todos</label>
              <input id="chkSelecionarTodosSabores" type="checkbox">
            </div>
          </div>
          <div class="col-md-8 col-8">
            <input id="txtBuscaSaboresPizza" class="form-control" placeholder="Buscar...">
          </div>
        </div>
        <div class="modal-body text-center conteudoSabores">
          <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary salvarVinculoSabores">Salvar</button>
      </div>
    </div>
  </div>
</div>
