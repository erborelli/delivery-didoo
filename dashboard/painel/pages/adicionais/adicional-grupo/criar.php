<?php
include_once('../../../../includes.php');

//post de insert
if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(AdicionalGrupo::gravarAdicionalGrupo($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/adicional-grupo/');
  }
}

?>

<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Criar Grupo de Adicionais</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Grupo de Adicionais</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Criar Grupo de Adicionais</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" required>
                </div>
                <div class="row">
                  <div class="form-group col-md-1">
                    <label for="ordem">Ordem</label>
                    <input type="number"  min="0" max="99" name="ordem" id="ordem" class="form-control" required>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="qtdMinima">Quantidade Min</label>
                    <input type="number" min="0" max="99" name="qtdMinima" id="qtdMinima" class="form-control" required>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="qtdMaxima">Quantidade Max</label>
                    <input type="number" min="0" max="99" name="qtdMaxima" id="qtdMaxima" class="form-control" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ativo">Ativo</label>
                  <input type="checkbox" id="ativo" name="ativo">
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<?php include_once('../../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
