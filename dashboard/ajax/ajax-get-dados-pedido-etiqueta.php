<?php

include_once('../includes.php');

if(!isset($_POST['idPedido']) || !is_numeric($_POST['idPedido'])) return false;

$idPedido    = ($_POST['idPedido']);
$dadosPedido = Pedido::getPedido($idPedido);

$layoutEtiqueta = Etiqueta::montarEtiqueta($dadosPedido,'');

echo $layoutEtiqueta;
