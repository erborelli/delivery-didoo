<?php

include_once('../includes.php');

$whatsCliente = $_POST['whatsCliente'];

//verifica se esse cliente ja existe
$cliente = Cliente::getClientePorContato($whatsCliente);

$retorno = ['sucesso' => false];

if(isset($cliente) && !empty($cliente)){
  $retorno['sucesso'] =  true;
  $retorno['dadosCliente'] = $cliente[0];
}

echo json_encode($retorno);
