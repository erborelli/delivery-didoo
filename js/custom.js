/**
 * Classe responsável pelo preloader
 * @method      Preloader
 */
var Preloader = {
  show: function(mensagem){
    if($('.preloaderFull').length){
      $('.preloaderFull .preloaderFull-texto').html(mensagem);
      return;
    }
    $('.preloaderFull').remove();
    var html = '<div class="preloaderFull flex-container align-middle align-center"><div><span class="preloaderFull-texto">'+mensagem+'<span class="preloaderFull-pontos"></span></span><img class="preloaderFull-image" src="'+CAMINHO_DASHBOARD+'/images/preloader/preloader.svg"> </div></div>';
    $('body').append(html);
    $('.preloaderFull').hide();
    $('.preloaderFull').fadeIn(100);
  },

  hide: function(){
    $('.preloaderFull').fadeOut(100, function(){
      $(this).remove();
    });
  }
}

$(document).on('click','.containerChamadaEnderecoMobile',function(){
  $('.boxEnderecoMobileInforme').toggle(400);
  $('.icon-arrow-down-mobile').toggleClass('open');
});

// identifica click fora do container
document.getElementById('modalPedidoPendente').onclick = function(e) {
  var container = $('#conteudoModalPedidoPendente');
  // se o alvo do click nao for o container e nenhum descendente do container
  if (!container.is(e.target) && container.has(e.target).length === 0)
  {
    // esconde o modal com a animação
    $("#conteudoModalPedidoPendente").css({"left":"0px"}).animate({"left":"2000px"}, "slow");
    // esconde o fundo preto do modal
    setTimeout(function(){
      $('#modalPedidoPendente').modal('toggle');
    },450)
    // ajustar o left que a primeira animaçao deixou com 2000 para 0 novamente
    setTimeout(function(){
      $("#conteudoModalPedidoPendente").css({"left":"unset"});
    },850)
    verificaSePedidoPendenteFlutuanteNaoEstaVisivelEMostra();
    return false;
  }
  // daqui pra baixo é caso clicar dentro da DIV
}

function verificaSePedidoPendenteFlutuanteNaoEstaVisivelEMostra(){
  // verifico se o box não está visivel
  if(!$('.containerPedidoPendenteFlutuante').is(':visible')){
    // logo após é necessário verificar, se também há pedidos na sessao
    // pois se não houver mais pedidos na sessao, não tem por que mostrar o box flutuante de pedidos pendente
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-verificar-pedidos-sessao.php',
      type    : 'GET',
      dataType: 'JSON',
      success : function (data) {
        if(data.sucesso){
          // exibe o box flutuante de pedidos pendentes
          $('.containerPedidoPendenteFlutuante').show(300);
        }
      },
      error: function(data){
        console.log(data);
      }
    });
  }
}

// minimizar box de pedido pendente
$(document).on('click','#minimizarBoxPedidoPendente',function(e){
  // animação para esconder o box
  $("#conteudoModalPedidoPendente").css({"left":"0px"}).animate({"left":"2000px"}, "slow");
  // caso o box flutuante nao estiver visivel eu mostro ele
  verificaSePedidoPendenteFlutuanteNaoEstaVisivelEMostra();
  // remover o fundo preto do modal
  setTimeout(function(){
    $('#modalPedidoPendente').modal('toggle');
  },450)
  // ajustar o left que a primeira animaçao deixou com 2000 para 0 novamente
  setTimeout(function(){
    $("#conteudoModalPedidoPendente").css({"left":"unset"});
  },850)
});

// script que add/remove class label observacoes
// $(document).on('click','.observations-textfield',function(){
//   $(this).addClass('is-focused');
// });
// $(document).on('blur','.observations-textfield',function(){
//   if($(this).find('input').val().length == 0){
//     $(this).removeClass('is-focused');
//   }
// });

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

// click expandir / minimizar categorias destaques
$(document).on('click','.featured-items .expandCategoria',function(){
  $(this).parent().find('.featured-items__body').slideToggle();
  $(this).find('.material-icons').toggleClass('iconeExpandido');
})

// click expandir / minimizar categorias default
$(document).on('click','.products-section__head .expandCategoria',function(){
  $(this).parents('.products-section').find('.products-section__body').slideToggle();
  $(this).find('.material-icons').toggleClass('iconeExpandido');
})

// buscador de pedidos na sessao
setInterval(function(){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-verificar-pedidos-sessao.php',
    type    : 'GET',
    dataType: 'JSON',
    success : function (data) {
      if(data.sucesso){
        var inicioLink = getLinkWhatsPorDispositivo();
        var url = inicioLink + '/send?phone=55'+WHATSESTEBELECIMENTO+'&text=' + encodeURIComponent(data.mensagem.replace(/\|/g,'\n'));
        $('.linkWhatsappPedido a').attr('href',url);
        // verifica se há um retorno do pedido
        if(data.retornoPedido){
          // se o modal de retorna nao estiver visivel eu mostro ele
          if(!$('#modalPedidoPendente').is(':visible')){
            $('.abrirModalPedidoPendente').trigger('click');
          }
          // esconde o aviso de pedido pendente
          $('.containerBoxPedidoPendente').hide();
          // traz a mensagem e o conteudo do retorno do pedido
          $('.containerBoxRetornoPedidoPendente').html(data.retornoPedido).show();
          // esconde o box flutuante
          $('.containerPedidoPendenteFlutuante').hide(300);
        }
      }
    },
    error: function(data){
      console.log(data);
    }
  });
},3000);

// EVENTO ADICIONAR PRODUTO PARA O PEDIDO
$(document).on('click','.items-list-item',function(){
  var tipoProduto = $(this).attr('tipo-produto');
  var elemento    = $(this);
  var idProduto   = $(elemento).attr('data-id');
  if(PEDIR_CEP == 'antes'){
    verificarCepSessao(tipoProduto,elemento,idProduto);
  }else {
    verificarAberturaPorTipoDeProduto(elemento, tipoProduto);
  }
});

function verificarCepSessaoFinalizarCompra(tipoProduto,elemento,idProduto){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-verificar-cep-endereco-sessao.php',
    type    : 'GET',
    dataType: 'JSON',
    async   : false,
    success : function (data) {
      if(data){
        verificacoesAbrirBoxFinalizarCompra();
        return false;
      }
      // caso antes nao havia um endereço cadastrado, eu gravo qual o produto que ele havia clicado
      // pois apos adicionar o endereço, deve ser aberto este produto novamente
      abrirBoxEndereco(0,'addSessao');
    },
    error: function(data){
      console.log(data);
    }
  });
}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function verificarCepSessao(tipoProduto,elemento,idProduto){
  if(getCookie('enderecoSessao') != 0){
    verificarAberturaPorTipoDeProduto(elemento, tipoProduto);
    return false;
  }
  // caso antes nao havia um endereço cadastrado, eu gravo qual o produto que ele havia clicado
  // pois apos adicionar o endereço, deve ser aberto este produto novamente
  abrirBoxEndereco(idProduto);
}

function verificarAberturaPorTipoDeProduto(elementoClicado, tipoProduto){
  if(tipoProduto == 'pizza'){
    abrirModalConfiguracaoPizza(elementoClicado);
    return false;
  }
  atualizarInformacoesModalAdicionarAoCarrinho(elementoClicado,'modalAdicionarPedidoCarrinho');
  abrirBoxEscolherQuantidadeProdutos(elementoClicado);
}

function atualizarInformacoesModalAdicionarAoCarrinho(elemento,classContainerModal){
  var idProduto    = $(elemento).attr('data-id');
  let dadosImagens = getImagensProduto(idProduto);
  var imgsProduto  = dadosImagens.imagens;
  var nomeProduto  = $(elemento).find('.title').html();
  var descProduto  = $(elemento).find('.description').html();
  var valorProduto = $(elemento).attr('data-price');
  var tipoProduto  = classContainerModal == 'modalAdicionarPedidoCarrinho' ? 'Padrao' : 'Pizza';
  $("#"+classContainerModal).find('#carouselImagensProdutos'+tipoProduto+' .carousel-inner').html(imgsProduto);
  if(dadosImagens.qtdImagens == 1){
    $("#"+classContainerModal).find('#carouselImagensProdutos'+tipoProduto+' .carousel-control').hide();
  }else {
    $("#"+classContainerModal).find('#carouselImagensProdutos'+tipoProduto+' .carousel-control').show();
  }
  $("#"+classContainerModal).find('.item-name').html(nomeProduto);
  $("#"+classContainerModal).find('.item-description').html(descProduto);
  $("#"+classContainerModal).find('.js-item-price.price').html('R$'+valorProduto);
  $("#"+classContainerModal).attr('data-id-produto',idProduto);
}

function getImagensProduto(idProduto){
  let boxImagens = {'imagens': '', 'qtdImagens': 0};
  let img        = '';
  $('#items-list-item-'+idProduto+' .carousel-inner .item img').each(function(index,element){
    img    = $(element).attr('src');
    active = index == 0 ? 'active' : '';
    boxImagens.imagens += '<div class="item '+active+'">';
    boxImagens.imagens +=   '<img class="imgProdutoPopup'+idProduto+' item-photo img-responsive js-item-single" width="237" height="237" src="'+img+'">';
    boxImagens.imagens += '</div>';
    boxImagens.qtdImagens++;
  });
  setTimeout(function(){
    console.log(idProduto);
    $('.imgProdutoPopup'+idProduto).wrap('<span style="display:block;"></span>')
    .css('display', 'block')
    .parent()
    .zoom();
  },250);
  return boxImagens;
}

function abrirBoxEscolherQuantidadeProdutos(produtoClicado){
  var idProduto = $(produtoClicado).attr('data-id');
  Preloader.show('Aguarde...');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-conteudo-produto-default.php',
    type    : 'POST',
    data    : {idProduto:idProduto},
    dataType: 'json',
    success: function(data){
      Preloader.hide();
      if(data.sucesso){
        // reseta os valores da qtd
        $('.input-amount').val(1);
        // reseta a observação
        $('#observacaoProduto').val('');
        // $('.observations-textfield').removeClass('is-focused');
        // click no botão que abre o modal para montar o produto
        $('.abrirBoxSelecaoQuantidade').trigger('click');
        // layout adicionais
        $('#modalAdicionarPedidoCarrinho .containerAdicionais').html(data.layoutAdicionais);
        ajustePositionModal('default');
      }
    },
    error: function(data){
      Preloader.hide();
      console.log('err');
      console.log(data);
    }
  });
}

function ajustePositionModal(tipoProduto){
  setTimeout(function(){
    var heightModal = tipoProduto == 'pizza' ? $('.containerInformacoesPizza').height() : $('.contanierEsquerdoInfosProd').height();
    if(heightModal <= 401){
      $('.price-container').css('position','absolute');
    }else {
      $('.price-container').css('position','sticky');
    }
  },300);
}

function adicionarProdutoCarrinho(idProduto,qtdProduto,tipoProduto){
  var observacao = $('#observacaoProduto').val();
  if(tipoProduto == 'pizza'){
    observacao = $('#observacaoProdutoPizza').val();
  }
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-produto-carrinho.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {idProduto :idProduto,
               qtdProduto:qtdProduto,
               observacao:observacao},
    success : function (data) {
      if(data.lojaFechada){
        exibirWarningAgendamento('Estamos Fechados :(','Desculpe estamos fechados no momento, verifique nossos horários de atendimento, ou agende seu pedido!');
        return false;
      }
      if(data){
        $('.js-cart-list').removeClass('empty').html(data.boxProduto);
        $('.containerItensCarrinhoMobile').html(data.boxProdutoMobile);
        $('.js-cart-description').html(data.labelCarrinhoMobile);
        $('.js-subtotal-value').html('R$'+data.subtotalCarrinho);
        $('.js-total-value').html('R$'+data.totalCarrinho);
        mensagemSucesso('Produto adicionado ao carrinho');
        return false;
      }
    },
    error: function(data){
      console.log(data);
    }
  });
}

$(document).on('click','.use-zipcode,.description-container',function(){
  resetarVisibilidadeEscolhaFrete();
  abrirBoxEndereco();
});

$(document).on('click','.agendarPedido',function(){
  var dataAgendamento = $('.campoInputData').val();
  var horario         = $('#horarioAgendamento').val();
  if(horario.length < 4){
    exibirWarning('Selecione o Horário de Entrega!','Nenhum horário de agendamento selecionado!');
    return false;
  }
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-agendar-pedido.php',
    type    : 'POST',
    data    : {dataAgendamento:dataAgendamento,
               horario        :horario},
    dataType: 'json',
    success: function(data){
      if(data.sucesso){
        $('.js-scheduled-order-description').html(data.label);
        // botao agendamento mobile do CARRINHO
        $('.box-fechamento .text').html('Pedido agendado: '+data.label);
        // botao agendamento mobile da HOME(rodapé)
        $('.btnAgendarPedido').html(data.label);
        $('.js-scheduled-content').removeClass('is-hidden');
        $('.btn-to-schedule').hide();
        $('#modalAgendarEntrega').modal('toggle');
        // verifica se o box da loja está escondido e exibe
        if($('.boxLojaFechada').is(':visible')) $('.boxLojaFechada').hide();
        if($('.boxLojaAberta').is(':hidden')) $('.boxLojaAberta').removeClass('is-hidden').show();
        setTimeout(function(){
          $('.store-cart-box .fechar-pedido').trigger('click');
        },300);
      }
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
});

// busca do produto
$(document).on('keyup','.buscarProdutoNome',function(e){
  // define a busca
  var busca = $(this).val().toLowerCase();
  // caso estiver apagando a busca, mostra todos os itens
  if(e.keyCode == 8 && busca.length <= 1){
    $('.products-section').show(300);
    $('.items-list-item').show(300);
  }
  // busca maior de 2 caracteres
  if(busca.length >= 2){
    // foreach das categorias
    $('.products-section').each(function(index,element){
      var qtdProdutosEncontrados = 0;
      // foreach produtos
      $(element).find('.items-list-item .title').each(function(indice,elemento){
        var textoProduto = $(elemento).text().toLowerCase();
        var match = textoProduto.indexOf(busca) !== -1;
        if(!match){
          $(elemento).parents('.items-list-item').hide(300);
        }else {
          qtdProdutosEncontrados++;
          $(elemento).parents('.items-list-item').show(300);
        }
      });
      // caso nao achou nenhum produto ele esconde a categoria
      if(qtdProdutosEncontrados == 0){
        $(element).hide(300);
      }else {
        $(element).show(300);
      }
    });
  }
});

function abrirCarrinho(){
  if($(window).width() < 992){
    // verifica se o carrinho já está aberto
    if(!$('#modalCarrinho').is(':visible')){
      $('.botaoAbrirModalCarrinho').trigger('click');
    }
  }
}

// scripts mobile
if($(window).width() < 992){

  // scroll mostrar flutuante mobile
  $.fn.scrollBottom = function() {
    return $(document).height() - this.scrollTop() - this.height();
  };
  var $window = $(window);
  $window.bind("scroll resize", function() {
    var scrollTop = $window.scrollTop()

    if(scrollTop > 550){
      $('.header-fixed').addClass('show');
    }else {
      $('.header-fixed').removeClass('show');
    }
  });

  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() > (document.documentElement.scrollHeight - 90)) {
      $('.logoQuandoMobile').show(300);
      $('.box-btWhats').addClass('paddinAdicional');
      return false;
    }
    $('.logoQuandoMobile').hide(200);
    $('.box-btWhats').removeClass('paddinAdicional');
  });

  // evento abrir search produtos
  $(document).on('click','.js-show-search',function(){
    $('.mobile-dropdown-menu').hide();
    $('.js-search-hidden-fixed').removeClass('hidden');
    $('.js-return-button-fixed').removeClass('hidden');
    $('.js-search-button-fixed').hide();
    // eventos header flutuante
    $('.js-categories-hidden').removeClass('float-left categories-filter');
    $('.js-search-hidden').removeClass('hidden');
    $('.js-return-button').removeClass('hidden');
    $('.js-search-button').hide();
  });
  // evento voltar para pesquisa por categoria
  $(document).on('click','.js-return-button-fixed',function(){
    $('.mobile-dropdown-menu').show();
    $('.js-search-hidden-fixed').addClass('hidden');
    $('.js-return-button-fixed').addClass('hidden');
    $('.js-search-button-fixed').show();
    // eventos header flutuante
    $('.js-categories-hidden').addClass('float-left categories-filter');
    $('.js-search-hidden').addClass('hidden');
    $('.js-return-button').addClass('hidden');
    $('.js-search-button').show();
  });
  // evento voltar para pesquisa por categoria no mobile, (sem o fixed na classe)
  $(document).on('click','.js-return-button',function(){
    $('.js-return-button-fixed').trigger('click');
  });

  // selecionar um filtro mobile
  $(document).on('change','.mobile-dropdown-menu',function(){
  // function changeFiltro(){
    var idCategoriaFiltrar = $(this).val();
    // caso a categoria for 0, quer dizer que é para visualizar todas
    if(idCategoriaFiltrar == 0){
      // então percorre todos os itens mostrando todos
      $('.listaToggle.cardapio .products-section').each(function(index,element){
        $(element).show(300);
      });
      return false;
    }
    // busca a categoria que está sendo procura e a exibe
    $('.listaToggle.cardapio .products-section[data-category_id="'+idCategoriaFiltrar+'"]').show(300);
    // percorre todas as outras categorias escondendo quem nao for o id procurado
    $('.listaToggle.cardapio .products-section').each(function(index,element){
      if($(element).attr('data-category_id') != idCategoriaFiltrar){
        $(element).hide(300);
      }
    });
  });

  $(document).on('click','.store-cart-box .containerCarrinho2 .js-cart-info-mobile',function(){
    abrirCarrinho();
  });

  $(document).on('click','.fecharModalAgendarPedidoMobile',function(){
    $('#modalAgendarEntrega').modal('toggle');
  });

  $(document).on('click','.js-delete-item',function(){
    var sessIdProduct = $(this).parents('.item-checkout').attr('data-id-sess');
    var itemClicado   = $(this);
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-remover-item-carrinho.php',
      type    : 'POST',
      data    : {sessIdProduct:sessIdProduct},
      dataType: 'json',
      success: function(data){
        if(data.sucesso){
          $(itemClicado).parents('.item-checkout').replaceWith('');
          // atualização dos valores
          $('.js-subtotal-value').html('R$'+data.valores.subtotal);
          $('.js-cart-description').html(data.labelCarrinhoMobile);
          $('.js-total-value').html('R$'+data.valores.total);
          if(data.qtdItens == 0){
            $('.js-cart-list').addClass('empty');
          }
        }
      },
      error: function(data){
        console.log('err');
        console.log(data);
      }
    });
  })

}

// ajusta tamanho tela modal
var tamanhoTela = $(window).width();
if(tamanhoTela > 1000){
  $('#modalCep .modal-dialog').css('width','410px');
}

// acao adicionar cupom
$(document).on('click','.btn-cupom',function(){
  $('.campoCupom').slideToggle();
});

// acao adicionar cupom
$(document).on('click','.aplicarCupomDesconto',function(){
  var hashCupom = isMobile() ? $('.codigo_cupom.mobile').val() : $('.codigo_cupom.desktop').val();
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-validar-cupom-desconto.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {hashCupom:hashCupom},
    success : function (data) {
      if(data.cupomExistente){
        exibirWarning('Já existe um cupom aplicado','já existe um cupom aplicado no seu carrinho');
        return false;
      }
      if(data.sucesso){
        exibirSucesso('Cupom aplicado com sucesso!','O cupom foi aplicado, e o desconto ja é válido no seu carrinho');
        $('.cupomAplicado').removeClass('is-hidden');
        $('.labelCupomAplicado').html(data.hashCupom);
        atualizarValoresCarrinho();
        return false;
      }
      exibirWarning('O cupom não existe ou atingiu a quantidade limite','');
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
});

function atualizarValoresCarrinho(){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-atualizar-valores-carrinho.php',
    type    : 'GET',
    dataType: 'JSON',
    success : function (data) {
      // atualizar valores carrinho
      $('.js-subtotal-value').html('R$'+data.subtotal);
      $('.js-total-value').html('R$'+data.total);
      if(data.retirada){
        $('.js-delivery-fee-value').html('Grátis (Buscar)');
      }else {
        $('.js-delivery-fee-value').html('R$'+data.frete);
      }
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
}

// acao click verificar cep local vermelho
$(document).on('click','.js-address-box .js-change-zipcode',function(){
  var cep = $('.js-input-zipcode').val();
  $('.zip-input').val(cep);
  abrirBoxEndereco();
});

// abre o box de endereço
function abrirBoxEndereco(idProduto = 0, sessaoFinalizar = ''){
  if(idProduto !== 0){
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-historico-produto.php',
      type    : 'POST',
      data    : {idProduto :idProduto},
    });
  }else if (sessaoFinalizar == 'addSessao') {
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-historico-produto.php',
      type    : 'POST',
    });
  }
  $('.botaoAbrirModalCep').trigger('click');
}

// evento fecha modal no X
$(document).on('click','.js-modal-close',function(){
  $('.btn-secondary').trigger('click');
});

// valor entrega para o bairro
function getValorEntregaBairro(bairro){
  var valorFrete = 0;
  $.ajax({
    async   : false,
    url     : CAMINHO_DASHBOARD+'ajax/ajax-calcular-valor-entrega-bairro.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {bairro:bairro},
    success : function (data) {
      if(bairro == bairroLoja && data == 0){
        $('.js-delivery-fee-value').attr('data-frete',0);
      }else {
        $('.js-delivery-fee-value').attr('data-frete',data);
      }
      valorFrete = data;
    },
    error: function(data){
      console.log(data);
    }
  });
}

function resetarVisibilidadeEscolhaFrete(){
  // title
  $('.titleModalInforme').hide(400);
  $('.titleModalRetirada').hide(400);
  $('.titleModalTipoEntrega').show(400);
  // layout
  $('.containerNossoEndereco').hide(400);
  $('.step-1.box-address').hide(400);
  $('.step-2.box-address').hide(400);
  $('.step-3.box-address').hide(400);
  $('.containerOpcoesRetirada').show(400);
}

$(document).on('click','.containerVoltarOpcoes',function(){
  resetarVisibilidadeEscolhaFrete();
});

// click na opção de retirada
$(document).on('click','.itemOpcaoRetirada',function(){
  // caso houver a classe disabled nao prossegue
  if($(this).hasClass('disabledRetirada')) return false;

  var opcaoEscolhida = $(this).attr('opcao-escolhida');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-selecionar-tipo-entrega.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {opcaoEscolhida:opcaoEscolhida},
    success: function(data){
      // substituir o layout da opção retirada pelo layout da opção escolhida
      if(data.sucesso){
        if(data.boxRetorno.length > 0){ // quer dizer que o cliente vai buscar / retirada
          // titles
          $('.titleModalTipoEntrega').hide(400);
          $('.titleModalRetirada').show(400);
          // layout
          $('.containerOpcoesRetirada').hide(400);
          $('.containerOpcaoRetirada').html(data.boxRetorno);
        }else if (data.tipoEndereco.length > 0 && data.tipoEndereco == 'cep') {
          // titles
          $('.titleModalInforme').show(400);
          $('.titleModalTipoEntrega').hide(400);
          // layout
          $('.containerOpcoesRetirada').hide(400);
          $('.step-1.box-address').show(400);
        }else if (data.tipoEndereco.length > 0 && data.tipoEndereco == 'digitando') {
          // titles
          $('.titleModalInforme').show(400);
          $('.titleModalTipoEntrega').hide(400);
          // layout
          $('.containerOpcoesRetirada').hide(400);
          $('.step-3.box-address').show(400);
        }
      }else if (data.lojaFechada) {
        exibirWarningAgendamento('Estamos Fechados :(','Desculpe estamos fechados no momento, verifique nossos horários de atendimento, ou agende seu pedido!');
      }
    },
    error:function(data){
      console.log(data);
    }
  });
});

// submit formulario de retirada no local
$(document).on('click','.btnConfirmarRetirada',function(){
  var dados = {};
  dados.logradouro = ruaLoja;
  dados.numero     = numeroLoja;
  dados.bairro     = bairroLoja;
  dados.localidade = cidadeLoja;
  dados.uf         = ufLoja;
  dados.cep        = cepLoja;
  getValorEntregaBairro(dados.bairro);
  dados.valorFrete = $('.js-delivery-fee-value').attr('data-frete');
  preencheDadosEndereco(dados);
  adicionarCepASessaoDoCliente(dados);
  atualizarLabelEnderecoDe('RETIRADA');
});

function atualizarLabelEnderecoDe(retiradaEntrega){
  $('.labelEnderecoDe').html(retiradaEntrega);
}

// evento submit cep modal
$(document).on('click','.step-1 .mdl-button__ripple-container',function(){
  //Nova variável "cep" somente com dígitos.
  var cep = $('.zip-input').val().replace(/\D/g, '');

  //Consulta o webservice viacep.com.br/
  $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    // caso cair aqui quer dizer que o cep está dentro da cidade do cliente
    if(1 == 1 /*dados.uf == 'SP' && dados.localidade == "Caraguatatuba"*/){

      getValorEntregaBairro(dados.bairro);
      var valorFrete = $('.js-delivery-fee-value').attr('data-frete');

      // caso nao houver bairro cadastrado para entrega
      if(!valorFrete){
        $('.retornoErroCep').html('entrega indisponível para sua localidade');
        return false;
      }

      $('.retornoErroCep').html('');
      preencheDadosEndereco(dados);
      $('.step-1.box-address').hide(400);
      if($('.step-2.box-address').hasClass('is-hidden')){
        $('.step-2.box-address').removeClass('is-hidden');
      }else {
        $('.step-2.box-address').show(400);
      }

      // só adiciona o evento da segunda etapa quando passar por aqui
      $(document).on('click','.step-2 #address-submit-btn .mdl-button__ripple-container',function(){
        // adciona umas variaveis adicionais
        dados.numero            = $('#confirmed-number').val();
        if(dados.numero.length == 0){
          exibirWarning('Digite o número da casa','Digite o número da casa');
          return false;
        }
        dados.valorFrete        = valorFrete;
        // então chama para adicionar a sessao
        var cepSessao = adicionarCepASessaoDoCliente(dados);
        atualizarLabelEnderecoDe('ENTREGA');
      });

    }else {
      // caso cair aqui é por que não está na cidade do cliente
      $('.retornoErroCep').html('entrega indisponível para sua localidade');
    }
  });

});

$(document).on('click','.step-3 #address-submit-btn .mdl-button__ripple-container',function(){
  // VALIDA CAMPOS PREENCHIDOS
  if($('.step-3 #address').val().length < 3 ||
     $('.step-3 #confirmed-number').val().length < 1 ||
     $('.step-3 #city').val().length < 2 ||
     $('.step-3 #neighborhood').val() == 0){
      exibirWarning('Preencha todos os campos','Há campos vazios, você precisa preencher todos!');
      return false;
  }

  dados            = {};
  dados.logradouro = $('.step-3 #address').val();
  dados.numero     = $('.step-3 #confirmed-number').val();
  dados.bairro     = $('.step-3 #neighborhood option:selected').text();
  dados.localidade = $('.step-3 #city').val();
  dados.referencia = $('.step-3 #reference').val();
  dados.uf         = ufLoja;
  dados.cep        = '00000-000';

  getValorEntregaBairro(dados.bairro);
  dados.valorFrete = $('.js-delivery-fee-value').attr('data-frete');
  preencheDadosEndereco(dados);
  adicionarCepASessaoDoCliente(dados);
  atualizarLabelEnderecoDe('ENTREGA');
});

function preencheDadosEndereco(dados){
  $('#confirmed-city').html(dados.localidade);
  $('#confirmed-address').html(dados.logradouro);
  $('#confirmed-neighborhood-label').html(dados.bairro);
}

function adicionarCepASessaoDoCliente(dados){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-validar-cep-endereco.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {dados:dados},
    success: function(data){
      // substituir o layout do endereço pelo endereço preenchido
      if(data.sucesso){
        $('#modalCep').modal('toggle');
        $('.store-cart-content .js-address-content-empty').addClass('is-hidden');
        $('.js-address-content').removeClass('is-hidden');
        $('.js-use-zipcode.use-zipcode').html('<span class="js-address-box-description"></span>');
        $('.js-address-box-description').html(data.endereco);
        atualizarValoresCarrinho();
        if(data.historico){
          setTimeout(function(){
            $('#items-list-item-'+data.historico).trigger('click');
          },400)
        }else if (data.historicoFinalizacao) {
          verificacoesAbrirBoxFinalizarCompra();
        }
      }else if (data.lojaFechada) {
        exibirWarningAgendamento('Estamos Fechados :(','Desculpe estamos fechados no momento, verifique nossos horários de atendimento, ou agende seu pedido!');
      }
    }
  });
}

function mascara(t, mask) {
  var i = t.value.length;
  var saida = mask.substring(1, 0);
  var texto = mask.substring(i)
  if (texto.substring(0, 1) != saida) {
    t.value += texto.substring(0, 1);
  }
}

function mensagemSucesso(mensagem){
  displayNotification('success', mensagem, 1000);
}

function exibirAvisoPedidoMinimo(valorMinimo){
  swal({
   title: 'Pedido Mínimo de R$'+valorMinimo,
   text: "O seu pedido deve atingir o valor mínimo",
   html: true,
   icon: "warning",
   className: 'swal-wide',
   confirmButtonColor: '#CACACA'
  });
}

$(document).on('click','#escolhaData, #escolhaHorario, .alterarData, .alterarHorario',function(){
  var tipoLayout = 'data';
  var label      = 'a Data';
  var idDia      = $('.campoInputData').attr('id-dia-agendado');
  var data       = $('.campoInputData').val();
  if($(this).hasClass('escolhaHorario')){
    tipoLayout = 'horario';
    label      = 'o Horário';
  }
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-data-horario.php',
    type    : 'POST',
    dataType: 'json',
    data    : {tipoLayout:tipoLayout,
               idDia     :idDia,
               data      :data},
    success: function(data){
      if(data.sucesso){
        sweetAlertHtml("Escolha "+label,data.layout);
        return false;
      }
      exibirWarning(data.mensagemErro);
    },
    error: function(data){
      console.log(data);
    },
  });
});

// aciona quando selecionado uma data
$(document).on('click','.containerItemDataAgendamento.ativo',function(){
  var dataSelecionadaSemFormatar = $(this).find('button').attr('data-sem-formatar');
  var dataSelecionada = $(this).find('button').attr('data');
  var idDia           = $(this).find('button').attr('id-dia');
  $('#escolhaData').attr('data-selecionada', dataSelecionadaSemFormatar);
  // caso já tiver selecionado um horário
  if($('.horarioAgendamento').attr('data-horario').length > 0){
    resertarHorarioAgendamento();
  }
  $('.escolhaData').hide();
  $('.campoInputData').val(dataSelecionada).attr('id-dia-agendado',idDia).show();
  $('.alterarData').show();
  swal.close();
});
// aciona quando selecionado um horário
$(document).on('click','.botaoHorariosAgendar',function(){
  let diaSelecionado  = $('#escolhaData').attr('data-selecionada');
  let horaSelecionada = $(this).attr('data');

  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-validar-quantidade-agendamentos-por-hora.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {
      diaSelecionado:  diaSelecionado,
      horaSelecionada: horaSelecionada
    },
    async   : false,
    success : function(data){
      // cai aqui caso retornou alguma mensagme de erro
      if(data.mensagemErro && data.mensagemErro.length > 0){
        exibirWarning(data.mensagemErro);
        return false;
      }
      // cai aqui caso nao retornou mensagem de erro
      $('.escolhaHorario').hide();
      $('.horarioAgendamento').val(horaSelecionada).attr('data-horario',horaSelecionada).show();
      $('.alterarHorario').show();
      swal.close();
    },
    error: function(data){
      console.log(data);
    },
  });

});

$(document).on('click','.containerPedidoPendenteFlutuante',function(){
  $("#conteudoModalPedidoPendente").css({"left":"2000px"}).animate({"left":"0px"}, "slow");
  $('#modalPedidoPendente').modal('toggle');
});

function resertarHorarioAgendamento(){
  $('.escolhaHorario').show();
  $('.horarioAgendamento').val('').attr('data-horario','').hide();
  $('.alterarHorario').hide();
}

function sweetAlertHtml(title, html){
  var el = createElementFromHTML(html);
  swal({
    title: title,
    content: el,
    buttons: false,
    className: 'swal-wide'
  });
}

function createElementFromHTML(htmlString) {
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();
  return div.firstChild;
}

function exibirAvisoCarrinhoVazio(valorMinimo){
  swal({
   title: 'Carrinho Vazio!',
   text: "Adicione um item no seu carrinho!",
   icon: "warning",
   buttons: {
     confirm: {
       text: "Ok",
       value: true,
       visible: true,
       className: "botaoOkWarning",
       closeModal: true
     }
   }
  });
}

$(document).on('click','.botaoAgendarPedido',function(){
  $('#modalCep').modal('hide');
  $('#modalAgendarEntrega').modal('show');
})

function exibirWarningAgendamento(mensagem,submensagem){
  swal({
    title: mensagem,
    text: submensagem,
    icon: "warning",
    buttons: true,
    dangerMode: true,
    buttons: {
      confirm: {
        text: "Ok",
        value: true,
        visible: true,
        className: "",
        closeModal: true
      },
      cancel: {
        text: "Agendar Pedido",
        value: false,
        visible: true,
        className: "botaoAgendarPedido",
        closeModal: true,
      }
    }
  });
}

function exibirWarning(mensagem,submensagem){
  swal({
    title: mensagem,
    text: submensagem,
    icon: "warning",
    buttons: true,
    dangerMode: true,
    buttons: {
      confirm: {
        text: "Ok",
        value: true,
        visible: true,
        className: "botaoOkWarning",
        closeModal: true
      }
    }
  });
}

function exibirSucesso(mensagem,submensagem){
  swal({
   title: mensagem,
   text: submensagem,
   icon: "success"
  });
}

$(document).on('click','.fechar-pedido, .btnFecharPedido',function(){
  if(PEDIR_CEP == 'depois'){
    verificarCepSessaoFinalizarCompra();
  }else {
    verificacoesAbrirBoxFinalizarCompra();
  }
});

function abrirFinalizacaoCompra(){
  // caso o carrinho nao estiver visivel ele dá um tempo,para abrir a finalizaçaõ
  if(!$('#modalCarrinho').is(':visible') && $(window).width() < 992) {
    setTimeout(function(){
      $('.botaoAbrirBoxFinalizarCompra').trigger('click');
    },550)
  }else {
    // caso o carrinho já estiver aberto, abre direto a finalização
    $('.botaoAbrirBoxFinalizarCompra').trigger('click');
  }
}

function verificacoesAbrirBoxFinalizarCompra(){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-validar-valor-pedido.php',
    type    : 'POST',
    dataType: 'JSON',
    async   : false,
    success : function(data){
      if(data.sucesso){
        // abre o carrinho
        abrirCarrinho();
        // depois abre a finalização de compra
        abrirFinalizacaoCompra();
      }else if(data.valorAbaixo){
        exibirAvisoPedidoMinimo(data.valor);
      }else if (data.carrinhoVazio) {
        exibirAvisoCarrinhoVazio();
      }
    },
    error: function(data){
      console.log(data);
    },
  });
}

// click botao remover qtd item
$(document).on('click','.btn-remove-item',function(){
  var qtdAtual = $('.input-amount').val();
  if(qtdAtual == 1){
    return false;
  }
  $('.input-amount').val(parseInt(qtdAtual) - 1);
});
// click botão adicionar qtd item
$(document).on('click','.btn-add-item',function(){
  var qtdAtual = $('.input-amount').val();
  if(qtdAtual == 99){
    return false;
  }
  $('.input-amount').val(parseInt(qtdAtual) + 1);
});

// voltar quando for endereço digitado por cep
$(document).on('click','.step-2 .voltarFormCep',function(){
  $('#modalCep .step-2.box-address').hide(400);
  $('#modalCep .step-1.box-address').show(400);
});

// voltar quando for endereço digitado manualmente
$(document).on('click','.step-3 .voltarFormCep',function(){
  $('#modalCep .step-3.box-address').hide(400);
  $('#modalCep .step-0 .containerOpcoesRetirada').show(400);
});

// finalizar compra
function finalizarCompra(){
  var nome      = $('#nomeCompleto').val();
  var cel       = $('#telefone').val();
  var pagamento = $('input[name="forma_pagamento"]:checked').val();
  var troco     = $('input[name="troco"]').is(':checked') ? true : false;
  var valorTroco= $('input[name="valorTroco"]').val();
  if(troco && valorTroco.length == 0){
    exibirWarning('Digite o valor do troco');
    return false;
  }
  Preloader.show('Aguarde');
  // var informacoesAdicionais = $('#informacoesAdicionais').val();
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-finalizar-pedido.php',
    type    : 'POST',
    dataType: 'json',
    data    : {
      nome       :nome,
      cel        :cel,
      pagamento  :pagamento,
      valorTroco :valorTroco
    },
    success: function(data){
      // substituir o layout do endereço pelo endereço preenchido
      if(data.sucesso){
        $('#botaoFecharModalFinalizarPedido').trigger('click');
        $('#modalCarrinho .js-modal-close').trigger('click');
        $('.containerTempoRedirecionar').show();
        setTimeout(function(){
          // caso o modal não estiver visivel abre ele
          if(!$('#modalPedidoPendente').is(':visible')){
            $('.containerBoxRetornoPedidoPendente').hide();
            $('.containerBoxPedidoPendente').show();
            $('.abrirModalPedidoPendente').trigger('click');
          }
          Preloader.hide();
        },2000);
        atualizarValoresCarrinho();
        limparItensCarrinho();
        var inicioLink = getLinkWhatsPorDispositivo();
        var url = inicioLink + '/send?phone=55'+WHATSESTEBELECIMENTO+'&text=' + encodeURIComponent(data.mensagem.replace(/\|/g,'\n'));
        $('.linkWhatsappPedido a').attr('href',url);
        // caso for um pedido de agendamento com a loja fechada
        if(data.agendamentoComLojaFechada){
          $('.boxLojaAberta').hide();
          $('.botaoEntregaAgendadaDesktop').hide();
          $('.boxLojaFechada').show();
          $('.btnAgendamentoLojaFechada').removeClass('is-hidden').show();
        }
        // CASO O CONFIG DE REDIRECIONAR WHATSAPP FOR IGUAL A SIM (S)
        if(REDIRECIONARWHATSAPP == 's'){
          var tempoRedirecionar = TEMPOREDIRECIONARWHATSAPP;
          setTimeout(countDown,1000);
          function countDown(){
            tempoRedirecionar--;
            if(tempoRedirecionar < 0){
              $('.containerTempoRedirecionar').hide();
              if(isMobile()){
                window.location.href = url;
              }else {
                window.open(url,'_blank');
              }
              return false;
            }
            $('.tempoRedirecionarWhats').html(tempoRedirecionar);
            setTimeout(countDown,1000);
          }
        }
        // nao lembro por que coloquei isso, (se nao me engano foi pq quando o pedido é agendado nao está removendo o box de agendamento sem dar o reoload)
        // setTimeout(function(){
        //   location.reload();
        // },300);
        return false;
      }
      if(data.mensagemErro && data.mensagemErro.length > 0){
        exibirWarning(data.mensagemErro, data.subMensagemErro);
      }
      Preloader.hide();
    },
    error: function(data){
      Preloader.hide();
      console.log(data);
    }
  });
}

var linkReplaceWhats = getLinkWhatsPorDispositivo() + '/send?phone=55'+WHATSESTEBELECIMENTO;
$('.linkWhats').attr('href',linkReplaceWhats);

/* Máscaras ER */
function mascaraTel(o, f) {
  v_obj = o
  v_fun = f
  setTimeout(execmascara(), 1)
}

function execmascara() {
  v_obj.value = v_fun(v_obj.value)
}

function mtel(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/^(\d{2})(\d)/g, '($1) $2'); //Coloca parênteses em volta dos dois primeiros dígitos
  v = v.replace(/(\d)(\d{4})$/, '$1 - $2'); //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}

function id(el) {
  return document.getElementById(el);
}
// window.onload = function() {
  $(document).on('keyup','#telefone',function() {
    mascaraTel(this, mtel);
  });
// }

$(document).on('click','.delete i',function(){
  var sessIdProduct = $(this).parents('.delete').attr('data-id-sess');
  var itemClicado   = $(this);
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-remover-item-carrinho.php',
    type    : 'POST',
    data    : {sessIdProduct:sessIdProduct},
    dataType: 'json',
    success: function(data){
      if(data.sucesso){
        $(itemClicado).parents('.list-item').replaceWith('');
        // atualização dos valores
        atualizarValoresCarrinho();
        if(data.qtdItens == 0){
          $('.js-cart-list').addClass('empty');
        }
      }
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
})

function limparItensCarrinho(){
  $('.js-cart-list').addClass('empty');
  $('.js-cart-list').html('');
}

// funcao link whatsapp
function getLinkWhatsPorDispositivo() {
    var linkAtual = 'https://web.whatsapp.com';
    if ($(window).width() < 992) {
      linkAtual = linkAtual.replace('web.', 'api.');
    } else {
      linkAtual = linkAtual.replace('api.', 'web.');
    }
  return linkAtual;
}

function replaceBadInputs(val) {
  // Replace impossible inputs as they appear
  val = val.replace(/[^\dh:]/, "");
  val = val.replace(/^[^0-2]/, "");
  val = val.replace(/^([2-9])[4-9]/, "$1");
  val = val.replace(/^\d[:h]/, "");
  val = val.replace(/^([01][0-9])[^:h]/, "$1");
  val = val.replace(/^(2[0-3])[^:h]/, "$1");
  val = val.replace(/^(\d{2}[:h])[^0-5]/, "$1");
  val = val.replace(/^(\d{2}h)./, "$1");
  val = val.replace(/^(\d{2}:[0-5])[^0-9]/, "$1");
  val = val.replace(/^(\d{2}:\d[0-9])./, "$1");

  if(val.length == 2){
    var valor = val + ':';
    document.getElementById("horarioAgendamento").value = val;
    val = valor;
  }

  return val;
}



// click adicionar carrinho produto default
$(document).on('click','.adicionarCarrinhoProdutoDefault',function(){
  // variavel de controle para dar o return false caso haja algum erro
  erro = false;
  $('#modalAdicionarPedidoCarrinho .containerAdicionais .containerAdicionaisItens').each(function(index,element){
    var qtdMin = $(element).attr('data-qtd-min-adicionais');
    var qtdMax = $(element).attr('data-qtd-max-adicionais');
    var qtdSelecionada = $(element).find('.checkboxAdicional:checked').length;
    var nomeGrupo = $(element).find('.labelSabores').attr('nome-grupo');
    // caso a quantidade min nao for atingida
    if(qtdMin > 0 && qtdSelecionada < qtdMin){
      erro = true;
      exibirWarning('Você precisa selecionar no mínimo ' + qtdMin + ' ' +nomeGrupo ,'A quantidade mínima de ' + nomeGrupo + ' não foi selecionada');
    }
  });
  if(erro)return false;

  var idProduto  = $('#modalAdicionarPedidoCarrinho').attr('data-id-produto');
  var qtdProduto = $('#modalAdicionarPedidoCarrinho .input-amount').val();
  adicionarProdutoCarrinho(idProduto,qtdProduto,'default');
  $('#modalAdicionarPedidoCarrinho .js-modal-close').trigger('click');
})



// click adicionar carrinho pizza
$(document).on('click','.adicionarCarrinhoPizza',function(){
  var qtdSaboresLimite       = $('.qtdSabores').attr('data-qtd-sabores');
  var qtdSaboresSelecionados = $('.saboresPizza .checkboxSaborPizza:checked').length;
  var idProduto              = $('#modalMontarPizza').attr('data-id-produto');
  if(qtdSaboresSelecionados == 0){
    exibirWarning('Você precisa selecionar no mínimo 1 sabor!','Nenhum sabor selecionado');
    return false;
  }
  // variavel de controle para dar o return false caso haja algum erro
  erro = false;
  $('#modalMontarPizza .containerAdicionais .containerAdicionaisItens').each(function(index,element){
    var qtdMin = $(element).attr('data-qtd-min-adicionais');
    var qtdMax = $(element).attr('data-qtd-max-adicionais');
    var qtdSelecionada = $(element).find('.checkboxAdicional:checked').length;
    var nomeGrupo = $(element).find('.labelSabores').attr('nome-grupo');
    // caso a quantidade min nao for atingida
    if(qtdMin > 0 && qtdSelecionada < qtdMin){
      erro = true;
      exibirWarning('Você precisa selecionar no mínimo ' + qtdMin + ' ' +nomeGrupo ,'A quantidade mínima de ' + nomeGrupo + ' não foi selecionada');
    }
  });
  if(erro)return false;
  var qtdProduto = $('#modalMontarPizza .input-amount').val();
  adicionarProdutoCarrinho(idProduto,qtdProduto,'pizza');
  $('#modalMontarPizza .js-modal-close').trigger('click');
});

// CHECKED DO SABOR PIZZA
$(document).on('click','.checkboxSaborPizza',function(){
  var qtdSaboresLimite         = $('.qtdSabores').attr('data-qtd-sabores');
  var qtdSaboresSelecionados   = $('.saboresPizza .checkboxSaborPizza:checked').length;
  var idVinculoSaborValorPizza = $(this).attr('id-sabor');
  var idProduto                = $('#modalMontarPizza').attr('data-id-produto');
  var acao                     = $(this).is(':checked') ? 'add' : 'remove';
  if(qtdSaboresSelecionados > qtdSaboresLimite){
    exibirWarning('Máximo de sabores atingido!','Você selecionou a quantidade máxima de sabores');
    $(this).prop('checked',false);
    return false;
  }
  ajaxCalcularSaborPizza(acao,idVinculoSaborValorPizza,idProduto);
});

// CHECKED DO ADICIONAL
$(document).on('click','.checkboxAdicional',function(){
  var qtdMaximaAdicionais       = $(this).parents('.containerAdicionaisItens').attr('data-qtd-max-adicionais');
  var qtdAdicionaisSelecionados = $(this).parents('.containerAdicionaisItens').find('.checkboxAdicional:checked').length;
  var idAdicional               = $(this).attr('id-adicional');
  var idProduto                 = $(this).parents('.modalMontagemProduto').attr('data-id-produto');
  var acao                      = $(this).is(':checked') ? 'add' : 'remove';
  var nomeGrupo                 = $(this).parents('.containerAdicionaisItens').find('.labelSabores').attr('nome-grupo');
  if(qtdAdicionaisSelecionados > qtdMaximaAdicionais){
    exibirWarning('Máximo de ' + nomeGrupo + ' atingido!','Você selecionou a quantidade máxima de ' + nomeGrupo);
    $(this).prop('checked',false);
    return false;
  }
  ajaxCalcularAdicional(acao,idAdicional,idProduto,$(this).parents('.modalMontagemProduto'));
});

function limparValoresPizza(idProduto){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-limpar-valores-pizza-ao-fechar-modal.php'
  });
}

$('#modalMontarPizza').on('hidden.bs.modal', function () {
  var idProduto = $('#modalMontarPizza').attr('data-id-produto');
  limparValoresPizza(idProduto);
});

function ajaxCalcularAdicional(acao,idAdicional,idProduto,elementContainer){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-calcular-valor-adicional.php',
    type    : 'POST',
    data    : {acao:acao,
               idAdicional:idAdicional,
               idProduto:idProduto},
    dataType: 'json',
    success: function(data){
      $(elementContainer).find('.js-item-price.price').html('R$'+data);
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
}

function ajaxCalcularSaborPizza(acao,idVinculoSaborValorPizza,idProduto){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-calcular-valor-sabor-pizza.php',
    type    : 'POST',
    data    : {acao:acao,
               idVinculoSaborValorPizza:idVinculoSaborValorPizza,
               idProduto:idProduto},
    dataType: 'json',
    success: function(data){
      $('.containerPrecoPizza .js-item-price.price').html('R$'+data);
    },
    error: function(data){
      console.log('err');
      console.log(data);
    }
  });
}

// Apply input rules as the user types or pastes input
$('.horarioAgendamento').keyup(function(){
  var val = this.value;
  var lastLength;
  do {
    // Loop over the input to apply rules repeately to pasted inputs
    lastLength = val.length;
    val = replaceBadInputs(val);
  } while(val.length > 0 && lastLength !== val.length);
  this.value = val;
});

// Check the final result when the input has lost focus
$('.horarioAgendamento').blur(function(){
  var val = this.value;
  val = (/^(([01][0-9]|2[0-3])h)|(([01][0-9]|2[0-3]):[0-5][0-9])$/.test(val) ? val : "");
  this.value = val;
});

$(document).on('click','.menu-item',function() {
  coloreAbaDeAcordComItem($(this));
  mudaLabelAba($(this).attr('data-item-menu'));
  $('.listaToggle').hide();
  $('.menu-item').removeClass('selected');
  $(this).addClass('selected');
  var alvo = $(this).attr('data-alvo');
  $('.'+alvo).show();
});

function mudaLabelAba(alvoMostrarConteudo){
  if(alvoMostrarConteudo != 'item1'){
    $('.lupaMobileConteudoMenu').hide();
  }else {
    $('.lupaMobileConteudoMenu').show();
  }
  $('.conteudoAbaMobile').hide();
  $('.'+alvoMostrarConteudo).show();
}

function coloreAbaDeAcordComItem(abaClicada){
  if($(abaClicada).hasClass('sub-ponta-esquerda')){
    $('#ItemsSearchBar').css('background','#66c9d2');
  }else if ($(abaClicada).hasClass('sub-ponta-direita')) {
    $('#ItemsSearchBar').css('background','#f38f38');
  }else if ($(abaClicada).hasClass('item-ponta-direita')) {
    $('#ItemsSearchBar').css('background','#4856a6');
  }else if ($(abaClicada).hasClass('item-ponta-esquerda')) {
    $('#ItemsSearchBar').css('background',CORFAIXAPRINCIPALMOBILE);
  }
}

$(document).on('click','.itemHomeMenuMobile',function(){
  let tempoAnimacao = 600;
  // animação do scroll
  $([document.documentElement, document.body]).animate({
    scrollTop: $("#header").offset().top
  }, tempoAnimacao);
  // depois da animação força o click no catálogo novamente
  $('.item-ponta-esquerda').trigger('click');
});

$(document).on('click','.voltarHome',function(){
  $('.item-ponta-esquerda').trigger('click');
});

// evento para abrir o dropdown de filtros quando ele está fechado
$(document).on('click','.categories-filter.dropdown .dropdown-toggle',function(){
  var ulFiltro = $(this).siblings('.js-list.dropdown-menu');
  $(ulFiltro).css({'overflow-y': 'scroll', 'height': '276px', 'width': '125%'});
});

// evento para fechar o dropdown de filtros quando ele está aberto
$(document).on('click','.categories-filter.dropdown.open .dropdown-toggle',function(){
  var ulFiltro = $(this).siblings('.js-list.dropdown-menu');
  $(ulFiltro).removeAttr('style');
});

// selecionar um filtro
$(document).on('click','.js-list.dropdown-menu li',function(){
  $(this).parents('.js-list.dropdown-menu').removeAttr('style');
  var idCategoriaFiltrar = $(this).find('a').attr('data-id');
  var labelSelecionada   = $(this).find('a').text();
  $('.js-selected').html(labelSelecionada);

  // caso a categoria for 0, quer dizer que é para visualizar todas
  if(idCategoriaFiltrar == 0){
    // então percorre todos os itens mostrando todos
    $('.listaToggle.cardapio .products-section').each(function(index,element){
      $(element).show(300);
    });
    return false;
  }
  // busca a categoria que está sendo procura e a exibe
  $('.listaToggle.cardapio .products-section[data-category_id="'+idCategoriaFiltrar+'"]').show(300);
  // percorre todas as outras categorias escondendo quem nao for o id procurado
  $('.listaToggle.cardapio .products-section').each(function(index,element){
    if($(element).attr('data-category_id') != idCategoriaFiltrar){
      $(element).hide(300);
    }
  });
});

// click selecionando forma de pagamento
// $(document).on('click','.containerPagamentosInput',function(){

//   console.log('containerPagamentosInput')

//   // desmarca todos os inputs
//   let inputs = this.parentElement.parentElement.getElementsByTagName('input')
//   for(let i=0;i<inputs.length;i++){
//     if(inputs[i].className=='containerPagamentosInput'){
//       inputs[i].setAttribute('checked',false)
//     }
//   }

//   // marca o atual input
//   this.setAttribute('checked',true)

//   // pede troco
//   if(this.getAttribute('data-troco')=='s'){
//     $('.opcaoTroco').show(300)
//   }

//   // não pede troco
//   else{
//     $('.opcaoTroco').hide(300)
//     $('.opcaoTroco input').prop('checked',false)
//     $('.trocoQuanto').val('').hide()
//   }
// })

// click selecionando forma de pagamento
$(document).on('click','.containerPagamentos',function(){
  // pede troco
  if(this.getAttribute('data-troco')=='s'){
    $('.opcaoTroco').show(300)
  }
  // não pede troco
  else{
    $('.opcaoTroco').hide(300)
    $('.opcaoTroco input').prop('checked',false)
    $('.trocoQuanto').val('').hide()
  }
})

$(document).on('click','.opcaoTroco',function(e){
  $('.trocoQuanto').show(300);
});

function abrirModalConfiguracaoPizza(elementoClicado){
  var idProduto = $(elementoClicado).attr('data-id');
  var idTamanho = $(elementoClicado).attr('id-tamanho');
  Preloader.show('Aguarde...');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-conteudo-montar-pizza.php',
    type    : 'POST',
    async   : true,
    data    : {idProduto:idProduto,
               idTamanho:idTamanho},
    dataType: 'JSON',
    success: function(data){
      Preloader.hide();
      if(data.sucesso){
        // reseta a observação
        $('#observacaoProdutoPizza').val('');
        // $('.observations-textfield').removeClass('is-focused');
        // substitui os sabores e adicionais
        $('.containerSabores .saboresPizza').html(data.layoutSabores);
        $('.containerSabores .containerAdicionais').html(data.layoutAdicionais);
        $('.containerSabores .qtdSabores').html(data.qtdSabores).attr('data-qtd-sabores',data.qtdSabores);
        ajustePositionModal('pizza');
      }
      atualizarInformacoesModalAdicionarAoCarrinho(elementoClicado,'modalMontarPizza');
      $('.abrirModalBoxMontarPizza').trigger('click');
    },
    error: function(data){
      Preloader.hide();
      console.log('err');
      console.log(data);
    }
  });
}

function isMobile(){
  if($(window).width() < 768){
    return true;
  }
  return false;
}

// estilos somente desktop
if($(window).width() > 992){
  $.fn.scrollBottom = function() {
    return $(document).height() - this.scrollTop() - this.height();
  };

  var $el     = $('.store-cart-box');
  var $window = $(window);

  $window.bind("scroll resize", function() {
    var scrollTop = $window.scrollTop()
    var calculo = ((screen.width - 992) / 2) - 9;
    if(scrollTop > 550){
      $el.addClass('fixaTopo');
      $('.header-fixed').addClass('show');
      $el.css({
        right: calculo,
        top  : '75px'
      });
    }else {
      $el.removeClass('fixaTopo');
      $('.header-fixed').removeClass('show');
    }
  });
}
