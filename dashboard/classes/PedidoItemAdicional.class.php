<?php

class PedidoItemAdicional{

  public static function gravarPedidoItemAdicional($id_pedido_item,$id_adicional,$valor){
    $link = getConnection();

    $sql  = "INSERT INTO pedido_item_adicional(id_pedido_item,id_adicional,valor) 
            VALUES(?,?,?)";

    $stmt = $link->prepare($sql);
    $stmt->execute([$id_pedido_item,$id_adicional,$valor]);
  }

  public static function getAdicionaisItemPedidoArray($idPedidoItem){
    if(empty($idPedidoItem) || !is_numeric($idPedidoItem)) return false;
    $link = getConnection();

    $sql  = "SELECT a.id, a.nome, a.preco FROM pedido_item_adicional pia
            INNER JOIN adicional a
            ON a.id = pia.id_adicional
            WHERE pia.id_pedido_item = ?";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedidoItem]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
