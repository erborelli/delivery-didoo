<?php

include_once('../includes.php');

// verifica se a loja está aberta
if(Atendimento::ALojaEstaFechadaESemAgendamento()){
  echo json_encode(['lojaFechada' => true]);exit;
}

// validação do post
if(!isset($_POST['idProduto'])      || !isset($_POST['qtdProduto']) ||
   !is_numeric($_POST['idProduto']) || !is_numeric($_POST['qtdProduto'])) return false;

$retorno = Carrinho::adicionarProduto($_POST);

echo json_encode($retorno);
