<?php

include_once('../includes.php');

if(!isset($_POST['idEntregador']) || !is_numeric($_POST['idEntregador'])) return false;

date_default_timezone_set('America/Sao_Paulo');

if(isset($_POST['dataFiltrar']) && !empty($_POST['dataFiltrar'])){
  $dataEntrega = $_POST['dataFiltrar'];
  $datasPieces = explode(' - ',$dataEntrega);
  foreach ($datasPieces as $key => $value) {
    $dataEntregaFormatada = date("Y-m-d", strtotime($value));
    if($key == 0){
      $dataEntrega = $dataEntregaFormatada.' - ';
    }else {
      $dataEntrega .= $dataEntregaFormatada;
    }
  }
}else {
  $dataEntrega = date('Y-m-d - Y-m-d');
}
$entregas    = Entregador::getEntregasPorIdEntregrador($_POST['idEntregador'],$dataEntrega);

$boxEntregas = '<table class="table">
                  <thead>
                    <tr>
                      <th style="width:60%;" scope="col">Endereco</th>
                      <th style="width:20%;" scope="col">Taxa Entrega</th>
                      <th style="width:20%;" scope="col">Data Pedido</th>
                    </tr>
                  </thead>
                  <tbody>';

$total = 0;
foreach ($entregas as $key => $value) {
  $dataFormatada = date('d/m/Y H:i:s', strtotime($value->data_pedido));
  $total       += $value->taxa_entrega;
  $boxEntregas .= '<tr>';
  $boxEntregas .= '<td>'.$value->endereco.'</td>';
  $boxEntregas .= '<td>'.$value->taxa_entrega.'</td>';
  $boxEntregas .= '<td>'.$dataFormatada.'</td>';
  $boxEntregas .= '</tr>';
}
$boxEntregas .= '<tr>
                  <td colspan="3"><b>Total: R$'.number_format($total,2,',','').'</b></td>
                 </tr>';
$boxEntregas .= '</tbody></table>';

echo $boxEntregas;
