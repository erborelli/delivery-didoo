<?php

class Sistema {

/**
  * Método responsável por processar os layouts
  * @method getLayout
  * @param  mixed   $variaveisEspecificas  Array de variáveis ou FALSE/NULL para quando não existirem variáveis
  * @param  string    $tema                 Nome do tema
  * @param  string    $sessao               Sessão do arquivo
  * @param  string    $layout               Nome do arquivo / conteudo do layout (quando não for arquivo = true)
  * @param  boolean   $arquivo              Define se o layout a ser processado é um arquivo ou um texto já pronto
  * @param  string    $root                 Caminho ROOT até o tema
  * @return string                          HTML pronto
  */
  public static function getLayout($variaveisEspecificas = false,/*$tema,*/$sessao, $layout,$arquivo = true){
    $root = $_SERVER["DOCUMENT_ROOT"];
    $template = null;
    // $padrao = MVC::getVariaveisPadroes();
    $padrao = [];
    if(!is_array($variaveisEspecificas)){
      $variaveis = $padrao;
    }else{
      $variaveis = array_merge($variaveisEspecificas,$padrao);
    }

    if($arquivo){
      $sessao .= strlen($sessao) ? '/' : '';
      // $layout = $root.'/tema/'.$tema.'/layout/'.$sessao.$layout.'';
      $layout = CAMINHO_SISTEMA.$sessao.$layout;
      if(file_exists($layout)){
        $template = file_get_contents($layout);
      }
    }else{
      $template = $layout;
    }
    if($template){
      $template = self::processarVariaveisLayout($template,$variaveis);
      $template = self::processarFuncoes($template);
      return $template;
    }
  }


  /**
   * Método responsável por processar funções
   * @method processarFuncoes
   * @param  string            $template
   * @return string
   */
  public static function processarFuncoes($template){
    $exp = '/\{\%(.*?)\%\}/';
    $resultado = preg_match_all($exp, $template, $matches);
    if($resultado > 0 and isset($matches[1])){
      $matches = $matches[1];
      foreach($matches as $key){
        $template = str_replace('{%'.$key.'%}',self::processarFuncao($key),$template);
      }
    }
    return $template;
  }


  /**
   * Método responsável por processar variáveis de um layout
   * @method processarVariaveisLayout
   * @param  string                   $template  Conteudo do layout
   * @param  array                    $variaveis Variáveis a serem impressas
   * @return boolean                  string
   */
  public static function processarVariaveisLayout($template,$variaveis){
    $exp = '/\{\{([^\[].*?)\}\}/';
    $resultado = preg_match_all($exp, $template, $matches);
    if(is_array($variaveis) and $resultado > 0 and isset($matches[1])){
      $matches = $matches[1];
      foreach($matches as $key){
        if(array_key_exists($key,$variaveis) and !is_array($variaveis[$key])){
          $value = $variaveis[$key];
        } else {
          continue;
        };
        $view = '{{'.$key.'}}';
        $template = str_replace($view,$value,$template);
      }
    }
    return $template;
  }

}
