
ALTER TABLE produto_adicional ADD id_adicional_grupo int;

CREATE TABLE adicional_grupo(
	id int not null auto_increment,
    ordem int,
    ativo enum('s','n'),
    nome varchar(200),
    qtdMinima int,
    qtdMaxima int,
    data_criacao datetime DEFAULT current_timestamp(),
    primary key(id)
);

CREATE TABLE adicionais_grupo(
	id int not null auto_increment,
    id_adicional_grupo int,
    id_adicional int,
    CONSTRAINT FOREIGN KEY (id_adicional_grupo) REFERENCES adicional_grupo(id),
    CONSTRAINT FOREIGN KEY (id_adicional_grupo) REFERENCES adicional(id),
    primary key(id)
);

CREATE TABLE categoria_adicional(
	id int not null auto_increment,
    id_categoria int,
    id_adicional int,
    id_adicional_grupo int,
    CONSTRAINT FOREIGN KEY (id_categoria) REFERENCES categoria(id),
    primary key(id)
);
