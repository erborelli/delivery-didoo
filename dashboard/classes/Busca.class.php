<?php

class Busca {

  public static function buscarNoModulo($nomeTabela,$campoBuscar,$valorBuscado){
    $link = getConnection();
    $sql  = 'SELECT * FROM '.$nomeTabela.' WHERE '.$campoBuscar.' LIKE "%'.$valorBuscado.'%"';
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,$nomeTabela);
  }

}
