<?php
  include_once('../includes.php');

  // redireciona o usuário caso não houver a sessao logada
  if(!Login::verificarUsuarioLogado()){
      header('location: ../login.php');
  }

  $totalPedidosHoje    = Pedido::getQtdPedidosHoje()['id'];
  $agendamentos        = Pedido::getAgendamentosHoje()['id'];
  $totalVendasHoje     = Pedido::getVendasHoje();
  $clientesRegistrados = Cliente::getClientesRegistrados()['clientes'];
?>

<!DOCTYPE html>
<html>

<?php include_once('head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?=$totalPedidosHoje?></h3>
                <p>Pedidos Hoje</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/pedidos" class="small-box-footer">Mais informações <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?=$agendamentos?></h3>
                <p>Agendamentos Hoje</p>
              </div>
              <div class="icon">
                <i class="fas fa-calendar-alt"></i>
              </div>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/agendamentos" class="small-box-footer">Mais informações <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>R$ <?=$totalVendasHoje?><sup style="font-size: 20px"></sup></h3>
                <p>Valor Total Vendas Hoje</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/pedidos" class="small-box-footer">
                Mais informações
                <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3 style="color: white;"><?=$clientesRegistrados?></h3>
                <p style="color: white;">Total Clientes Registrados</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/clientes" style="color: white;" class="small-box-footer">
                <div style="color: white;">
                  Mais informações
                  <i style="color: white;" class="fas fa-arrow-circle-right"></i>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-6 connectedSortable">

            <!-- aqui vao os cards -->

          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-6 ">

            <!-- aqui vao os cards -->

          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include_once('includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
