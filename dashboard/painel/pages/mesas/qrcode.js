$(function () {
    $('#compose-textarea').summernote()
})

function atualizarLabels(){
    let lblSuperior    = document.getElementById('lblSuperior')
    let lblInferior    = document.getElementById('lblInferior')
    let lblInferiorUrl = document.getElementById('lblInferiorUrl')

    lblSuperior.textContent    = document.getElementById('txtTituloSuperior').value
    lblInferior.textContent    = document.getElementById('txtDescricaoInferior').value
    lblInferiorUrl.textContent = document.getElementById('txtUrlInferior').value
}

function imprimirQrCode(){
    var conteudo = document.getElementById('imprimir').innerHTML;
    impressao = window.open('about:blank');
    impressao.document.write(conteudo);
    impressao.window.print();
    impressao.window.close();
}

function abrirModalQrCode(mesa){
    document.getElementById('txtTituloSuperior').value    = 'CATÁLOGO DIGITAL'
    document.getElementById('txtDescricaoInferior').value = 'LEIA O QRCODE ACIMA OU DIGITE'
    document.getElementById('txtUrlInferior').value       = CAMINHO+'menu/qrcode/?mesa='+mesa
    document.getElementById('txtUrlQrCode').value         = CAMINHO+'menu/qrcode/?mesa='+mesa

    $('#qrCodeModal').modal('toggle')
    gerarQrCode()
}

function gerarQrCode(){
    $('#qrcode').empty()
    $('#qrcode').css({
        'width'  : 300,
        'height' : 300
    })
    $('#qrcode').qrcode(
        {
            render: 'image',
            size: 300,
            text: $('.qr-url').val()
        })

    atualizarLabels()
}
