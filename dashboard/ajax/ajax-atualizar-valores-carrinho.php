<?php

include_once('../includes.php');

$retirada = isset($_SESSION['endereco']) && isset($_SESSION['endereco']['cep']) && $_SESSION['endereco']['cep'] == CEP ? true : false;

if($retirada){
  $frete = 0;
}else {
  $frete = 0.00;
  if(isset($_SESSION['carrinho']['frete'])){
    $frete = $_SESSION['carrinho']['frete'];
  }elseif (isset($_SESSION['endereco']['valorFrete'])) {
    $frete = $_SESSION['endereco']['valorFrete'];
    $_SESSION['carrinho']['frete'] = $frete;
  }
  // frete
  $frete    = number_format($frete,2);
}
// subtotal
$subtotal = isset($_SESSION['carrinho']['subtotal']) ? $_SESSION['carrinho']['subtotal'] : 0.00;
$subtotal = number_format($subtotal,2);
// total
$total    = isset($_SESSION['carrinho']['total'])    ? $_SESSION['carrinho']['total']    : $frete + $subtotal;
$total    = number_format($total,2);

echo json_encode(['total'    => $total,
                  'frete'    => $frete,
                  'subtotal' => $subtotal,
                  'retirada' => $retirada]);
