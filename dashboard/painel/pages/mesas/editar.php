<?php
include_once('../../../includes.php');

//post de salvar edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(Mesa::updateMesa($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/mesas/');
  }
}

//se id do mesa não recebido, redireciona para listagem
if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/mesas/');
}

$mesa = Mesa::getMesaPorId($_GET['id']);

//caso tenha retornado false
if(!$mesa){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/mesas/');
}

if(isset($mesa[0])){
    $mesa = $mesa[0];
}

?>

<!DOCTYPE html>
<html>
  <style>
    #div{
      border: 1px solid;
      padding: 5px;
      border-radius: 10px;
      border-color: #d2d4d7
    }
  </style>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Mesa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Mesa</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Editar Mesa</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <input type="hidden" name="id" id="id" value="<?=$mesa['id']?>">
              <div class="card-body">
                <div class="form-group">
                  <label for="numero">Número</label>
                  <input type="text" name="numero" id="numero" class="form-control" value="<?=$mesa['numero']?>">
                </div>
                <div class="form-group">
                  <label for="descricao">Descrição</label>
                  <input type="text" name="descricao" id="descricao" class="form-control" value="<?=$mesa['descricao']?>">
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>

  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
