<?php

class Cupom {

    public static function getCupomPorHash($hash){
      $sql  = "SELECT * FROM cupom WHERE hash_cupom = :hash";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':hash', $hash);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cupom');
    }

    public static function incrementarCupom(){
      if(!isset($_SESSION['carrinho']['cupom'])) return false;
      $link = getConnection();
      // query update
      $sql = "UPDATE cupom SET qtd_utilizado = qtd_utilizado + 1 WHERE hash_cupom = ?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$_SESSION['carrinho']['cupom']]);

      // query retornar a qtd utilizada dos cupons e a qtd disponivel
      $sql = "SELECT qtd_disponivel,qtd_utilizado FROM cupom WHERE hash_cupom = ?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$_SESSION['carrinho']['cupom']]);
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cupom');
    }

    public static function inserirCupomUtilizacao($idPedido,$idCliente,$dadosUtilizacao){
      if(!isset($idPedido) || !isset($idCliente) || empty($dadosUtilizacao)) return false;
      $dadosUtilizacao = $dadosUtilizacao[0];
      $labelUtilizacao = 'Utilizado '.$dadosUtilizacao->qtd_utilizado.' de '.$dadosUtilizacao->qtd_disponivel.' disponíveis';
      $link = getConnection();
      $sql = "INSERT INTO cupom_utilizacao (id_cliente,id_pedido,hash_cupom_utilizado,utilizacao) VALUES (?,?,?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$idCliente,$idPedido,$_SESSION['carrinho']['cupom'],$labelUtilizacao]);
      return true;
    }

    public static function getUsosCupomCliente($idCliente){
      $sql  = "SELECT * FROM cupom_utilizacao a
               INNER JOIN cupom b ON a.hash_cupom_utilizado = b.hash_cupom
               WHERE a.id_cliente = :idCliente";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idCliente', $idCliente);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cupom');
    }

    public static function getClientesUsouCupom($hashCupom){
      $sql  = "SELECT * FROM cupom_utilizacao a
               INNER JOIN cliente b ON a.id_cliente = b.id
               WHERE a.hash_cupom_utilizado = :hashCupom
               ORDER BY a.data_utilizacao";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':hashCupom', $hashCupom);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cliente');
    }

    public static function getCupomPorId($id){
      $sql  = "SELECT * FROM cupom WHERE id = :id";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cupom');
    }

    public static function getCupons(){
      $sql  = "SELECT * FROM cupom";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cupom');
    }

    public static function insertCupom($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "INSERT INTO cupom (nome_cupom,hash_cupom,tipo_desconto,valor_desconto,qtd_disponivel) VALUES (?,?,?,?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['hash'],'porcentagem',$dadosPost['desconto'],$dadosPost['qtd_disponivel']]);
      return true;
    }

    public static function updateCupom($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE cupom SET nome_cupom=?,hash_cupom=?,valor_desconto=?,qtd_disponivel=?
              WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['hash'],$dadosPost['desconto'],$dadosPost['qtd_disponivel'],$_GET['id']]);
      return true;
    }

    public static function temCupomNaSessao(){
      if(isset($_SESSION['carrinho']) &&
         isset($_SESSION['carrinho']['cupom']) &&
         isset($_SESSION['carrinho']['valorDescontoCupom']) &&
         is_numeric($_SESSION['carrinho']['valorDescontoCupom'])) return true;
      return false;
    }

    public static function getValorDescontoCupom(){
      if(!Cupom::temCupomNaSessao()) return 0;
      return Cupom::calcularValorDescontoCupom($_SESSION['carrinho']['subtotal'],$_SESSION['carrinho']['valorDescontoCupom']);
    }

    public static function calcularValorDescontoCupom($totalCarrinho, $descontoCupom){
      return (($totalCarrinho * $descontoCupom) / 100);
    }

    public static function calcularValorProduto($valorProduto){
      if(Cupom::temCupomNaSessao()){
        $desconto         = (($valorProduto * $_SESSION['carrinho']['valorDescontoCupom']) / 100);
        $valorComDesconto = $valorProduto - $desconto;
        return number_format($valorComDesconto,2);
      }
      return number_format($valorProduto,2);
    }

    public static function atualizarSessaoValoresCupom(){
      $subtotalCarrinho = $_SESSION['carrinho']['subtotal'];
      $valorDesconto    = (($subtotalCarrinho * $_SESSION['carrinho']['valorDescontoCupom']) / 100);
      $subTotal         = $subtotalCarrinho - $valorDesconto;
      $total            = (($subtotalCarrinho + $_SESSION['carrinho']['frete']) - $valorDesconto);

      $_SESSION['carrinho']['totalDescontoCupom'] = number_format($valorDesconto,2);
      $_SESSION['carrinho']['subtotal']           = number_format($subTotal,2);
      $_SESSION['carrinho']['total']              = number_format($total,2);
    }

}
