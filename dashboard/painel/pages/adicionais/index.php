<?php
include_once('../../../includes.php');
$adicionais = Adicional::getAdicionais();
?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
        <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Adicionais</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Adicionais</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/adicionais/criar.php">
                    <i class="fas fa-plus"></i> Novo Adicionais
                </a>
                <br><br>
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th style="width: 20%">
                                        Nome
                                    </th>
                                    <th style="width: 25%">
                                        Preço
                                    </th>
                                    <th style="width: 20%">
                                        Data Criação
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($adicionais as $adicional){ ?>
                                    <tr>
                                        <td><?=$adicional->nome?></td>
                                        <td><?=$adicional->preco?></td>
                                        <td><?=$adicional->data?></td>
                                        <td class="project-actions text-right">
                                            <a class="btn btn-info btn-sm" href="<?=CAMINHO_DASHBOARD.'/painel/pages/adicionais/editar.php?id='.$adicional->id?>">
                                                <i class="fas fa-pencil-alt"></i>Editar
                                            </a>
                                            <a data-id="<?=$adicional->id?>" data-modulo="adicional" class="btn btn-danger btn-sm" href="#">
                                                <i class="fas fa-trash"></i>Excluir
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/adicionais/criar.php">
                    <i class="fas fa-plus"></i> Novo Adicionais
                </a>
            </section>
        </div>
        <footer class="main-footer">
            <strong><?=NOMELOJA?></strong>
        </footer>
        <aside class="control-sidebar control-sidebar-dark"></aside>
    </div>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/jquery/jquery.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>
    <script>
        $(function () {
            $('#compose-textarea').summernote()
        })
    </script>
</body>
</html>