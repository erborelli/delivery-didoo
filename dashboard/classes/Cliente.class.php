<?php

class Cliente {

    public static function getClientesRegistrados(){
      $link = getConnection();
      $sql  = "SELECT count(id) as clientes FROM cliente";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    public static function insertClienteVendaInterna($dadosCliente){
      $link = getConnection();
      // verifica se o contato está na sessao
      if(!isset($dadosCliente['contato'])) return false;

      //verifica se esse cliente ja existe
      $cliente = self::getClientePorContato($dadosCliente['contato']);

      // caso já existir o cliente ele somente retorno true e nao insere
      if(!empty($cliente)) return $cliente[0]->id;

      $endereco = $_SESSION['venda_interna']['frete']['logradouro'].', n: '.$_SESSION['venda_interna']['frete']['numero'].', '.$_SESSION['venda_interna']['frete']['bairro'].', '.$_SESSION['venda_interna']['frete']['localidade'].' - '.$_SESSION['venda_interna']['frete']['uf'];

      // caso chegar aqui vai inserir o cliente
      $sql  = "INSERT INTO cliente (nome,endereco,contato) VALUES (?,?,?)";
      $stmt = $link->prepare($sql);
      $stmt->execute([$dadosCliente['nome'],$endereco,$dadosCliente['contato']]);

      // retorna id do cliente inserido acima
      return $link->lastInsertId();
    }


    public static function insertCliente(){
      $link = getConnection();

      // verifica se o contato está na sessao
      if(!isset($_SESSION['cliente']['contato'])) return false;

      //verifica se esse cliente ja existe
      $cliente = self::getClientePorContato($_SESSION['cliente']['contato']);

      // caso já existir o cliente ele somente retorno true e nao insere
      if(!empty($cliente)) return $cliente[0]->id;

      $endereco = $_SESSION['endereco']['logradouro'].', '.$_SESSION['endereco']['numero'].', '.$_SESSION['endereco']['bairro'].', '.$_SESSION['endereco']['localidade'].' - '.$_SESSION['endereco']['uf'];

      // caso chegar aqui vai inserir o cliente
      $sql  = "INSERT INTO cliente (nome,endereco,contato) VALUES (?,?,?)";
      $stmt = $link->prepare($sql);
      $stmt->execute([$_SESSION['cliente']['nome'],$endereco,$_SESSION['cliente']['contato']]);

      // retorna id do cliente inserido acima
      return $link->lastInsertId();
    }


    public static function insertClienteModulo($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;

      $link = getConnection();
      $sql = "INSERT INTO cliente (nome,endereco,contato) VALUES (?,?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['endereco'],$dadosPost['contato']]);
      return true;
    }


    public static function getClientePorContato($contato){
      $link = getConnection();
      $sql  = "SELECT * FROM cliente WHERE contato = :contato";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':contato', $contato);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cliente');
    }


    public static function updateCliente($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE cliente SET nome=?,endereco=?,contato=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['endereco'],$dadosPost['contato'],$_GET['id']]);
      return true;
    }


    public static function getCliente($idCliente){
      $link = getConnection();
      $sql  = "SELECT * FROM cliente WHERE id = :idCliente";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idCliente', $idCliente);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cliente');
    }


    public static function getClientes(){
      $link = getConnection();
      $sql  = "SELECT * FROM cliente";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Cliente');
    }

    /**
     * @method Verificar usuario e senha
     * @param array cliente
     * @return boolean status do login
     */
    public static function logar(array $cliente){

      //no caso de receber um json
      if(isset($cliente['param'])){
        $cliente = json_decode($cliente['param'],true);
      }

      //verifica se recebeu os dados corretamente
      if(!isset($cliente['cpf']) || !isset($cliente['senha'])){
        return null;
      }

      $link = getConnection();
      $sql  = "SELECT * FROM cliente
               WHERE cpf=:CPF";

      $stmt = $link->prepare($sql);
      $stmt->bindParam(':CPF', $cliente['cpf']);
      $stmt->execute();
      $retornoCliente = $stmt->fetchAll(PDO::FETCH_CLASS,'Cliente');

      //verifica dados cliente
      if($retornoCliente==null || !isset($retornoCliente[0])){
        return null;
      }

      //verifica senha
      if($retornoCliente[0]->senha != $cliente['senha']){
        return null;
      }

      //se cpf e senha encontrados
      return [
        'id'       =>$retornoCliente[0]->id,
        'nome'     =>$retornoCliente[0]->nome,
        'endereco' =>$retornoCliente[0]->endereco,
        'contato'  =>$retornoCliente[0]->contato,
        'cpf'      =>$retornoCliente[0]->cpf
      ];
    }

    /**
     * @method cadastrar usuario no app
     * @param array $cliente
     * @return boolean status do cadastro
     */
    public static function insertClienteApp(array $cliente){
      //verifica dados clientes
      if(!isset($cliente['nome'])    || !isset($cliente['endereco']) || 
         !isset($cliente['contato']) || !isset($cliente['cpf'])){
         return false;
      }

      $link = getConnection();
      $sql = "INSERT INTO cliente(nome,endereco,contato,cpf,senha)
              VALUES(?,?,?,?,?)";

      $stmt= $link->prepare($sql);
      return $stmt->execute(
        [$cliente['nome'],$cliente['endereco'],$cliente['contato'],
        $cliente['cpf'],$cliente['senha']]);
    }


    /**
     * @method atualizar usuario no app
     * @param array $cliente
     * @return boolean status do cadastro
     */
    public static function updateClienteApp(array $cliente){
      //verifica dados clientes
      if(!isset($cliente['nome'])    || !isset($cliente['endereco']) || 
         !isset($cliente['contato']) || !isset($cliente['cpf']) || 
         !isset($cliente['id'])){
         return false;
      }

      $link = getConnection();
      $sql = "UPDATE cliente SET
                nome=?,
                endereco=?,
                contato=?
              WHERE id=?
              AND  cpf=?";
              
      $stmt= $link->prepare($sql);
      return $stmt->execute(
        [$cliente['nome'],$cliente['endereco'],$cliente['contato'],
        $cliente['id'],$cliente['cpf']]);
    }
}