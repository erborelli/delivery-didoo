<?php

include_once('../includes.php');

$dados     = array_filter($_POST['dadosVinculo']);
$idProduto = $_POST['idProduto'];

// excluir os vinculos para deopis passar
ProdutoAdicional::excluirVinculorPorIdProduto($idProduto);

// realiza os vinculos
ProdutoAdicional::vincularAdicionaisProduto($idProduto,$dados);
