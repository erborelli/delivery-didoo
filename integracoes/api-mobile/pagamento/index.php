<?php

require_once('../include.php');

//retorna todos as formas de pagamento
if(isset($_GET['formas-pagamento'])){

    $pagamento = [
        [
            'id'  =>1,
            'nome'=>'dinheiro',
            'troco'=>true
        ],
        [
            'id'  =>2,
            'nome'=>'cartão',
            'troco'=>false
        ],
        [
            'id'  =>3,
            'nome'=>'picpay',
            'troco'=>false
        ],
        [
            'id'  =>4,
            'nome'=>'boleto',
            'troco'=>false
        ]
    ];

    echo(json_encode($pagamento,true));
    http_response_code(200);
    return;
}

http_response_code(401);
json_encode(['mensagem'=>'parametros incorretos ou vazio'],true);
return;