create table forma_pagamento(
	id int not null auto_increment,
	nome varchar(255),
    descrição varchar(255) default '',
    troco enum('s','n') default 'n',
    
    primary key(id)
);

INSERT INTO forma_pagamento(nome,troco) VALUES('Dinheiro','s');
INSERT INTO forma_pagamento(nome,troco) VALUES('Cartão','n');


ALTER TABLE pedido MODIFY column tipo_pagamento VARCHAR(255);