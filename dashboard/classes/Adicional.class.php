<?php

class Adicional {


  public static function getAdicionais($array=false){
    $link = getConnection();
    $sql  = "SELECT *,DATE_FORMAT(data_criacao,'%d/%m/%Y %H:%i:%s') as data FROM adicional ORDER BY id";
    $stmt = $link->prepare($sql);
    $stmt->execute();

    if($array){
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function getAdicionaisPorProdutoCategoria($idProduto){
    $link = getConnection();
    $sql  = "SELECT DISTINCT(a.id),a.nome,a.preco,c.*
             FROM adicional a
             INNER JOIN adicionais_grupo b on a.id = b.id_adicional
             INNER JOIN adicional_grupo c on b.id_adicional_grupo = c.id
             INNER JOIN produto d ON d.id = ".$idProduto."
             LEFT JOIN  categoria_adicional e ON d.id_categoria = e.id
             LEFT JOIN  produto_adicional f ON d.id = f.id_produto
             ORDER BY c.ordem";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }

  /**
   * retorna todos os adicionais de um grupo de adicionais, tabela 'adicionais_grupo'
   */
  public static function getAdicionaisPorIdAdicionalGrupo($idAdicionalGrupo,$array=false){
    if(empty($idAdicionalGrupo)) return false;
    $link = getConnection();

    $sql = 'SELECT a.*
            FROM adicionais_grupo as ag
            INNER JOIN adicional as a
            ON ag.id_adicional = a.id
            WHERE ag.id_adicional_grupo = ?';

    $stmt = $link->prepare($sql);
    $stmt->execute([$idAdicionalGrupo]);
    if($array){
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function getLabelAdicionaisPorIds($idsAdicionais){
    $idsAdicionais = implode(',',$idsAdicionais);
    $link = getConnection();
    $sql  = "SELECT nome FROM adicional WHERE id IN (".$idsAdicionais.")";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function gravarAdicional($dadosPost){
    if(!isset($dadosPost) || empty($dadosPost)) return false;
    $link = getConnection();
    $sql = "INSERT INTO adicional (nome,preco) VALUES (?,?)";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['nome'],$dadosPost['preco']]);
    return true;
  }

  public static function updateAdicional($dadosUpdate){
    if(!isset($dadosUpdate) || empty($dadosUpdate)) return false;
    $link = getConnection();
    $sql = "UPDATE adicional SET nome=?,preco=? WHERE id=?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosUpdate['nome'],$dadosUpdate['preco'],$_GET['id']]);
    return true;
  }

  public static function getAdicionalPorId($idAdicional){
    if(!is_numeric($idAdicional)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM adicional WHERE id = ".$idAdicional;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function getAdicionaisProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT *, a.id as idAdicional
             FROM adicional a
             LEFT JOIN produto_adicional b
             ON a.id = b.id_adicional
             AND b.id_produto = ?
             ORDER BY a.id";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function getAdicionaisProdutoPorAdicional($idProduto,$adicional){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT *, a.id as idAdicional
             FROM adicional a
             LEFT JOIN produto_adicional b
             ON a.id = b.id_adicional
             AND b.id_produto = ?
             WHERE a.nome LIKE '%$adicional%'
             ORDER BY a.id";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  /**
   * Retorna APENAS adicionais vinculados ao produto
   */
  public static function getAdicionaisPorProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $sql  = "SELECT a.nome,a.preco, a.id
            FROM adicional a
            INNER JOIN produto_adicional pa
            ON a.id = pa.id_adicional
            WHERE pa.id_produto = ?
            ORDER BY a.nome";

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Retorna APENAS adicionais vinculados ao produto
   */
  public static function getObjAdicionaisPorProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM adicional a
            INNER JOIN produto_adicional pa
            ON a.id = pa.id_adicional
            WHERE pa.id_produto = ?
            ORDER BY a.nome";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }

  public static function getValorAdicional($idAdicional){
    if(!is_numeric($idAdicional)) return false;
    $link = getConnection();
    $sql  = "SELECT preco FROM adicional
             WHERE id = ".$idAdicional;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getValorAdicionaisVendaInterna($adicionaisArray){
    $valorAdicionais=0;
    if(count($adicionaisArray)>0){
      foreach($adicionaisArray as $adicional){
        $valorAdicionais += $adicional['valor'];
      }
    }
    return $valorAdicionais;
  }

  public static function insereIdAdicionalGrupoNoAdicional($dadosAdicional){
    $dados = json_decode($dadosAdicional['dados']);
    $dados = json_decode($dados->adicionais[0]);
    if(!isset($dados) || empty($dados)) return '';
    $idAdicional = $dados->id_adicional;

    $link = getConnection();
    $sql  = "SELECT a.id as id_grupo, a.nome as nome_grupo, a.qtdMinima as qtd_min_grupo, a.qtdMaxima as qtd_max_grupo
             FROM adicional_grupo a
             INNER JOIN adicionais_grupo b ON a.id = b.id_adicional_grupo
             WHERE b.id_adicional = ".$idAdicional;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    $dadosAdicionalGrupo = $stmt->fetch(PDO::FETCH_OBJ);
    // caso nao houver vinculo com nenhum grupo retorno falso
    if(empty($dadosAdicionalGrupo)) return false;
    // caso passar aqui quer dizer que há um grupo vinculado
    $dadosAdicionalGrupo->adicionais[0] = json_encode($dados);
    return $dadosAdicionalGrupo;
  }

  public static function padronizarAdicionais($grupoAdicionais){
    $adicionais    = json_decode('['.$grupoAdicionais->adicionais[0].']');
    $auxAdicionais = [];
    foreach ($adicionais as $chave => $valor) {
      $auxAdicionais[$valor->id_adicional] = (array)$valor;
    }
    return $auxAdicionais;
  }


  public static function getRelacionamentoPorAdicional($idAdicional){
    $sql = 'SELECT id_produto as "id", "produto" as tipo, p.nome
            FROM produto_adicional as a
            INNER JOIN produto as p
            ON p.id = a.id_produto
            WHERE id_adicional = ?
            UNION
            SELECT id_categoria as "id", "categoria" as tipo, c.nome
            FROM categoria_adicional as a
            INNER JOIN categoria as c
            ON c.id = a.id_categoria
            WHERE id_adicional = ?';

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idAdicional,$idAdicional]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }


  public static function getAdicionaisPorProdutoFormatados($idProduto){
    if(!is_numeric($idProduto)) return '';
    $adicionaisSemFormatar = self::getTodosAdicionaisPorProduto($idProduto);
    $adicionaisFormatados  = [];
    // percorre o foreach formatando o array recebido
    foreach ($adicionaisSemFormatar as $key => $value) {
      $grupoAdicionais = json_decode($value['dados']);
      // se for do tipo adicional eu chamo o metodo que recupera o do grupo do adicional
      if($value['tipoAdicional'] == 'adicional'){
        $grupoAdicionais = self::insereIdAdicionalGrupoNoAdicional($value);
        // caso retornou false, que nao tem vinculo, então eu vou pra prox iteração
        if(!$grupoAdicionais) continue;
      }
      // se nao tiver adicionais o grupo eu vou pra proxima iteração
      if(empty($grupoAdicionais->adicionais[0])) continue;
      // verifico se já existe esse grupo de adicionais
      if(isset($adicionaisFormatados[$grupoAdicionais->id_grupo])){
        $novosAdicionais        = self::padronizarAdicionais($grupoAdicionais);
        $adicionaisJaformatados = $adicionaisFormatados[$grupoAdicionais->id_grupo]->adicionais;
        $adicionaisFormatados[$grupoAdicionais->id_grupo]->adicionais = ($novosAdicionais + $adicionaisJaformatados);
      }else {
        // senao eu seto
        $adicionaisFormatados[$grupoAdicionais->id_grupo] = $grupoAdicionais;
        $adicionais = self::padronizarAdicionais($grupoAdicionais);
        $adicionaisFormatados[$grupoAdicionais->id_grupo]->adicionais = $adicionais;
      }
    }
    return $adicionaisFormatados;
  }

  public static function getTodosAdicionaisPorProdutoAdicionaisEmGrupo($idProduto){
    $adicionaisBd = self::getTodosAdicionaisPorProduto($idProduto);

    $adicionaisArray[0] = '';
    foreach($adicionaisBd as $chave => $adicionalBd){
      if($adicionalBd['tipo']=='adicionais' && $adicionalBd['tipoAdicional']=='adicional'){
        $adicionaisArray[0] .= json_decode($adicionalBd['dados'])->adicionais[0].',';
        unset($adicionaisBd[$chave]);
      }
    }

    if($adicionaisArray[0] != ''){
      $adicionaisArray[0] = substr_replace($adicionaisArray[0], "", -1);
      $adicionaisBd[] = [
        'tipo'          =>'adicional',
        'tipoAdicional' => 'grupo',
        'dados' => json_encode([
          'id_grupo'      => 0,
          'nome_grupo'    => 'Adicionais',
          'qtd_min_grupo' => 0,
          'qtd_max_grupo' => 999,
          'adicionais'    => $adicionaisArray,
        ]),
      ];
    }
    return $adicionaisBd;
  }

  public static function getTodosAdicionaisPorProduto($idProduto){
    $sql = "SELECT
              'categoria' as tipo,
              IF(ca.id_adicional>0,
                ('adicional'),('grupo')
                ) as tipoAdicional,
                IF(ca.id_adicional>0,
                (
                  json_object(
                    'adicionais',json_array(
                      (
                        SELECT
                          GROUP_CONCAT(
                            json_object('id_adicional',a.id,'nome_adicional',a.nome,'preco_adicional',a.preco)
                          )
                        FROM adicional as a
                        WHERE a.id = ca.id_adicional
                      )
                    )
                  )
                ),
                (
                  SELECT
                    json_object(
                      'id_grupo',g.id,
                      'nome_grupo',g.nome,
                      'qtd_min_grupo',g.qtdMinima,
                      'qtd_max_grupo',g.qtdMaxima,
                      'adicionais',json_array(
                        (
                        SELECT
                          GROUP_CONCAT(
                            json_object('id_adicional',a.id,'nome_adicional',a.nome,'preco_adicional',a.preco)
                          )
                        FROM adicionais_grupo as aig
                        INNER JOIN adicional as a
                        ON aig.id_adicional = a.id
                        WHERE aig.id_adicional_grupo = ca.id_adicional_grupo
                        )
                      )
                    ) as grupos
                  FROM adicional_grupo as g
                  WHERE g.id = ca.id_adicional_grupo
                )
              ) as dados
            FROM categoria_adicional as ca
            INNER JOIN produto as p
            ON p.id_categoria = ca.id_categoria
            WHERE p.id = ?

            UNION

            SELECT
              'adicionais' as tipo,
              IF(pa.id_adicional>0,
                ('adicional'),('grupo')
                ) as tipoAdicional,
                IF(pa.id_adicional>0,
                (
                  json_object(
                    'adicionais',json_array(
                      (
                        SELECT
                          GROUP_CONCAT(
                            json_object('id_adicional',a.id,'nome_adicional',a.nome,'preco_adicional',a.preco)
                          )
                        FROM adicional as a
                        WHERE a.id = pa.id_adicional
                                )
                            )
                        )
                    ),
                    (
                  SELECT
                    json_object(
                      'id_grupo',g.id,
                      'nome_grupo',g.nome,
                      'qtd_min_grupo',g.qtdMinima,
                      'qtd_max_grupo',g.qtdMaxima,
                      'adicionais',json_array(
                        (
                        SELECT
                          GROUP_CONCAT(
                            json_object('id_adicional',a.id,'nome_adicional',a.nome,'preco_adicional',a.preco)
                          )
                        FROM adicionais_grupo as aig
                        INNER JOIN adicional as a
                        ON aig.id_adicional = a.id
                        WHERE aig.id_adicional_grupo = pa.id_adicional_grupo
                        )
                      )
                    ) as grupos
                  FROM adicional_grupo as g
                  WHERE g.id = pa.id_adicional_grupo
                    )
                ) as dados
            FROM produto_adicional as pa
            WHERE pa.id_produto = ?";

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto,$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
