<?php

include_once('../includes.php');

$retorno = ['sucesso' => false];

if(!isset($_POST) || !isset($_POST['sessIdProduct'])) echo json_encode($retorno);

$retorno['sucesso']  = true;

$produtoRemovido = unserialize($_SESSION['carrinho']['itens'][$_POST['sessIdProduct']]);

// remove da sessao o produto
unset($_SESSION['carrinho']['itens'][$_POST['sessIdProduct']]);

// atualiza os valroes da sessao
Carrinho::calculaEAtualizarValoresSessaoCarrinho();

// label carrinho mobile
$qtdItensCarrinho = count($_SESSION['carrinho']['itens']);
$retorno['labelCarrinhoMobile'] = 'Seu carrinho está vazio.';
if($qtdItensCarrinho > 0){
  $retorno['labelCarrinhoMobile'] = $qtdItensCarrinho > 1 ? $qtdItensCarrinho.' ITENS NO SEU CARRINHO' : $qtdItensCarrinho.' ITEM NO SEU CARRINHO';
}

// verifica a quantidade que restou na sessao
$retorno['qtdItens'] = count($_SESSION['carrinho']['itens']);
$retorno['valores']  = Carrinho::getValoresCarrinho();

echo json_encode($retorno);
