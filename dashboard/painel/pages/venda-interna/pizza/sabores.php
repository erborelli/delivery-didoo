

<div class="modal fade" id="saboresPizzaModal" tabindex="-1" role="dialog" aria-labelledby="saboresPizzaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="saboresPizzaModalLabel">Adicionar Sabores na Pizza</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
            <label>Máximo de </label>
            <label id="lblQtdSaboresPizza"></label>
            <label>sabores</label>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">Sabor</th>
                        <th style="text-align: center;">Preço</th>
                        <th style="text-align: center;">Selecionar</th>
                    </tr>
                </thead>
                <tbody id="tbodySaboresPizza">
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnConfirmarSaborPizza" class="btn btn-primary">Continuar</button>
      </div>
    </div>
  </div>
</div>