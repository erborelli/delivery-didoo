<?php
include_once('../../../includes.php');

if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(Cliente::insertClienteModulo($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/clientes/');
  }
}

?>

<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Criar Cliente</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cliente</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Criar Cliente</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome Cliente</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label for="endereco">Endereço</label>
                  <input type="text" name="endereco" id="endereco" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label for="contato">Contato</label>
                  <input type="text" name="contato" id="contato" class="form-control" value="">
                </div>
              </div>

              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>

          </div>

        </form>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
