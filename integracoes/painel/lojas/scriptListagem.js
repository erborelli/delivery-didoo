
let dadosLojas = JSON.parse(document.getElementById('dados').textContent)

var editar  = function(cell, formatterParams, onRendered){
    return "<button class='btn btn-primary btn-sm'>Editar</button>";
}
var excluir = function(cell, formatterParams, onRendered){
    return "<button class='btn btn-danger btn-sm'>Excluir</button>";
}

var table = new Tabulator("#tblLojas",{
    data:dadosLojas,
    columns:[
        {title:"Código", field:"id", headerFilter:"input"},
        {title:"Nome", field:"nome", headerFilter:"input"},
        {title:"URL", field:"url", headerFilter:"input"},
        {formatter:editar, width:65, hozAlign:"center",
            cellClick:function(e, cell){
                window.location.href = 'formulario/?id='+cell.getRow().getData().id
            }
        },
        {formatter:excluir, width:70, hozAlign:"center",
            cellClick:function(e, cell){
                if(confirm('Deseja excluir a loja '+cell.getRow().getData().nome+' ?')){
                    $.ajax({
                        url     : CAMINHO_DASHBOARD+'ajax/lojas/ajax-excluir-loja.php?id='+cell.getRow().getData().id,
                        type    : 'GET',
                        success: function(data){
                          window.location.reload()
                        },
                        error: function(data){
                          alert('Erro ao excluir loja!')
                        },
                      });
                }
            }
        },
    ],
    //autoColumns:true,
    pagination:"local",
    paginationSize:10,
    paginationSizeSelector:[10, 50, 100, 300, 1000],
})