<?php
    require_once('../../../includes.php');
    $lojasJson = json_encode(Lojas::getLojas());
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listagem de lojas</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../libs/tabulator/css/semantic-ui/tabulator_semantic-ui.min.css">
</head>
<body>
    <div id="dados" style="display: none;">
        <?=$lojasJson?>
    </div>
    <div>
        <div style="text-align: center;">
            <h4>Listagem de lojas</h4>
        </div>
        <a class="btn btn-primary" href="formulario/">Nova Loja</a>
        <a class="btn btn-warning" href="../">Voltar Painel</a>
        <div id="tblLojas"></div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
    <script src="../libs/tabulator/js/tabulator.min.js"></script>
    <script src="scriptListagem.js?v=15"></script>
</body>
</html>