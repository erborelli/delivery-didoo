<?php

include_once('../../../includes.php');

if(!isset($_GET['id'])) return '';

$idPedido    = ($_GET['id']);
$dadosPedido = Pedido::getPedido($idPedido);

$layoutEtiqueta = Etiqueta::montarEtiqueta($dadosPedido);

echo $layoutEtiqueta;
