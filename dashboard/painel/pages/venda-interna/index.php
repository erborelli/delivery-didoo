<?php

include_once('../../../includes.php');

$avisoVendaInterna = '';
if(empty(Produto::getProdutosSessionVendaInterna()) && isset($_POST) && isset($_POST['btnEnviar'])){
  $avisoVendaInterna = '<span class="avisoVendaInterna">O carrinho está vazio, adicione itens no carrinho para gerar um pedido na venda interna!</span>';
  $produtos       = Produto::getProdutosAtivosEInativosArray();
  $selectProdutos = '';
  foreach ($produtos as $produto) {
    $selectProdutos .= '<option value="'.$produto['id'].'">'.$produto['nome'].'</option>';
  }
  $cep      = '';
  $numero   = '';
  $frete    = 0;
  $rua      = '';
  $bairro   = '';
  $uf       = '';
  $cidade   = '';
  $endereco = '';
  $carrinhoVendaInterna = [];
}else {
  // ação de salvar venda interna
  if(isset($_POST) && isset($_POST['btnEnviar'])){
    //verifica endereço
    if(!empty($_POST['logradouro']) && !empty($_POST['bairro']) && !empty($_POST['uf']) && !empty($_POST['localidade'])){
      // $cep        = '';
      $logradouro = $_POST['logradouro'].', n:'.$_POST['numero'].', '.$_POST['bairro'].', '.$_POST['localidade'].', '.$_POST['uf'].', ';
      $_SESSION['venda_interna']['frete']['endereco'] = $logradouro;
    }

    $idPedidoGerado = Pedido::gravarPedidoVendaInterna($_POST);

    //se opção salvar cliente estiver marcada na venda interna
    if(isset($_POST['cadastrarCliente']) && $_POST['cadastrarCliente'] == 'on'){

      //insere cliente no banco de dados
      Cliente::insertClienteVendaInterna(
        ['nome'    => $_POST['nomeCliente'],
        'endereco' => $_SESSION['venda_interna']['frete']['endereco'].' n:'.$_SESSION['venda_interna']['frete']['numero'],
        'contato'  => $_POST['whatsCliente']]);
      }

      //venda finalizada é redirecionado para página de pedidos
      if(isset($idPedidoGerado) && is_numeric($idPedidoGerado)){
        unset($_SESSION['venda_interna']);
        header('location:'.CAMINHO_DASHBOARD.'painel/pages/pedidos');
      }
    }

    $produtos       = Produto::getProdutosAtivosEInativosArray();
    $selectProdutos = '';
    foreach ($produtos as $produto) {
      $selectProdutos .= '<option value="'.$produto['id'].'">'.$produto['nome'].'</option>';
    }

    //itens da venda
    $carrinhoVendaInterna = isset($_SESSION['venda_interna']['carrinho']) ? $_SESSION['venda_interna']['carrinho'] : [];

    //endereço da venda
    $cep      = '';
    $numero   = isset($_SESSION['venda_interna']['frete']['numero'])   ? $_SESSION['venda_interna']['frete']['numero']   : '';
    $frete    = isset($_SESSION['venda_interna']['frete']['valor'])    ? $_SESSION['venda_interna']['frete']['valor']    : 0;
    $rua      = isset($_SESSION['venda_interna']['frete']['logradouro']) ? $_SESSION['venda_interna']['frete']['logradouro'] : '';
    $bairro   = isset($_SESSION['venda_interna']['frete']['bairro'])   ? $_SESSION['venda_interna']['frete']['bairro']   : '';
    $uf       = isset($_SESSION['venda_interna']['frete']['uf'])       ? $_SESSION['venda_interna']['frete']['uf']       : '';
    $cidade   = isset($_SESSION['venda_interna']['frete']['localidade']) ? $_SESSION['venda_interna']['frete']['localidade'] : '';

    if(isset($_SESSION['venda_interna']['frete']['endereco'])){
      $endereco = $_SESSION['venda_interna']['frete']['endereco'];
    }else {
      $endereco = $rua.' '.$numero.' '.$bairro.' '.$cidade.' '.$uf.' '.$cep;
    }
}

// recebe todas as formas de pagamento
$formasPagamento = FormaPagamento::getTodos(true);

?>

<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>
<?php include_once('pizza/sabores.php'); ?>
<?php include_once('adicionais/adicionais.php'); ?>
<?php include_once('adicionais/grupo-adicionais.php'); ?>

<body class="hold-transition sidebar-mini">

<input id='txtTipoProduto' style="display: none;">

<div class="wrapper">
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Venda Interna</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Venda Interna</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <?=$avisoVendaInterna?>

    <section class="content">
      <div class="row">
        <form style="display:contents;" method="post">
          <div class="col-md-9">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Realizar Venda Interna</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">

                    <!-- Produto Cadastrado -->
                    <div class="row" style="padding-bottom:10px;">
                      <div class="col-md-7" style="display:table;">
                        <label>Selecione os produtos</label>
                        <select class="js-example-basic-single js-states form-control select2" id="selectProdutos">
                          <?=$selectProdutos?>
                        </select>
                      </div>
                      <div class="col-md-2" style="display:table;">
                        <label>Qtd:</label>
                        <input type="number" class="form-control quantidadeVendaInterna" name="quantidade" min="1" value="1">
                      </div>
                      <div class="col-md-2">
                        <button type="button" class="btn btn-primary addVendaInterna" style="margin-top: 32px; width: 120px;">
                          Adicionar
                          <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>

                    <!-- Produto Custom -->
                    <button type="button" class="btn btn-primary" onclick="mostrarProdutoCustom(this)">Adicionar Custom</button>
                    <br><br>
                      <div id="dvProdutoCustom" style="display: none;">
                        <div class="row">
                          <div class="col-md-5">
                            <label>Nome Produto</label>
                            <input name="nomeProdutoCustom" class="form-control">
                          </div>
                          <div class="col-md-2">
                            <label>R$ Unid.</label>
                            <input name="valorProdutoCustom" class="form-control" type="number">
                          </div>
                          <div class="col-md-2">
                            <label>Qtd:</label>
                            <input name="quantidadeProdutoCustom" class="form-control" min="1" type="number">
                          </div>
                          <div class="col-md-1">
                            <button onclick="ocultarDivCustom()" type="button" class="btn" style="margin-top: 32px; background-color: #fe2b3e;">
                              <i class="fa fa-ban" style="color:white" aria-hidden="true"></i>
                            </button>
                          </div>
                          <div class="col-md-2">
                            <button id="addVendaInternaCustom" type="button" class="btn btn-primary" style="margin-top: 32px; width: 115px;">
                              Adicionar
                              <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                      </div>

                    <div class="form-group" style="margin-top: 20px;">
                      <label>Origem Venda</label>
                      <select name="origemVenda" style="width: 58%;" class="js-example-basic-single js-states form-control" id="origemVenda">
                      <?php
                        $origens = ['ifood' => 'Ifood','litoral-na-mesa' => 'Litoral na Mesa','sistema' => 'Sistema'];
                        $origem  = isset($_SESSION['venda_interna']['origemVenda']) ? $_SESSION['venda_interna']['origemVenda'] : '';
                        foreach ($origens as $key => $value) {
                          $selectedOrigem = $origem == $key ? 'selected' : '';
                          echo '<option '.$selectedOrigem.' value="'.$key.'">'.$value.'</option>';
                        }
                      ?>
                      </select>
                    </div>

                    <br><br>
                    <div>
                      <label>Endereço Cliente: </label><br>
                      <div class="containerEndereco" style="padding-top:5px;">
                        <!-- <div class="form-group row">
                          <label class="col-sm-2">CEP</label>
                          <div class="col-md-2">
                            <input class="form-control cepVendaInterna" type="text" name="cep" value="<?//=$cep?>">
                          </div>
                        </div> -->
                        <div class="form-group row">
                          <label class="col-sm-2">Rua/Av</label>
                          <div class="col-md-8 itemCep">
                            <input class="form-control ruaVendaIntera" required type="text" name="logradouro" value="<?=$rua?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2">Nº</label>
                          <div class="col-md-2">
                            <input class="form-control numeroVendaIntera" required type="text" name="numero" value="<?=$numero?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2">Bairro</label>
                          <div class="col-md-8 itemCep">
                            <input class="form-control bairroVendaIntera" required type="text" name="bairro" value="<?=$bairro?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2">UF</label>
                          <div class="col-md-2 itemCep">
                            <input class="form-control ufVendaIntera" required type="text" name="uf" value="<?=$uf?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2">Cidade</label>
                          <div class="col-md-8 itemCep">
                            <input class="form-control cidadeVendaIntera" required type="text" name="localidade" value="<?=$cidade?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row" style="padding-top:50px;">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-4">
                        <label for="whatsCliente">Whatsapp Cliente:</label>
                        <input required maxlength="17" type="text" name="whatsCliente" id="whatsCliente" class="form-control" placeholder="Whats cliente">
                      </div>
                      <div class="col-md-5">
                        <label for="nomeCliente">Nome Cliente:</label>
                        <input required type="text" name="nomeCliente" id="nomeCliente" class="form-control" placeholder="Nome cliente">
                      </div>
                      <div class="col-md-3">
                        <label for="trocoClinte">Método de Pagamento:</label><br>
                        <select id="slcFormaPagamento" name="pagamento" class="form-control">
                          <?php foreach($formasPagamento as $formaPagamento){ ?>
                            <option value="<?=$formaPagamento['nome']?>" data-troco="<?=$formaPagamento['troco']?>"><?=$formaPagamento['nome']?></option>
                          <?php } ?>
                        </select>
                        <div class="boxMetodoPagamento" style="padding-top:10px">
                          <label for="trocoClinte">Troco (opcional):</label>
                          <input type="number" min="0" value="0" name="trocoClinte" id="trocoClinte" class="form-control" placeholder="Troco">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div style="padding-top:20px;" class="form-check">
                  <input class="form-check-input" type="checkbox" val="sim" name="cadastrarCliente">
                  <label class="form-check-label"><b>CADASTRAR CLIENTE</b></label>
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Gerar Pedido" class="btn btn-success float-left">
            </div>

          </div>
          <div class="col-md-3">
            <div class="card card-primary containerCarrinho">
              <div class="card-header">
                <h3 class="card-title">Carrinho</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                <?php
                $total    = 0;
                $subTotal = 0;
                  echo '<b>Produtos:</b><br>
                  <span class="produtosCarrinhoVendaInterna">';
                  foreach ($carrinhoVendaInterna as $produtos){
                    foreach($produtos as $produto){
                      $subTotal += number_format(($produto['valor']  * $produto['quantidade']),2,'.','');

                      //monta string de adicionais
                      $listaAdicionais = '';
                      if(isset($produto['adicionais'])){
                        foreach($produto['adicionais'] as $adicionais){
                          $listaAdicionais .= $adicionais['nome'].', ';
                        }
                      }
                      if($listaAdicionais!=''){
                        $listaAdicionais = substr($listaAdicionais,0,-2);
                        $listaAdicionais = '<li><b>Adicionais:</b> '.$listaAdicionais.'</li>';
                      }

                      //monta string de sabores
                      $listaSabores = '';
                      if(isset($produto['sabores'])){
                        foreach($produto['sabores'] as $sabores){
                          $listaSabores .= $sabores['nome'].', ';
                        }
                        if($listaSabores!=''){
                          $listaSabores = substr($listaSabores,0,-2);
                          $listaSabores = '<li><b>Sabores:</b> '.$listaSabores.'</li>';
                        }
                      }

                      echo '<span style="font-size:15px;">
                              <ul>
                                <li><b>Nome:</b> '.$produto['nome'].'</li>'.
                                $listaAdicionais.
                                $listaSabores.
                                '<li><b>Quantidade:</b> '.$produto['quantidade'].'</li>
                                <li><b>Valor:</b>R$ '.number_format($produto['valor']*$produto['quantidade'],2,'.','.').'</small></li>
                              </ul>
                            </span>'.
                            '<div style="text-align:center">
                              <HR WIDTH=30% ALIGN=CENTER NOSHADE>
                            </div>';
                    }
                  }
                  echo '</span>';
                    $total = number_format(($subTotal + $frete),2,'.','');
                    $_SESSION['venda_interna']['valores']['total']    = $total;
                    $_SESSION['venda_interna']['valores']['subtotal'] = $subTotal;
                    $_SESSION['venda_interna']['valores']['frete']    = $frete;

                  echo '<br>
                    <b>Frete:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; R$ <span class="freteVendaInterna">'.$frete.'</span><br>
                    <b>Subtotal:</b> R$ <span class="subtotalCarrinho">'.$subTotal.'</span><br>
                    <b>Total:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; R$ <span class="totalCarrinho">'.$total.'</span><br><br>
                    <b>Endereço</b>: <span class="enderecoVendaInterna">'.$endereco.'</span>
                    nº: <span class="numeroCarrinho">'.$numero.'</span>';
                 ?>

              </div>
              <button type="button" name="button" class="limparVendaInterna">Limpar Venda Interna</button>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/pedidos" target="_blank"><button type="button" name="button" class="botoesCarrinhoVendaInterna">Pedidos</button></a>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/clientes" target="_blank"><button type="button" name="button" class="botoesCarrinhoVendaInterna">Clientes</button></a>
              <a href="<?=CAMINHO_DASHBOARD?>painel/pages/entregadores" target="_blank"><button type="button" name="button" class="botoesCarrinhoVendaInterna">Entregadores</button></a>
            </div>
          </div>

        </form>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php');?>
<script src="<?=CAMINHO_DASHBOARD.'js/venda-interna/adicionais.js?v='.strtotime("now")?>"></script>
<script src="<?=CAMINHO_DASHBOARD.'js/venda-interna/pizza.js?v='.strtotime("now")?>"></script>
<script src="<?=CAMINHO_DASHBOARD.'js/venda-interna/produtoCustom.js?v='.strtotime("now")?>"></script>
<script>

  // enter no input nao submit form
  $(document).ready(function() {
    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
  });

  //Initialize Select2 Elements
  $('.select2').select2({
    theme: 'bootstrap4'
  });

</script>
</body>
</html>
