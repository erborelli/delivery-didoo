<?php

include_once('../includes.php');

$idProduto    = $_POST['idProduto'];
$sabores      = '';
$saboresPizza = PizzaSabores::getVinculosSaboresPizzaPorIdProduto($idProduto);

foreach ($saboresPizza as $key => $value) {
  $vinculado  = is_numeric($value->id) ? 'checked' : '';
  $sabores   .= '<tr class="containerSelectSabores">
                  <td><input id-sabor="'.$value->idSabor.'" type="checkbox" '.$vinculado.'></td>
                  <td class="labelSaborProduto">'.$value->label.'</td>
                </tr>';
}

$retorno = '
<table class="table tabelaVinculoSaboresProduto" id-produto="'.$idProduto.'">
  <thead>
    <tr>
      <th style="width:50%;" scope="col">Vincular</th>
      <th style="width:50%" scope="col">Sabor</th>
    </tr>
  </thead>
  <tbody>
    '.$sabores.'
  </tbody>
</table>';

echo json_encode($retorno);
