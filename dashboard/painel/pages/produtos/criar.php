<?php
include_once('../../../includes.php');

// caso houver o post da ação da edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  $_POST['imagensProduto'] = [];
  if(!empty($_FILES) && isset($_FILES['imagensProduto'])){
    foreach ($_FILES['imagensProduto']['tmp_name'] as $key => $tmpName) {
      if($_FILES['imagensProduto']['error'][$key] == 0){
        $destinoArquivo = CAMINHO_UPLOAD . 'produtos/' . basename($_FILES['imagensProduto']['name'][$key]);
        move_uploaded_file($tmpName, $destinoArquivo);
        $_POST['imagensProduto'][] = $_FILES['imagensProduto']['name'][$key];
      }else {
        $_POST['imagensProduto'][] = '/default.png';
      }
    }
  }
  $_POST['imagensProduto'] = implode(',',$_POST['imagensProduto']);
  $idProduto = Produto::insertProduto($_POST);
  // validar que o produto foi adicionado
  $produto   = Produto::getProduto($idProduto);
  $produto   = $produto[0];
  if($produto->tipo == 'pizza'){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/editar.php?id='.$idProduto);
    exit;
  }
  // caso tenha retornado o objt do produto
  if($produto){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/');
  }
}

$categorias = Categoria::getCategorias();

?>


<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<style media="screen">
  .previewImgs{
    display: none;
    font-weight: bold;
  }
  .gallery img{
    margin-left: 10px;
  }
</style>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Criar Produto</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produto</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Criar Produto</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="tipo">Tipo Produto</label>
                  <select name="tipo" class="form-control custom-select selectTipoProduto">
                    <option value="padrao">Produto Normal (Perfume / Óculos / Brinco)</option>
                    <option value="pizza">Produto com Variação (Sapato / Pizza / Esfiha / Crepe)</option>
                  </select>
                </div>
                <div class="avisoTipoProduto"><b>Nota:</b> Somente é possível configurar os sabores e tamanhos da (Pizza / Esfiha), após salvar! Os campos serão exibidos nesta mesma tela, abaixo do campo "Categoria"</div>
                <div class="form-group">
                  <label for="nome">Nome Produto</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="">
                </div>
                <div class="form-group formPreco">
                  <label for="preco">Preço</label>
                  <input type="text" name="preco" id="preco" value="" class="form-control"></input>
                </div>
                <div class="form-group">
                  <label for="categoria">Categoria</label>
                  <select name="categoria" class="form-control custom-select">
                    <option selected="" disabled="">Selecione Categoria</option>
                    <?php
                      foreach ($categorias as $key => $value) {
                        $selected = $value->id == $produto->id_categoria ? 'selected' : '';
                        echo '<option '.$selected.' value="'.$value->id.'">'.$value->nome.'</option>';
                      }
                     ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="descricao">Descrição</label>
                  <textarea name="descricao" id="descricao" class="form-control" rows="4"></textarea>
                </div>

                <div class="form-group">
                  <span class="previewImgs">Preview das Imagens Selecionadas:</span>
                  <div class="gallery"></div>
                  <label for="imagensProduto">Foto Produto (300x300)</label><br>
                  <!-- <input type="image" id="inputEstimatedBudget" class="form-control"> -->
                  <input type="file" name="imagensProduto[]" id="imagensProduto" multiple>
                  <!-- <input type="submit" value="Upload Image" name="submit"> -->
                </div>

              </div>

              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>

          </div>

        </form>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script type="text/javascript">
  $('#preco').mask("#.##0.00", {reverse: true});

  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            if(filesAmount > 3){
              alert('Você pode realizar no máximo upload de 3 imagens!');
              return false;
            }
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<img width="100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#imagensProduto').on('change', function() {
        imagesPreview(this, 'div.gallery');
        $('.previewImgs').show();
    });
  });
</script>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
