<!-- Modal -->
<div id="modalCarrinho" class="modal w920 fade modalCarrinho" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog w920" role="document" data-cy="modal-dialog" style="width: 100%;">
    <div class="modal-content w920" style="top: 0px;">
      <div class="modal-close-content">
        <i class="material-icons close-button js-modal-close" data-ga="modal-close" data-cy="modal-dialog__close">close</i>
      </div>
      <div class="modal-header" style="padding: 25px 30px 20px;">
        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif; font-size: 24px;">Informe seu endereço</h4>
      </div>
      <div class="modal-header-mobile" style="">
        <button class="modal-header-content-back js-modal-close">
          <i class="material-icons icon">keyboard_arrow_left</i>
          <span class="text"> Voltar</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-secondary" style="display:none" data-dismiss="modal">Close</button>
        <div class="checkout-mobile js-main-contents-checkout-mobile">
          <div class="checkout-mobile__sections width-100p">
            <div class="js-mobile-step-1">
              <div class="js-checkout-mobile-section checkout-mobile-section">
                <div class="checkout__title">
                  <h2 class="checkout__title-text" style="padding: 0 15px;">
                    <i class="material-icons">shopping_cart</i> Meu carrinho
                  </h2>
                </div>
                <div class="row checkout-details">
                  <div class="mdl-card mdl-card--full-width mdl-card--auto-height col-xs-12">

                    <div class="checkout-details__head checkout-separator-bottom">

                      <div class="row checkout-details__title">
                        <div class="col-xs-7">
                          Produto </div>
                        <div class="col-xs-2 text-right">
                          Qtde. </div>
                        <div class="col-xs-3 text-right">
                          Preço </div>
                      </div>
                    </div>

                    <div class="checkout-details__body">

                      <div class="checkout-details__body--content js-checkout-details-content">
                        <div class="row">
                          <div class="col-xs-12">

                            <div class="row containerItensCarrinhoMobile">
                              <?php
                              if(!$carrinhoVazio){
                                // itens carrinho
                                foreach ($_SESSION['carrinho']['itens'] as $key => $value) {
                                  $produto        = unserialize($value);
                                  $total          = $produto->valor *  $produto->quantidade;
                                  $total          = number_format($total,2);
                                  $produto->valor = number_format($produto->valor,2);
                                  echo '
                                  <ul class="col-xs-12 checkout-details-items-list checkout-separator-bottom">
                                    <li class="checkout-details-items-list-item checkout-details-items-list-item-'.$produto->id.'" data-id="'.$produto->id.'" data-status=""
                                      data-numeric_price="'.$produto->valor.'" data-price="0,00" data-view_order="0" data-ref="items-list-item-628149" data-base_price="0" data-cover_photo="">

                                      <div class="item-checkout js-item row" data-id-sess="'.$produto->idSessaoCarrinho.'">
                                        <div class="col-xs-7 js-edit-checkout-details-items-list" data-id="'.$produto->id.'" data-status=""
                                          data-numeric_price="'.$produto->valor.'" data-price="0,00" data-view_order="0" data-ref="items-list-item-628149" data-base_price="0" data-cover_photo="">
                                          <div class="item-checkout-name-box">
                                            <span class="title">'.$produto->nome.'</span><br>
                                            '.$boxSabores.'
                                            '.$boxAdicionais.'
                                            '.$labelObservacao.'
                                          </div>
                                        </div>
                                        <div class="col-xs-2">
                                          <div class="item-checkout-amount text-align-right" data-id="'.$produto->id.'" data-price="0" data-properties_price="'.$produto->valor.'">
                                            '.$produto->quantidade.'
                                          </div>
                                        </div>
                                        <div class="col-xs-3">
                                          <div class="item-checkout-price js-price text-align-right" data-price="0">
                                            R$'.$produto->valor.'
                                            <i class="material-icons item-checkout-delete js-delete-item" data-id="'.$produto->id.'">highlight_off</i>
                                            <i class="input-amount" data-id="'.$produto->valor.'"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>';
                                }
                              }
                              ?>

                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12">


                            <div class="checkout-info checkout-mobile-section-info js-checkout-mobile-section js-cart-info checkout-mobile-section">

                              <div class="subtotal cupomAplicado <?=$classeCupomAplicado?>">
                                <span class="text">Cupom aplicado</span>
                                <span class="value labelCupomAplicado float-right js-value"><?=$labelCupomAplicado?></span>
                              </div>

                              <div class="sub-total">
                                <label class="text">Subtotal <span class="value float-right js-value js-subtotal-value" data-value="1">R$<?=$_SESSION['carrinho']['subtotal']?></span>
                                </label>
                              </div>

                              <div class="discount js-discount is-hidden">
                                <label class="text">Desconto <span class="js-discount-text"></span>
                                  <span class="value float-right js-value" data-value="0"></span>
                                </label>
                              </div>

                              <div class="delivery-fee js-delivery-fee">
                                <label class="text">Taxa de entrega <span class="value float-right js-value js-delivery-fee-value" data-value="R$<?=$_SESSION['carrinho']['frete']?>">R$<?=$_SESSION['carrinho']['frete']?></span>
                                </label>
                              </div>

                              <div class="total">
                                <label class="text">Total <span class="value float-right js-value js-total-value" data-value="<?=$_SESSION['carrinho']['total']?>">R$<?=$_SESSION['carrinho']['total']?></span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="branco card-content box-fechamento">

                <!-- BOTAO FECHAR PEDIDO -->
                <a data-target="#modal-pedido" class="btn fechar-pedido modal-trigger">Fechar Pedido</a>

                <!-- BOTAO CUPOM -->
                <button type="submit" class="btn btn-cupom roxo">Aplicar Cupom</button>
                <div class="input-field is-hidden campoCupom">
                  <input placeholder="Código do Cupom" type="text" class="codigo_cupom mobile roxo-texto" name="codigo_cupom" value="">
                  <button class="aplicarCupomDesconto">aplicar</button>
                </div>

                <!-- BOTAO AGENDAR PEDIDO -->
                <button data-target="#modalAgendarEntrega" data-toggle="modal" class="botaoAgendaEntregaModalMobile mdl-button mdl-js-button mdl-js-ripple-effect js-schedule-order width-100p text-align-left text-muted" data-upgraded=",MaterialButton,MaterialRipple">
                  <i class="material-icons action-color">event</i>
                  <span class="text"><?=$labelAgendamento?></span>
                  <span class="mdl-button__ripple-container">
                    <span class="mdl-ripple is-animating" style="width: 856.24px; height: 856.24px; transform: translate(-50%, -50%) translate(162px, 26px);"></span>
                  </span>
                </button>

                <?php if($mostrarValorMinimo){ ?>
                  <button class="btn btnPedidoMinimoMobile" style="background-color: rgb(153, 153, 153);">
                    <span class="text js-go-to-checkout-text" style="color: white;">Pedido mínimo de R$<?=$valorMinimoPedido?></span>
                  </button>
                <?php } ?>

                <div class="row">
                  <div class="col s12">
                    <p class="center-align">Pague na entrega com dinheiro</p>
                    <p class="center-align"><strong>OU</strong></p>
                    <img src="<?=CAMINHO?>/img/home/bandeiras.png" class="responsive-img center-block imgBandeirasCartoes">
                  </div>
                </div>

              </div>

              <!-- boc cupom / finalizar -->

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer is-hidden"></div>
  </div>
</div>
<!-- Fim Modal -->
