<?php

include_once('../includes.php');

if(!isset($_POST) || empty($_POST)) exit;

$idProduto     = $_POST['idProduto'];
$imagemExcluir = $_POST['imgProduto'];

$link = getConnection();
$stmt = $link->prepare('SELECT foto_produto FROM produto WHERE id = '.$idProduto);
$stmt->execute();
$imagensProduto = $stmt->fetch(PDO::FETCH_OBJ)->foto_produto;

// explode o que retornou do banco
$arrayImagensProduto = explode(',',$imagensProduto);
// dá um flip pra procurar pela posição do valor da imagem que ele quer excluir
$arrayAoContrario    = array_flip($arrayImagensProduto);
// dá o unset
unset($arrayAoContrario[$imagemExcluir]);
// desflipa o array para salvar novamente no banco sem a imagem que foi excluida
$arrayImagensProduto = array_flip($arrayAoContrario);

// dá o implode para salvar no banco novamente
$arrayImagensProduto = implode(',',$arrayImagensProduto);

// Sql de update no banco
$sqlUpdate  = "UPDATE produto SET foto_produto=? WHERE id=?";
$stmt       = $link->prepare($sqlUpdate);
$stmt->execute([$arrayImagensProduto, $idProduto]);

// remove a imagem do diretorio
unlink(CAMINHO_UPLOAD.'produtos/'.$imagemExcluir);
