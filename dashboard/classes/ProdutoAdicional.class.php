<?php

class ProdutoAdicional{

  public static function excluirVinculorPorIdProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql = "DELETE FROM produto_adicional WHERE id_produto=?";
    $stmt= $link->prepare($sql);
    return $stmt->execute([$idProduto]);
  }
  
  public static function excluirVinculorAdicionalPorIdProduto($idProduto,$idAdicional){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql = "DELETE FROM produto_adicional WHERE id_produto=? AND id_adicional=?";
    $stmt= $link->prepare($sql);
    return $stmt->execute([$idProduto,$idAdicional]);
  }

  public static function excluirVinculorAdicionalGrupoPorIdProduto($idProduto,$idAdicionalGrupo){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql = "DELETE FROM produto_adicional WHERE id_produto=? AND id_adicional_grupo=?";
    $stmt= $link->prepare($sql);
    return $stmt->execute([$idProduto,$idAdicionalGrupo]);
  }

  /**
   * vincula 'adicional' a 'produto'
   */
  public static function vincularAdicionalProduto($idProduto,$idAdicional){
    if(!is_numeric($idProduto) || empty($idAdicional)) return false;

    $sql  = "INSERT INTO produto_adicional(id_produto,id_adicional) VALUES(?,?)";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto,$idAdicional]); 
  }

  /**
   * vincula 'adicional grupo' a 'produto'
   */
  public static function vincularAdicionalGrupoAoProduto($idProduto,$idAdicionalGrupo){
    if(!is_numeric($idProduto) || empty($idAdicionalGrupo)) return false;
    
    $sql  = "INSERT INTO produto_adicional(id_produto,id_adicional_grupo) VALUES(?,?)";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto,$idAdicionalGrupo]);
  }

  public static function vincularAdicionaisProduto($idProduto,$dadosVinculo){
    if(!is_numeric($idProduto) || empty($dadosVinculo)) return false;
    $link = getConnection();
    foreach ($dadosVinculo as $key => $value) {
      // CASO FOR MARCADO TRUE NO CHECKED PARA VINCULAR
      if($value == 'true'){
        $sql  = "INSERT INTO produto_adicional (id_produto,id_adicional) VALUES (?,?)";
        $stmt = $link->prepare($sql);
        $stmt->execute([$idProduto,$key]);
      }
    }
  }

  /**
   * verifica se existe vinculo entre 'produto' e 'adicional' / 'adicional grupo'
   */
  public static function verificarVinculo($idProduto,$idAdicional,$idAdicionalGrupo=0){
    $sql = "SELECT * FROM produto_adicional WHERE id_produto=? AND ";
    $id = 0;

    // adicional
    if($idAdicionalGrupo==0){
      $sql .= "id_adicional=?";
      $id   = $idAdicional;
    }
    
    // adicional grupo
    else{
      $sql .= "id_adicional_grupo=?";
      $id   = $idAdicionalGrupo;
    }

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto,$id]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
