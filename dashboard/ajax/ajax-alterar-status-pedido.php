<?php

include_once('../includes.php');

if(!isset($_POST['idPedido'])   || !is_numeric($_POST['idPedido'])    ||
   !isset($_POST['novoStatus']) || !is_numeric($_POST['novoStatus'])) return false;

Pedido::alterarStatusPedido($_POST['idPedido'], $_POST['novoStatus']);
