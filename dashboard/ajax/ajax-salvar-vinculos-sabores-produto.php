<?php

include_once('../includes.php');

$dados     = array_filter($_POST['dadosVinculo']);
$idProduto = $_POST['idProduto'];

// excluir os vinculos para deopis passar
ProdutoPizzaSabores::excluirVinculosPorIdProduto($idProduto);

// realiza os vinculos
ProdutoPizzaSabores::vincularSaboresProduto($idProduto,$dados);
