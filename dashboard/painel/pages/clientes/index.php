<?php
include_once('../../../includes.php');

$clientes = Cliente::getClientes();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Clientes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Clientes</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal cupons utilizados -->
    <div class="modal fade" id="modalCuponsUtilizados" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cupons Utilizados</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <section class="content" id="containerClientes">
      <div class="">
        <a class="btn btn-primary btn-sm" style="margin: 15px 0px;" href="<?=CAMINHO_DASHBOARD?>painel/pages/clientes/criar.php">
          <i class="fas fa-plus"></i>Novo Cliente
        </a>
        <div class="col-sm-4 text-right" style="right:20px; float: right; margin-top: 14px;">
          <div class="imprimirConteudoClientes" style="cursor:pointer;width:100px;display:inline;">
            <i class="fas fa-print">&nbsp;&nbsp;</i>Imprimir
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Clientes</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 20%">
                          Nome
                      </th>
                      <th style="width: 25%">
                          Endereço
                      </th>
                      <th style="width: 20%">
                          Contato
                      </th>
                      <th style="width: 20%">
                          Cupons
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <?php

                    foreach ($clientes as $key => $value) {

                      echo '<tr>
                              <td>'.$value->nome.'</td>
                              <td>'.$value->endereco.'</td>
                              <td class="project-state text-left">
                                <span class="">'.$value->contato.'</span>
                              </td>
                              <td class="project-state text-left">
                                <span data-toggle="modal" data-target="#modalCuponsUtilizados" class="cuponsUtilizados" data-id-cliente="'.$value->id.'" class="">Cupons Utilizados</span>
                              </td>
                              <td class="project-actions text-right">
                                <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'/painel/pages/clientes/editar.php?id='.$value->id.'">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Editar
                                </a>
                                <a data-id="'.$value->id.'" data-modulo="cliente" class="btn btn-danger btn-sm" href="#">
                                    <i class="fas fa-trash">
                                    </i>
                                    Excluir
                                </a>
                              </td>
                          </tr>';
                      }
                    ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/clientes/criar.php">
          <i class="fas fa-plus"></i>Novo Cliente
      </a>
    </section>
  </div>
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
