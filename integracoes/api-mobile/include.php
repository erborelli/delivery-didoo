<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../../db.php');
require_once('autenticacao.php');

spl_autoload_register(function ($class_name) {
    require_once('../../../dashboard/classes/'.$class_name.'.class.php');
});

$_DELETE = [];
$_PUT =    [];

if(!strcasecmp($_SERVER['REQUEST_METHOD'], 'DELETE')){
    parse_str(file_get_contents('php://input'), $_DELETE);
}

if($_SERVER['REQUEST_METHOD'] == 'PUT'){
    parse_str(file_get_contents('php://input'), $_PUT);
}