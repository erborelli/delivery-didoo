<?php

  $visibilidadePedidoAceitar = '';
  if(ACEITARPEDIDOSAUTOMATICO == 's'){
    $visibilidadePedidoAceitar = 'd-none';
  }

 ?>

<head>

  <script>
      var CAMINHO           = '<?=CAMINHO?>';
      var CAMINHO_DASHBOARD = '<?=CAMINHO_DASHBOARD?>';
  </script>

  <?php
    // numero aleatorio get script
    $getScript = strtotime("now");
  ?>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Painel | <?=NOMELOJA?></title>
  <link rel="icon" type="image/png" href="<?=CAMINHO_IMAGEM?>/favicon/<?=FAVICON?>" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/plugins/fontawesome-free/css/all.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/plugins/toastr/toastr.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/dist/css/adminlte.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- estilos custom -->
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>css/custom.css?<?=$getScript?>">
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=CAMINHO_DASHBOARD?>painel/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
  <link rel="stylesheet" href="<?=CAMINHO?>/css/sweetalert.min.css"/>

  <script src="<?=CAMINHO_DASHBOARD?>painel/plugins/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
  <script src="<?=CAMINHO_DASHBOARD?>js/custom.js?<?=$getScript?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- Toastr -->
  <script src="<?=CAMINHO_DASHBOARD?>painel/plugins/toastr/toastr.min.js"></script>
</head>

<button type="button" class="btn d-none btn-primary abrirModalAvisoNovoPedido" data-toggle="modal" data-target="#modalAvisoNovoPedido"></button>
<div id="modalAvisoNovoPedido" class="modal" tabindex="-1" role="dialog" data-hora-abertura="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">HÁ <span class="qtdPedidosPendentes"></span> NOVO(S) PEDIDO(S)!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body conteudoModalPedido">
        <div class="qtdPedidos"></div>
        <div class="layoutPedidos"></div>
      </div>
      <!-- <div class="modal-footer text-center d-block">
        <button type="button" class="btn botaoAceitarTodosPedidos ">Aceitar todos</button>
        <button type="button" class="btn botaoNegarTodosPedidos ">Negar todos</button>
      </div> -->
    </div>
  </div>
</div>
