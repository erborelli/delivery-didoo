<?php

include_once('../includes.php');

$retorno = ['sucesso' => false,'label' => ''];

if(isset($_POST) && isset($_POST['horario']) && isset($_POST['dataAgendamento'])){
  // adiciona na sessao o agendamento
  $dataFormatada = $_POST['dataAgendamento'];
  $date = date_create(str_replace('/','-',$_POST['dataAgendamento']));
  $_POST['dataAgendamento'] = date_format($date,"Y/m/d");
  $_SESSION['carrinho']['agendamento'] = $_POST;
  $retorno['label']   = $dataFormatada.' ÀS '.$_POST['horario'];
  $retorno['sucesso'] = true;
}

echo json_encode($retorno);
