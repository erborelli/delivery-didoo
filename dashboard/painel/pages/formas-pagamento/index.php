<?php
  include_once('../../../includes.php');

  if(isset($_POST) && !empty($_POST)){
    FormaPagamento::gravar($_POST);
  }
  $formasPagamento = FormaPagamento::getTodos();
?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Formas de Pagamento</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Formas de Pagamento</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
      <div class="col-md-12">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Novo Cadastro</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <input type="hidden" name="id" id="id" class="form-control" value="">
            <div class="form-group col-md-4" style="float:left;">
              <label for="nome">Nome</label>
              <input type="text" name="nome" id="nome" class="form-control" value="">
            </div>
            <div class="form-group col-md-4" style="float:left;">
              <label for="valor">Pedir Troco?</label>
              <select name="troco" class="form-control">
                <option value="n">Não</option>
                <option value="s">Sim</option>
              </select>
            </div>
            <div class="form-group col-md-4" style="float:left;top:20px;">
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </div>
      </div>
    </form>

    <section class="content">
      <div class="card card-secondary">
        <div class="card-header">
          <h3 class="card-title">Listagem</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%">
                          #
                      </th>
                      <th style="width: 35%">
                          Nome
                      </th>
                      <th style="width: 35%">
                          Pedir Troco
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <?php foreach ($formasPagamento as $formaPagamento) { ?>
                    <tr>
                      <td>#</td>
                      <td><a><?=$formaPagamento->nome?></a><br></td>
                      <td><a><?=($formaPagamento->troco == 's' ? 'Sim' : 'Não')?></a><br></td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm botaoEditarFrete" href="<?=CAMINHO_DASHBOARD.'/painel/pages/formas-pagamento/editar.php?id='.$formaPagamento->id?>">
                              <i class="fas fa-pencil-alt"></i>Editar
                          </a>
                          <a data-id="<?=$formaPagamento->id?>" data-modulo="forma_pagamento" class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"></i>Excluir
                          </a>
                      </td>
                    </tr>
                  <?php } ?>
              </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
