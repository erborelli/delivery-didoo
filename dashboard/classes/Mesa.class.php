<?php

class Mesa{
    public static function getMesas(){
        $sql  = "SELECT *,DATE_FORMAT(data_criacao,'%d/%m/%Y %H:%i:%s') as data FROM mesa;";
        $link = getConnection();
        $stmt = $link->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function gravarMesa($mesa){
        if(!isset($mesa) || empty($mesa)) return false;
        $sql = "INSERT INTO mesa(numero,descricao) VALUES(?,?)";
        $link = getConnection();
        $stmt= $link->prepare($sql);
        $stmt->execute([$mesa['numero'],$mesa['descricao']]);
        return true;
    }

    public static function getMesaPorId($idMesa){
        $sql  = "SELECT * FROM mesa WHERE id = ?";
        $link = getConnection();
        $stmt = $link->prepare($sql);
        $stmt->execute([$idMesa]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function updateMesa($mesa){
        if(!isset($mesa) || empty($mesa)) return false;
        $sql = "UPDATE mesa SET numero=?, descricao=? WHERE id=?";
        $link = getConnection();
        $stmt= $link->prepare($sql);
        $stmt->execute([$mesa['numero'],$mesa['descricao'],$mesa['id']]);
        return true;
    }
}