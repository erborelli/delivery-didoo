<?php
$nomeCliente    = '';
$whatsCliente   = '';

if(isset($_SESSION['cliente']) && !empty($_SESSION['cliente'])){
  $nomeCliente    = $_SESSION['cliente']['nome'];
  $whatsCliente   = $_SESSION['cliente']['contato'];
  $formaPagamento = $_SESSION['cliente']['formaPagamento'];
}

$formasPagamento = FormaPagamento::getTodos(true);
?>

<div id="modal-pedido" class="w920 fade modal open" tabindex="0" style="z-index: 99999; display: none;">
  <div class="modal-content">
    <div class="modalFinalizarPedido box-address js-step-2" data-cy="address-modal__step-2">

      <form method="post" onsubmit="finalizarCompra();return false">

        <div class="col-6 mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded" data-upgraded=",MaterialTextfield">
          <label class="address-second-step-label" for="nomeCompleto">Nome Completo</label>
          <input value="<?=$nomeCliente?>" class="mdl-textfield__input address-modal-input" required type="text" id="nomeCompleto" autocomplete="true" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
          <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded" data-upgraded=",MaterialTextfield">
          <label class="address-second-step-label" for="contato">Telefone / Whatsapp</label>
          <input value="<?=$whatsCliente?>" maxlength="17" class="mdl-textfield__input address-modal-input" required type="tel" name="telefone" id="telefone" autocomplete="false" data-next="confirmed-reference" data-cy="address-modal__input--confirmed-complement">
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded" data-upgraded=",MaterialTextfield">
          <div class="form-group row">
            <?php foreach ($formasPagamento as $key => $value){ ?>
              <div class="col-md-2">
                <input id="<?=$key.'Input'?>" class="containerPagamentos" name="forma_pagamento" type="radio" data-troco="<?=$value['troco']?>" value="<?=$value['nome']?>" <?=$value['troco']=='s'?'checked="true"':''?>>
                <label for="<?=$key.'Input'?>"><?=$value['nome']?></label>
              </div>
            <?php } ?>
          </div>
          <br>
          <label class="opcaoTroco">
            <input type="radio" name="troco" value="sim">
            <span class="opcaoTrocoSpan">Troco?</span>
          </label>
          <input placeholder="Troco pra quanto?" type="tel" class="is-hidden trocoQuanto mdl-textfield__input address-modal-input" name="valorTroco" id="valorTroco" autocomplete="false">
        </div>

        <div class="actions">
          <button type="button" data-toggle="modal" data-target="#modal-pedido" id="botaoFecharModalFinalizarPedido" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-takeout js-reset" data-ga-action="address__modal_back" data-ga-category="address" data-upgraded=",MaterialButton,MaterialRipple">
            <span class="value">Voltar</span>
            <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>
          </button>
          <button id="address-submit-btn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit" data-ga-action="address__submit_details" data-ga-category="address"
          data-cy="address-modal__button--submit" data-upgraded=",MaterialButton,MaterialRipple">
            <span class="value">Confirmar</span>
            <span class="mdl-button__ripple-container finalizarCompraBotao">
              <span class="mdl-ripple"></span>
            </span>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
