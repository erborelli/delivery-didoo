<?php

include_once('../includes.php');

if(!isset($_GET['tipo'])) return false;

$dados = [];
if($_GET['tipo']=='produtos'){
    $dados = Produto::getProdutosArrayPorNomeVerificaAdicional($_GET['idAdicional'],$_GET['tipoAdicional'],$_GET['busca']);
}else if($_GET['tipo']=='categorias'){
    $dados = Categoria::getCategoriasArrayPorNomeVerificaAdicional($_GET['idAdicional'],$_GET['tipoAdicional'],$_GET['busca']);
}

else if($_GET['tipo']=='adicionais'){
    $dados = Adicional::getAdicionais(true);
}else if($_GET['tipo']=='gruposAdicionais'){
    $dados = AdicionalGrupo::getGrupos(true);
}

echo json_encode($dados);