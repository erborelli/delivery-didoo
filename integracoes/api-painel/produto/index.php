<?php

require_once('../include.php');

//deleta registro
if(count($_DELETE)){
    echo('delete');
    http_response_code(200);
    return;
}

//atualiza registro
if(count($_PUT)){
    $produto = json_decode($_PUT);
    Produto::updateProduto($produto);
    http_response_code(200);
    return;
}

//insere registro
if(count($_POST)){
    $produto = json_decode($_POST);
    Produto::insertProduto($produto);
    http_response_code(200);
    return;
}

//retorna registro por id
if(isset($_GET['id'])){
    $produto = Produto::getProduto($_GET['id']);
    echo(json_encode($produto,true));
    http_response_code(200);
    return;
}

//retorna todos os registro ativos
$produtos = Produto::getListagemProdutos();
echo(json_encode($produtos,true));
http_response_code(200);
return;