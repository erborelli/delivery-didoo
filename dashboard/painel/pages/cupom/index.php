<?php
include_once('../../../includes.php');

$cupons = Cupom::getCupons();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Cupom</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cupom</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Modal -->
    <div class="modal fade" id="modalQuemUsouCupom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div style="max-width:700px;" class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Clientes que utilizaram:</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/cupom/criar.php">
        <i class="fas fa-plus"></i> Novo Cupom
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Cupons</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 15%">
                          Nome
                      </th>
                      <th style="width: 10%">
                          Hash
                      </th>
                      <th style="width: 10%">
                          Desconto
                      </th>
                      <th style="width: 15%">
                          Data Criação
                      </th>
                      <th style="width: 13%">
                          Qtd disponível
                      </th>
                      <th style="width: 13%">
                          Qtd utilizada
                      </th>
                      <th style="width: 10%">
                          Utilizações
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <?php

                    foreach ($cupons as $key => $value) {

                      echo '<tr>
                              <td>'.$value->nome_cupom.'</td>
                              <td>'.$value->hash_cupom.'</td>
                              <td>'.number_format($value->valor_desconto).'%</td>
                              <td>'.$value->data_criacao.'</td>
                              <td>'.$value->qtd_disponivel.'</td>
                              <td>'.$value->qtd_utilizado.'</td>
                              <td data-id-cupom="'.$value->hash_cupom.'" data-toggle="modal" data-target="#modalQuemUsouCupom" class="verQuemUsou">Quem usou</td>
                              <td class="project-actions text-right">
                                  <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'/painel/pages/cupom/editar.php?id='.$value->id.'">
                                      <i class="fas fa-pencil-alt"></i>Editar
                                  </a>
                                  <a data-id="'.$value->id.'" data-modulo="cupom" class="btn btn-danger btn-sm" href="#">
                                      <i class="fas fa-trash"></i>Excluir
                                  </a>
                              </td>
                          </tr>';
                      }
                    ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/cupom/criar.php">
        <i class="fas fa-plus"></i> Novo Cupom
      </a>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
