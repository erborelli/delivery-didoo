<?php

require_once('../include.php');

//deleta registro
if(count($_DELETE)){
    echo('delete');
    http_response_code(200);
    return;
}

//atualiza registro
if(count($_PUT)){
    $pedido = json_decode($_PUT);
    //Pedido::update
    http_response_code(200);
    return;
}

//insere registro
if(count($_POST)){
    $pedido = json_decode($_POST);
    //Pedido::gravarPedido($pedido);
    http_response_code(200);
    return;
}

//retorna registro por id
if(isset($_GET['id'])){
    $pedido = Pedido::getDadosPedido($_GET['id']);
    echo(json_encode($pedido,true));
    http_response_code(200);
    return;
}

//retorna todos os registro ativos
$pedidos = Pedido::getPedidos();
echo(json_encode($pedidos,true));
http_response_code(200);
return;