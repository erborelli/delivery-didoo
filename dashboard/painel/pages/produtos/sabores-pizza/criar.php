<?php
include_once('../../../../includes.php');

$erro = false;

if(isset($_POST) && isset($_POST['btnEnviar'])){
  // grava o sabor da pizza retornando o id gerado na gravação
  $idSaborPizza = PizzaSabores::gravarSaborPizza($_POST);
  if(is_numeric($idSaborPizza) && $idSaborPizza > 0 && isset($_POST['precoTamanho']) && !empty($_POST['precoTamanho'])){
    PizzaSabores::gravarVinculosPizzaTamanhoSaboresValores($_POST['precoTamanho'],$idSaborPizza);
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/sabores-pizza/');
  }
}

$tamanhosPizza = PizzaTamanhos::getTamanhosPizza();
$qtdTamanhos   = count($tamanhosPizza);

// caso for vazio os tamanhos de pizzas cadastrados
if($qtdTamanhos == 0) {
  $erro  = true;
  $aviso = '<div class="avisoErroModulo">
              <h3><b>Atenção:</b> É necessário ter <b>tamanhos de pizzas</b> cadastrados para continuar!</h3>
              <i class="fas fa-arrow-down"></i>
              <a href="'.CAMINHO_DASHBOARD.'painel/pages/produtos/tamanhos-pizza">link para cadastrar tamanhos</a>
            </div>';
}else {
  $classeColunas = 12 / $qtdTamanhos;
  $boxValoresPorTamanho = '';
  foreach ($tamanhosPizza as $key => $value) {
    $boxValoresPorTamanho .= '
    <div class="form-group col-md-'.$classeColunas.'">
    <label for="label">Tamanho: '.$value->label.'</label><br>
    <input placeholder="preço" type="text" name="precoTamanho['.$value->id.']" id="label" class="form-control" value="">
    </div>';

  }
}

?>

<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Criar Sabor (Pizza / Esfiha)</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produto</li>
              <li class="breadcrumb-item active">Sabor (Pizza / Esfiha)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Criar Sabor (Pizza / Esfiha)</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <?php if($erro){
                echo $aviso;
              }else{ ?>
                <div class="card-body">
                  <div class="form-group">
                    <label for="label">Label Sabor</label>
                    <input required type="text" name="label" id="label" class="form-control" value="">
                  </div>
                </div>
                <div class="card-body">
                  <label for="descricao">Descrição</label>
                  <textarea name="descricao" id="descricao" class="form-control" rows="4"></textarea>
                </div>
                <div class="card-body d-flex">
                  <?=$boxValoresPorTamanho?>
                </div>
                <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
              <?php } ?>
            </div>

          </div>

        </form>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include_once('../../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
