<?php

class Etiqueta {

  /**
   * Recebe um objeto do pedido e retorna a etiqueta formatada
   */
  public static function montarEtiqueta($obPedido,$alerta = 'alerta'){
    $retiradaLocal      = '';
    $agendamentoEntrega = '';
    // layout agendamento
    if($obPedido->agendamento == 's'){
      $dataAgendado = date_format(date_create($obPedido->data_agendamento),'d/m/Y H:i:s');
      $agendamentoEntrega  = Sistema::getLayout(['data_agendamento' => $dataAgendado],'dashboard/layout/etiqueta','agendamento-entrega.html');;
    }
    if($obPedido->retirada == 's') $retiradaLocal = '(Retirada no Local)';
    $obPedido->agendamentoEntrega = $agendamentoEntrega;
    $obPedido->endereco           = $retiradaLocal.' '.$obPedido->endereco;
    $obPedido->data_pedido        = date('d/m/Y H:i:s',strtotime($obPedido->data_pedido));
    $obPedido->troco              = $obPedido->troco == 0 ? 'Não' : 'R$'.$obPedido->troco;
    $obPedido->imgEtiqueta        = MOSTRARLOGOETIQUETA == 's' ? '<span><img src="'.CAMINHO.'dashboard/images/logoDashboard/'.LOGODASHBOARD.'" width="150"></span>' : '<span class="nomeEstabelecimento">'.NOMELOJA.'</span>';
    // itens (produtos) pedido
    $obPedido->itensPedido     = Pedido::getLayoutItensEtiqueta($obPedido->id);
    // caso for utilizado algum cupom
    $obPedido->cupomUtilizado = '';
    $obPedido->descontoCupom  = '';
    if($obPedido->cupom_utilizado){
      $obPedido->cupomUtilizado = '<div class="txtEtiqueta"><b>Cupom Utilizado: </b><span class="cupomUtilizado">'.$obPedido->cupom_utilizado.'</span></div>';
      $obPedido->descontoCupom  = '<div class="txtEtiqueta"><b>Desconto Cupom: </b>R$ <span class="descontoCupom">'.$obPedido->desconto_cupom.'</span></div>';
    }
    $obPedido->pontoReferencia = '';
    $obPedido->alerta          = $alerta;
    if(isset($obPedido->ponto_referencia) && !empty($obPedido->ponto_referencia)){
      $obPedido->pontoReferencia = '<div class="txtEtiqueta"><b>Ponto Referência:</b> <span class="endCliPedido">'.$obPedido->ponto_referencia.'</span></div>';
    }
    return Sistema::getLayout((array)$obPedido,'dashboard/painel/pages/etiqueta','layout-etiqueta.html');
  }

}
