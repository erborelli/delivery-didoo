<?php

$mostrarMenuPizza = true;
$estruturaProduto = '';
if($mostrarMenuPizza){

  $estruturaProduto = [
                        'link' => CAMINHO_DASHBOARD.'painel/pages/produtos',
                        'classIcon'          => '',
                        'classLi'            => '',
                        'iconPosLabel'       => '<i class="right fas fa-angle-left"></i>',
                        'labelAtivo'         => '',
                        'iconePersonalizado' => '<svg fill="#C2C7D0" width="20" heigth="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M 12.4375 0.1875 L 0 5.09375 L 0 18.53125 L 15 25.90625 L 26 18.53125 L 26 5.09375 Z M 12.46875 2.34375 L 23.6875 6.375 L 21.03125 7.78125 L 9.75 3.40625 Z M 7.03125 4.5 L 18.6875 9.03125 L 15.59375 10.65625 L 3.4375 5.90625 Z M 2.0625 6.4375 L 15 11.5 L 15 23.5 L 14.84375 23.59375 L 2 17.28125 L 2 6.46875 Z M 6.5 10.65625 L 5.03125 12.5625 L 6 12.875 L 6 15.625 L 7 16 L 7 13.21875 L 8 13.53125 Z M 10.5 12.03125 L 9 13.96875 L 10 14.3125 L 10 17.34375 L 11 17.71875 L 11 14.65625 L 11.96875 14.96875 Z M 5 16.03125 L 5 17.25 L 12 20.5625 L 12 19.3125 Z"></path></svg>',
                        'label'              => 'Produtos',
                        'filhos' => [
                          'produtos'   => [
                            'label' => 'Produtos',
                            'link'  => CAMINHO_DASHBOARD.'painel/pages/produtos'
                          ],
                          'sabores-pizza' => [
                            // <br><small>(Pizza / Esfiha)</small>
                            'label' => 'Cor / Material',
                            'link'  => CAMINHO_DASHBOARD.'painel/pages/produtos/sabores-pizza'
                          ],
                          'tamanhos-pizza' => [
                            // Tamanhos <small>(Pizza / Esfiha)</small>
                            'label' => 'Peso ou Ml',
                            'link'  => CAMINHO_DASHBOARD.'painel/pages/produtos/tamanhos-pizza'
                          ]
                        ]
                      ];
}else{
  $estruturaProduto = [
                        'link' => CAMINHO_DASHBOARD.'painel/pages/produtos',
                        'classIcon'          => '',
                        'classLi'            => '',
                        'iconPosLabel'       => '',
                        'labelAtivo'         => '',
                        'iconePersonalizado' => '<svg fill="#C2C7D0" width="20" heigth="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M 12.4375 0.1875 L 0 5.09375 L 0 18.53125 L 15 25.90625 L 26 18.53125 L 26 5.09375 Z M 12.46875 2.34375 L 23.6875 6.375 L 21.03125 7.78125 L 9.75 3.40625 Z M 7.03125 4.5 L 18.6875 9.03125 L 15.59375 10.65625 L 3.4375 5.90625 Z M 2.0625 6.4375 L 15 11.5 L 15 23.5 L 14.84375 23.59375 L 2 17.28125 L 2 6.46875 Z M 6.5 10.65625 L 5.03125 12.5625 L 6 12.875 L 6 15.625 L 7 16 L 7 13.21875 L 8 13.53125 Z M 10.5 12.03125 L 9 13.96875 L 10 14.3125 L 10 17.34375 L 11 17.71875 L 11 14.65625 L 11.96875 14.96875 Z M 5 16.03125 L 5 17.25 L 12 20.5625 L 12 19.3125 Z"></path></svg>',
                        'label'              => 'Produtos',
                      ];
}


$itensMenu = ['painel'        => ['link'               => CAMINHO_DASHBOARD.'painel/',
                                  'classIcon'          => 'fas fa-tachometer-alt',
                                  'classLi'            => 'has-treeview menu-open',
                                  'iconPosLabel'       => '<i class="right fas fa-angle-left"></i>',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Resumo'],

              'pedidos'       => ['link'               => CAMINHO_DASHBOARD.'painel/pages/pedidos',
                                  'classIcon'          => 'fas fa-clipboard-list',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Pedidos'],

              'agendamentos'  => ['link'               => CAMINHO_DASHBOARD.'painel/pages/agendamentos',
                                  'classIcon'          => 'fas fa-calendar-alt',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Agendamentos'],

              'venda-interna'      => ['link' => CAMINHO_DASHBOARD.'painel/pages/venda-interna',
                                  'classIcon'          => 'fas fa-file-invoice-dollar',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Venda Interna'],

              'clientes'      => ['link' => CAMINHO_DASHBOARD.'painel/pages/clientes',
                                  'classIcon'          => 'fas fa-users',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Clientes'],

              'mesas'         => ['link' => CAMINHO_DASHBOARD.'painel/pages/mesas',
                                  'classIcon'          => 'fas fa-paste',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '<i class="right fas fa-angle-left"></i>',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Comandas / Mesas',
                                  'filhos'             => [
                                    'mesas'   => [
                                      'label' => 'Mesa',
                                      'link'  => CAMINHO_DASHBOARD.'painel/pages/mesas/'
                                    ],
                                  ]
                                ],

              'categorias'    => ['link' => CAMINHO_DASHBOARD.'painel/pages/categorias',
                                  'classIcon'          => 'fas fa-list-ul',
                                  'classLi'            => '',
                                  'iconPosLabel'       => '',
                                  'labelAtivo'         => '',
                                  'iconePersonalizado' => '',
                                  'label'              => 'Categorias'],

              'produtos'      => $estruturaProduto,

             'adicionais'     => ['link' => CAMINHO_DASHBOARD.'painel/pages/adicionais',
                                 'classIcon'          => 'fas fa-plus-circle',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '<i class="right fas fa-angle-left"></i>',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Adicionais',
                                 'filhos'             => [
                                   'adicionais'   => [
                                     'label' => 'Adicionais',
                                     'link'  => CAMINHO_DASHBOARD.'painel/pages/adicionais/'
                                   ],
                                   'adicional-grupo'   => [
                                     'label' => 'Grupo de Adicionais',
                                     'link'  => CAMINHO_DASHBOARD.'painel/pages/adicionais/adicional-grupo/'
                                   ],
                                   'vinculos'   => [
                                     'label' => 'Associação em Massa',
                                     'link'  => CAMINHO_DASHBOARD.'painel/pages/adicionais/vinculos/'
                                   ],
                                 ]
                               ],

             'atendimento'   => ['link' => CAMINHO_DASHBOARD.'painel/pages/atendimento',
                                 'classIcon'          => 'far fa-clock',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Horários Atendimento'],

             'agendamento'   => ['link' => CAMINHO_DASHBOARD.'painel/pages/agendamento',
                                 'classIcon'          => 'fas fa-calendar',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Horários Agendamento'],

             'logistica'     => ['link' => CAMINHO_DASHBOARD.'painel/pages/logistica',
                                 'classIcon'          => 'fas fa-shipping-fast',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Áreas de Entrega'],

             'cupom'         => ['link' => CAMINHO_DASHBOARD.'painel/pages/cupom',
                                 'classIcon'          => 'fas fa-tag',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Cupons'],

         'formas-pagamento' => ['link' => CAMINHO_DASHBOARD.'painel/pages/formas-pagamento',
                                 'classIcon'          => 'fas fa-hand-holding-usd',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Formas de Pagamento'],

             'entregadores'  => ['link' => CAMINHO_DASHBOARD.'painel/pages/entregadores',
                                 'classIcon'          => '',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '<svg fill="#C2C7D0" width="28" heigth="28" version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200.000000 200.000000" preserveAspectRatio="xMidYMid meet"><g transform="translate(0.000000,200.000000) scale(0.100000,-0.100000)" stroke="none"><path d="M923 1740 c-88 -53 -82 -190 10 -233 91 -43 176 1 201 106 6 24 10 26 34 20 44 -11 67 -9 67 7 0 9 -19 20 -50 28 -31 9 -66 29 -92 53 -37 34 -49 39 -90 39 -30 0 -60 -7 -80 -20z"/><path d="M791 1453 c-43 -21 -74 -70 -103 -163 -20 -64 -23 -96 -23 -220 0 -127 2 -149 20 -180 28 -48 72 -66 205 -85 148 -20 156 -22 174 -40 8 -8 36 -69 61 -135 54 -138 86 -176 139 -166 39 8 76 43 76 73 0 40 -111 336 -137 365 -28 32 -90 55 -177 67 -103 15 -110 20 -103 94 3 33 9 63 14 68 6 6 34 -3 69 -20 69 -35 130 -50 199 -51 l50 0 28 -66 c16 -37 54 -143 85 -235 36 -107 62 -169 70 -169 8 0 26 8 40 18 61 43 159 55 223 28 19 -8 58 -33 87 -56 32 -26 52 -36 52 -27 0 23 -57 95 -97 121 -34 22 -60 31 -145 50 -19 4 -30 16 -38 40 -17 53 -73 119 -134 160 -56 37 -72 66 -38 66 10 0 36 -18 57 -40 44 -45 77 -51 99 -17 32 51 26 158 -10 184 -30 22 -51 15 -104 -32 -65 -58 -73 -57 -104 3 -18 35 -26 38 -44 20 -9 -9 -12 -7 -12 8 0 32 -32 54 -76 54 -87 0 -181 46 -234 113 -16 20 -18 29 -8 54 24 63 -23 133 -89 133 -21 0 -53 -8 -72 -17z"/><path d="M192 1153 l3 -28 198 -3 197 -2 0 30 0 30 -201 0 -200 0 3 -27z"/><path d="M192 953 l3 -128 193 -3 192 -2 0 130 0 130 -80 0 -80 0 0 -45 c0 -43 -1 -45 -30 -45 -29 0 -30 2 -30 45 l0 45 -85 0 -86 0 3 -127z"/> <path d="M252 780 c-52 -12 -57 -22 -42 -83 15 -63 12 -87 -25 -225 -32 -116 -30 -122 36 -122 46 0 53 10 67 90 7 41 22 80 40 105 90 128 287 139 387 22 35 -40 65 -112 65 -154 0 -14 5 -33 10 -44 10 -18 24 -19 280 -19 256 0 270 1 280 19 14 27 13 37 -6 55 -13 14 -48 16 -222 16 -114 0 -213 4 -219 8 -19 12 -53 120 -53 168 0 24 5 64 12 88 11 40 10 47 -7 66 -17 19 -28 20 -294 19 -152 -1 -291 -5 -309 -9z"/> <path d="M465 556 c-111 -51 -128 -208 -32 -287 36 -29 101 -41 150 -28 132 35 168 214 60 296 -42 32 -130 42 -178 19z"/> <path d="M1535 556 c-37 -16 -82 -68 -91 -102 -10 -40 2 -114 23 -142 35 -47 70 -66 130 -69 105 -7 173 57 173 162 0 65 -26 114 -76 142 -45 26 -112 30 -159 9z"/></g></svg>',
                                 'label'              => 'Entregadores'],

             'configuracoes' => ['link' => CAMINHO_DASHBOARD.'painel/pages/configuracoes',
                                 'classIcon'          => 'fas fa-sliders-h',
                                 'classLi'            => '',
                                 'iconPosLabel'       => '',
                                 'labelAtivo'         => '',
                                 'iconePersonalizado' => '',
                                 'label'              => 'Configurações'],

              'usuarios'     => ['link' => CAMINHO_DASHBOARD.'painel/pages/controle-acesso/usuarios/',
                                    'classIcon'          => 'fas fa-user-tie',
                                    'classLi'            => '',
                                    'iconPosLabel'       => '',
                                    'labelAtivo'         => '',
                                    'iconePersonalizado' => '',
                                    'label'              => 'Usuários',
                                  // 'filhos'             => [
                                    // 'usuarios'   => [
                                      // 'label' => 'Usuários',
                                      // 'link'  => CAMINHO_DASHBOARD.'painel/pages/controle-acesso/usuarios/'
                                    // ],
                                    // 'grupo-usuario'   => [
                                    //   'label' => 'Grupos de Usuários',
                                    //   'link'  => CAMINHO_DASHBOARD.'painel/pages/controle-acesso/grupo-usuario/'
                                    // ],
                                    // 'permissoes-paginas'   => [
                                    //   'label' => 'Permissões Páginas',
                                    //   'link'  => CAMINHO_DASHBOARD.'painel/pages/controle-acesso/permissoes-paginas/'
                                    // ],
                                  // ]
                                ],
             ]
?>

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

        <?php

            $pedacosUrl = explode('/',$_SERVER['REQUEST_URI']);
            array_pop($pedacosUrl);
            $moduloAtual = (end($pedacosUrl));
            foreach ($itensMenu as $key => $value) {
                $estruturaFilhos = '';
                $itensFilho      = '';
                if(isset($value['filhos'])){
                  foreach ($value['filhos'] as $chave => $valor) {
                    $active      = $chave == $moduloAtual ? 'active' : '';
                    $itensFilho .= '<li class="nav-item">
                                      <a href="'.$valor['link'].'" class="nav-link '.$active.'">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>'.$valor['label'].'</p>
                                      </a>
                                    </li>';
                  }

                  // caso o item atual que está sendo montado no drop ser o modulo atual, traz aberto
                  $visibilidadeFilhos = '';
                  if(in_array($key,$pedacosUrl)){
                    $visibilidadeFilhos    = 'style="display:block"';
                    $value['iconPosLabel'] = '<i class="right fas fa-angle-down"></i>';
                  }
                  $estruturaFilhos    = '<ul class="nav nav-treeview itensFilhos" '.$visibilidadeFilhos.'>
                                          '.$itensFilho.'
                                         </ul>';
                }

                $value['labelAtivo'] = in_array($key,$pedacosUrl) ? 'active' : '';
                // caso eu nao estiver no painel (que sempre está na url) e a key montada estiver sendo a do painel, eu nao seto como ativa
                if($moduloAtual != 'painel' && $key == 'painel') $value['labelAtivo'] = '';

                echo '<li class="nav-item '.$value['classLi'].'">
                        <a href="'.$value['link'].'" class="nav-link '.$value['labelAtivo'].'">
                            <i class="nav-icon '.$value['classIcon'].'"></i>
                            '.$value['iconePersonalizado'].'
                            <p>
                            '.$value['label'].
                              $value['iconPosLabel'].'
                            </p>
                        </a>
                        '.$estruturaFilhos.'
                       </li>';
            }
        ?>

    </ul>
</nav>
