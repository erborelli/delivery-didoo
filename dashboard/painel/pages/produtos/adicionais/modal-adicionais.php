<?php
  include_once(dirname(__FILE__).'/../../../../includes.php');
?>

<!-- Modal -->
<div class="modal fade" id="modalAdicionais" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div style="max-width:600px;" class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Vincular Produto/Adicional:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="conteudoEntregas" style="width: 90%;">
        <div class="row">
          <input type="hidden" id="txtIdProdutoModalAdicionais" value="">
          <div class="col-md-4 col-4">
            <div style="margin-top: 5px; margin-left: 10px;">
              <label>Selecionar Todos</label>
              <input id="chkModalAdicionaisSelecionarTodos" type="checkbox">
            </div>
          </div>
          <div class="col-md-8 col-8">
            <input id="txtBuscaAdicionaisProdutos" class="form-control" placeholder="Buscar...">
          </div>
        </div>
        <div class="modal-body text-center conteudoAdicionais">
          <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary salvarVinculoAdicionais">Salvar</button>
      </div>
    </div>
  </div>
</div>
