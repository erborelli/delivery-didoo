<!-- jquery -->
<script src="<?=CAMINHO_DASHBOARD?>painel/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=CAMINHO_DASHBOARD?>painel/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=CAMINHO_DASHBOARD?>painel/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=CAMINHO_DASHBOARD?>painel/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="<?=CAMINHO_DASHBOARD?>painel/plugins/summernote/summernote-bs4.min.js"></script>
<!-- select2 -->
<script src="<?=CAMINHO_DASHBOARD?>painel/plugins/select2/js/select2.full.min.js"></script>
<!-- fontawesome -->

<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
