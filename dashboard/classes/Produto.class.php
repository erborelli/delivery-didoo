<?php

class Produto {

    public static function getListagemProdutos(){
      $link = getConnection();
      $sql  = "SELECT * FROM produto WHERE inativo = 'n'";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function getProdutosAtivosEInativos(){
      $link = getConnection();
      $sql  = "SELECT * FROM produto";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function getProdutosAtivosEInativosArray(){
      $link = getConnection();
      $sql  = "SELECT * FROM produto";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getProdutoPorID($idProduto){
        if(!isset($idProduto) || !is_numeric($idProduto) || empty($idProduto)){
          return false;
        }
        $link = getConnection();
        $sql  = "SELECT * FROM produto WHERE id = :idProduto AND inativo = 'n'";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':idProduto', $idProduto);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public static function getProduto($idProduto){
        if(!isset($idProduto) || !is_numeric($idProduto) || empty($idProduto)){
          return false;
        }
        $link = getConnection();
        $sql  = "SELECT * FROM produto WHERE id = :idProduto AND inativo = 'n'";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':idProduto', $idProduto);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function verificarProdutoPizza($idProduto){
      if(!is_numeric($idProduto)) return false;
      $link = getConnection();
      $sql  = "SELECT tipo FROM produto WHERE id = :idProduto";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idProduto', $idProduto);
      $stmt->execute();
      $tipoProduto = $stmt->fetch(PDO::FETCH_OBJ);
      if(empty($tipoProduto)) return false;
      $tipoProduto = $tipoProduto->tipo;
      if($tipoProduto == 'pizza') return true;
      return false;
    }

    public static function getProdutoArray($idProduto){
      if(!isset($idProduto) || !is_numeric($idProduto) || empty($idProduto)){
        return false;
      }
      $link = getConnection();

      $sql  = "SELECT * FROM produto
              WHERE id = :idProduto
              AND inativo = 'n'";

      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idProduto', $idProduto);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

    public static function getProdutoCustom(){
      $link = getConnection();
      $sql  = "SELECT * FROM produto WHERE nome = 'produto custom'";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function getProdutoCustomArray(){
      $link = getConnection();

      $sql  = "SELECT * FROM produto
              WHERE nome = 'produto custom'";

      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getProdutoMesmoInativado($idProduto){
        if(!isset($idProduto) || !is_numeric($idProduto) || empty($idProduto)){
          return false;
        }
        $link = getConnection();
        $sql  = "SELECT * FROM produto WHERE id = :idProduto";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':idProduto', $idProduto);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function getTipoProduto($idProduto){
      if(!isset($idProduto) || !is_numeric($idProduto) || empty($idProduto)){
        return false;
      }
      $link = getConnection();
      $sql  = "SELECT tipo FROM produto WHERE id = :idProduto";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idProduto', $idProduto);
      $stmt->execute();

      $retornoBd = $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
      if(!isset($retornoBd[0]->tipo)){
        return false;
      }

      return $retornoBd[0]->tipo;
    }

    public static function updateProduto($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;

      $link = getConnection();
      $sql = "UPDATE produto SET nome=?,valor=?,id_categoria=?,descricao=?,foto_produto=?,qtd_sabores=?,id_tamanho=?
              WHERE id=?";
      $stmt= $link->prepare($sql);
      $qtdSabores = isset($dadosPost['qtdSabores']) ? $dadosPost['qtdSabores'] : 0;
      $idTamanho  = isset($dadosPost['idTamanho'])  ? $dadosPost['idTamanho']  : null;
      $stmt->execute([$dadosPost['nome'],$dadosPost['preco'],$dadosPost['categoria'],$dadosPost['descricao'],$dadosPost['imagensProduto'],$qtdSabores,$idTamanho,$_GET['id']]);
      return true;
    }

    public static function ativarInativarProduto($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $inativar  = $dadosPost['inativo'];
      $idProduto = $dadosPost['idProduto'];
      $link = getConnection();
      $sql = "UPDATE produto SET inativo=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$inativar,$idProduto]);
      return true;
    }

    public static function getFotosProdutoPorId($idProduto){
      $link = getConnection();
      $sql  = "SELECT foto_produto FROM produto WHERE id = :idProduto";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idProduto', $idProduto);
      $stmt->execute();
      return $stmt->fetch(PDO::FETCH_OBJ)->foto_produto;
    }

    public static function insertProduto($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "INSERT INTO produto (nome,valor,id_categoria,descricao,foto_produto,tipo) VALUES (?,?,?,?,?,?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$dadosPost['preco'],$dadosPost['categoria'],$dadosPost['descricao'],$dadosPost['imagensProduto'],$dadosPost['tipo']]);
      return $link->lastInsertId();
    }

    public static function getProdutosPorIdCategoria($idCategoria){
      $link = getConnection();
      $sql  = "SELECT * FROM produto WHERE id_categoria = ? AND inativo = 'n' AND valor > 0 ORDER BY valor ASC";
      $stmt = $link->prepare($sql);
      $stmt->execute([$idCategoria]);
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Produto');
    }

    public static function getProdutosArrayPorNome($nome=''){
      $link = getConnection();
      $sql  = "SELECT * FROM produto WHERE nome LIKE CONCAT('%',?,'%') ORDER BY valor ASC";
      $stmt = $link->prepare($sql);
      $stmt->execute([$nome]);
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getProdutosArrayPorNomeVerificaAdicional($idAdicional,$tipoAdicional,$nome=''){
      $tipo = '';
      if($tipoAdicional=='adicionais'){
        $tipo = 'a.id_adicional';
      }else{
        $tipo = 'a.id_adicional_grupo';
      }

      $sql  = "SELECT p.*,
                    (
                      IF(
                        (SELECT a.id FROM produto_adicional as a WHERE $tipo=? AND a.id_produto=p.id LIMIT 1)
                        IS NOT NULL,'s','n'
                      )
                    ) as adicional
                FROM produto as p
                WHERE p.nome LIKE CONCAT('%',?,'%') ORDER BY p.nome ASC;";

      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute([$idAdicional,$nome]);
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getProdutosSession(){
      if(isset($_SESSION['carrinho']) && isset($_SESSION['carrinho']['itens']) && !empty($_SESSION['carrinho']['itens'])){
        return $_SESSION['carrinho']['itens'];
      }
      return [];
    }

    /**
     * calcula os valores do prduto, levando em consideração os adicionais e extras
     * @param $calcularAdicionarAoCarrinho : identifica se o calculo é na hora de adicional ao carrinho
     */
    public static function calcularValorProduto($idProduto, $calcularAdicionarAoCarrinho = false){
      if(!isset($idProduto)) return 0;

      // valida se há valores adicionais ou valores de sabores de pizza a serem calculados
      $calcularValoresExtras = false;
      if(isset($_SESSION['valoresProduto']) && (isset($_SESSION['valoresProduto']['valoresPizza'][$idProduto]) || isset($_SESSION['valoresProduto']['valoresAdicional'][$idProduto]))){
        $calcularValoresExtras = true;
      }

      $produto = self::getProdutoPorID($idProduto);
      if($produto->tipo == 'pizza' && $calcularValoresExtras){
        return self::calcularValoresExtras($_SESSION['valoresProduto'],$idProduto);
      }elseif ($calcularValoresExtras) {
        $valor = self::calcularValoresExtrasProdutoDefault($_SESSION['valoresProduto'],$idProduto,$produto->valor);
        if($calcularAdicionarAoCarrinho) unset($_SESSION['valoresProduto']);
        return $valor;
      }else {
        return $produto->valor;
      }
    }

    public static function calcularValoresExtrasProdutoDefault($dadosValoresExtras,$idProduto,$valorProduto){
      if(!is_numeric($idProduto) || empty($dadosValoresExtras)) return 0;
      $valoresExtras = 0;
      foreach ($dadosValoresExtras as $key => $value) {
        if($key == 'valoresAdicional'){
          $valoresExtras += array_sum($value[$idProduto]);
        }
      }
      return $valoresExtras + $valorProduto;
    }

    /**
     * Responsável por calcular os tipos de valores adicionais a um produto
     * @param $dadosValoresExtras | Referente aos valores adicionais de sabores ou de adicionais ao produto
     */
    public static function calcularValoresExtras($dadosValoresExtras,$idProduto){
      if(!is_numeric($idProduto) || empty($dadosValoresExtras)) return 0;
      $calculoValorPizza  = Config::getConfigEspecifico('configValorPizza');
      $valoresExtras = 0;
      foreach ($dadosValoresExtras as $key => $value) {
        if($key == 'valoresPizza'){
          switch ($calculoValorPizza) {
            case 'maior':
              $valoresExtras += max($value[$idProduto]);
            break;
            default:
              if(count($value[$idProduto]) == 0)break;
              $valoresExtras += array_sum($value[$idProduto])/count($value[$idProduto]);
            break;
          }
        }else {
          $valoresExtras += array_sum($value[$idProduto]);
        }
      }
      return $valoresExtras;
    }

    public static function getProdutosSessionVendaInterna(){
      if(isset($_SESSION['venda_interna']) && isset($_SESSION['venda_interna']['carrinho']) && !empty($_SESSION['venda_interna']['carrinho'])){
        return $_SESSION['venda_interna']['carrinho'];
      }
      return [];
    }

}
