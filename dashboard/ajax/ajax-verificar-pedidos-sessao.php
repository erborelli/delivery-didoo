<?php

include_once('../includes.php');

$retorno = ['sucesso'  => false,
            'mensagem' => isset($_SESSION['mensagemPedidoTelegram']) ? $_SESSION['mensagemPedidoTelegram'] : ''];

// caso nao houver pedidos na session
if(!isset($_SESSION['pedidos']) || empty($_SESSION['pedidos'])){
  echo json_encode($retorno);exit;
}

// pega o id do pedido pendente na sessao do cliente
$idPedido     = array_keys($_SESSION['pedidos'])[0];
$dadosPedido  = Pedido::getStatusObservacaoPedido($idPedido);
$statusPedido = $dadosPedido->id_status;

// chegou aqui, quer dizer que há pedidos na sessao
$retorno['sucesso'] = true;
if($statusPedido == 1){
  echo json_encode($retorno);
  exit;
}

//caso a observação for vazia e o estabelecimento aceitou o pedido
if($statusPedido == 2 && empty($dadosPedido->observacao)){
  $observacao   = '';
}else {
  $observacao   = !empty($dadosPedido->observacao)
  ? 'Observação: <b>"'.$dadosPedido->observacao.'"</b>'
  : 'O estabelecimento não deixou nenhuma observação';

  // caso o config de mostrar observação do estabelecimento for não, zera a observação
  if(MOSTRAROBSERVACAOPEDIDO == 'n') $observacao   = '';
}

// caso o status do pedido for igual a 2(aceito)
if($statusPedido == 2 && ACEITARPEDIDOSAUTOMATICO == 's'){
  $retorno['retornoPedido'] = '<h3>Tudo certo!<br><br> Recebemos seu pedido! <br><br>Prazo estimado: '.TEMPOESPERA.' <br><br> '.$observacao.'</h3>';
  if($dadosPedido->agendamento == 's'){
    $dataAgendada = date_format(date_create($dadosPedido->data_agendamento),'d/m/Y H:i:s');
    $retorno['retornoPedido'] = '<h3>Tudo certo!<br><br> Recebemos seu pedido! <br><br>Agendado para entrega: <br><b>'.$dataAgendada.'</b> <br><br> '.$observacao.'</h3>';
  }
  unset($_SESSION['pedidos']);
}elseif($statusPedido == 2 && $dadosPedido->agendamento == 's'){
  $dataAgendada = date_format(date_create($dadosPedido->data_agendamento),'d/m/Y H:i:s');
  $retorno['retornoPedido'] = '<h3>Sucesso!<br><br> Seu pedido foi aceito! <br><br>Seu pedido está agendado para entrega: <br><b>'.$dataAgendada.'</b> <br><br> '.$observacao.'</h3>';
  unset($_SESSION['pedidos']);
}elseif($statusPedido == 2){
  $retorno['retornoPedido'] = '<h3>Sucesso!<br><br> Seu pedido foi aceito! <br><br>Prazo estimado: '.TEMPOESPERA.' <br><br> '.$observacao.'</h3>';
  unset($_SESSION['pedidos']);
}elseif ($statusPedido == 3) { // negado pelo estabelecimento
  $retorno['retornoPedido'] = '<h3>Sentimos muito, seu pedido foi negado! <br> :( <br><br>'.$observacao.'</h3>';
  unset($_SESSION['pedidos']);
}

echo json_encode($retorno);
