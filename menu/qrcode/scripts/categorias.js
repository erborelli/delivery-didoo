
function montarCategorias(categorias){
    let tbody = document.getElementById('tbodyCategorias')
    categorias.forEach(c => {
        
        let tr         = document.createElement('tr')
        let tdId       = document.createElement('td')
        let tdNome     = document.createElement('td')
        let tdClick    = document.createElement('td')
        let dvProdutos = document.createElement('div')
        let iClick     = document.createElement('i')
        let spanNome   = document.createElement('span')

        dvProdutos.style.display         = 'none'
        dvProdutos.style.marginTop       = '20px'
        dvProdutos.style.borderRadius    = '10px'
        dvProdutos.style.borderColor     = '#e3a106'
        dvProdutos.style.borderWidth     = '2px'
        dvProdutos.style.borderStyle     = 'solid'

        dvProdutos.style.backgroundColor = '#ffeac4'
        tdClick.style.verticalAlign      = 'middle'
        dvProdutos.tagName   = c.id+'DvProdutos'
        tdId.style.display   = 'none'
        tdId.textContent     = c.id
        spanNome.textContent = c.nome
        iClick.className     = 'fas fa-chevron-down icone'
        
        tdClick.onclick = function(){
            let i   = this.getElementsByTagName('i')[0]
            let div = this.parentElement.getElementsByTagName('td')[1].getElementsByTagName('div')[0]
            if(div.style.display == ''){
                div.style.display = 'none'
                i.className = 'fas fa-chevron-down icone'
            }else{
                div.style.display = ''
                i.className = 'fas fa-chevron-up icone'
            }
        }

        spanNome.onclick = function(){
            let div = this.parentElement.getElementsByTagName('div')[0]
            let i   = this.parentElement.parentElement.getElementsByClassName('icone')[0]
            if(div.style.display == ''){
                div.style.display = 'none'
                i.className = 'fas fa-chevron-down icone'
            }else{
                div.style.display = ''
                i.className = 'fas fa-chevron-up icone'
            }
        }
        
        montarProdutos(dvProdutos,c.produtos)
        
        tdClick.append(iClick)
        tdNome.append(spanNome)
        tdNome.append(dvProdutos)
        tr.append(tdId)
        tr.append(tdNome)
        tr.append(tdClick)
        tbody.appendChild(tr)
    })
}