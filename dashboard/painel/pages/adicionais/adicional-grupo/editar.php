<?php
include_once('../../../../includes.php');

//post de salvar edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  if(AdicionalGrupo::updateAdicionalGrupo($_POST)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/adicional-grupo/');
  }
}

//se id do adicional não recebido, redireciona para listagem
if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/adicional-grupo/');
}

$adicionalGrupo = AdicionalGrupo::getGrupoPorId($_GET['id']);
$adicionais     = AdicionalGrupo::getAdicionaisPorIdAdicionalGrupo($_GET['id'],true);

//caso tenha retornado false
if(!$adicionalGrupo){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/adicionais/adicional-grupo/');
}

?>

<!DOCTYPE html>
<html>
  <style>
    #div{
      border: 1px solid;
      padding: 5px;
      border-radius: 10px;
      border-color: #d2d4d7
    }
  </style>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>
  <?php include_once('modalAdicionais.php'); ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Grupo Adicional</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Grupo Adicional</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
        <form action="" style="display:contents;" method="post">

          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Editar Grupo Adicional</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <input id="idAdicionalGrupo" type="hidden" name="id" style="display: none;" value="<?=$adicionalGrupo->id?>">
                <div class="form-group">
                  <label for="nome">Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="<?=$adicionalGrupo->nome?>" required>
                </div>
                <div class="row">
                  <div class="form-group col-md-1">
                    <label for="ordem">Ordem</label>
                    <input type="number"  min="0" max="99" name="ordem" id="ordem" class="form-control" value="<?=$adicionalGrupo->ordem?>" required>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="qtdMinima">Quantidade Min</label>
                    <input type="number" min="0" max="99" name="qtdMinima" id="qtdMinima" class="form-control" value="<?=$adicionalGrupo->qtdMinima?>" required>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="qtdMaxima">Quantidade Max</label>
                    <input type="number" min="0" max="99" name="qtdMaxima" id="qtdMaxima" class="form-control" value="<?=$adicionalGrupo->qtdMaxima?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ativo">Ativo</label>
                  <input type="checkbox" id="chkAtivo" onchange="alterarStatusAtivo()">
                  <input type="hidden" name="ativo" id="ativo" value="<?=$adicionalGrupo->ativo?>">
                </div>
              </div>
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
      <br>

      <!-- Listagem de adicionais-->
      <div id="div" style="background-color: white;">
        <div style="margin-left: 10px; text-align: center;">
          <h4>Adicionais</h4>
        </div>
        <button type="button" class="btn btn-primary btn-sm" style="color: white;" onclick="adicionarAdicional()"><i class="fas fa-trash"></i> Adicionar</button>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Preço</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="tbodyAdicionais">
            <?php foreach($adicionais as $adicional){ ?>
              <tr>
                <td style="display: none;"><?=$adicional['id']?></td>
                <td><?=$adicional['nome']?></td>
                <td><?=$adicional['preco']?></td>
                <td>
                  <button onclick="removerAdicional(this)" type="button" class="btn btn-danger btn-sm" style="color: white;">
                    <i class="fas fa-trash"></i> Remover
                  </button>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <br>

    </section>
  </div>

  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
  <script>
    let ativo    = document.getElementById('ativo')
    let chkAtivo = document.getElementById('chkAtivo')
    if(ativo.value=='s'){
      chkAtivo.checked = true
    }else{
      chkAtivo.checked = false
      ativo.value='n'
    }

    function alterarStatusAtivo(){
      let ativo    = document.getElementById('ativo')
      let chkAtivo = document.getElementById('chkAtivo')
      if(chkAtivo.checked==true){
        ativo.value='s'
      }else{
        ativo.value='n'
      }
    }

    function removerAdicional(btn){
      let idAdicional      = btn.parentElement.parentElement.getElementsByTagName('td')[0].textContent
      let idAdicionalGrupo = document.getElementById('idAdicionalGrupo').value
      btn.parentElement.parentElement.remove()

      $.ajax({
          url     : CAMINHO_DASHBOARD+'ajax/ajax-remover-adicionar-adicionais-grupo.php?tipo=remover&idAdicional='+idAdicional+'&idAdicionalGrupo='+idAdicionalGrupo,
          type    : 'GET',
          dataType: 'JSON',
      })
    }
    function adicionarAdicional(){
      desmarcarTodos()
      $('#modalAdicionais').modal('show')
    }
  </script>
</div>
<?php include_once('../../../includes-scripts/includes-scripts-estrutura.php'); ?>
</body>
</html>
