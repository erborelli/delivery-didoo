<?php

include_once('../includes.php');

$retorno = ['novosPedidos' => false, 'qtdPedidos' => 0];

$pedidosEmEspera = Pedido::getPedidosPorStatus(1)->qtd;

if($pedidosEmEspera > 0){
  $retorno['novosPedidos']  = true;
  $retorno['qtdPedidos']    = $pedidosEmEspera;
  $retorno['layoutPedidos'] = TemaDashboard::montarLayoutPedidosEmEspera();
}

echo json_encode($retorno);
