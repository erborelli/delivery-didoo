<!-- Modal -->
<div id="modalCep" class="modal w920 fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog w920" role="document" data-cy="modal-dialog" style="width: 100%;">
    <div class="modal-content w920" style="top: 0px;">
      <div class="modal-close-content">
        <i class="material-icons close-button js-modal-close" data-ga="modal-close" data-cy="modal-dialog__close">close</i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title titleModalInforme" id="myModalLabel" style="display:none;">Informe seu endereço</h4>
        <h4 class="modal-title titleModalRetirada" id="myModalLabel" style="display:none;">Nosso Endereço:</h4>
        <h4 class="modal-title titleModalTipoEntrega" id="myModalLabel">Entrega ou retirada?</h4>
      </div>
      <div class="modal-header-mobile" style="">
        <button class="modal-header-content-back js-modal-close">
          <i class="material-icons icon">keyboard_arrow_left</i>
          <span class="text"> Voltar</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-secondary" style="display:none" data-dismiss="modal">Close</button>
        <div class="address-modal js-address-modal">
          <div class="js-click-ga" data-ga-action="address__modal_view" data-ga-category="address"></div>

          <!-- ETAPA 0 - (ENTREGA / RETIRADA) -->
          <div class="step-0 tipo-entrega">

            <div class="containerOpcoesRetirada">
              <div class="container">
                <h3 class="headerEntregaMobile hidden-lg hidden-md text-center"><b>Entrega ou retirada?</b></h3>
                <div opcao-escolhida="entrega" class="itemOpcaoRetirada">Entrega</div>
                <?php
                $classeDisabledRetirada = OFERECERRETIRADA == 'n' ? 'disabledRetirada' : '';
                $textpRetirada = OFERECERRETIRADA == 'n' ? '<div class="containerAvisoRetirada"><span>Desculpe nós somente entregamos!</span></div>' : '';
                ?>
                <div opcao-escolhida="retirada" class="itemOpcaoRetirada <?=$classeDisabledRetirada?>">
                  Vou Buscar
                </div>
                <?=$textpRetirada?>
              </div>
            </div>

          </div>

          <?php
          $bairros    = Frete::getFretes();
          $boxBairros = '';
          foreach ($bairros as $key => $value) {
            $boxBairros .= '<option value="'.$value->id.'">'.$value->bairro.'</option>';
          }
          if(INFORMARENDERECO == 'cep'){ ?>

          <!-- ETAPA 1 -->
          <div class="step-1 box-address" style="display:none;">
            <div class="description zip-description">
              Precisamos da sua localização para verificar se entregamos em sua região
            </div>
            <div class="actions-geolocation js-mobile-geolocation visible-xs-block">
              <span class="separator">
                busque pelo endereço
              </span>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field has-placeholder is-upgraded" data-upgraded=",MaterialTextfield">
              <form autocomplete="off" onsubmit="return false;" style="margin-bottom:unset;">
                <input class="mdl-textfield__input address-modal-input zip-input" type="tel" pattern="([0-9\-]*)?" id="zipcode" data-next="submit-step1-btn" placeholder="00000-000" data-cy="address-modal__input--zipcode" onkeypress="mascara(this, '#####-###')" maxlength="9">
              </form>
              <span class="mdl-textfield__error zipcode-error js-error retornoErroCep" data-cy="address-modal__error--step-1" style="padding-top:unset;margin-top:unset;"></span>
            </div>

            <div class="js-actions actions-without-takeout">

              <button class="hidden-xs mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit height-60 js-submit-step-1" data-ga-action="address__modal_submit_zip" data-ga-category="address" data-cy="address-modal__button--zip"
                data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">BUSCAR CEP</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
              <button id="submit-step1-btn" class="visible-xs-block mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit height-60 js-submit-step-1" data-ga="modal-search-address" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Buscar endereço</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
              <div class="address-modal-or-box js-address-modal-or-box is-hidden">
                <span class="line"></span>
                <p class="or-phrase">OU</p>
                <span class="line"></span>
              </div>
              <button class="is-hidden mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit height-60 js-schedule-order" data-ga="modal-address-schedule-order" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Agendar pedido</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>

              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-takeout is-hidden js-submit-takeout js-has_takeout" data-ga-action="address__modal_select_takeout" data-ga-category="address"
                data-cy="address-modal__button--takeout" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">RETIRAR NA LOJA</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
            </div>

            <div class="unknown-zip" data-ga="modal-dunno-zipcode" data-ga-action="address__modal_find_zip" data-ga-category="address">
              <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="hidden-xs" target="_blank">
                Não sabe seu CEP? <span class="dont-know-zip">Clique aqui.</span></a>
              <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="visible-xs-block" target="_blank">
                Não sabe seu CEP? <span class="dont-know-zip">Clique aqui.<span></span></span></a>
            </div>
            <span class="containerVoltarOpcoes">
              <span class="material-icons">keyboard_backspace</span>
              Voltar para opções
            </span>
          </div>
          <!-- fim primeira etapa -->
          <!-- segunda etapa -->
          <div class="step-2 box-address is-hidden js-step-2" data-cy="address-modal__step-2">
            <div class="address-second-step-field">
              <label for="confirmed-city" class="address-second-step-label">Cidade:</label>
              <span id="confirmed-city" class="address-second-step-value"></span>
            </div>

            <div class="address-second-step-field">
              <label for="confirmed-address" class="address-second-step-label js-label">Rua, Avenida, Alameda, etc:</label>
              <span id="confirmed-address" class="address-second-step-value" data-cy="address-modal__confirmed-address"></span>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-hidden is-upgraded" data-upgraded=",MaterialTextfield">
                <input class="mdl-textfield__input address-modal-input" type="text" id="confirmed-street" data-next="confirmed-neighborhood" data-cy="address-modal__input--confirmed-street">
                <label class="mdl-textfield__label address-modal-label" for="confirmed-street">Rua, Avenida, Alameda, etc</label>
                <span class="mdl-textfield__error address-second-step-error js-invalid-street is-hidden" data-cy="address-modal__error--confirmed-street">Por favor forneça uma rua</span>
              </div>
            </div>

            <div class="address-second-step-field">
              <label for="confirmed-neighborhood" class="address-second-step-label js-label">Bairro:</label>
              <span id="confirmed-neighborhood-label" class="address-second-step-value"></span>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-hidden is-upgraded" data-upgraded=",MaterialTextfield">
                <input class="mdl-textfield__input address-modal-input" type="text" id="confirmed-neighborhood" data-next="confirmed-number" data-cy="address-modal__input--confirmed-neighborhood">
                <label class="mdl-textfield__label address-modal-label" for="confirmed-neighborhood">Bairro</label>
                <span class="mdl-textfield__error address-second-step-error js-error is-hidden" data-cy="address-modal__error--confirmed-neighborhood">Por favor forneça um bairro</span>
              </div>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <input class="mdl-textfield__input address-modal-input" type="text" id="confirmed-number" pattern="([0-9]*)?" autocomplete="false" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Número</label>
              <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
            </div>

            <div class="actions">

              <button class="mdl-button mdl-js-button voltarFormCep mdl-js-ripple-effect mdl-button--accent btn-takeout js-reset" data-ga-action="address__modal_back" data-ga-category="address" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Voltar</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>

              <button id="address-submit-btn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit js-submit-step-2" data-ga-action="address__submit_details" data-ga-category="address"
                data-cy="address-modal__button--submit" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Confirmar</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
            </div>
          </div>
          <!-- final segunda etapa -->

          <?php }elseif(INFORMARENDERECO == 'digitando'){ ?>

          <!-- terceira etapa -->
          <div class="step-3 box-address js-step-3" data-cy="address-modal__step-3" style="display:none;">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <input class="mdl-textfield__input address-modal-input" type="text" id="address" pattern="([0-9]*)?" autocomplete="false" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Rua, Avenida, Alameda, etc:</label>
              <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <input class="mdl-textfield__input address-modal-input" type="text" id="confirmed-number" pattern="([0-9]*)?" autocomplete="false" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Número:</label>
              <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
            </div>

            <div class="selectBairrosModal mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Bairro:</label>
              <select class="js-example-basic-single" name="neighborhood" style="width: 100%" id="neighborhood">
                <option value="0">Selecione o Bairro</option>
                <?=$boxBairros?>
              </select>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <input class="mdl-textfield__input address-modal-input" type="text" id="city" pattern="([0-9]*)?" autocomplete="false" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Cidade:</label>
              <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded is-focused" data-upgraded=",MaterialTextfield">
              <input class="mdl-textfield__input address-modal-input" type="text" id="reference" pattern="([0-9]*)?" autocomplete="false" data-next="confirmed-complement" data-cy="address-modal__input--confirmed-number">
              <label class="mdl-textfield__label address-modal-label" for="confirmed-number">Ponto Referência (opcional):</label>
              <span class="mdl-textfield__error address-second-step-error js-invalid-number is-hidden" data-cy="address-modal__error--confirmed-number">Por favor, forneça um número válido</span>
            </div>

            <div class="actions">

              <button class="mdl-button mdl-js-button voltarFormCep mdl-js-ripple-effect mdl-button--accent btn-takeout js-reset" data-ga-action="address__modal_back" data-ga-category="address" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Voltar</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>

              <button id="address-submit-btn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn-submit js-submit-step-2" data-ga-action="address__submit_details" data-ga-category="address"
                data-cy="address-modal__button--submit" data-upgraded=",MaterialButton,MaterialRipple">
                <span class="value">Confirmar</span>
                <span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
            </div>
          </div>

          <?php } ?>

          <div class="containerOpcaoRetirada"></div>

          <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate loading is-hidden js-loading is-upgraded" data-upgraded=",MaterialProgress">
            <div class="progressbar bar bar1" style="width: 0%;"></div>
            <div class="bufferbar bar bar2" style="width: 100%;"></div>
            <div class="auxbar bar bar3" style="width: 0%;"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer is-hidden"></div>
    </div>
  </div>
</div>
<!-- Fim Modal -->
