

-- adiciona a opção mesa na origem do pedido
ALTER TABLE pedido 
MODIFY COLUMN origem ENUM('ifood','delivery-much','litoral-na-mesa','sistema','mesa');

CREATE TABLE mesa(
	id int not null auto_increment,
    numero int not null,
    descricao varchar(200),
    data_criacao datetime DEFAULT current_timestamp(),
    primary key (id)
);

CREATE TABLE mesa_item (
  id int NOT NULL AUTO_INCREMENT,
  id_mesa int(11) DEFAULT NULL,
  id_produto int(11) DEFAULT NULL,
  nome_produto varchar(500) DEFAULT NULL,
  qtd int DEFAULT NULL,
  valor decimal(10,2) DEFAULT NULL,
  observacoes varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  foreign key (id_produto) references produto(id)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

CREATE TABLE mesa_item_adicional(
  id int NOT NULL AUTO_INCREMENT,
  id_mesa_item int DEFAULT NULL,
  id_adicional int DEFAULT NULL,
  valor decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;


CREATE TABLE mesa_item_sabor(
  id int NOT NULL AUTO_INCREMENT,
  id_mesa_item int NOT NULL,
  id_sabor int NOT NULL,
  sabor varchar(50) NOT NULL DEFAULT '',
  valor decimal(10,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

