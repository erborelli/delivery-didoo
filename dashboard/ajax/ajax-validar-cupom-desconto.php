<?php

include_once('../includes.php');

$retorno = ['sucesso' => false];

if(isset($_SESSION['carrinho']['cupom'])){
  $retorno['sucesso']        = true;
  $retorno['cupomExistente'] = true;
  echo json_encode($retorno);exit;
}

// caso nao for setado a hash no post
if(!isset($_POST) || !isset($_POST['hashCupom'])){
  echo json_encode($retorno);
  exit;
}

$hashCupom = $_POST['hashCupom'];
$cupom = Cupom::getCupomPorHash($hashCupom);

// caso nao houver cupom com essa hash
if(empty($cupom)){
  echo json_encode($retorno);
  exit;
}

$cupom = $cupom[0];

// caso a qtd utilizada for maior que a disponivel
if($cupom->qtd_utilizado >= $cupom->qtd_disponivel){
  echo json_encode($retorno);
  exit;
}

// adiciona a hash do cupom na sessao
$_SESSION['carrinho']['cupom']              = $cupom->hash_cupom;
$_SESSION['carrinho']['valorDescontoCupom'] = $cupom->valor_desconto;

Cupom::atualizarSessaoValoresCupom();

$retorno['sucesso']   = true;
$retorno['hashCupom'] = $_SESSION['carrinho']['cupom'];

echo json_encode($retorno);
