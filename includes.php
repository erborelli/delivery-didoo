<?php

// Report all PHP errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

session_start();

// include banco
include(dirname(__FILE__).'/db.php');

// load de classes
spl_autoload_register(function ($class_name) {
  include dirname(__FILE__).'/dashboard/classes/' . $class_name . '.class.php';
});

require_once 'configuracao.php';

// responsável por setar todas variaveis do config do banco como globais no php
Config::setConfigGlobais();

$varsScript = '<script>
                var CAMINHO                   = "'.CAMINHO.'"
                var CAMINHO_DASHBOARD         = "'.CAMINHO_DASHBOARD.'"
                var PEDIR_CEP                 = "'.PEDIRCEP.'"
                var WHATSESTEBELECIMENTO      = "'.WHATSAPPESTABELECIMENTO.'"
                var COMPRARPELAVENDACELULAR   = "'.COMPRARPELAVENDACELULAR.'"
                var TEMPOREDIRECIONARWHATSAPP = "'.TEMPOREDIRECIONARWHATSAPP.'"
                var REDIRECIONARWHATSAPP      = "'.REDIRECIONARWHATSAPP.'"
                var URL_API_MOBILE            = "'.CAMINHO.'integracoes/api-mobile/"
                var URL_IMG_PRODUTOS          = "'.CAMINHO.'dashboard/images/produtos/"
                var CHAVE_API_MOBILE          = "112233"
              </script>';

?>
