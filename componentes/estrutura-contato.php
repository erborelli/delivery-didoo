<div class="listaToggle contato">
  <div class="row store-contact_us">
    <div class="col-xs-12 col-sm-12 col-md-6 store-contact_us-col-right">
      <span class="store-contact_us-lbl">Contato</span>
      <div class="store-contact_us-box store-telephone" data-cy="store-contact-us__telephone">
        <i class="material-icons">call</i>
        <span class="hidden-xs hidden-sm">
          <a class="linkWhats" target="_blank" href=""><?=WHATSAPPFORMATADO?></a>
        </span>
        <span class="visible-xs-inline-block visible-sm-inline-block phone-action">
          <a class="linkWhats" target="_blank" href=""><?=WHATSAPPFORMATADO?></a>
        </span>
      </div>
    </div>
  </div>
</div>
