<?php

include_once('../includes.php');

$dados = [];
$dados['email'] = $_POST['email'];
$dados['senha'] = $_POST['senha'];

$retorno = Login::realizarLogin($dados);

if($retorno){
    Login::logarUsuario();
    $_SESSION['usuario']['dados'] = Usuario::getDadosUsuario($dados['email']);
    $retorno = true;
}

echo json_encode($retorno);
