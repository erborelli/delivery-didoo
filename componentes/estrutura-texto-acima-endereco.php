
<div class="loyaltyprogram-progressbar-box js-loyalty-program-box" data-cy="loyaltyprogram__box">
  <div class="infobox-title"><?=$dadosHome['tituloPromocional']?></div>
  <div class="infobox-description truncated">
    <?=$dadosHome['textoPromocional']?>
  </div>
<div class="infobox-subtitle js-infobox-subtitle">
  <div class="order-done" data-cy="loyaltyprogram__order--done">
  </div>
</div>
<span class="is-hidden js-value_base_count">20</span>
</div>
