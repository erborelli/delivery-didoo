<?php

require_once('../include.php');

// insere item na mesa
if(isset($_GET['inserir-mesa'])){
    //$pedido = json_decode($_POST);
    // $retornoPedido = Pedido::gravarPedidoMesa($pedido);
    echo(json_encode($_POST,true));
    http_response_code(200);
    return;
}

//insere pedido
if(isset($_GET['inserir-pedido'])){
    $retornoPedido = Pedido::gravarPedidoMobile($_POST);
    echo(json_encode($retornoPedido,true));
    http_response_code(200);
    return;
}

//deleta registro
if(count($_DELETE)){
    echo('delete');
    http_response_code(200);
    return;
}

//atualiza registro
if(count($_PUT)){
    $pedido = json_decode($_PUT);
    var_dump($pedido);
    http_response_code(200);
    return;
}

//retorna pedido por id
if(isset($_GET['id']) && isset($_GET['id-cliente'])){
    $pedido = Pedido::getDadosPedido($_GET['id'],$_GET['id-cliente']);
    echo(json_encode($pedido,true));
    http_response_code(200);
    return;
}

//retorna todos os pedidos do cliente
if(isset($_GET['id-cliente']) && (intval($_GET['id-cliente'])>0)){
    $pedidos = Pedido::getPedidosPorCliente(intval($_GET['id-cliente']));
    echo(json_encode($pedidos,true));
    http_response_code(200);
    return;
}

http_response_code(401);
json_encode(['mensagem'=>'parametros incorretos ou vazio'],true);
return;