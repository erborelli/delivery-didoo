<?php

class ArquivoImpressao{

    /**
     * gera arquivo com informações do pedido, para impressão
     */
    public static function gerarPedidoImpressao($idPedidoInserido){
        $pastaDestino   = CAMINHO_SISTEMA.'pedidos-para-impressao/';
        $pedidoArray    = Pedido::getPedidoComItens($idPedidoInserido,true);
        $formatoArquivo = 'xml';
        $arquivoFinal   = '';
        
        switch($formatoArquivo){
            case 'xml':
                $arquivoFinal = self::gerarXmlPedido($pedidoArray);
                break;
            case 'json':
                $arquivoFinal = json_encode($pedidoArray);
                break;
        }
        self::gravarArquivo($arquivoFinal,$pastaDestino,$formatoArquivo,$idPedidoInserido);
    }

    /**
     * grava arquivo em uma pasta
     */
    private static function gravarArquivo($conteudo,$pastaDestino,$formato,$nomeArquivo){
        $arquivo = fopen($pastaDestino.$nomeArquivo.'.'.$formato,'w');
        if ($arquivo == false) die('Não foi possível criar o arquivo.');
        fwrite($arquivo, $conteudo);
        fclose($arquivo);
    }

    private static function gerarXmlPedido(array $array){

        $entrega = $array['agendamento'] == 's' ? 'true' : 'false';

        $xml = '<order>
                    <instructions>'.'Troco para '.$array['troco'].'</instructions>
                    <missed_reason/>
                    <billing_details/>
                    <fulfillment_option/>
                    <id>'.$array['id'].'</id>
                    <total_price>'.$array['total_pedido'].'</total_price>
                    <sub_total_price>'.$array['subtotal'].'</sub_total_price>
                    <tax_value>'.$array['taxa_entrega'].'</tax_value>
                    <persons>0</persons>
                    <latitude>0</latitude>
                    <longitude>0</longitude>
                    <client_first_name>'.$array['nome_cliente'].'</client_first_name>
                    <client_last_name></client_last_name>
                    <client_email></client_email>
                    <client_phone>'.$array['whats_cliente'].'</client_phone>
                    <restaurant_name>'.NOMELOJA.'</restaurant_name>
                    <currency>BRL</currency>
                    <type>delivery</type>
                    <status>accepted</status>
                    <source></source>
                    <pin_skipped>true</pin_skipped>
                    <accepted_at>'.$array['data_pedido'].'</accepted_at>
                    <tax_type>NET</tax_type>
                    <tax_name>Sales Tax</tax_name>
                    <fulfill_at></fulfill_at>
                    <reference/>
                    <restaurant_id></restaurant_id>
                    <client_id></client_id>
                    <updated_at>'.$array['data_pedido'].'</updated_at>
                    <restaurant_phone>'.WHATSAPPFORMATADO.'</restaurant_phone>
                    <restaurant_timezone>America/Sao_Paulo</restaurant_timezone>
                    <company_account_id></company_account_id>
                    <pos_system_id></pos_system_id>
                    <restaurant_key></restaurant_key>
                    <restaurant_country>Brazil</restaurant_country>
                    <restaurant_city>'.CIDADE.'</restaurant_city>
                    <restaurant_state>'.ESTADO.'</restaurant_state>
                    <restaurant_zipcode>'.CEP.'</restaurant_zipcode>
                    <restaurant_street>'.ENDERECO.'</restaurant_street>
                    <restaurant_latitude></restaurant_latitude>
                    <restaurant_longitude></restaurant_longitude>
                    <client_marketing_consent>true</client_marketing_consent>
                    <restaurant_token/>
                    <gateway_transaction_id/>
                    <gateway_type/>
                    <api_version>2</api_version>
                    <payment>'.$array['tipo_pagamento'].'</payment>
                    <for_later>'.$entrega.'</for_later>
                    <client_address>'.$array['endereco'].'</client_address>
                    <client_address_parts>
                    <street>'.$array['endereco'].'</street>
                    <city></city>
                    <more_address>'.$array['ponto_referencia'].'</more_address>
                    </client_address_parts>';
        
        $xml .= self::gerarXmlPedidoItem($array);
        $xml .= '</order>';
        return $xml;
    }

    private static function gerarXmlPedidoItem(array $array){
        $xml = '';
        foreach($array['produtos'] as $item){
            $total_item_price = ($item['valor'] * $item['qtd']);
            $xml .= '<item>
                        <id>'.$item['id'].'</id>
                        <name>'.$item['nome_produto'].'</name>
                        <total_item_price>'.$total_item_price.'</total_item_price>
                        <price>'.$item['valor'].'</price>
                        <quantity>'.$item['qtd'].'</quantity>
                        <instructions>'.$item['observacoes'].'</instructions>
                        <type>item</type>
                        <type_id></type_id>
                        <tax_rate>0</tax_rate>
                        <tax_value>0</tax_value>
                        <parent_id/>
                        <item_discount>0</item_discount>
                        <cart_discount_rate>0</cart_discount_rate>
                        <cart_discount>0</cart_discount>
                        <tax_type>NET</tax_type>';

            $xml .= self::gerarXmlPedidoItemAdicional($item);
            $xml .= '</item>';
        }
        return $xml;
    }

    private static function gerarXmlPedidoItemAdicional(array $array){
        $xml='';
        foreach($array['adicionais'] as $adicional){
            $xml .= '<option>
                        <id>'.$adicional['id'].'</id>
                        <name>'.$adicional['nome'].'</name>
                        <price>'.$adicional['preco'].'</price>
                        <group_name>adicionais</group_name>
                        <quantity>1</quantity>
                        <type>option</type>
                        <type_id></type_id>
                    </option>';
        }
        return $xml;
    }
}