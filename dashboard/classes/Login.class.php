<?php

class Login {

    public static function getHashSessaoUsuario(){
        return isset($_SESSION['usuario']) && isset($_SESSION['usuario']['hash']) ? true : '';
    }

    public static function verificarUsuarioLogado(){
        if(self::getHashSessaoUsuario() && $_SESSION['usuario']['logado'] == true){
            return true;
        }
        return false;
    }

    public static function realizarLogin($dados){
        if(!isset($dados['email']) || !isset($dados['senha'])){
            return 'Insira o e-mail e senha!';
        }

        $link = getConnection();
        $dados['senha'] = md5($dados['senha']);
        try {
            $sql  = "SELECT id FROM usuarios WHERE email = :email AND senha = :senha";
            $stmt = $link->prepare($sql);
            $stmt->bindParam(':email', $dados['email']);
            $stmt->bindParam(':senha', $dados['senha']);
            $stmt->execute();
            return $stmt->fetch();
        }
        catch (PDOException $e) {
            return false;
        }
    }

    public static function logarUsuario(){
        $_SESSION['usuario'] = [];
        $_SESSION['usuario']['hash']   = [];
        $_SESSION['usuario']['logado'] = true;
    }

    public static function redirecionarLogin(){
        // caso quem chamou for o ajax, não valida login
        if(preg_match('/ajax/',$_SERVER['REQUEST_URI'])){
          return;
        }

        // caso não estiver na página de login (por que senão gera redirecionamntos multiplos) ele passa na validação
        if(!preg_match('/login/',$_SERVER['REQUEST_URI'])){
            // redireciona o usuário caso não houver a sessao logada
            if(!Login::verificarUsuarioLogado()){
                header('location: '.CAMINHO_DASHBOARD.'login.php');
            }
        }
    }

}
