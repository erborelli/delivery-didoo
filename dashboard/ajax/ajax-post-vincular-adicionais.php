<?php

include_once('../includes.php');

if(isset($_POST['tipoItens']) && isset($_POST['adicional']) && isset($_POST['tipoAdicional'])){

    $idAdicional = $_POST['adicional'];

    // produto
    if($_POST['tipoItens']=='produto'){

        // exclui vinculo de produtos desmarcados
        if(isset($_POST['itensDesmarcados'])){
            // loop pelos produtos desmarcados
            foreach($_POST['itensDesmarcados'] as $idItem){
                // exclui por tipo de adicional
                if($_POST['tipoAdicional']=='adicionais'){
                    ProdutoAdicional::excluirVinculorAdicionalPorIdProduto(intval($idItem),intval($idAdicional));
                }
                else if($_POST['tipoAdicional']=='gruposAdicionais'){
                    ProdutoAdicional::excluirVinculorAdicionalGrupoPorIdProduto(intval($idItem),intval($idAdicional));
                }
            }
        }
        
        if(isset($_POST['itens'])){
            // loop pelos produtos marcados
            foreach($_POST['itens'] as $idItem){
                // vircular adicional ao produto
                if($_POST['tipoAdicional']=='adicionais'){
                    ProdutoAdicional::excluirVinculorAdicionalPorIdProduto(intval($idItem),intval($idAdicional));
                    ProdutoAdicional::vincularAdicionalProduto(intval($idItem),intval($idAdicional));
                }
                
                // vincular grupo de adicional ao produto
                else if($_POST['tipoAdicional']=='gruposAdicionais'){
                    ProdutoAdicional::excluirVinculorAdicionalGrupoPorIdProduto(intval($idItem),intval($idAdicional));
                    ProdutoAdicional::vincularAdicionalGrupoAoProduto(intval($idItem),intval($idAdicional));
                }
            }
        }
        echo true;
        return;
    }

    // categoria
    else if($_POST['tipoItens']=='categoria'){

        // exclui vinculo de categoria desmarcadas
        if(isset($_POST['itensDesmarcados'])){
            // loop pelas categoria desmarcadas
            foreach($_POST['itensDesmarcados'] as $idItem){
                // exclui por tipo de adicional
                if($_POST['tipoAdicional']=='adicionais'){
                    CategoriaAdicional::excluirVinculoAdicionalPorIdCategoria(intval($idItem),intval($idAdicional));
                }
                else if($_POST['tipoAdicional']=='gruposAdicionais'){
                    CategoriaAdicional::excluirVinculoAdicionalGrupoPorIdCategoria(intval($idItem),intval($idAdicional));
                }
            }
        }

        if(isset($_POST['itens'])){
            // loop pelas categorias marcadas
            foreach($_POST['itens'] as $idItem){
                // vincular adicional a categoria
                if($_POST['tipoAdicional']=='adicionais'){
                    // remover todos os adicionais da categoria
                    CategoriaAdicional::excluirVinculoAdicionalPorIdCategoria(intval($idItem),intval($idAdicional));
                    CategoriaAdicional::vincularAdicionalCategoria(intval($idItem),intval($idAdicional));
                }
                
                // vincular grupo de adicionais a categoria
                else if($_POST['tipoAdicional']=='gruposAdicionais'){
                    CategoriaAdicional::excluirVinculoAdicionalGrupoPorIdCategoria(intval($idItem),intval($idAdicional));
                    CategoriaAdicional::vincularAdicionalGrupoAoCategoria(intval($idItem),intval($idAdicional));
                }
            }
        }
        echo true;
        return;
    }
}
echo false;
return;