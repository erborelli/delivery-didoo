<?php
include_once('../../../includes.php');

// data default
date_default_timezone_set('America/Sao_Paulo');
$dataFiltrarPedidos = isset($_GET['filtrarData']) ? $_GET['filtrarData'] : false;
$dataFiltrada       = isset($_GET['filtrarData']) ? $_GET['filtrarData'] : date('d-m-Y - d-m-Y');

$totalRegistros = 10;
$pagina         = isset($_GET['pagina']) ? $_GET['pagina'] : 0;
$pagianAtual    = $pagina == 0 ? 1 : $pagina;
$inicio         = $pagianAtual - 1;
$inicio         = $inicio * $totalRegistros;


$pedidos         = isset($_GET['filtrarData']) ? Pedido::getPedidosLimitadosPorData(0,999,$dataFiltrarPedidos) : Pedido::getPedidosLimitados($inicio,$totalRegistros);
$qtdTotalPedidos = Pedido::getTotalPedidos();

$botaoAnterior = '';
$botaoProximo  = '';
$paginaAtual   = '';
if(!isset($_GET['filtrarData'])){
  $paginaAtual = '<span class="pagAtual">Pag. Atual: '.$pagianAtual.'</span>';
  // botoes anterior / proximo
  $totalPaginas = $qtdTotalPedidos / $totalRegistros; // verifica o número total de páginas
  // Botões "Anterior e próximo"
  $anterior      = $pagianAtual -1;
  $proximo       = $pagianAtual +1;
  if ($pagianAtual>1) {
    $botaoAnterior = "<a href='?pagina=$anterior'><button class='botoesPaginacao'><< Página Anterior</button></a>";
  }
  if ($pagianAtual < $totalPaginas) {
    $botaoProximo  = "<a href='?pagina=$proximo'><button class='botoesPaginacao'>Próxima Página >></button></a>";
  }
}

$selectStatusPedido = '';

function retornarStatusPedidoPorId($idStatus){
  $possiveisStatus = [1 => 'pendente',
                      2 => 'aceito',
                      3 => 'negado',
                      4 => 'cancelado'];
  return $possiveisStatus[$idStatus];
}

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pedidos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pedidos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Modal -->
    <div class="modal fade" id="modalRelatorio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div style="max-width:1200px;" class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Relatório Pedidos:</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <div style="margin-top:20px;margin-left:20px;">
              <label>Filtrar Por Data:</label>
              <input id="dateRelatorioPedidos" class="filtrarDataRelatorioPedidos" type="text" value="<?=date('d-m-Y - d-m-Y')?>">
              <button data-id-entregador="" class="filtraPorDataRelatorioPedidos" style="display:none;" type="button" name="button">Filtrar</button>
            </div>

          <div id="conteudoEntregas">
            <style>
            /* css etiqueta */
            html,body{
              margin:0px;
              padding:0px;
            }
            .table td, .table th {
              padding: .75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }
            .text-center {
              text-align: center!important;
            }
            .modal-body {
              position: relative;
              -ms-flex: 1 1 auto;
              flex: 1 1 auto;
              padding: 1rem;
            }
            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }
            tr {
                display: table-row;
                vertical-align: inherit;
                border-color: inherit;
                text-align:center;
            }
            .totalLoja{
              padding-left:60px !important;
              text-align: left;
            }
            .totalEntregas{
              text-align: right;
            }
            </style>
            <div class="modal-body text-center relatorioPedidos">
              <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
            </div>
          </div>
          <div class="modal-footer">
            <div class="containerImpressaoEntregadores">
              <i class="fas fa-print">&nbsp;&nbsp;</i>Imprimir
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

  <!-- Modal -->
  <div class="modal fade" id="modalEtiqueta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="width:345px;margin:0 auto;">
        <div class="modal-header">
          <!-- <h5 class="modal-title" id="exampleModalLabel">Etiqueta</h5> -->
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center" id="conteudoEtiqueta">

          <style>
          /* css etiqueta */
          html,body{
            margin:0px;
            padding:0px;
          }
          .etiqueta{
            width: 289px;
            height: auto;
            margin:0 7px;
            overflow: hidden;
            z-index: 1;
            float:left;
            font-family: arial;
          }
          .etiqueta .bloco:first-child{
            margin-top:0;
          }
          .etiqueta .bloco{
            width: 96%;
            font-size:15px;
            color:#C00;
            border: 1px solid #000;
            padding: 5px;
            margin: 5px 0 0 0;
            position:relative;
          }
          .etiqueta .bloco .txtEtiqueta{
            color:#000;
            margin:3px 0 0 0;
          }
          .etiqueta .bloco .imgEtiqueta span.inativo{
            display:none;
          }
          .etiqueta .bloco .imgEtiqueta span.ativo{
            display: table-cell;
          }
          .etiqueta .bloco .imgEtiqueta span{
            vertical-align:middle;
            display:table-cell;
          }
          .etiqueta .bloco .imgEtiqueta{
            width: 100%;
            height:110px;
            text-align: center;
            display:table;
            vertical-align:middle;
          }
          .etiqueta .bloco .imgEtiqueta img{
            vertical-align:middle;
          }
          .etiqueta .tracejado{
            width: 100%;
            border-bottom: 2px dashed #000;
            margin: 5px 0 0 0;
          }
          .impressora{
            float: right;
            cursor: pointer;
          }
          .selos{
            text-align: left;
            float: left;
          }
          @media print {
            .selos, .impressora {
              visibility: hidden;
            }
          }

          .fancyVerEtiquetaMultiestoque{
            padding-left: 30px;
            font-size: 11px;
            color: blue;
            cursor: pointer;
          }
          .fancybox-slide--iframe .fancybox-content{
            overflow: hidden !important;
            border-radius: 10px !important;
          }
          </style>

          <!-- conteudo etiqueta -->
          <div style="width: 260px;max-width:100%;max-height: 900px;height: auto;" class=" pace-done">
            <div class="pace  pace-inactive">
              <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
              </div>
              <div class="pace-activity"></div>
            </div>
            <div id="fancyTamanhoEtiqueta" style="text-align:left;">
              <div class="etiqueta" style="width: 260px;">
                <div class="bloco">
                  <div class="txtEtiqueta"><b>Pedido:</b> <span class="idPedido"></span></div>
                  <div class="txtEtiqueta"><b>Data:</b> <span class="dataHoraPedido"></span></div>
                </div>
                <div class="tracejado"></div>
                <div class="bloco">
                  <div class="imgEtiqueta">
                    <span>
                      <img src="<?=CAMINHO?>dashboard/images/logoDashboard/<?=LOGODASHBOARD?>" width="150">
                    </span>
                  </div>
                </div>
                <div class="bloco">
                  <div class="txtEtiqueta text-center"><b>Dados Cliente:</b></div>
                  <div class="txtEtiqueta" style="padding-top: 5px;"><b>Nome:</b> <span class="nomeCliPedido"></span></div>
                  <div class="txtEtiqueta"><b>Contato:</b> <span class="telCliPedido"></span></div>
                  <div class="txtEtiqueta"><b>Endereco:</b> <span class="endCliPedido"></span></div>
                  <div class="txtEtiqueta"><b>Itens Pedido:</b>
                    <div class="itensPedido"></div>
                  </div>
                  <div class="txtEtiqueta"><b>Pagamento: </b><span class="pagamentoCliPedido"></span></div>
                  <div class="txtEtiqueta"><b>Total: </b>R$ <span class="valorCliPedido"></span></div>
                  <div class="txtEtiqueta"><b>Troco: </b><span class="trocoCliPedido"></span></div>
                </div>
              </div>

              <div style="width: 250px; float:left; margin: 10px;">
                <div class="impressora">
                  <i class="fas fa-print"></i>
                </div>
              </div>
            </div>
            <div style="clear:both"></div>
          </div>
          <!-- fim conteudo etiqueta -->
        </div>
      </div>
    </div>
  </div>

    <!-- Main content -->
    <!-- busca / filtros -->
    <div class="col-sm-12 d-flex">
      <div class="col-sm-8">
        <div class="row">
          <div style="padding:3px;margin-right:25px;">
            <label data-toggle="modal" data-target="#modalRelatorio" class="retirarRelatorioPedidos">Gerar Relatório</label>
          </div>
          <form class="" method="GET">
            <div class="form-group">
              <label>Filtrar Por Data:</label>
              <input id="date" class="dataFiltrar js-schedule-hour-input input-field enabled" type="text" value="<?=$dataFiltrada?>">
              <?php if(isset($_GET['filtrarData'])){
                  echo '<a href="'.CAMINHO_DASHBOARD.'painel/pages/pedidos/"><span class="removerFiltro text-right" type="button" name="button">Remover Filtro</span></a>';
              }?>
            </div>
          </form>
        </div>
      </div>
      <div class="col-sm-4 text-right" style="right:20px;">
        <div class="imprimirConteudoProdutos" style="cursor:pointer;width:100px;display:inline;">
          <i class="fas fa-print">&nbsp;&nbsp;</i>Imprimir
        </div>
      </div>
    </div>

    <section class="content" id="containerProdutos">
      <style>
      .card {
    box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
    margin-bottom: 1rem;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-header {
    background-color: transparent;
    border-bottom: 1px solid rgba(0,0,0,.125);
    padding: .75rem 1.25rem;
    position: relative;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 0 solid rgba(0,0,0,.125);
}
.p-0 {
    padding: 0!important;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.table:not(.table-dark) {
    color: inherit;
}
.card-body>.table {
    margin-bottom: 0;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
    background-color: transparent;
}
table {
    border-collapse: collapse;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(0,0,0,.05);
}
.tr-pedido-aceito{
  background-color: #9aff9a !important;
  font-weight: bold !important;
}
tr small{
  font-weight: bold !important;
}
.tr-pedido-pendente{
  background-color: #ffd17c !important;
  font-weight: bold !important;
}
.tr-pedido-negado{
  background-color: #ff9f9f !important;
  font-weight: bold !important;
}
.tr-pedido-cancelado{
  background-color: #ff9f9f !important;
  font-weight: bold !important;
}
*, ::after, ::before {
    box-sizing: border-box;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
.table:not(.table-dark) {
    color: inherit;
}
.projects td {
    vertical-align: middle;
}
.small, small {
    font-size: 80%;
    font-weight: 400;
}
.table td, .table th {
    padding: .75rem;
    /* vertical-align: top; */
    border-top: 1px solid #dee2e6;
}
      </style>
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Pedidos</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                    <th style="width: 2%">
                      #
                    </th>
                    <th style="width: 5%">
                      Nº Pedido
                    </th>
                    <th style="width: 25%">
                        Endereço
                    </th>
                    <th style="width: 12%">
                        Nome Cliente
                    </th>
                    <th style="width: 20%">
                        Itens Pedido
                    </th>
                    <th style="width: 10%">
                        Total Pedido
                    </th>
                    <th style="width: 10%" class="text-center">
                        Data Pedido
                    </th>
                    <th style="width: 8%" class="text-center">
                      Status
                    </th>
                  </tr>
              </thead>
              <tbody>


                  <?php
                    // entregadores cadastrados
                    $entregadores = Entregador::getEntregadores();

                    foreach ($pedidos as $key => $value) {
                      // box entregadores
                      $value->troco = $value->troco == 0 ? 'Não' : $value->troco;
                      $idEntregador    = $value->id_entregador;
                      $boxEntregadores = '<option value="0">Entregadores</option>';
                      foreach ($entregadores as $chave => $valor) {
                        $selectedEntregador = $valor->id == $idEntregador ? 'selected' : '';
                        $boxEntregadores .= '<option '.$selectedEntregador.' value="'.$valor->id.'">'.$valor->nome.'</option>';
                      }

                      $selectStatusPedido = Pedido::getSelectStatusPedidoPorId($value->id_status);

                      $itensPedido       = PedidoItem::getItensPedido($value->id);
                      $boxItensPedido    = '';
                      $saboresFormatados = '';

                      //produtos do pedido
                      foreach ($itensPedido as $item){
                        $boxItensPedido .= '<br>'.$item->qtd.' - '.$item->nome_produto;
                        //adicionais do produto
                        $adicionaisProduto = PedidoItemAdicional::getAdicionaisItemPedidoArray($item->id);

                        if(count($adicionaisProduto)>0){
                          $adicionaisStr = '';
                          foreach($adicionaisProduto as $adicional){
                            $adicionaisStr .= $adicional['nome'].', ';
                          }
                          $adicionaisStr   = substr($adicionaisStr,0,-2);
                          $boxItensPedido .= '<small><li><b>Adicionais:</b> '.$adicionaisStr.'</li></small>';
                        }

                        //verificar sabores em caso de pizza
                        if(Produto::verificarProdutoPizza($item->id_produto)){
                          $sabores = PedidoItemSabor::getSaboresItemPedido($item->id);
                          foreach($sabores as $sabor){
                            $saboresFormatados.=$sabor['sabor'].', ';
                          }
                          $saboresFormatados = '<small><li><b>Sabores:</b> '.substr($saboresFormatados,0,-2).'</li></small>';
                        }

                        // verificar observacções
                        $observacao = '';
                        if(isset($item->observacoes) && !empty($item->observacoes)){
                          $observacao = '<small><li><b>Observação:</b> '.$item->observacoes.'</li></small><br>';
                        }
                      }
                      $pontoReferencia = '';
                      if(isset($value->ponto_referencia) && !empty($value->ponto_referencia)){
                        $pontoReferencia = '<br>Ponto Referência: '.$value->ponto_referencia;
                      }

                      $valorPedido = number_format($value->total_pedido,2);
                      $dataPedido = date("d/m/Y H:i:s", strtotime($value->data_pedido));
                      echo '<tr id-pedido="'.$value->id.'" class="tr-container-linha-pedido tr-pedido-'.retornarStatusPedidoPorId($value->id_status).'">
                              <td data-id-pedido="'.$value->id.'" style="cursor:pointer" class="slideDown">
                                <i class="fas fa-angle-down"></i>
                              </td>
                              <td>'.$value->id.'</td>
                              <td><small>'.$value->endereco.$pontoReferencia.'</small><br></td>
                              <td class="project-state text-left">
                                  <span class="">'.$value->nome_cliente.'</span>
                              </td>
                              <td class="project-actions">'.
                                $boxItensPedido.
                                $saboresFormatados.
                                $observacao.
                              '</td>
                              <td class="project-actions">
                                <span>'.$valorPedido.'</span>
                              </td>
                              <td class="project-actions text-center">
                                <span>'.$dataPedido.'</span>
                              </td>
                              <td class="project-actions text-center">
                                <select data-id-status="'.$value->id_status.'" class="selectStatusPedido">
                                  '.$selectStatusPedido.'
                                </select>
                              </td>
                            </tr>
                            <tr style="display:none;" class="containerInfosExtras'.$value->id.'">
                              <td colspan="9" style="padding:unset;">
                                <table style="width:100%;">
                                  <tbody>
                                    <tr style="background: white;">
                                      <th style="width:15%">Entregador</th>
                                      <th style="width:14%">Taxa Entrega</th>
                                      <th style="width:14%">Contato</th>
                                      <th style="width:14%">Etiqueta</th>
                                      <th style="width:14%">Cupom</th>
                                      <th style="width:14%">Origem</th>
                                      <th style="width:14%">Troco</th>
                                    </tr>
                                    <tr style="background: white;">
                                      <td>
                                        <select data-id-pedido="'.$value->id.'" class="selectEntregadores">
                                          '.$boxEntregadores.'
                                        </select>
                                      </td>
                                      <td>'.$value->taxa_entrega.'</td>
                                      <td>'.$value->whats_cliente.'</td>
                                      <td>
                                        <span data-id-pedido="'.$value->id.'" data-toggle="modal" data-target="#modalEtiqueta" class="etiquetaAncora">etiqueta</span>
                                      </td>
                                      <td>'.$value->cupom_utilizado.'</td>
                                      <td>'.$value->origem.'</td>
                                      <td>'.$value->troco.'</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>';
                      }
                    ?>

              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <div class="col-sm-12 d-flex flex-row-reverse containerPaginacao">
      <div class="col-sm-8">
        <div>
          <?=$botaoAnterior?>
          <?=$paginaAtual?>
          <?=$botaoProximo?>
        </div>
      </div>
    </div>


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=CAMINHO_DASHBOARD?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=CAMINHO_DASHBOARD?>vendor/bootstrap/js/popper.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=CAMINHO_DASHBOARD?>painel/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=CAMINHO_DASHBOARD?>painel/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="<?=CAMINHO_DASHBOARD?>painel/plugins/summernote/summernote-bs4.min.js"></script>
<!-- date range picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- Page Script -->
<?php
  $dataPieces = explode(' - ',$dataFiltrada);
?>
<script>
  $(function() {
    $('#date').daterangepicker({
      opens: 'left',
      minYear: 2020,
      maxYear: parseInt(moment().format('YYYY'),10),
      startDate: '<?=$dataPieces[0]?>',
      endDate  : '<?=$dataPieces[1]?>',
      locale: {
        format: 'DD/MM/YYYY'
      }
    }, function(start, end, label) {
      var dataFiltrar  = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
      dataFiltrar = dataFiltrar.split("/").join("-");
      window.location.href = location.protocol + '//' + location.host + location.pathname + '?filtrarData=' + dataFiltrar;
    });
  });
  $(function() {
    $('#dateRelatorioPedidos').daterangepicker({
      opens: 'left',
      minYear: 2020,
      maxYear: parseInt(moment().format('YYYY'),10),
      startDate: '<?=$dataPieces[0]?>',
      endDate  : '<?=$dataPieces[1]?>',
      locale: {
        format: 'DD/MM/YYYY'
      }
    }, function(start, end, label) {
      var dataFiltrar  = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
      atualizarRelatorioProtudos(dataFiltrar);
    });
  });
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })

  $('input[name="dates"]').daterangepicker();
</script>
</body>
</html>
