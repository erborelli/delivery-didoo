ALTER TABLE `pedido`
	ADD COLUMN `tipo_entrega` ENUM('entrega','retirada') NULL DEFAULT 'entrega' AFTER `tipo_pagamento`;
	
INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('informarEndereco', 'cep', 'cep / digitando');
INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('oferecerRetirada', 'n', 'oferecer retirada de produtos no local (s/n)');

UPDATE `config` SET `hash`='rua', `valor`='Rua auzl', `obs`='nome da rua do local' WHERE  `hash`='endereco' AND `obs` IS NULL LIMIT 1;
INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('numero', '123', 'numero do endereço');
INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('bairro', 'jardim planalto', NULL);
