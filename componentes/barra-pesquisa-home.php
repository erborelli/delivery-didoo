<div id="ItemsSearchBar" class="items-search-bar items-search-bar-home col-desktop-9" data-component="ItemsSearchBar">
  <div>
    <div class="header-fixed js-header-fixed hidden-md hidden-lg without-promo-box-mobile">
      <div class="mobile-header js-mobile-header">
        <div id="CategoriesFilter" class="categories-filter js-categories-hidden dropdown float-left">
          <select name="" id="" class="mobile-dropdown-menu js-list-mobile form-control">
            <?=$boxFiltroCategoriaMobile?>
          </select>
        </div>
        <button class="js-return-button return-button btn hidden"><i class="icon material-icons color-settings-secondary">arrow_back</i></button>
        <div class="hidden js-search-hidden items-search">
          <input type="text" data-ga="cardapio-item-search" class="buscarProdutoNome" placeholder="Busque por um produto">
          <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
          <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
        </div>
        <button class="js-search-button search-button btn">
          <i class="icon material-icons js-show-search color-settings-secondary">search</i>
        </button>
      </div>
    </div>

    <div id="CategoriesFilter" class="categories-filter dropdown float-left js-categories-filter-body" data-facebooktab=":eq(1)">
      <div class="item1 conteudoAbaMobile">
        <select name=""  class="font-size-18 mobile-dropdown-menu js-list-mobile  hidden-md hidden-lg form-control filtro-cabecalho-mobile" data-ga-action="menu__filter_category" data-ga-category="menu">
          <?=$boxFiltroCategoriaMobile?>
        </select>
        <button class="dropdown-toggle js-dropdown hidden-sm hidden-xs " type="button" data-toggle="dropdown" data-ga-action="menu__filter_category" data-ga-category="menu" aria-haspopup="true" aria-expanded="true" value="Filtrar catálogo">
          <span class="js-selected">Filtrar catálogo</span>
          <span class="caret color-settings-secondary"></span>
        </button>
        <ul class="js-list dropdown-menu categories-filter__dropdown-menu hidden-sm hidden-xs" aria-labelledby="CategoriesFilter" data-ga-action="menu__filter_category_select" data-ga-category="menu">
          <?=$boxFiltroCategoria?>
        </ul>
      </div>

      <!-- item 2 (Area de entrega) -->
      <div class="item2 conteudoAbaMobile">
        <div class="conteudoMenuItem">
          <span>Áreas de Entrega</span>
          <div class="voltarHome">
            <svg fill="#fff" width="30px" height="30px" id="Layer_1" style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path stroke="#fff" stroke-width="0.5" d="M21.146,8.576l-7.55-6.135c-0.925-0.751-2.267-0.751-3.192,0c0,0,0,0,0,0L2.855,8.575C2.59,8.79,2.439,9.108,2.439,9.448  v11.543c0,0.62,0.505,1.13,1.125,1.13h5.062c0.62,0,1.125-0.51,1.125-1.13v-7.306h4.499v7.306c0,0.62,0.505,1.13,1.125,1.13h5.062  c0.62,0,1.125-0.51,1.125-1.13V9.448C21.561,9.108,21.41,8.79,21.146,8.576z M20.436,20.997h-5.062V13.68  c0-0.62-0.505-1.119-1.125-1.119H9.75c-0.62,0-1.125,0.499-1.125,1.119v7.317H3.564V9.448l7.55-6.134  c0.513-0.418,1.26-0.417,1.773,0l7.55,6.134V20.997z"></path></svg>
            <span>Voltar Home</span>
          </div>
        </div>
      </div>
      <!-- item 3 (Contato) -->
      <div class="item3 conteudoAbaMobile">
        <div class="conteudoMenuItem">
          <span>Contato</span>
          <div class="voltarHome">
            <svg fill="#fff" width="30px" height="30px" id="Layer_1" style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path stroke="#fff" stroke-width="0.5" d="M21.146,8.576l-7.55-6.135c-0.925-0.751-2.267-0.751-3.192,0c0,0,0,0,0,0L2.855,8.575C2.59,8.79,2.439,9.108,2.439,9.448  v11.543c0,0.62,0.505,1.13,1.125,1.13h5.062c0.62,0,1.125-0.51,1.125-1.13v-7.306h4.499v7.306c0,0.62,0.505,1.13,1.125,1.13h5.062  c0.62,0,1.125-0.51,1.125-1.13V9.448C21.561,9.108,21.41,8.79,21.146,8.576z M20.436,20.997h-5.062V13.68  c0-0.62-0.505-1.119-1.125-1.119H9.75c-0.62,0-1.125,0.499-1.125,1.119v7.317H3.564V9.448l7.55-6.134  c0.513-0.418,1.26-0.417,1.773,0l7.55,6.134V20.997z"></path></svg>
            <span>Voltar Home</span>
          </div>
        </div>
      </div>
      <!-- item 4 (Sobre Nos) -->
      <div class="item4 conteudoAbaMobile">
        <div class="conteudoMenuItem">
          <span>Sobre Nós</span>
          <div class="voltarHome">
            <svg fill="#fff" width="30px" height="30px" id="Layer_1" style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path stroke="#fff" stroke-width="0.5" d="M21.146,8.576l-7.55-6.135c-0.925-0.751-2.267-0.751-3.192,0c0,0,0,0,0,0L2.855,8.575C2.59,8.79,2.439,9.108,2.439,9.448  v11.543c0,0.62,0.505,1.13,1.125,1.13h5.062c0.62,0,1.125-0.51,1.125-1.13v-7.306h4.499v7.306c0,0.62,0.505,1.13,1.125,1.13h5.062  c0.62,0,1.125-0.51,1.125-1.13V9.448C21.561,9.108,21.41,8.79,21.146,8.576z M20.436,20.997h-5.062V13.68  c0-0.62-0.505-1.119-1.125-1.119H9.75c-0.62,0-1.125,0.499-1.125,1.119v7.317H3.564V9.448l7.55-6.134  c0.513-0.418,1.26-0.417,1.773,0l7.55,6.134V20.997z"></path></svg>
            <span>Voltar Home</span>
          </div>
        </div>
      </div>

    </div>
    <div class="js-items-search-bar-fixed items-search-bar-fixed hidden-md hidden-lg lupaMobileConteudoMenu">
      <button class="js-return-button-fixed return-button btn hidden"><i class="icon material-icons color-settings-secondary">arrow_back</i></button>
      <div id="SearchBar" class="hidden js-search-hidden-fixed items-search">
        <input type="text" class="buscarProdutoNome" data-ga="cardapio-item-search" placeholder="Busque por um produto">
        <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
        <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
      </div>
      <button class="js-search-button-fixed search-button btn">
        <i class="icon material-icons js-show-search color-settings-secondary">search</i>
      </button>
    </div>

    <div class="hidden-sm hidden-xs items-search">
      <input type="text" class="buscarProdutoNome" data-ga-action="menu__search_submit" data-ga-category="menu" placeholder="Busque por um produto">
      <i class="icon material-icons icon-search color-settings-secondary padding-right-10">search</i>
      <i class="icon material-icons js-clear color-settings-secondary font-size-20 padding-right-10 is-hidden">cancel</i>
    </div>
    <div class="items-search-result-ga-click js-items-search-result-click" data-ga-action="menu__search_select_result" data-ga-category="menu"></div>

  </div>
</div>
<div class="clearfix"></div>
