<?php

class Impressao{
    public static function imprimirPedido($idPedido){
        if(defined('GERARARQUIVOIMPRESSAO') && GERARARQUIVOIMPRESSAO=='s'){
            ArquivoImpressao::gerarPedidoImpressao($idPedido);
        }
    }
}