<?php
include_once('../../../includes.php');

if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/');
}

// caso houver o post da ação da edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  Frete::updateFrete($_POST);
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/logistica/');
}

$idFrete = $_GET['id'];
$frete   = Frete::getFretePorId($idFrete);
$frete   = $frete[0];

// caso tenha retornado false
if(!$frete){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/logistica/');
}

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Logística</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Logística</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
      <div class="col-md-12">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Cadastrar Bairro / Preço</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group col-md-4" style="float:left;">
              <label for="bairro">Bairro</label>
              <input type="text" name="bairro" id="bairro" class="form-control" value="<?= $frete->bairro ?>">
            </div>
            <div class="form-group col-md-4" style="float:left;">
              <label for="valor">Preço</label>
              <input type="text" name="valor" id="valor" class="form-control" value="<?= $frete->valor ?>">
            </div>
            <div class="form-group col-md-4" style="float:left;top:20px;">
              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>

          </div>
        </div>

      </div>

    </form>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
