INSERT INTO `pedido_status` (`id`, `status`, `obs`) VALUES
	(1, 'pendente', 'aguardando o estabelecimento aceitar o pedido'),
	(2, 'aceito', 'aceito pelo estabelecimento'),
	(3, 'negado', 'negado pelo estabelecimento'),
	(4, 'cancelado', 'cancelado pelo cliente');
/*!40000 ALTER TABLE `pedido_status` ENABLE KEYS */;

ALTER TABLE `pedido`
	ADD COLUMN `id_status` INT(11) NOT NULL DEFAULT 1 AFTER `id_entregador`;


ALTER TABLE `pedido`
	ADD COLUMN `observacao` VARCHAR(500) NULL DEFAULT NULL AFTER `origem`;