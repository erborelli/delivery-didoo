
document.getElementById("print-table").addEventListener("click", function(){
    table.print(false, true)
})

document.getElementById("download-xlsx").addEventListener("click", function(){
    table.download("xlsx", "data.xlsx", {sheetName:"pedidos"})
})


let tabledata = JSON.parse(document.getElementById('dados').textContent)

var table = new Tabulator("#tblPedidos",{
    data:tabledata,
    columns:[
        {title:"Loja", field:"loja", headerFilter:"input"},
        {title:"Data Pedido", field:"data_pedido", headerFilter:"input"},
        //{title:"Código", field:"id", headerFilter:"input"},
        //{title:"id_entregador", field:"id_entregador", headerFilter:"input"},
        //{title:"id_status", field:"id_status", headerFilter:"input"},
        //{title:"visualizado", field:"visualizado", headerFilter:"input"},
        {title:"Cliente", field:"nome_cliente", headerFilter:"input"},
        //{title:"Whats Cliente", field:"whats_cliente", headerFilter:"input"},
        //{title:"Forma Pagamento", field:"tipo_pagamento", headerFilter:"input"},
        {title:"R$ Total", field:"total_pedido", headerFilter:"input" ,bottomCalc:"sum", bottomCalcParams:{precision:2}},
        {title:"R$ Subtotal", field:"subtotal", headerFilter:"input",bottomCalc:"sum", bottomCalcParams:{precision:2}},
        {title:"R$ Entrega", field:"taxa_entrega", headerFilter:"input",bottomCalc:"sum", bottomCalcParams:{precision:2}},
        //{title:"Agendado", field:"agendamento", headerFilter:"input"},
        //{title:"Cupom", field:"cupom_utilizado", headerFilter:"input"},
        //{title:"Troco", field:"troco", headerFilter:"input"},
        //{title:"Origem", field:"origem", headerFilter:"input"},
        //{title:"Retirada", field:"retirada", headerFilter:"input"},
        //{title:"Observação", field:"observacao", headerFilter:"input"},
        //{title:"Endereço", field:"endereco", headerFilter:"input"},
        //{title:"Data Agendamento", field:"data_agendamento", headerFilter:"input"},
        //{title:"Tipo Entrega", field:"tipo_entrega", headerFilter:"input"},
        //{title:"Referencia", field:"ponto_referencia", headerFilter:"input"},
    ],
    pagination:"local",
    paginationSize:20,
    paginationSizeSelector:[20, 100, 300, 1000],
    // autoColumns:true,
    // persistence:{
    //     sort:true,
    //     filter:true,
    //     columns:true,
    // },
})