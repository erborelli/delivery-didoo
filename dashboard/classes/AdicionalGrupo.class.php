<?php

class AdicionalGrupo{

  public static function inserirAdicionalNoGrupo($idAdicional,$idGrupo){
    $sql  = "INSERT INTO adicionais_grupo(id_adicional_grupo,id_adicional) VALUES(?,?)";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idGrupo,$idAdicional]);
  }

  public static function removerAdicionalDoGrupo($idAcicional,$idGrupo){
    $sql  = "DELETE FROM adicionais_grupo WHERE id_adicional_grupo=? AND id_adicional=?";

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idGrupo,$idAcicional]);
  }

  public static function updateAdicionalGrupo($post){
    $sql  = "UPDATE adicional_grupo SET ordem=?,ativo=?,nome=?,qtdMinima=?,qtdMaxima=? WHERE id=?";

    $link = getConnection();
    $stmt = $link->prepare($sql);
    return $stmt->execute([$post['ordem'],$post['ativo'],$post['nome'],$post['qtdMinima'],$post['qtdMaxima'],$post['id']]);
  }

  public static function gravarAdicionalGrupo($post){
    $sql   = "INSERT INTO adicional_grupo(ordem,ativo,nome,qtdMinima,qtdMaxima) VALUES(?,?,?,?,?);";
    $ativo = isset($post['ativo']) ? $ativo='s':$ativo='n';

    $link = getConnection();
    $stmt = $link->prepare($sql);
    return $stmt->execute([$post['ordem'],$ativo,$post['nome'],$post['qtdMinima'],$post['qtdMaxima']]);
  }

  /**
   * retorna todos os Adicionais Grupo
   */
  public static function getGrupos($array=false){
    $sql  = "SELECT *,DATE_FORMAT(data_criacao,'%d/%m/%Y %H:%i') AS data_criacao_formatada FROM adicional_grupo ORDER BY ordem;";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute();

    if($array){
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    return $stmt->fetchAll(PDO::FETCH_CLASS,'adicionalGrupo');
  }

  public static function getGrupoPorId($idAdicionalGrupo, $array=false){
    $sql  = "SELECT * FROM adicional_grupo WHERE id=?";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idAdicionalGrupo]);

    $adicionalGrupo = null;
    if($array){
      $adicionalGrupo = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else{
      $adicionalGrupo = $stmt->fetchAll(PDO::FETCH_CLASS,'adicionalGrupo');
    }
    return isset($adicionalGrupo[0]) ? $adicionalGrupo[0] : $adicionalGrupo;
  }

  public static function getAdicionaisPorIdAdicionalGrupo($idAdicionalGrupo, $array=false){
    $sql = 'SELECT a.* FROM adicionais_grupo as ag
            INNER JOIN adicional as a
            ON a.id = ag.id_adicional
            WHERE ag.id_adicional_grupo = ?';

    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$idAdicionalGrupo]);

    if($array){
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Adicional');
  }
}
