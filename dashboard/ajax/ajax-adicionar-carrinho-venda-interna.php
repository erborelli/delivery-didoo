<?php

include_once('../includes.php');

if(!isset($_SESSION['venda_interna']['carrinho'])){
  $_SESSION['venda_interna']['carrinho'] = [];
}

// se for produto cadastrado
if(isset($_POST['idProduto'])){
  $produto = Produto::getProdutoArray($_POST['idProduto']);
  $produto[0]['quantidade'] = $_POST['quantidade'];;

  //se for pizza
  if(isset($_POST['tipo']) && isset($_POST['sabores']) && $_POST['tipo']=='pizza'){
    $produto[0]['sabores'] = json_decode($_POST['sabores'],true);
    $produto[0]['valor']   = PizzaSabores::getValorPizzaVendaInterna($produto[0]['sabores']);
  }

  //se tiver adicionais
  if(isset($_POST['adicionais'])){
    $produto[0]['adicionais'] = json_decode($_POST['adicionais'],true);
    $produto[0]['valor']     += Adicional::getValorAdicionaisVendaInterna($produto[0]['adicionais']);
  }

  $_SESSION['venda_interna']['carrinho'][] = $produto;
}



// se for produto custom
else{
  $nomes       = [];
  $valores     = [];
  $quantidades = [];

  //recebe array com produtos custons
  $produtosCustom = json_decode($_POST['produtosCustom'],true);

  //monta array com nomes, valores e quantidades dos produtos recebidos
  forEach($produtosCustom as $produtoCustom){
    if($produtoCustom['name']=='nomeProdutoCustom'){
      $nomes[] = $produtoCustom['value'];
    }
    if($produtoCustom['name']=='valorProdutoCustom'){
      $valores[] = $produtoCustom['value'];
    }
    if($produtoCustom['name']=='quantidadeProdutoCustom'){
      $quantidades[] = $produtoCustom['value'];
    }
  }

  //monta array de produtos,['quantidade','nome','valor']
  $produtosArray = [];
  forEach($nomes as $key => $nome){

    $produto = Produto::getProdutoCustomArray();

    $produto[0]['quantidade'] = $quantidades[$key];
    $produto[0]['nome']       = $nomes[$key];
    $produto[0]['valor']      = $valores[$key];

    $produtosArray[] = $produto[0];
  }
  $_SESSION['venda_interna']['carrinho'][] = $produtosArray;
}