<?php

class Frete {

    public static function getFretes(){
      $link = getConnection();
      $sql  = "SELECT * FROM frete";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Frete');
    }

    public static function salvarFrete($dados){
      if(empty($dados)) return false;
      $link = getConnection();
      $sql  = "INSERT INTO frete (bairro,valor) VALUES (?,?)";
      $stmt = $link->prepare($sql);
      $stmt->execute([trim($dados['bairro']),$dados['valor']]);
    }

    public static function updateFrete($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE frete SET bairro=?,valor=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([trim($dadosPost['bairro']),$dadosPost['valor'],$_GET['id']]);
      return true;
    }

    public static function getFretePorId($idFrete){
        if(!isset($idFrete) || !is_numeric($idFrete) || empty($idFrete)){
          return false;
        }
        $link = getConnection();
        $sql  = "SELECT * FROM frete WHERE id = :idFrete";
        $stmt = $link->prepare($sql);
        $stmt->bindParam(':idFrete', $idFrete);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'Frete');
    }

    public static function getValorEntregaBairro($bairro){
      $link = getConnection();
      $sql  = "SELECT * FROM frete WHERE bairro = :bairro";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':bairro', $bairro);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Frete');
    }

}
