<?php
  $visibilidadeCategoria = CATEGORIASHOME == 'fechada' ? 'style="display:none;"' : 'style="display:block"';
?>
<?php if(!empty($dadosHome['destaques1']) || !empty($dadosHome['destaques2'])){ ?>

  <div class="col-mobile-12 col-desktop-9 medium-transition listaToggle cardapio active">
    <div class="js-featured-items-content featured-items clearfix js-featured-items featured-items-desktop" style="display:inline-block;">
      <div class="featured-items__head js-featured-items-head products-section expandCategoria" data-ga="featured-items-title">
        <h2 class="title d-inline" style="vertical-align:super;display:inline;">Destaques</h2>
        <span class="material-icons">expand_more</span>
      </div>

      <?php if(!empty($dadosHome['destaques1'])){ ?>

      <div class="featured-items__body" <?=$visibilidadeCategoria?> >
        <div class="featured-items__body__content products-section">
          <ul class="featured-items-list">
            <?php

              $destaques1 = explode(',',$dadosHome['destaques1']);

              foreach ($destaques1 as $key => $value) {
                $produto = Produto::getProduto($value);
                if(!$produto) continue;
                $produto = $produto[0];

                // foto produto
                $fotoProduto = !empty($produto->foto_produto) ? CAMINHO_IMAGEM.'produtos/'.$produto->foto_produto : '';
                $labelProduto       = $key == 0 ? 'Mais pedido' : 'Sugestão da Casa';
                $classeBotaoProduto = $key == 0 ? 'MOST_ORDERED' : 'CUSTOM';

                echo '<li id="items-list-item-'.$produto->id.'" class="items-list-item js-featured-item-tooltip items-list-featured items-list-featured--'.$classeBotaoProduto.' mdl-button__ripple-container mdl-js-ripple-effect js-add-cart js-click-item items-list-item--mobile"
                      title="" data-status="ACTIVE" data-price="'.$produto->valor.'" data-description="'.$produto->descricao.'" data-category_id="'.$produto->id_categoria.'" data-id="'.$produto->id.'" data-item-name="'.$produto->nome.'" data-name="'.$produto->nome.'">
                      <div class="item-content">
                        <div class="item__left-side">
                          <p class="title">'.$produto->nome.' </p>
                          <div>
                            <p class="description">'.$produto->nome.'</p>
                          </div>
                          <p>
                          </p>
                          <div class="prices-container">
                            <div class="to-price">
                              <span class="price-description">
                              </span>
                              <span class="price-value">
                                R$'.$produto->valor.' </span>
                            </div>
                          </div>
                          <p></p>
                        </div>
                        <div class="item__right-side ">
                          <img src="'.$fotoProduto.'" width="100" height="100" alt="Pão na chapa" data-src="'.$fotoProduto.'" loading="lazy" class="mdl-js-ripple-effect--ignore-events border-r-10">
                        </div>
                      </div>
                      <span class="add-btn">
                        <i class="material-icons">add</i>
                      </span>
                      <div class="featured-tag featured-'.$classeBotaoProduto.'" data-id="247555">
                        '.$labelProduto.' </div>
                      <span class="mdl-ripple"></span>
                    </li>';
              }

            ?>
          </ul>
        </div>
      </div>

    <?php } ?>
    <?php if(!empty($dadosHome['destaques2'])){ ?>

      <div class="featured-items__body" <?=$visibilidadeCategoria?>>
        <div class="featured-items__body__content products-section" style="padding-top:10px;">
          <ul class="featured-items-list">

            <?php

              $destaques2 = explode(',',$dadosHome['destaques2']);

              foreach ($destaques2 as $key => $value) {
                $produto = Produto::getProduto($value);
                if(!$produto) continue;
                $produto = $produto[0];

                // foto produto
                $fotoProduto        = !empty($produto->foto_produto) ? CAMINHO_IMAGEM.'produtos/'.$produto->foto_produto : '';
                $labelProduto       = $key == 0 ? 'Mais pedido' : 'Sugestão da Casa';
                $classeBotaoProduto = $key == 0 ? 'MOST_ORDERED' : 'CUSTOM';
                echo '<li id="items-list-item-'.$produto->id.'" class="items-list-item js-featured-item-tooltip items-list-featured items-list-featured--'.$classeBotaoProduto.' mdl-button__ripple-container mdl-js-ripple-effect js-add-cart js-click-item items-list-item--mobile"
                      title="" data-status="ACTIVE" data-description="'.$produto->descricao.'" data-category_id="'.$produto->id_categoria.'" data-id="'.$produto->id.'" data-item-name="'.$produto->nome.'" data-name="'.$produto->nome.'" data-price="'.$produto->valor.'">
                      <div class="item-content">
                        <div class="item__left-side">
                          <p class="title">'.$produto->nome.' </p>
                          <div>
                            <p class="description">'.$produto->descricao.'</p>
                          </div>
                          <p>
                          </p>
                          <div class="prices-container">
                            <div class="to-price">
                              <span class="price-description">
                              </span>
                              <span class="price-value">
                                R$'.$produto->valor.' </span>
                            </div>
                          </div>
                          <p></p>
                        </div>
                        <div class="item__right-side ">
                          <img src="'.$fotoProduto.'" width="100" height="100" alt="Pão na chapa" data-src="'.$fotoProduto.'" loading="lazy" class="mdl-js-ripple-effect--ignore-events border-r-10">
                        </div>
                      </div>
                      <span class="add-btn">
                        <i class="material-icons">add</i>
                      </span>
                      <div class="featured-tag featured-'.$classeBotaoProduto.'" data-id="247555">
                        '.$labelProduto.' </div>
                      <span class="mdl-ripple"></span>
                    </li>';
              }
            ?>

          </ul>
        </div>
      </div>

      <?php } ?>

    </div>
  </div>
<?php } ?>
