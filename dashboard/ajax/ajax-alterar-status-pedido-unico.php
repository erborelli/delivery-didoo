<?php

include_once('../includes.php');

if(!isset($_POST['idPedido']) || !isset($_POST['acao']) ||
   !is_numeric($_POST['idPedido']) || empty($_POST['acao'])) return false;

$obsPedido = isset($_POST['obsPedido']) && !empty($_POST['obsPedido']) ? $_POST['obsPedido'] : '';

if($_POST['acao'] == 'cancelar'){
  Pedido::cancelarPedido($_POST['idPedido'],$obsPedido);
}else {
  Pedido::aprovarPedido($_POST['idPedido'],$obsPedido);
}
