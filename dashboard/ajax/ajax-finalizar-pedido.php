<?php

include_once('../includes.php');

// time zone SP
date_default_timezone_set('America/Sao_Paulo');

$retorno = ['mensagem'                  => '',
            'sucesso'                   => false,
            'agendamentoComLojaFechada' => false,
            'mensagemErro'              => '',
            'subMensagemErro'           => ''];

if(empty($_POST['nome']) || empty($_POST['cel'])){
  $retorno['mensagemErro'] = 'Verifique os dados preenchidos!';
  echo json_encode($retorno);exit;
}

// verifico se já não existe um pedido na sessao pendente de ser aceito
if(Pedido::verificarPedidosNaSessao()){
  $retorno['mensagemErro'] = 'Você já possui um pedido pendente!';
  $retorno['subMensagemErro'] = 'Não é possível realizar outro pedido enquanto houver 1 pedido pendente!';
  echo json_encode($retorno);exit;
}

// validações do troco
$labelTroco        = '';
$_SESSION['troco'] = 0;
if(!empty($_POST['valorTroco']) && $_POST['valorTroco'] > 0){
  $_POST['valorTroco'] = str_replace(',','.',$_POST['valorTroco']);
  $labelTroco          = 'Troco: Sim |Troco para: R$'.$_POST['valorTroco'].' |';
  $_SESSION['troco']   = $_POST['valorTroco'];
}else{
  $labelTroco = 'Troco: Não |';
}

// seta as informações do usuário que vieram no post na sessao
$_SESSION['cliente'] = ['nome'           => $_POST['nome'],
                        'contato'        => $_POST['cel'],
                        'formaPagamento' => $_POST['pagamento']
                      ];

// faz verificações se existem as informações na sessao
if(isset($_SESSION['endereco']) && isset($_SESSION['carrinho']) && isset($_SESSION['cliente'])){
  $retorno['sucesso']  = true;

  $pedido = '';
  foreach ($_SESSION['carrinho']['itens'] as $key => $value) {
    $produto = unserialize($value);
    $extensaoSabores    = Pedido::montarSaboresEnviarWhatsapp($produto);
    $extensaoAdicionais = Pedido::montarAdicionaisEnviarWhatsapp($produto);
    $pedido .= $produto->quantidade.'un - '.$produto->nome.' - R$'.Cupom::calcularValorProduto($produto->valor).' |'.$extensaoSabores.$extensaoAdicionais.'|';
  }

  // caso for um pedido agendado
  $extensaoAgendamento = '';
  if(isset($_SESSION['carrinho']['agendamento'])){
    // caso a loja estiver fechada
    if(!Atendimento::getAtendimentoLoja()['aberta']) $retorno['agendamentoComLojaFechada'] = true;
    $dataFormatada      = date("d/m/Y", strtotime($_SESSION['carrinho']['agendamento']['dataAgendamento']));
    $horaAgendamento    = $_SESSION['carrinho']['agendamento']['horario'];
    $extensaoAgendamento = 'Gostaria de agendar um pedido para o dia '.$dataFormatada.' ás '.$horaAgendamento.', |';
  }

  // caso for retirada no local
  $retiradaNaLoja = '';
  if(isset($_SESSION['endereco'])        &&
     isset($_SESSION['endereco']['cep']) &&
     !empty($_SESSION['endereco'])       &&
     $_SESSION['endereco']['cep'] == CEP) $retiradaNaLoja = '(Retirada no local)';

  // caso foi utilizado algum cupom
  $cupomUtilizado     = '';
  $valorDescontoCupom = '';
  if(Cupom::temCupomNaSessao()){
    $cupomUtilizado     = 'Cupom utilizado: '.$_SESSION['carrinho']['cupom'].'|';
    $valorDescontoCupom = 'Valor do desconto: R$'.$_SESSION['carrinho']['totalDescontoCupom'].'|';
  }

  // grava o pedido no banco e retorna o id do respectivo item criado
  $idPedido  = Pedido::gravarPedido();

  $retorno['mensagem'] = '### NOVO PEDIDO ### |Olá '.NOMELOJA.', |'
                          .$extensaoAgendamento
                          .'Nº Pedido: '.$idPedido.' |'
                          .'Cliente: '.$_SESSION['cliente']['nome'].', |'
                          .'Endereço: '.$_SESSION['endereco']['logradouro'].', '
                          .$_SESSION['endereco']['numero'].', '.$_SESSION['endereco']['bairro'].', '
                          .$_SESSION['endereco']['localidade'].' - '.$_SESSION['endereco']['uf'].' ||'
                          .'### VALORES ### |'
                          .'Forma Pag: '.$_SESSION['cliente']['formaPagamento'].' |'
                          .'Taxa Entrega: R$'.$_SESSION['carrinho']['frete'].' '.$retiradaNaLoja.' |'
                          .$cupomUtilizado
                          .$valorDescontoCupom
                          .'Valor Total: R$'.$_SESSION['carrinho']['total'].' |'
                          .$labelTroco.'|'
                          .'### PRODUTOS PEDIDO ### |'
                          .$pedido;

  // seto na sessao a mensagem que vai ser enviada também pelo telegram
  $_SESSION['mensagemPedidoTelegram'] = $retorno['mensagem'];

  // grava o cliente que está fazendo o pedido
  $idCliente = Cliente::insertCliente();
  // caso houver um cupom na sessao salva a utilização do mesmo
  if(isset($_SESSION['carrinho']['cupom'])){
    $utilizacaoCupom = Cupom::incrementarCupom();
    Cupom::inserirCupomUtilizacao($idPedido,$idCliente,$utilizacaoCupom);
  }

  $dadosClienteRecorrente = [];
  $dadosClienteRecorrente['endereco'] = $_SESSION['endereco'];
  $dadosClienteRecorrente['cliente']  = $_SESSION['cliente'];
  $tempoExpiracao                     = time() + (10 * 365 * 24 * 60 * 60);
  $_SESSION['pedidos'][$idPedido]     = 1;
  setcookie("clienteRecorrente", serialize($dadosClienteRecorrente), $tempoExpiracao, '/');

  $auxFrete = $_SESSION['carrinho']['frete'];

  unset($_SESSION['valoresProduto']);
  // unset no troco
  unset($_SESSION['carrinho']['troco']);
  // depois de enviar a mensagem devo destruir a seção e mostrar uma mensagem de pedido efetuado
  unset($_SESSION['carrinho']);

  $_SESSION['carrinho']['frete']    = $auxFrete;
  $_SESSION['carrinho']['subtotal'] = 0;

}

echo json_encode($retorno);
