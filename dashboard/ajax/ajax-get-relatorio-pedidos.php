<?php

include_once('../includes.php');

if(!isset($_POST['dataPedidos'])) return false;

date_default_timezone_set('America/Sao_Paulo');

if(isset($_POST['dataPedidos']) && !empty($_POST['dataPedidos'])){
  // echo "<pre>"; print_r($_POST['dataPedidos']); echo "</pre>";
  $dataEntrega = $_POST['dataPedidos'];
  $datasPieces = explode(' - ',$dataEntrega);
  foreach ($datasPieces as $key => $value) {
    $value = str_replace('/','-',$value);
    $dataEntregaFormatada = date("Y-m-d", strtotime($value));
    if($key == 0){
      $dataEntrega = $dataEntregaFormatada.' - ';
    }else {
      $dataEntrega .= $dataEntregaFormatada;
    }
  }
}else {
  $dataEntrega = date('Y-m-d - Y-m-d');
}

$pedidos    = Pedido::getPedidosLimitadosPorData(0,999,$dataEntrega);

$boxEntregas = '<table class="table">
                  <thead>
                    <tr>
                      <th style="width:40%;" scope="col">Endereço</th>
                      <th style="width:20%;" scope="col">Taxa Entrega</th>
                      <th style="width:20%"  scope="col">Total Loja</th>
                      <th style="width:20%;" scope="col">Data Pedido</th>
                    </tr>
                  </thead>
                  <tbody>';

$totalEntregas = 0;
$totalLoja     = 0;
foreach ($pedidos as $key => $value) {
  $dataFormatada = date('d/m/Y H:i:s', strtotime($value->data_pedido));
  $totalEntregas += $value->taxa_entrega;
  $totalLoja     += $value->subtotal;
  $boxEntregas .= '<tr>';
  $boxEntregas .= '<td>'.$value->endereco.'</td>';
  $boxEntregas .= '<td>'.$value->taxa_entrega.'</td>';
  $boxEntregas .= '<td>'.$value->subtotal.'</td>';
  $boxEntregas .= '<td>'.$dataFormatada.'</td>';
  $boxEntregas .= '</tr>';
}
$boxEntregas .= '<tr>
                  <td class="totalEntregas" colspan="2"><b>Total Entregas: R$'.number_format($totalEntregas,2,',','.').'</b></td><br>
                  <td class="totalLoja" colspan="2"><b>Total Loja    : R$'.number_format($totalLoja,2,',','.').'</b></td>
                </tr>

                ';
$boxEntregas .= '</tbody></table>';

echo $boxEntregas;
