<?php

include_once('../includes.php');

$retorno = '';

if(isset($_POST) && isset($_POST['numero']) && is_numeric($_POST['numero'])){
  $_SESSION['venda_interna']['frete']['numero'] = $_POST['numero'];
  $retorno = $_POST['numero'];
}

echo json_encode($retorno);
