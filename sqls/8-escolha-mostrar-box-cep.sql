ALTER TABLE `config`
	ADD COLUMN `obs` VARCHAR(500) NULL DEFAULT NULL AFTER `valor`;

INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('pedirCep', 'depois', '\'antes\' mostrar antes do cliente selecionar o produto, \'depois\' mostrar na hora da finalização da compra');

