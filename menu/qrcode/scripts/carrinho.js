
if(COMPRARPELAVENDACELULAR=='s'){
    document.getElementById('dvCarrinho').style.display = ''
}else{
    document.getElementById('dvCarrinho').style.display = 'none'
}

function abrirCarrinho(){
    $('#carrinhoModal').modal('toggle')
}

// adiciona item no carrinho
function montarCarrinho(item){
    let tbody        = document.getElementById('tbodyCarrinho')
    let tr           = document.createElement('tr')
    let tdId         = document.createElement('td')
    let tdNome       = document.createElement('td')
    let tdQtd        = document.createElement('td')
    let tdAdicionais = document.createElement('td')
    let tdValor      = document.createElement('td')
    let tdRemover    = document.createElement('td')
    let btnRemover   = document.createElement('button')
    let iBtnRemover  = document.createElement('i')

    tdId.style.display = 'none'
    tdId.textContent   = item.produto.id
    tdNome.textContent = getNomeProduto(item.produto.nome,item.sabores)
    tdQtd.textContent  = item.quantidade
    tdQtd.style.paddingLeft  = '5px'
    tdAdicionais.textContent = getNomeAdicionais(item.adicionais)
    tdRemover.style.padding  = '0px'
    tdRemover.style.margin   = '0px'
    tdRemover.style.verticalAlign = 'middle'
    btnRemover.setAttribute('onclick','removerItemCarrinho(this)')
    btnRemover.className         = 'btn btn-danger'
    btnRemover.style.textAlign   = 'center'
    iBtnRemover.className        = 'fa fa-trash'

    setValorTotal(item.adicionais,item.sabores,item.produto.valor,tdValor)
    
    btnRemover.append(iBtnRemover)
    tdRemover.append(btnRemover)
    tr.append(tdId)
    tr.append(tdNome)
    tr.append(tdQtd)
    tr.append(tdAdicionais)
    tr.append(tdValor)
    tr.append(tdRemover)
    tbody.append(tr)
}


// remove item do carrinho
function removerItemCarrinho(btn){
    let tr        = btn.parentElement.parentElement
    let idItem    = tr.getElementsByTagName('td')[0].textContent
    let valorItem = tr.getElementsByTagName('td')[4].textContent
    tr.remove()

    let posicao = 0
    carrinho.forEach(i=>{
        if(i.produto.id == idItem){
            carrinho.splice([posicao],1)
        }
        posicao++
    })

    let valorTotalAnterior = Number(document.getElementById('lblValorTotalCarrinhoModal').textContent)
    document.getElementById('lblValorTotalCarrinhoModal').textContent = (Number(valorTotalAnterior) - Number(valorItem)).toFixed(2)
    document.getElementById('spnValorCarrrinho').textContent          = (Number(valorTotalAnterior) - Number(valorItem)).toFixed(2)
}

//retorna os nomes de adicionais separados com virgula
function getNomeAdicionais(adicionaisArray){
    let adicionaisStr = ''
    adicionaisArray.forEach(a => {
        adicionaisStr += a.nome+', '
    })
    return adicionaisStr.substring(-2,adicionaisStr.length-2)
}

function setValorTotal(adicionais,sabores,valorItem,tdValor){
    let lblValorTotal   = document.getElementById('lblValorTotalCarrinhoModal')
    let valorAdicionais = calcularValorAdicionais(adicionais)

    // se produto tiver sabores
    if(sabores.length>0){
        $.ajax({
            url     : URL_API_MOBILE+'sabor-pizza/?tipo-valor',
            type    : 'GET',
            dataType: 'JSON',
            headers: {
              'chave-acesso': CHAVE_API_MOBILE
            },
            success:function(data){
                let valorSabores          = calcularValorSabor(data.valor,sabores)
                tdValor.textContent       = ((Number(valorSabores)+Number(valorAdicionais)) * Number(document.getElementById('txtQuantidade').value)).toFixed(2)
                lblValorTotal.textContent = (Number(lblValorTotal.textContent) + (Number(tdValor.textContent))).toFixed(2)
                document.getElementById('spnValorCarrrinho').textContent = (lblValorTotal.textContent)
            }
        })
    }

    // se produto não tiver sabores
    else{
        tdValor.textContent       = ((Number(valorAdicionais) + Number(valorItem)) * Number(document.getElementById('txtQuantidade').value)).toFixed(2)
        lblValorTotal.textContent = (Number(lblValorTotal.textContent) + Number(tdValor.textContent)).toFixed(2)
        document.getElementById('spnValorCarrrinho').textContent = (lblValorTotal.textContent)
    }
}

function calcularValorAdicionais(adicionaisArray){
    let adicionaisTotal = 0
    adicionaisArray.forEach(a => {
        adicionaisTotal += Number(a.valor)
    })
    return adicionaisTotal
}

function calcularValorSabor(tipo,sabores){
    if(tipo=='maior'){
        let valorMaior = 0
        sabores.forEach(s=>{
            if(valorMaior < Number(s.valor)){
                valorMaior = Number(s.valor)
            }
        })
        return valorMaior
    }else if(tipo=='media'){
        let valorMedio = 0
        sabores.forEach(s=>{
            valorMedio += Number(s.valor)
        })
        return (valorMedio/sabores.length)
    }
}

// retorna nome de produto com os sabores
function getNomeProduto(nome, saboresArray){

    if(saboresArray.length != 0){
        nome += ' - '
        saboresArray.forEach(s => {
            nome += s.nome+', '
        })
        return nome.substring(-2,nome.length-2)
    }else{
        return nome
    }
}

function finalizarPedidoCarrinho(){

    let tbodyCarrinho = document.getElementById('tbodyCarrinho')
    let trs = tbodyCarrinho.getElementsByTagName('tr')

    // se tiver produtos no carrinho
    if(trs.length>0){
        //abrir modal com nome e wathsapp
        $('#finalizarModal').modal('toggle')
    
        $('#carrinhoModal').modal('toggle')
    }

    // se não tiver produtos no carrinho
    else{
        alert('Nenhum produto adicionado ao carrinho!')
    }

}