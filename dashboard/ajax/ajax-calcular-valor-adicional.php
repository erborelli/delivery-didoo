<?php

include_once('../includes.php');

if(!is_numeric($_POST['idAdicional']) || !is_numeric($_POST['idProduto'])) exit;

$valorAdicional = Adicional::getValorAdicional($_POST['idAdicional'])->preco;

// CASO AINDA NÃO FOI SETADO NA SESSION VALORES DO ADICIONAL EU SETO
if(!isset($_SESSION['valoresProduto']) || !isset($_SESSION['valoresProduto']['valoresAdicional'])){
  $_SESSION['valoresProduto']['valoresAdicional'] = [];
}

// CASO HOUVER OUTROS INDICES SALVOS NA SESSAO REFERENTES A OUTROS IDS, LIMPA ELES
if(!empty($_SESSION['valoresProduto']) &&
   !isset($_SESSION['valoresProduto']['valoresAdicional'][$_POST['idProduto']])) $_SESSION['valoresProduto']['valoresAdicional'] = [];

if($_POST['acao'] == 'add'){
  $_SESSION['valoresProduto']['valoresAdicional'][$_POST['idProduto']][$_POST['idAdicional']] = $valorAdicional;
}else {
  unset($_SESSION['valoresProduto']['valoresAdicional'][$_POST['idProduto']][$_POST['idAdicional']]);
}

$valorProduto = Produto::calcularValorProduto($_POST['idProduto']);

echo json_encode(number_format($valorProduto,2));
