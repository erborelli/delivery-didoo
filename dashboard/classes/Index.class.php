<?php

class Index {

  public static function montarLayoutSaboresPizza($sabores){
    if(empty($sabores)) return '';
    $layouotSabores = '';
    foreach ($sabores as $key => $value) {
      $value->label = ucwords($value->label);
      $layouotSabores .= Sistema::getLayout((array)$value,'layout/produtos/pizza','layout-modal-pizza-sabor-item.html');
    }
    return $layouotSabores;
  }

  public static function montarLayoutAdicionais($estruturaAdicionais, $idProduto){
    if(empty($estruturaAdicionais)) return '';
    $boxAdicionaisEGrupos = '';
    foreach ($estruturaAdicionais as $key => $grupoAdicional) {
      $layouotAdicionais  = '';
      foreach ($grupoAdicional->adicionais as $key => $adicionais) {
        $adicionais['label']           = ucwords($adicionais['nome_adicional']);
        $adicionais['descricao']       = '';
        $adicionais['preco_adicional'] = number_format($adicionais['preco_adicional'],2);
        $adicionais['id_produto']      = number_format($idProduto);
        $layouotAdicionais .= Sistema::getLayout((array)$adicionais,'layout/produtos/adicional','layout-modal-adicional-item.html');
      }
      $grupoAdicional->nome_grupo      = ucfirst($grupoAdicional->nome_grupo);
      $grupoAdicional->labelQtdMinMax  = self::montarLabelQtdMinMaxAdicionais($grupoAdicional->qtd_min_grupo,$grupoAdicional->qtd_max_grupo);
      $grupoAdicional->adicionais      = $layouotAdicionais;
      $boxAdicionaisEGrupos .= Sistema::getLayout((array)$grupoAdicional,'layout/produtos/adicional','layout-modal-adicional-grupo.html');
    }
    return $boxAdicionaisEGrupos;
  }

  public static function montarLabelQtdMinMaxAdicionais($qtdMin,$qtdMax){
    if(!is_numeric($qtdMin) || !is_numeric($qtdMax)) return '';
    $plural  = $qtdMax == 1 ? 'opção' : 'opções';
    $retorno = 'Selecione de '.$qtdMin.' até '.$qtdMax.' '.$plural;
    if($qtdMin == 0 && $qtdMax == 0){
      $retorno = '(Seleção opcional) Selecione quantos itens desejar!';
    }elseif($qtdMin === $qtdMax){
      $retorno = 'É obrigatório a seleção de '.$qtdMax.' '.$plural;
    }elseif ($qtdMin === 0) {
      $retorno = '(Seleção opcional) de até '.$qtdMax.' '.$plural;
    }
    return $retorno;
  }

  public static function getValorBasePizza($idProduto,$idTamanho){
    if(!is_numeric($idProduto) || !is_numeric($idTamanho)) return 0;
    $valoresSaboresPizza = PizzaSabores::getSaboresPizzaHome($idProduto,$idTamanho);
    if(empty($valoresSaboresPizza)) return 0;
    return $valoresSaboresPizza[0]->preco;
  }

  public static function getProdutosDestaqueHome($idsProdutosDestaque1,$idsProdutosDestaque2){
    // caso os dois destaques estiverem vazios
    if(empty($idsProdutosDestaque1) && empty($idsProdutosDestaque2)) return '';

    // organizando o array
    $destaques   = [];
    $destaques1  = explode(',',$idsProdutosDestaque1);
    $destaques2  = explode(',',$idsProdutosDestaque2);
    $destaques[] = $destaques1;
    $destaques[] = $destaques2;

    $visibilidadeCategoria = CATEGORIASHOME == 'fechada' ? 'style="display:none;"' : 'style="display:block"';
    $layoutDestaques       = '';
    // percorre os grupos dos destaques 1 e 2
    foreach ($destaques as $key => $produtos) {
      if(empty($produtos)) continue;
      $layoutItensDestaques = '';
      // percorre os produtos do destaque em questão
      foreach ($produtos as $chave => $idProduto) {
        $produto = Produto::getProduto($idProduto);
        if(!$produto) continue;
        $produto = $produto[0];
        // foto produto
        $produto->imagensProduto     = self::montarImagensProduto($produto->foto_produto, $produto->nome);
        $produto->preLabelValor      = $produto->tipo == 'pizza' ? 'A partir de: ' : '';
        $produto->valor              = $produto->tipo == 'pizza' ? self::getValorBasePizza($produto->id,$produto->id_tamanho) : $produto->valor;
        $produto->idCategoria        = $produto->id_categoria;
        $produto->idTamanho          = !is_numeric($produto->id_tamanho) ? 0 : $produto->id_tamanho;
        $produto->labelProduto       = $chave == 0 ? 'Mais pedido' : 'Sugestão da Casa';
        $produto->classeBotaoProduto = $chave == 0 ? 'MOST_ORDERED' : 'CUSTOM';
        $layoutItensDestaques       .= Sistema::getLayout((array)$produto,'layout/produtos/destaques','produtos-destaques-item.html');
      }
      $layoutDestaques   .= Sistema::getLayout(['itens' => $layoutItensDestaques],'layout/produtos/destaques','produtos-destaques-item-container.html');
    }
    return Sistema::getLayout(['destaques' => $layoutDestaques],'layout/produtos/destaques','produtos-destaques.html');
  }

  public static function montarImagensProduto($imagens, $nomeProduto){
    $imagens    = explode(',',$imagens);
    $boxImagens = '';
    foreach ($imagens as $key => $value) {
      $active = $key == 0 ? 'active' : '';
      $boxImagens .= Sistema::getLayout(['fotoProduto' => CAMINHO_IMAGEM.'produtos/'.$value, 'nome' => $nomeProduto, 'active' => $active],'layout/produtos/foto','produtos-foto-item.html');
    }
    return $boxImagens;
  }

  public static function getProdutosHome($categorias){
    // MONTA BOX PRODUTOS
    $boxCategorias = '';
    foreach ($categorias as $key => $categoria) {
      // traz os produtos da categoria que está sendo percorrida
      $produtosPorCategoria = Produto::getProdutosPorIdCategoria($categoria->id);
      $categoria->produtos  = '';
      foreach ($produtosPorCategoria as $chave => $produto) {
        // foto produto
        $produto->imagensProduto = self::montarImagensProduto($produto->foto_produto, $produto->nome);
        $produto->preLabelValor  = $produto->tipo == 'pizza' ? 'A partir de: ' : '';
        $produto->valor          = $produto->tipo == 'pizza' ? self::getValorBasePizza($produto->id,$produto->id_tamanho) : $produto->valor;
        $produto->idCategoria    = $categoria->id;
        $produto->idTamanho      = !is_numeric($produto->id_tamanho) ? 0 : $produto->id_tamanho;
        $categoria->produtos    .= Sistema::getLayout((array)$produto,'layout/produtos','produto-item.html');
      }

      $categoria->nomeCatMobile = preg_replace('/\|/', '|<br/>', $categoria->nome);
      if(empty($produtosPorCategoria)) continue;
      if(!empty($produtosPorCategoria)){
        $categoria->visibilidadeCategorias = CATEGORIASHOME == 'fechada' ? 'none' : 'block';
        $boxCategorias .= Sistema::getLayout((array)$categoria,'layout/produtos','estrutura-categorias.html');
      }
    }
    return Sistema::getLayout(['categorias'=>$boxCategorias],'layout/produtos','estrutura-produtos.html');
  }


}
