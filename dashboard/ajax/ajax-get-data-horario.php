<?php
include_once('../includes.php');

$tipoLayout = $_POST['tipoLayout'];
$retorno    = ['mensagemErro' => '', 'layout' => '', 'sucesso' => true];

if($tipoLayout == 'data'){
  $retorno['layout'] = Agendamento::getLayoutDiasAgendamento();
}else {
  if(is_numeric($_POST['idDia'])){
    $retorno['layout'] = Agendamento::getLayoutHorariosAgendamento($_POST['idDia'],$_POST['data']);
  }else {
    $retorno['mensagemErro'] = 'Selecione Primeiro a Data';
    $retorno['sucesso']      = false;
  }
}

echo json_encode($retorno);
