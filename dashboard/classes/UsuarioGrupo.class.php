<?php

class UsuarioGrupo {
    public static function getGrupos(){
        $link = getConnection();
        $sql  = "SELECT * FROM usuario_grupo";
        $stmt = $link->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,'UsuarioGrupo');
    }

    public static function getGrupoPorId($id){
      $sql  = "SELECT * FROM usuario_grupo WHERE id = ?";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute([$id]);
      return $stmt->fetchAll(PDO::FETCH_CLASS,'UsuarioGrupo');
    }

    public static function updateGrupo($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE usuario_grupo SET nome=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$_GET['id']]);
      return true;
    }

    public static function gravarGrupo($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $sql = "INSERT INTO usuario_grupo(nome) VALUES(?)";
      $link = getConnection();
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome']]);
      return true;
    }
}
