
let queryString = window.location.search
let urlParams   = new URLSearchParams(queryString)
var numeroMesa  = urlParams.get('mesa')
document.getElementById('txtMesaCliente').value = numeroMesa

getLogo()
carregarProdutos()

function getLogo(){
    $.ajax({
        url     : URL_API_MOBILE+'imagem/?logo',
        type    : 'GET',
        dataType: 'JSON',
        headers: {
            'chave-acesso': CHAVE_API_MOBILE
        },
        success: function(urlLogo){
            console.log(urlLogo)
            document.getElementById('imgLogo').setAttribute('src',urlLogo)
        },
        error: function(data){
            console.log(data);
        },
    })
}