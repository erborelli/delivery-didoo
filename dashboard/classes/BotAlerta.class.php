<?php

//require_once('../../db.php');
//require_once('../../dashboard/classes/Config.class.php');

class BotAlerta{
    private $chatId = ''; //-1001358550570
    private $token  = ''; //908112744:AAFGQKipuqgyeprx4KSw4vBilHO7yVIJAlU

    public function avisarPedido($mensagemPedido){
      $mensagemPedido = preg_replace('/\|/',PHP_EOL,$mensagemPedido);
      // $mensagemPedido = preg_replace('/#/','%23',$mensagemPedido);
      $this->enviarMensagem($mensagemPedido);
    }

    public function enviarMensagem($mensagem){
        if($this->getConfigBot()){
            $url = 'https://api.telegram.org/bot'.$this->token.
                    '/sendMessage?chat_id='.$this->chatId.
                    '&text='.urlencode($mensagem);
            $retorno = file_get_contents($url);
            return $this->tratarRetorno($retorno);
        }
        return false;
    }

    private function tratarRetorno($retornoJson){
        $retorno = json_decode($retornoJson,true);
        if(isset($retorno['ok']) && $retorno['ok'] == true){
            return true;
        }
        return false;
    }

    private function getConfigBot(){
        $retornoChatId = Config::getVariavelConfigHash('botChatId');
        $retornoToken  = Config::getVariavelConfigHash('botToken');

        if(isset($retornoChatId['valor'])){
            $this->chatId = $retornoChatId['valor'];
        }

        if(isset($retornoToken['valor'])){
            $this->token  = $retornoToken['valor'];
        }

        if($this->token != ''){
            return true;
        }
        return false;
    }
}

// $bot = new BotAlerta();
// $bot->enviarMensagem('teste com bd');
