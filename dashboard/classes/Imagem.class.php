<?php

class Imagem{
    public static function redimensionar($origem,$destino,$largura,$altura,$corBorda,$qualidade,$zoomCrop){
        THB::processar([
            'q'    => $qualidade,
            'w'    => $largura,
            'h'    => $altura,
            'cc'   => $corBorda,
            'zc'   => $zoomCrop,
            'src'  => $origem,
            'save' => $destino
        ]);
    }

    /**
     * @param $origem  - caminho de origem com nome e extensão da imagem
     * @param $destino - caminho de destino com nome e extensão da imagem
     */
    public static function redimensionarPadrao($origem,$destino){
        $qualidade = 100;
        $largura   = 675;
        $altura    = 300;
        $corBorda  = 'FFFFFF';
        $zoomCrop  = 1;
        self::redimensionar($origem,$destino,$largura,$altura,$corBorda,$qualidade,$zoomCrop);
    }
}