<?php

include_once('../includes.php');

$retorno = ['boxEndereco' => '', 'sucesso' => false];

// verifica se a loja está aberta
if(Atendimento::ALojaEstaFechadaESemAgendamento()){
  echo json_encode(['lojaFechada' => true]);exit;
}
if(isset($_POST) && isset($_POST['dados'])){
  $_SESSION['endereco']          = $_POST['dados'];
  $_SESSION['carrinho']['frete'] = $_POST['dados']['valorFrete'];
  $_SESSION['carrinho']['total'] = $_SESSION['carrinho']['subtotal'] + $_POST['dados']['valorFrete'];
  // caso houver o cookie de cliente recorrente atualiza ele
  if(isset($_COOKIE['clienteRecorrente'])){
    $tempoExpiracao  = time() + (10 * 365 * 24 * 60 * 60);
    $cookieExistente = unserialize($_COOKIE['clienteRecorrente']);
    $cookieExistente['endereco'] = $_SESSION['endereco'];
    setcookie('clienteRecorrente', serialize($cookieExistente), $tempoExpiracao, '/');
  }

  // aqui eu vou verificar se existe algum historico de produto na sessao do cliente
  if(isset($_SESSION['historico'])){
    // adiciono o id do produto no historico
    $retorno['historico'] = $_SESSION['historico'];
    // e logo apos excluo da sessao
    unset($_SESSION['historico']);
  }elseif (isset($_SESSION['historicoFinalizacao'])) {
    // adiciono o id do produto no historico
    $retorno['historicoFinalizacao'] = true;
    // e logo apos excluo da sessao
    unset($_SESSION['historicoFinalizacao']);
  }

  $cep = $_POST['dados']['cep'];
  if($cep == '00000-000'){
    $cep = '';
  }
  $retorno['sucesso']        = true;
  setcookie('enderecoSessao', serialize($_POST['dados']), time() + (86400 * 30 * 7), '/');
  $retorno['endereco']       = $_POST['dados']['logradouro'].',
                             '.$_POST['dados']['numero'].',
                             '.$_POST['dados']['bairro'].',
                             '.$_POST['dados']['localidade'].',
                             '.$_POST['dados']['uf'].',
                             '.$cep;
}

echo json_encode($retorno);
