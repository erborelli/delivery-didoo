ALTER TABLE `pedido`
	ADD COLUMN `desconto_cupom` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `taxa_entrega`;
	
INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('mostrarEnderecoMobile', 'n', 'mostrar endereço mobile (s/n)');
