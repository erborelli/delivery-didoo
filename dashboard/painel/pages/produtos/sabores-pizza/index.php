<?php
include_once('../../../../includes.php');

$saboresPizza = PizzaSabores::getSaboresPizza();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sabores (Pizza / Esfiha)</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sabores (Pizza / Esfiha)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/sabores-pizza/criar.php">
        <i class="fas fa-plus"></i> Novo Sabor
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Produtos - Sabores (Pizza / Esfiha)</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 5%">
                          #
                      </th>
                      <th style="width: 25%">
                          Sabor
                      </th>
                      <th style="width: 25%">
                          Descrição / Ingredientes
                      </th>
                      <th style="width: 25%">
                          Tamanho / Preço
                      </th>
                  </tr>
              </thead>
              <tbody>

                <?php

                  foreach ($saboresPizza as $key => $value) {
                    $tamanhosPizzaAtual      = PizzaSabores::getTamanhosValoresPizzaPorIdSabor($value->id);
                    $boxTamanhosValoresPizza = '';
                    foreach ($tamanhosPizzaAtual as $chave => $valor) {
                      $boxTamanhosValoresPizza .= $valor->label.': R$'.$valor->preco.'<br>';
                    }
              echo  '<tr>
                      <td>#</td>
                      <td><a>'.$value->label.'</a><br></td>
                      <td><a>'.$value->descricao.'</a><br></td>
                      <td class="project_progress">
                        '.$boxTamanhosValoresPizza.'
                    </td>

                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'painel/pages/produtos/sabores-pizza/editar.php?id='.$value->id.'">
                              <i class="fas fa-pencil-alt"></i>Editar
                          </a>
                          <a data-id="'.$value->id.'" data-modulo="pizza_sabores" class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"></i>Excluir
                          </a>
                      </td>
                  </tr>';
                }
                  ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/produtos/sabores-pizza/criar.php">
        <i class="fas fa-plus"></i> Novo Sabor
      </a>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
<script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>

<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
