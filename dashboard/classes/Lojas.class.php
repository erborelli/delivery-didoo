<?php

class Lojas{
    public static function getLojas(){
      $sql  = "SELECT * FROM lojas";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getLojaPorId($id){
      $sql  = "SELECT * FROM lojas WHERE id=?";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute([$id]);
      $loja = $stmt->fetchAll(PDO::FETCH_ASSOC);

      if(isset($loja[0])){
        return $loja[0];
      }
      return $loja;
    }

    public static function gravarLoja($loja){
      if(!isset($loja) || empty($loja)) return false;
      $sql = "INSERT INTO lojas(nome,url) VALUES(?,?)";
      $link = getConnection();
      $stmt= $link->prepare($sql);
      $stmt->execute([$loja['nome'],$loja['url']]);
      return true;
  }

  public static function updateLoja($loja){
    if(!isset($loja) || empty($loja)) return false;
    $sql = "UPDATE lojas SET nome=?, url=? WHERE id=?";
    $link = getConnection();
    $stmt= $link->prepare($sql);
    $stmt->execute([$loja['nome'],$loja['url'],$loja['id']]);
    return true;
  }

  public static function excluirLoja($id){
    if(empty($id)) return false;
    $sql = "DELETE FROM lojas WHERE id=?";
    $link = getConnection();
    $stmt= $link->prepare($sql);
    $stmt->execute([$id]);
    return true;
  }
}