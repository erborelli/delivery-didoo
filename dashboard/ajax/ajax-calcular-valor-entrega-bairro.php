<?php

include_once('../includes.php');

// verifica se a loja está aberta
if(Atendimento::ALojaEstaFechadaESemAgendamento()){
  echo json_encode(['lojaFechada' => true]);exit;
}

$bairro     = $_POST['bairro'];
$dadosFrete = Frete::getValorEntregaBairro($bairro);

if($bairro == BAIRRO){
  echo json_encode(0);exit;
}

if(empty($dadosFrete)) return false;

$dadosFrete = $dadosFrete[0];

echo json_encode($dadosFrete->valor);
