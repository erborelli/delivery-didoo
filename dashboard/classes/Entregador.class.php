<?php

class Entregador {

    public static function getEntregadores(){
      $link = getConnection();
      $sql  = "SELECT * FROM entregador";
      $stmt = $link->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Entregador');
    }

    public static function updatePedidoEntregador($idEntregador,$idPedido){
      if((!isset($idEntregador) || !is_numeric($idEntregador)) ||
         (!isset($idPedido)     || !is_numeric($idPedido))) return false;
      $link = getConnection();
      $sql = "UPDATE pedido SET id_entregador=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$idEntregador,$idPedido]);
      return true;
    }

    public static function inserirEntregador($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "INSERT INTO entregador (nome) VALUES (?)";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome']]);
      return true;
    }

    public static function getEntregador($idEntregador){
      $link = getConnection();
      $sql  = "SELECT * FROM entregador WHERE id = :idEntregador";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idEntregador', $idEntregador);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Entregador');
    }

    public static function updateEntregador($dadosPost){
      if(!isset($dadosPost) || empty($dadosPost)) return false;
      $link = getConnection();
      $sql = "UPDATE entregador SET nome=? WHERE id=?";
      $stmt= $link->prepare($sql);
      $stmt->execute([$dadosPost['nome'],$_GET['id']]);
      return true;
    }

    public static function getEntregasPorIdEntregrador($idEntregador,$dataEntrega = ''){
      if(empty($dataEntrega)) $dataEntrega = date('Y-m-d - Y-m-d');
      $dataPieces  = explode(' - ',$dataEntrega);
      $dataInicial = $dataPieces[0];
      $dataFinal   = $dataPieces[1];
      $link = getConnection();
      $sql  = "SELECT * FROM pedido
               WHERE id_entregador = :idEntregador
               AND data_pedido BETWEEN '".$dataInicial." 00:00:00' AND '".$dataFinal." 23:59:59'";
      $stmt = $link->prepare($sql);
      $stmt->bindParam(':idEntregador', $idEntregador);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS,'Entregador');
    }
}
