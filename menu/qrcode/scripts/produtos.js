
// recebe todos os produtos ativos pra venda
function carregarProdutos(){
  $.ajax({
      url     : URL_API_MOBILE+'produto',
      type    : 'GET',
      dataType: 'JSON',
      headers: {
          'chave-acesso': CHAVE_API_MOBILE
      },
      success: function(data){
          montarCategorias(data)
      },
      error: function(data){
          console.log(data);
      },
  })
}


//listagem de produtos dentro da categoria
function montarProdutos(tdProduto, produto){
  
  produto.forEach(p => {
    let tr      = document.createElement('tr')
    let tdNome  = document.createElement('td')
    let tdFoto  = document.createElement('td')
    let tdValor = document.createElement('td')
    let tdSelecionar   = document.createElement('td')
    let btnSelecionar  = document.createElement('button')
    let iBtnSelecionar = document.createElement('i')
    let imagem  = document.createElement('img')

    // oculta valor de produto, utilizado na pizza
    let valorProduto = ''
    if(p.valor!=999.00){
      valorProduto = p.valor
    }

    // não mostra foto do produto caso seja a imagem padrão
    if(p.foto_produto!='/default.png'){
      imagem.style.height       = '50px'
      imagem.style.width        = '50px'
      imagem.style.borderRadius = '5px'
      imagem.setAttribute('src',URL_IMG_PRODUTOS+p.foto_produto)

      // se venda-celular permitir compra, abrir modal no click da imagem
      if(COMPRARPELAVENDACELULAR=='s'){
        imagem.onclick = function(){mostrarImagemModal(p)}
      }
      // se venda-celular for apenas de vizualização, abrir modal no clique da linha
      else{
        tr.onclick = function(){mostrarImagemModal(p)}
      }
    }

    tr.style.height    = '50px'
    tdNome.textContent = p.nome
    tdNome.style.verticalAlign = 'middle'
    tdValor.textContent = valorProduto
    tdValor.style.verticalAlign      = 'middle'
    tdSelecionar.style.verticalAlign = 'middle'
    tdFoto.style.verticalAlign       = 'middle'
    
    // mostra / oculta botão de seleção de produto conforme config
    if(COMPRARPELAVENDACELULAR=='s'){
      btnSelecionar.className          = 'btn btn-success'
      btnSelecionar.style.width        = '12px'
      btnSelecionar.style.height       = '24px'
      iBtnSelecionar.className         = 'fas fa-plus-circle'
      iBtnSelecionar.style.marginLeft  = '-8px'
      iBtnSelecionar.style.marginTop   = '-3px'
      iBtnSelecionar.onclick = function(){selecionarProduto(p)}
      btnSelecionar.append(iBtnSelecionar)
      tdSelecionar.append(btnSelecionar)
    }

    tr.append(tdNome)
    tdFoto.append(imagem)
    tr.append(tdFoto)
    tr.append(tdValor)
    tr.append(tdSelecionar)
    tdProduto.append(tr)
  })
}


function mostrarImagemModal(produto){
  document.getElementById('imagemModalLabel').textContent = produto.nome
  document.getElementById('imagemModalP').textContent     = produto.descricao
  
  if(produto.foto_produto!='/default.png'){
    document.getElementById('imagemModalImg').setAttribute('src',URL_IMG_PRODUTOS+produto.foto_produto)
  }

  $('#imagemModal').modal('toggle')
}


//monta modal com sabores e adicionais
function selecionarProduto(p){
  // limpa tabelas
  document.getElementById('dvPedido').innerHTML = ''

  document.getElementById('txtTipo').value              = p.tipo
  document.getElementById('txtIdProduto').value         = p.id
  document.getElementById('lblValor').textContent       = p.valor
  document.getElementById('txtNomeProduto').textContent = p.nome
  document.getElementById('pedidoModalP').textContent   = p.descricao
  document.getElementById('txtQuantidade').value        = 1
  document.getElementById('txtObservacoes').value       = ''

  if(p.valor==999.00){
    document.getElementById('lblDescricaoValor').style.display = 'none'
    document.getElementById('lblValor').style.display          = 'none'
  }else{
    document.getElementById('lblDescricaoValor').style.display = ''
    document.getElementById('lblValor').style.display          = ''
  }

  if(p.foto_produto!='/default.png'){
    document.getElementById('imgProduto').style.height = '300px'
    document.getElementById('imgProduto').style.width  = '300px'
    document.getElementById('imgProduto').setAttribute('src',URL_IMG_PRODUTOS+p.foto_produto)
  }else{
    document.getElementById('imgProduto').style.height = '0px'
    document.getElementById('imgProduto').style.width  = '0px'
  }

  if(p.tipo=='pizza'){
    getSabores(p.id)
  }else{
    getAdicionais(p.id)
  }
  $('#pedidoModal').modal('toggle')
}


// SABORES
function getSabores(idProduto){
  $.ajax({
    url     : URL_API_MOBILE+'produto',
    type    : 'GET',
    dataType: 'JSON',
    data : {
      'sabores-produto' : idProduto
    },
    headers: {
      'chave-acesso': CHAVE_API_MOBILE
    },
    success:function(data){
      montarSabores(data,idProduto)
    }
  })
}


function getImagemProduto(url, img){

  console.log(img)

  if(img=='/default.png' || img=='default.png'){
    return ''
  }
}


function montarSabores(sabores,idProduto){
  let tbl    = criarTabela('theadSabores',sabores,'sabores')
  let div    = document.getElementById('dvPedido')

  let dvCentro = document.createElement('div')
  let titulo   = document.createElement('h5')

  dvCentro.style.textAlign = 'center'
  titulo.textContent       = 'Sabores'

  dvCentro.append(titulo)
  div.append(dvCentro)
  div.append(tbl)

  getAdicionais(idProduto)
}


// ADICIONAIS
function getAdicionais(idProduto){
  $.ajax({
    url  : URL_API_MOBILE+'produto',
    type : 'GET',
    data : {
      'adicionais-produto' : idProduto
    },
    headers: {
      'chave-acesso': CHAVE_API_MOBILE
    },
    success:function(data){
      let adicionaisArray = Object.values(JSON.parse(data))
      montarAdicionais(adicionaisArray)
    },
    error: function(data){
        console.log(data);
    }
  })
}

function montarAdicionais(adicionais){
  let div = document.getElementById('dvPedido')

  adicionais.forEach(a => {
    if(a.tipoAdicional=='grupo'){
      let grupo      = JSON.parse(a.dados)
      let adicionais = JSON.parse('['+grupo.adicionais[0]+']')

      adicionais.qtd_min = grupo.qtd_min_grupo
      adicionais.qtd_max = grupo.qtd_max_grupo
      
      let tbl = criarTabela('theadAdicionais',adicionais,'adicionais')
      let dvCentro = document.createElement('div')
      let titulo   = document.createElement('h5')

      dvCentro.style.textAlign = 'center'
      titulo.textContent = grupo.nome_grupo
    
      dvCentro.append(titulo)
      div.append(dvCentro)
      div.append(tbl)
    }
  })
}


function selecionarAtributo(chk, tr, min, max,tbody){
  
  let qtdSlc = getQtdSelecionado(tbody)

  if(chk.checked){
    if(qtdSlc>max){
      chk.checked=false
      alert('Máximo '+max+'!')
    }
  }
}

function getQtdSelecionado(tbody){
  let qtd = 0

  let trs = tbody.getElementsByTagName('tr')
  for(let t=0;t<trs.length;t++){
    let tds = trs[t].getElementsByTagName('td')
    if(tds[3].getElementsByTagName('input')[0].checked){
      qtd++
    }
  }
  return qtd
}

//cria table para adicionais e sabores
function criarTabela(nome,dados,tipo){
  let tbl   = document.createElement('table')
  let thead = document.createElement('thead')
  let tbody = document.createElement('tbody')

  //quantidades min e max de um grupo
  let txtMin = document.createElement('input')
  let txtMax = document.createElement('input')
  txtMin.style.display = 'none'
  txtMax.style.display = 'none'
  txtMin.className = 'min'
  txtMax.className = 'max'
  txtMin.type = 'hidden'
  txtMax.type = 'hidden'

  if(tipo=='sabores'){
    txtMin.value = 1
    txtMax.value = dados[0].qtd_sabores
  }else if(tipo=='adicionais'){
    txtMin.value = dados.qtd_min
    txtMax.value = dados.qtd_max
  }
  
  tbl.className = 'table table-bordered'
  tbody.setAttribute('name',nome)
  
  // cria o thead
  let trHead = document.createElement('tr')
  let thNome  = document.createElement('th')
  let thValor = document.createElement('th')
  let thCheck = document.createElement('th')

  thNome.textContent  = 'Nome'
  thNome.style.width = '70%'
  thValor.textContent = 'Valor R$'
  thCheck.textContent = ''

  trHead.append(thNome)
  trHead.append(thValor)
  trHead.append(thCheck)
  thead.append(trHead)

  // cria os trs
  dados.forEach(l=>{
    let tr      = document.createElement('tr')
    let tdId    = document.createElement('td')
    let tdNome  = document.createElement('td')
    let tdValor = document.createElement('td')
    let tdCheck = document.createElement('td')
    let chk     = document.createElement('input')

    tdCheck.style.textAlign = 'center'
    tdId.style.display      = 'none'
    chk.type                = 'checkbox'
    
    if(tipo=='sabores'){
      
      //marcar ou desmarcar checkbox
      chk.onchange = function(){selecionarAtributo(this,this.parentElement,1,l.qtd_sabores,tbody)}

      tdId.textContent    = l.idSaborValor
      tdNome.textContent  = l.sabor
      tdValor.textContent = getDecimal(l.preco)
    }else if(tipo=='adicionais'){

      //marcar ou desmarcar checkbox
      chk.onchange = function(){selecionarAtributo(this,this.parentElement,dados.qtd_min,dados.qtd_max,tbody)}

      tdId.textContent    = l.id_adicional
      tdNome.textContent  = l.nome_adicional
      tdValor.textContent = getDecimal(l.preco_adicional)
    }

    tdCheck.append(chk)
    tr.append(tdId)
    tr.append(tdNome)
    tr.append(tdValor)
    tr.append(tdCheck)
    tbody.append(tr)
  })
  tbl.append(txtMin)
  tbl.append(txtMax)
  tbl.append(thead)
  tbl.append(tbody)
  
  return tbl
}

function getDecimal(numero){
  return parseFloat(numero).toFixed(2)
}