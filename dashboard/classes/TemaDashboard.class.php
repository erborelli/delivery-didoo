<?php

class TemaDashboard {

  public static function montarLayoutPedidosEmEspera(){
    $pedidosPendentes = Pedido::getPedidosPendentes();
    $boxPedidos       = '';
    foreach ($pedidosPendentes as $key => $value) {
      // variaveis agendamento
      $value->entregaAgendada = '';
      $value->agendadoPara    = '';
      if($value->agendamento == 's'){
        $value->entregaAgendada = '<br><small class="d-contents"><b class="d-contents">(Entrega Agendada)</b></small>';
        $value->agendadoPara    = '<li><b>Agendado para:</b> '.date_format(date_create($value->data_agendamento),'d/m/Y H:i:s').'</li>';
      }
      // VISIBILIDADE OBSERVAÇÃO PEDIDO
      $value->visibilidadeCampoObservacao = '';
      $value->margin                      = '';
      if(MOSTRAROBSERVACAOPEDIDO == 'n'){
        $value->visibilidadeCampoObservacao = 'd-none';
        $value->margin                      = 'm-b-30';
      }
      // VISIBILIDADE PEDIDO ACEITAR
      $value->visibilidadePedidoVisualizar = 'd-none';
      $value->visibilidadePedidoAceitar    = '';
      if($value->id_status == 2 && $value->visualizado == 'n'){
        $value->visibilidadePedidoVisualizar = '';
        $value->visibilidadePedidoAceitar    = 'd-none';
      }
      $value->pedido       = self::getPedidoFormatadoAvisoNovoPedido($value->id);
      $value->linkEtiqueta = CAMINHO_DASHBOARD.'painel/pages/etiqueta/imprimir-etiqueta.php?id='.$value->id;
      $value->taxa_entrega  = $value->retirada == 's' ? 'Grátis (Retirada)' : 'R$'.$value->taxa_entrega;
      $boxPedidos .= Sistema::getLayout((array)$value,'dashboard/painel/layout/alerta-pedido','layout-alerta-pedido-item.html');
    }
    return $boxPedidos;
  }

  public static function getPedidoFormatadoAvisoNovoPedido($idPedido){
    if(!is_numeric($idPedido)) return '';
    $itensPedido = PedidoItem::getItensPedido($idPedido);
    $retorno = '';
    foreach ($itensPedido as $key => $value) {
      $preco = number_format($value->qtd * $value->valor,2);
      $retorno .= $value->qtd.' - '.$value->nome_produto.' | R$'.$preco.'<br>';
    }
    return $retorno;
  }

}
