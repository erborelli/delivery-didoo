<?php

// Report all PHP errors
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

session_start();

// include banco
include(dirname(__FILE__).'/../db.php');

// load de classes
spl_autoload_register(function ($class_name) {
  include dirname(__FILE__).'/classes/' . $class_name . '.class.php';
});

require_once dirname(__FILE__).'/../configuracao.php';

// responsável por setar todas variaveis do config do banco como globais no php
Config::setConfigGlobais();
// redireicona par ao login caso o usuário nao esteja logado
Login::redirecionarLogin();
