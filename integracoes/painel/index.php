<?php
    set_time_limit(500);
    require_once('../../includes.php');

    if(!Login::verificarUsuarioLogado()){
        header('location: '.CAMINHO.'/dashboard/login.php');
    }

    $retorno = [];
    $lojas   = Lojas::getLojas();

    foreach($lojas as $loja){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $loja['url'].'/integracoes/api-painel/pedido/');
        curl_setopt($curl, CURLOPT_POSTFIELDS, "foo");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'CHAVE-ACESSO: 112233'
        ]);

        // loja atual
        $retornoAtual = json_decode(curl_exec($curl),true);
        if(!empty($retornoAtual)){
            // pedidos da loja atual
            foreach($retornoAtual as $key => $ra){
                if(!empty($retornoAtual[$key]['data_pedido'])){

                    // busca por datas
                    if(isset($_GET['data-inicial']) && isset($_GET['data-final'])){
                        $dataPedido  = new DateTime(substr($retornoAtual[$key]['data_pedido'],0,10));
                        $dataInicial = new DateTime($_GET['data-inicial']);
                        $dataFinal   = new DateTime($_GET['data-final']);
    
                        if( ($dataPedido->getTimestamp() >= $dataInicial->getTimestamp()) && ($dataPedido->getTimestamp() <= $dataFinal->getTimestamp()) ){
                            //adiciona loja de origem
                            $retornoAtual[$key]['loja'] = $loja['nome'];
                            //trata formato da data
                            $data = new DateTime($retornoAtual[$key]['data_pedido']);
                            $retornoAtual[$key]['data_pedido'] = $data->format('d/m/Y H:i:s');
                            //muda de S/N para SIM/NÃO
                            $retornoAtual[$key]['agendamento'] = $retornoAtual[$key]['agendamento'] == 's' ? 'sim' : 'não';
                            $retornoAtual[$key]['retirada']    = $retornoAtual[$key]['retirada']    == 's' ? 'sim' : 'não';
                        }else{
                            $retornoAtual[$key] = null;
                        }
                    }else{
                        //adiciona loja de origem
                        $retornoAtual[$key]['loja'] = $loja['nome'];
                        //trata formato da data
                        $data = new DateTime($retornoAtual[$key]['data_pedido']);
                        $retornoAtual[$key]['data_pedido'] = $data->format('d/m/Y H:i:s');
                        //muda de S/N para SIM/NÃO
                        $retornoAtual[$key]['agendamento'] = $retornoAtual[$key]['agendamento'] == 's' ? 'sim' : 'não';
                        $retornoAtual[$key]['retirada']    = $retornoAtual[$key]['retirada']    == 's' ? 'sim' : 'não';
                    }
                }
            }
        }
        if(!empty($retornoAtual)){
            $retorno = array_merge($retorno,$retornoAtual);
        }
        curl_close($curl);
    }
    $retorno = json_encode($retorno);
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Painel Monitoramento</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="libs/tabulator/css/semantic-ui/tabulator_semantic-ui.min.css">
</head>
<body>
    <div style="text-align: center;">
        <br><h4>Totais de Pedidos</h4>
    </div>
    <div id="dados" style="display: none;">
        <?=$retorno?>
    </div>

    <div style="display: flex; height: 39px;">
        <a class="btn btn-primary" href="lojas/">Lojas</a>
        <button style="margin-left: 5px;" id="print-table" class="btn btn-primary">Imprimir</button>
        <button style="margin-left: 5px;" id="download-xlsx" class="btn btn-primary">Baixar Excel</button>
        <form style="margin-left: 100px;" action="" method="GET" class="form-inline">
            <input name="data-inicial" type="date" class="form-control" placeholder="Data Inicial" value="<?= isset($_GET['data-inicial']) ? $_GET['data-inicial'] : '' ?>">
            <input  name="data-final"  type="date" class="form-control" placeholder="Data Final"   value="<?= isset($_GET['data-final'])   ? $_GET['data-final']   : '' ?>">
            <button class="btn btn-primary" type="submit">Filtrar</button>
        </form>
    </div>

    <div id="tblPedidos"></div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://oss.sheetjs.com/sheetjs/xlsx.full.min.js"></script>
    <script src="libs/tabulator/js/tabulator.min.js"></script>
    <script src="scriptTabela.js?v=11"></script>
</body>
</html>