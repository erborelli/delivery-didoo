<?php
//retorna adicionais produto

include_once('../includes.php');

//caso não receba o id do produto, retorna erro
if(!isset($_GET['idProduto'])){
    echo('erro');
    return;
}

echo json_encode(Adicional::getTodosAdicionaisPorProdutoAdicionaisEmGrupo($_GET['idProduto']),true);