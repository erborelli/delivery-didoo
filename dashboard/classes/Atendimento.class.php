<?php

class Atendimento {

  public static function getDiasAtendimento(){
    $link = getConnection();
    $sql  = "SELECT * FROM atendimento ORDER BY id_dia ASC, id ASC";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Atendimento');
  }

  public static function agruparDiasAtendimento($diasAtendimento){
    $retorno = [];
    foreach ($diasAtendimento as $key => $value) {
      $idDia = $value->id_dia;
      $retorno[$idDia][] = $value;
    }
    return $retorno;
  }

  public static function montarLayoutDiasAtendimento($diasAtendimento){
    $diasAtendimento = self::agruparDiasAtendimento($diasAtendimento);
    $retorno         = '';
    foreach ($diasAtendimento as $key => $value) {
      if(count($value) > 1){
        foreach ($value as $chave => $valor) {
          if($chave == 0){
            $layoutHorarioAdicionalAbreA  = '';
            $layoutHorarioAdicionalFechaA = '';
            $layoutHorarioAdicionalAbreB  = '';
            $layoutHorarioAdicionalFechaB = '';
            if(isset($value[1])){
              $layoutHorarioAdicionalAbreA = '<br><input maxlength="8" class="horarioAtendimento" name="abre['.$value[1]->id_dia.']['.$value[1]->id.']" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'" value="'.$value[1]->abre.'">';
              $layoutHorarioAdicionalFechaA = '<br><input maxlength="8" class="horarioAtendimento" name="abre['.$value[1]->id_dia.']['.$value[1]->id.']" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'" value="'.$value[1]->fecha.'"><span class="removerLinhaHorario" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'"><i class="fas fa-minus-circle"></i><span></span></span>';
            }
            if(isset($value[2])){
              $layoutHorarioAdicionalAbreB = '<br><input maxlength="8" class="horarioAtendimento" name="abre['.$value[2]->id_dia.']['.$value[2]->id.']" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'" value="'.$value[2]->abre.'">';
              $layoutHorarioAdicionalFechaB = '<br><input maxlength="8" class="horarioAtendimento" name="abre['.$value[2]->id_dia.']['.$value[2]->id.']" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'" value="'.$value[2]->fecha.'"><span class="removerLinhaHorario" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'"><i class="fas fa-minus-circle"></i><span></span></span>';
            }
            $checked           = $valor->fechado == 's' ? 'checked' : '';
            $mostrarBotaSalvar = $valor->fechado == 's' ? 'd-none' : '';
            $classFechado      = $valor->fechado == 's' ? 'disabledbutton' : '';
            $retorno .= '<tr>
            <th scope="row">'.$valor->label.'<br> <span data-id-dia="'.$valor->id_dia.'" class="addSubHorario">+ horário</span> </th>

            <td class="abre '.$classFechado.'">
            <input maxlength="8" data-id-dia="'.$valor->id_dia.'" data-id="'.$valor->id.'" class="horarioAtendimento" name="abre['.$valor->id.']['.$valor->id_dia.']" value="'.$valor->abre.'">
            '.$layoutHorarioAdicionalAbreA.'
            '.$layoutHorarioAdicionalAbreB.'
            </td>

            <td class="fecha '.$classFechado.'">
            <input maxlength="8" data-id-dia="'.$valor->id_dia.'" data-id="'.$valor->id.'" class="horarioAtendimento" name="fecha['.$valor->id.']['.$valor->id_dia.']" value="'.$valor->fecha.'">
            '.$layoutHorarioAdicionalFechaA.'
            '.$layoutHorarioAdicionalFechaB.'
            </td>

            <td class="fechado"><input data-id="'.$valor->id.'" data-id-dia="'.$valor->id_dia.'" name="fechado['.$valor->id.']['.$valor->id_dia.']" '.$checked.' type="checkbox"></td>
            <td class="salvar '.$mostrarBotaSalvar.'" data-id="'.$valor->id.'" data-id-dia="'.$valor->id_dia.'"><button class="botaoSalvar">salvar</button></td>
            </tr>';
            break;
          }
        }

      }else {
        $value             = $value[0];
        $checked           = $value->fechado == 's' ? 'checked' : '';
        $mostrarBotaSalvar = $value->fechado == 's' ? 'd-none' : '';
        $classFechado      = $value->fechado == 's' ? 'disabledbutton' : '';
        $retorno .= '<tr>
        <th scope="row">'.$value->label.'<br> <span data-id-dia="'.$value->id_dia.'" class="addSubHorario">+ horário</span> </th>

        <td class="abre '.$classFechado.'">
        <input maxlength="8" data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" class="horarioAtendimento" name="abre['.$value->id.']['.$value->id_dia.']" value="'.$value->abre.'">
        </td>

        <td class="fecha '.$classFechado.'">
        <input maxlength="8" data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" class="horarioAtendimento" name="fecha['.$value->id.']['.$value->id_dia.']" value="'.$value->fecha.'">
        </td>

        <td class="fechado"><input data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" name="fechado['.$value->id.']['.$value->id_dia.']" '.$checked.' type="checkbox"></td>
        <td class="salvar '.$mostrarBotaSalvar.'" data-id="'.$value->id.'" data-id-dia="'.$value->id_dia.'"><button class="botaoSalvar">salvar</button></td>
        </tr>';
      }
    }
    return $retorno;
  }

  public static function excluirHorariosPorIdDia($idDia){
    $link = getConnection();
    $sql  = "DELETE FROM atendimento WHERE id_dia = ".$idDia;
    $stmt = $link->prepare($sql);
    $stmt->execute();
  }

  public static function getLabelPorIdDia($idDia){
    if(!is_numeric($idDia)) return false;
    $relacaoIdDiaLabel = [
      1 => 'segunda',
      2 => 'terca',
      3 => 'quarta',
      4 => 'quinta',
      5 => 'sexta',
      6 => 'sabado',
      7 => 'domingo',
    ];
    return $relacaoIdDiaLabel[$idDia];
  }

  public static function updateHorarioAtendimento($dadosPost){
    if(!isset($dadosPost['abre']) || !isset($dadosPost['fecha']) || !isset($dadosPost['id_dia'])) return false;

    $dadosAbre  = $dadosPost['abre'];
    $dadosFecha = $dadosPost['fecha'];
    self::excluirHorariosPorIdDia($dadosPost['id_dia']);
    $link  = getConnection();
    $label = self::getLabelPorIdDia($dadosPost['id_dia']);
    foreach ($dadosAbre as $key => $value) {
      $sql  = "INSERT INTO atendimento (id_dia,label,abre,fecha) VALUES (?,?,?,?)";
      $stmt = $link->prepare($sql);
      $stmt->execute([$value['idDia'],$label,$value['abre'],$dadosFecha[$key]['fecha']]);
    }

    return true;
  }

  public static function fecharLoja($dadosPost){
    $link = getConnection();
    $sql  = "UPDATE atendimento SET fechado=? WHERE id_dia = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['fechado'],$dadosPost['id_dia']]);
    return true;
  }

  public static function getAtendimentoLoja(){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    // inicia algumas variaveis
    $retorno = ['aberta' => false, 'horarioFechamento' => ''];
    // link conexao
    $link = getConnection();
    // recupero o id do dia da semana
    $idDataHoje = date('N');
    // monto a sql
    $sql  = "SELECT * FROM atendimento WHERE id_dia = ?";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idDataHoje]);
    $dadosAtendimento  = $stmt->fetchAll(PDO::FETCH_CLASS,'Atendimento');
    $lojaAberta        = self::verificarLojaAberta($dadosAtendimento);
    $retorno['aberta'] = $lojaAberta['aberta'];
    $retorno['horarioFechamento'] = $lojaAberta['horarioFechamento'];
    return $retorno;
  }

  /**
   * Return boolean (true/false)
   * o método é como se fosse uma pergunta, caso true a loja está fechada
   * caso false a loja está aberta
   */
  public static function ALojaEstaFechadaESemAgendamento(){
    if(!Atendimento::getAtendimentoLoja()['aberta'] && !Agendamento::verificarAgendamentoSessao()){
      return true;
    }
    return false;
  }

  public static function verificarLojaAberta($horariosAtendimento){
    $retorno = ['aberta' => false, 'horarioFechamento' => ''];
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    $horarioAtual = date("H:i:s");
    foreach ($horariosAtendimento as $key => $value) {
      // caso estiver fechado já retorno false (loja fechada) e checa se agora é antes ou deopis do horário de abertura
      if($value->fechado == 'n' &&
      ($horarioAtual > $value->abre && $horarioAtual < $value->fecha)){
        $retorno['aberta']            = true;
        $retorno['horarioFechamento'] = $value->fecha;
        break;
      }
    }
    // caso for empty o horario de fechamento, eu seto o ultimo horário como horario de fechamento
    if(empty($retorno['horarioFechamento'])){
      $ultimoHorario = end($horariosAtendimento);
      $retorno['horarioFechamento'] = $ultimoHorario->fecha;
    }
    return $retorno;
  }

}
