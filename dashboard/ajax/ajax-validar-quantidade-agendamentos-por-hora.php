<?php

include_once('../includes.php');

$retorno = ['mensagemErro' => ''];

if(!isset($_POST) || !isset($_POST['diaSelecionado']) || !isset($_POST['horaSelecionada']) ||
    empty($_POST['diaSelecionado']) || empty($_POST['horaSelecionada'])){
  $retorno['mensagemErro'] = 'Horário e dia inválido!';
  echo json_encode($retorno);exit;
}

$quantidadeDeAgendamentosPermitidos = QUANTIDADEDEAGENDAMENTOSPORINTERVALO;
$quantidadeAgendamentosAtivos       = Agendamento::validarQuantidadeDeAgendamentosPorDataEHora($_POST['diaSelecionado'], $_POST['horaSelecionada']);

if($quantidadeAgendamentosAtivos >= $quantidadeDeAgendamentosPermitidos){
  $retorno['mensagemErro'] = 'Desculpe, este horário já está agendado!';
}

echo json_encode($retorno);
