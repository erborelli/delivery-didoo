<?php

include_once('../includes.php');

$idProduto = $_POST['idProduto'];
$retorno = ['sucesso' => false, 'layoutAdicionais' => ''];

// caso nao vier um id de produto
if(!is_numeric($idProduto)){
  echo json_encode($retorno);exit;
}

$adicionais  = Adicional::getAdicionaisPorProdutoFormatados($idProduto);
$retorno['layoutAdicionais'] = Index::montarLayoutAdicionais($adicionais, $idProduto);
$retorno['sucesso']          = true;

echo json_encode($retorno);
