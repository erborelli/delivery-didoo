<?php
    include_once('../../../includes.php');
    $mesas = Mesa::getMesas();
?>
<!DOCTYPE html>
<html>
    <style>
        #qrcode {
            width: 128px;
            height: 128px;
            margin: 0 auto;
            text-align: center;
        }

        #qrcode a {
            font-size: 0.8em;
        }

        .qr-url, .qr-size {
            padding: 0.5em;
            border: 1px solid #ddd;
            border-radius: 2px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .qr-url {
            width: 79%;
        }

        .qr-size {
            width: 20%;
        }
    </style>
    <link href="https://printjs-4de6.kxcdn.com/print.min.css">

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>
        <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

        <!-- modal qrcode -->
        <div class="modal fade" id="qrCodeModal" tabindex="-1" role="dialog" aria-labelledby="qrCodeModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="qrCodeModalLabel">QR Code Mesa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label>Titulo superior</label>
                            <input id="txtTituloSuperior" onkeyup="atualizarLabels()" class="form-control" value="">
                        </div>
                        <div class="row">
                            <label>Descrição inferior</label>
                            <input id="txtDescricaoInferior" onkeyup="atualizarLabels()" class="form-control" value="">
                        </div>
                        <div class="row">
                            <label>URL inferior</label>
                            <input id="txtUrlInferior" onkeyup="atualizarLabels()" class="form-control" value="">
                        </div>
                        <br>
                        <div class="qr-code-generator">
                            <input id="txtUrlQrCode" type="text" class="qr-url" style="display: none;" value="">
                            <div class="form-group row">
                                <div class="col-md-6" style="margin-top: 8px; display: none;">
                                    <label>Tamanho - 100 até 400 pixels:</label>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <input type="number" class="qr-size form-control" value="400" min="100" max="400">
                                </div>
                            </div>
                            <div id="imprimir">
                                <div>
                                    <img src="<?=CAMINHO.'dashboard/images/logo/'.LOGO?>" style="height: 150px; margin-left: 70px;"><br><br>
                                    <label style="margin-left: 70px;" id="lblSuperior"></label>
                                    <div id="qrcode"></div>
                                    <br>
                                    <label style="margin-left: 5px;" id="lblInferior"></label><br>
                                    <label style="margin-left: 5px;" id="lblInferiorUrl"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="imprimirQrCode()">Imprimir</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Mesas</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Mesas</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/mesas/criar.php">
                    <i class="fas fa-plus"></i>
                    Nova Mesa
                </a>
                <br><br>
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th style="width: 20%">
                                        Número
                                    </th>
                                    <th style="width: 25%">
                                        Descrição
                                    </th>
                                    <th style="width: 20%">
                                        Data Criação
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($mesas as $mesa){ ?>
                                    <tr>
                                        <td><?=$mesa['numero']?></td>
                                        <td><?=$mesa['descricao']?></td>
                                        <td><?=$mesa['data']?></td>
                                        <td class="project-actions text-right">
                                            <button class="btn btn-primary btn-sm" onclick="abrirModalQrCode('<?=$mesa['numero']?>')">
                                                <i class="fas fa-qrcode"></i>
                                                Gerar QrCode
                                            </button>
                                            <a class="btn btn-info btn-sm" href="<?=CAMINHO_DASHBOARD.'/painel/pages/mesas/editar.php?id='.$mesa['id']?>">
                                                <i class="fas fa-pencil-alt"></i>
                                                Editar
                                            </a>
                                            <a data-id="<?=$mesa['id']?>" data-modulo="mesa" class="btn btn-danger btn-sm" href="#">
                                                <i class="fas fa-trash"></i>
                                                Excluir
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/mesas/criar.php">
                    <i class="fas fa-plus"></i>
                    Nova mesas
                </a>
            </section>
        </div>
        <footer class="main-footer">
            <strong><?=NOMELOJA?></strong>
        </footer>
        <aside class="control-sidebar control-sidebar-dark"></aside>
    </div>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/jquery/jquery.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>
    <script src="jquery-qrcode.js?v=<?=strtotime('now')?>"></script>
    <script src="qrcode.js?v=<?=strtotime('now')?>"></script>
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</body>
</html>