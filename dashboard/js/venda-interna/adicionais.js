

function mostrarAdicionais(){
    consultarAdicionaisPorProduto()
}


function consultarAdicionaisPorProduto(){
    var idProduto = $('#selectProdutos').val()

    $.ajax({
        url  : CAMINHO_DASHBOARD+'ajax/ajax-consultar-adicionais-produto.php',
        type : 'GET',
        data : {
          idProduto : idProduto
        },
        success:function(data){
            let adicionaisArray = Object.values(JSON.parse(data))

            //se produto tiver adicionais
            if(adicionaisArray.length > 0){
                montarTabelaAdicionaisProduto(adicionaisArray)
            }

            //se produto não tiver adicionais
            else{
                if($('#txtTipoProduto').val()=='pizza'){
                    adicionarPizzaCarrinho()
                    $('#txtTipoProduto').val('')
                }else{
                    adicionarProdutoCarrinho()
                    $('#txtTipoProduto').val('')
                }
            }
        },
        error: function(data){
            console.log(data);
        }
    })
}


/**
 * monta os grupos de adicionais no modal
 * @param {*} e      grupo de adicional
 * @param {*} numero numero do modal atual do grupo 
 */
function montarGruposAdicionaisModal(e,numero){
    let grupo = JSON.parse(e.dados)

    criarModal(numero)

    // função de confirmação do modal
    let btnConfirmarFooter = document.getElementById('btnConfirmarFooter'+numero)
    btnConfirmarFooter.addEventListener("click",function(){
        let numeroModal = btnConfirmarFooter.parentElement.getElementsByTagName('input')[0].value
        let qtdSelecionados = 0

        // qtd de adicionais selecionados
        let trs = document.getElementById(numeroModal+'Tbody').getElementsByTagName('tr')
        for(let l=0;l<trs.length;l++){
            if(trs[l].getElementsByTagName('td')[2].getElementsByTagName('input')[0].checked){
                qtdSelecionados++
            }
        }

        // mensagem de quantidade minima de adicionais
        let txtQtdMin = document.getElementById('txtQtdMin'+numeroModal)
        if(qtdSelecionados < txtQtdMin.textContent){
            alert('selecione no minimo '+txtQtdMin.textContent+' opções para continuar!')
            return
        }
        
        // mensagem de quantidade máxima de adicionais
        let txtQtdMax = document.getElementById('txtQtdMax'+numeroModal)
        if(qtdSelecionados > txtQtdMax.textContent){
            alert('selecione no máximo '+txtQtdMax.textContent+' opções!')
            return
        }

        // fecha modal
        document.getElementById('btnClose'+numeroModal).click()

        // verifica se é o último adicional
        if(numeroModal==1){
            confirmarAdicionais()
        }
    })

    document.getElementById(numero+'Titulo').textContent    = grupo.nome_grupo
    document.getElementById('txtQtdMin'+numero).textContent = grupo.qtd_min_grupo
    document.getElementById('txtQtdMax'+numero).textContent = grupo.qtd_max_grupo
    
    let thead = document.getElementById(numero+'Thead')
    thead.innerHTML = null
    
    let trThead      = document.createElement('tr')
    let thAdicional  = document.createElement('th')
    let thValor      = document.createElement('th')
    let thSelecionar = document.createElement('th')

    thAdicional.textContent  = 'Nome'
    thValor.textContent      = 'Preço R$'
    
    thead.appendChild(thAdicional)
    thead.appendChild(thValor)
    thead.appendChild(thSelecionar)
    thead.appendChild(trThead)
    
    document.getElementById(numero+'Tbody').innerHTML = null
    let adicionais = JSON.parse('['+grupo.adicionais[0]+']')
    adicionais.forEach(a => {

        let tr             = document.createElement('tr')
        let tdAdicional    = document.createElement('td')
        let tdValor        = document.createElement('td')
        let tdSelecionar   = document.createElement('td')
        let tdId           = document.createElement('td')
        let chkSelecionado = document.createElement('input')

        chkSelecionado.className = 'form-check-input'
        chkSelecionado.type      = 'checkbox'
        //chkSelecionado.setAttribute('onclick','selecionarAdicional(this)')
        
        tdSelecionar.style.textAlign = 'center'
        tdSelecionar.append(chkSelecionado)
        
        tdId.style.display      = 'none'
        tdId.textContent        = a.id_adicional
        tdAdicional.textContent = a.nome_adicional
        tdValor.textContent     = a.preco_adicional

        tr.append(tdAdicional)
        tr.append(tdValor)
        tr.append(tdSelecionar)
        tr.append(tdId)
        document.getElementById(numero+'Tbody').append(tr)
    })
}



function montarTabelaAdicionaisProduto(dadosAdicionais){
    let numeroModal = 1
    // loop montando os modais de grupos de adicionais
    dadosAdicionais.forEach(g => {
        if(g.tipoAdicional=='grupo'){
            montarGruposAdicionaisModal(g,numeroModal)
            $('#'+numeroModal+'Modal').modal()
            numeroModal++
        }
    })
}


function selecionarAdicional(elemento){

    let lblValorTotalAdicionais = document.getElementById('lblValorTotalAdicionais')
    let valorSelecionadoAtual   = elemento.parentElement.parentElement.getElementsByTagName('td')[1].textContent

    //marcar adicional soma valor do total
    if(elemento.checked){
        lblValorTotalAdicionais.textContent = Number(lblValorTotalAdicionais.textContent)+Number(valorSelecionadoAtual)
    
    //desmarcar adicional subtrai valor do total
    }else{
        lblValorTotalAdicionais.textContent = Number(lblValorTotalAdicionais.textContent)-Number(valorSelecionadoAtual)
    }
}


//botão confirmar modal adicionais
function confirmarAdicionais(){
    if($('#txtTipoProduto').val()=='pizza'){
        adicionarPizzaCarrinho()
        $('#txtTipoProduto').val('')
    }else{
        adicionarProdutoCarrinho()
        $('#txtTipoProduto').val('')
    }
}


//monta json com nome e valor dos adicionais
function montarJsonAdicionais(){
    let dvModaisJS = document.getElementById('modaisJS')
    let tbodys = dvModaisJS.getElementsByTagName('tbody')

    // loop pelos adicionais
    let adicionais = []
    for(let e=0;e<tbodys.length;e++){

        let tds = tbodys[e].getElementsByClassName('form-check-input')
        for(let i=0;i<tds.length;i++){
            let adicionalAtual = new Object()
            if(tds[i].checked){
                adicionalAtual.nome  = tds[i].parentElement.parentElement.getElementsByTagName('td')[0].textContent
                adicionalAtual.valor = tds[i].parentElement.parentElement.getElementsByTagName('td')[1].textContent
                adicionalAtual.id    = tds[i].parentElement.parentElement.getElementsByTagName('td')[3].textContent
                adicionais.push(adicionalAtual)
            }
        }
    }
    return JSON.stringify(adicionais)
}


/**
 * cria modal padrão bootstrap para os adicionais / grupos adicionais
 * @param {*} nomeModal string concatenada aos ids dos elementos
 */
function criarModal(nomeModal){
    let dvPrimaria   = document.createElement('div')
    let dvSecundaria = document.createElement('div')
    let dvTerciaria  = document.createElement('div')

    let dvTitulo   = document.createElement('div')
    let h5Titulo   = document.createElement('h5')
    let btnTitulo  = document.createElement('button')
    let spanTitulo = document.createElement('span')

    let dvCorpo    = document.createElement('div')
    let tableCorpo = document.createElement('table')
    let theadCorpo = document.createElement('thead')
    let tbodyCorpo = document.createElement('tbody')

    let dvFooter           = document.createElement('div')
    let txtNumeroModal     = document.createElement('input')
    let btnConfirmarFooter = document.createElement('button')
    let btnCancelarFooter  = document.createElement('button')

    let lblQtdMax = document.createElement('label')
    let lblQtdMin = document.createElement('label')
    let txtQtdMin = document.createElement('label')
    let txtQtdMax = document.createElement('label')
    let br        = document.createElement('br')

    lblQtdMax.textContent = 'Quantidade máxima: '
    lblQtdMin.textContent = 'Quantidade minima: '
    lblQtdMax.id = 'lblQtdMax'+nomeModal
    lblQtdMin.id = 'lblQtdMin'+nomeModal
    txtQtdMin.id = 'txtQtdMin'+nomeModal
    txtQtdMax.id = 'txtQtdMax'+nomeModal

    // estrutura principal
    dvPrimaria.id        = nomeModal+'Modal'
    dvPrimaria.className = 'modal fade'
    dvPrimaria.tabIndex  = 1
    dvPrimaria.setAttribute('role','dialog')
    dvPrimaria.setAttribute('aria-labelledby','grupoAdicionaisModalLabel')
    dvPrimaria.setAttribute('aria-hidden','true')
    dvSecundaria.className = 'modal-dialog'
    dvSecundaria.setAttribute('role','document')
    dvTerciaria.className = 'modal-content'

    // div titulo
    dvTitulo.className  = 'modal-header'
    h5Titulo.id         = nomeModal+'Titulo'
    h5Titulo.className  = 'modal-title'
    btnTitulo.id        = 'btnClose'+nomeModal
    btnTitulo.type      = 'button'
    btnTitulo.className = 'close'
    btnTitulo.setAttribute('data-dismiss','modal')
    btnTitulo.setAttribute('aria-label','Close')
    spanTitulo.setAttribute('aria-hidden','true')
    spanTitulo.innerHTML = '&times;'

    // div corpo
    dvCorpo.className    = 'modal-body'
    tableCorpo.className = 'table table-bordered table-striped'
    tbodyCorpo.id        = nomeModal+'Tbody'
    theadCorpo.id        = nomeModal+'Thead'

    // div footer
    dvFooter.className   = 'modal-footer'
    txtNumeroModal.setAttribute('value',nomeModal)
    txtNumeroModal.name  = 'nomeDoModal'
    txtNumeroModal.style.display = 'none'
    btnCancelarFooter.setAttribute('type','button')
    btnCancelarFooter.setAttribute('class','btn btn-secondary')
    btnCancelarFooter.setAttribute('data-dismiss','modal')
    btnCancelarFooter.textContent = 'Cancelar'
    btnConfirmarFooter.setAttribute('type','button')
    btnConfirmarFooter.setAttribute('class','btn btn-primary')
    btnConfirmarFooter.id = 'btnConfirmarFooter'+nomeModal
    btnConfirmarFooter.textContent = 'Continuar'

    //div titulo
    btnTitulo.appendChild(spanTitulo)
    dvTitulo.appendChild(h5Titulo)
    dvTitulo.appendChild(btnTitulo)

    // div corpo
    dvCorpo.appendChild(lblQtdMin)
    dvCorpo.appendChild(txtQtdMin)
    dvCorpo.appendChild(br)
    dvCorpo.appendChild(lblQtdMax)
    dvCorpo.appendChild(txtQtdMax)
    tableCorpo.appendChild(theadCorpo)
    tableCorpo.appendChild(tbodyCorpo)
    dvCorpo.appendChild(tableCorpo)

    // div footer
    dvFooter.appendChild(txtNumeroModal)
    dvFooter.appendChild(btnCancelarFooter)
    dvFooter.appendChild(btnConfirmarFooter)
    
    //estrutura principal
    dvTerciaria.appendChild(dvTitulo)
    dvTerciaria.appendChild(dvCorpo)
    dvTerciaria.appendChild(dvFooter)
    dvSecundaria.appendChild(dvTerciaria)
    dvPrimaria.appendChild(dvSecundaria)

    document.getElementById('modaisJS').appendChild(dvPrimaria)
}