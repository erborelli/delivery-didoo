<button type="button" class="btn abrirModalPedidoPendente" style="display:none" data-toggle="modal" data-target="#modalPedidoPendente"></button>
<div id="modalPedidoPendente" data-backdrop="static" class="modal open fade" tabindex="0" style="z-index: 99999; display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="conteudoModalPedidoPendente">
      <div class="modal-close-content">
        <i class="material-icons close-button js-modal-close" id="minimizarBoxPedidoPendente" data-ga="modal-close" data-cy="modal-dialog__close">close</i>
      </div>
      <div class="box-address js-step-2" data-cy="address-modal__step-2">
        <div class="text-center mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded containerBoxPedidoPendente" data-upgraded=",MaterialTextfield">
          <h1>Recebemos seu Pedido!</h1>
          <h2>Aguardando resposta do estabelecimento.</h2>
          <!-- <img src="img/home/preload.gif" width="200px" alt=""> -->
        </div>
        <div class="text-center mdl-textfield mdl-js-textfield mdl-textfield--floating-label address-modal-field is-upgraded containerBoxRetornoPedidoPendente" data-upgraded=",MaterialTextfield">

        </div>
        <?php if(REDIRECIONARWHATSAPP == 's'){ ?>
          <div class="containerRedirectWhats">
            <span class="containerTempoRedirecionar">Você será redirecionado para o Whatsapp em: <span class="tempoRedirecionarWhats"><?=TEMPOREDIRECIONARWHATSAPP?></span>s</span>
          </div>
        <?php } ?>
        <div class="linkWhatsappPedido">
          <a href="javascript:void(0)" target="_blank">
            <img src="<?=CAMINHO?>/img/home/whatsapp-logo-icone.jpg" width="50">
            <span style="font-size: 18px;font-weight: bold;">
              Enviar uma cópia do pedido por whatsapp
            </span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
