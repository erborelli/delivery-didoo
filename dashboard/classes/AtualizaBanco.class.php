<?php

class AtualizaBanco{
    private static $ultimaVersao=0;

    public static function executar(){
        if(self::getPermissaoAtualizar()){
            $sqlFinal = self::getSqlsPendentes();
            if(self::executarSqls($sqlFinal)){
                if(self::atualizarHashConfig()){
                    return ['status'=>true,'mensagem'=>'Banco de dados atualizado com sucesso!'];
                }
                return ['status'=>false,'mensagem'=>'Erro ao atualizar a hash "VERSAOBANCO" para a última versão!'];
            }
            if(strlen($sqlFinal)==0){
                return ['status'=>true,'mensagem'=>'Banco de dados já esta atualizado! Versão '.VERSAOBANCO];
            }else{
                return ['status'=>false,'mensagem'=>'Erro ao executar SQL '.$sqlFinal];
            }
        }
        return ['status'=>false,'mensagem'=>'Hash "VERSAOBANCO" não existe na tabela "CONFIG"!'];
    }

    private static function getPermissaoAtualizar(){
        if(defined('VERSAOBANCO')){
            return true;
        }
        return false;
    }

    private static function getSqlsPendentes(){
        $path      = CAMINHO_SISTEMA.'sqls/';
        $diretorio = dir($path);
        $sqlFinal  ='';
        
        // percorre todos os arquivos da pasta sqls
        while($arquivo = $diretorio -> read()){
            if(strlen($arquivo)>2){
                // quebra o nome do arquivo em partes
                $numero = explode('-',$arquivo);
    
                // se arquivo começar com um número, e for maior que 0
                if( (intval($numero[0])>0) && ($numero[0]>VERSAOBANCO) ){    
                    self::$ultimaVersao = $numero[0];
                    $executar  = '';
                    $listaSqls = file($path.$arquivo);
                    foreach($listaSqls as $sql){
                        $executar .= $sql.' ';
                    }
                    $sqlFinal .= $executar;
                }
            }
        }
        $diretorio -> close();
        return $sqlFinal;
    }

    private static function executarSqls($sql){
        $link = getConnection();
        $stmt= $link->prepare($sql);
        return $stmt->execute();
    }

    private static function atualizarHashConfig(){
        $sql = 'UPDATE config SET valor=? WHERE hash="versaoBanco"';
        $link = getConnection();
        $stmt= $link->prepare($sql);
        return $stmt->execute([self::$ultimaVersao]);
    }
}