

-- Copiando estrutura para tabela mirie_coxinhas.adicional
CREATE TABLE IF NOT EXISTS `adicional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `preco` decimal(10,2) NOT NULL,
  `data_criacao` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.atendimento
CREATE TABLE IF NOT EXISTS `atendimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '0',
  `abre` time DEFAULT NULL,
  `fecha` time DEFAULT NULL,
  `fechado` enum('s','n') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '0',
  `data_criacao` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '0',
  `endereco` varchar(300) NOT NULL DEFAULT '0',
  `contato` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `contato` (`contato`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.config
CREATE TABLE IF NOT EXISTS `config` (
  `hash` varchar(50) DEFAULT NULL,
  `valor` varchar(1500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.cupom
CREATE TABLE IF NOT EXISTS `cupom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cupom` varchar(50) DEFAULT NULL,
  `hash_cupom` varchar(50) DEFAULT NULL,
  `tipo_desconto` enum('porcentagem','valor') DEFAULT 'porcentagem',
  `valor_desconto` decimal(10,2) DEFAULT NULL,
  `data_criacao` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `qtd_disponivel` int(11) DEFAULT NULL,
  `qtd_utilizado` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_cupom` (`hash_cupom`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.cupom_utilizacao
CREATE TABLE IF NOT EXISTS `cupom_utilizacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `id_pedido` int(11) DEFAULT NULL,
  `hash_cupom_utilizado` varchar(50) DEFAULT NULL,
  `utilizacao` varchar(50) DEFAULT NULL,
  `data_utilizacao` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.entregador
CREATE TABLE IF NOT EXISTS `entregador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '0',
  `data_criacao` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.frete
CREATE TABLE IF NOT EXISTS `frete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bairro` varchar(50) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bairro` (`bairro`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(50) NOT NULL DEFAULT '',
  `nome` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.menu_item
CREATE TABLE IF NOT EXISTS `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `id_item_pai` int(11) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pedido
CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_entregador` int(11) DEFAULT NULL,
  `endereco` varchar(300) NOT NULL DEFAULT '',
  `whats_cliente` varchar(50) NOT NULL DEFAULT '',
  `nome_cliente` varchar(50) NOT NULL DEFAULT '',
  `total_pedido` decimal(10,2) NOT NULL DEFAULT 0.00,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `taxa_entrega` decimal(10,2) NOT NULL DEFAULT 0.00,
  `agendamento` enum('s','n') NOT NULL DEFAULT 'n',
  `cupom_utilizado` varchar(50) DEFAULT NULL,
  `tipo_pagamento` enum('cartao','dinheiro') DEFAULT 'cartao',
  `troco` decimal(10,2) DEFAULT NULL,
  `origem` enum('ifood','litoral-na-mesa','sistema') DEFAULT 'sistema',
  `data_agendamento` datetime NOT NULL DEFAULT current_timestamp(),
  `data_pedido` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pedido_item
CREATE TABLE IF NOT EXISTS `pedido_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `nome_produto` varchar(50) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `observacoes` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pedido_item_adicional
CREATE TABLE IF NOT EXISTS `pedido_item_adicional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido_item` int(11) DEFAULT NULL,
  `id_adicional` int(11) DEFAULT NULL,
  `valor` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pizza_sabores
CREATE TABLE IF NOT EXISTS `pizza_sabores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL,
  `data_criacao` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pizza_sabores_valores
CREATE TABLE IF NOT EXISTS `pizza_sabores_valores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tamanho` int(11) NOT NULL,
  `id_sabor` int(11) NOT NULL,
  `preco` decimal(10,2) NOT NULL DEFAULT 0.00,
  `descricao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.pizza_tamanhos
CREATE TABLE IF NOT EXISTS `pizza_tamanhos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '',
  `data_criacao` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.produto
CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT 0.00,
  `id_categoria` int(11) NOT NULL DEFAULT 0,
  `descricao` varchar(1500) NOT NULL DEFAULT '0',
  `foto_produto` varchar(500) DEFAULT NULL,
  `inativo` enum('s','n') DEFAULT 'n',
  `tipo` enum('padrao','pizza') NOT NULL DEFAULT 'padrao',
  `qtd_sabores` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.produto_adicional
CREATE TABLE IF NOT EXISTS `produto_adicional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `id_adicional` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.produto_pizza_sabores
CREATE TABLE IF NOT EXISTS `produto_pizza_sabores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL DEFAULT 0,
  `id_sabor` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela mirie_coxinhas.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
