**Link com vídeo de configuração, e com os arquivos e banco de dados atualizados para configurar o sistema:**
https://drive.google.com/drive/folders/1zjOdWovEy6gQiK80gLn3vaYKGn-XgFTM?usp=sharing



**Passos para a configuração do sistema:**

1 - Configurar o banco de dados, usuário do banco de dados e url do sistema no arquivo db.php, localizado na raiz do projeto.

2 - Garantir que o php.ini esteja com as configurações 'allow_url_fopen = On' e 'allow_url_include = On'





**Possíveis problemas e suas resoluções:**

Não estar exibindo as imagens: Verificar as permissões de pastas no diretório "dashboard/images";