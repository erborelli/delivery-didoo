-- Copiando estrutura para tabela mirie_coxinhas.pedido_item_sabor
CREATE TABLE IF NOT EXISTS `pedido_item_sabor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido_item` int(11) NOT NULL,
  `id_sabor` int(11) NOT NULL,
  `sabor` varchar(50) NOT NULL DEFAULT '',
  `valor` decimal(10,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
