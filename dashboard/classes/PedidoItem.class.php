<?php

class PedidoItem {

  /**
   * utilizado pra calcular valor de sabores de pizza
   */
  private static function calcularValorSabores($item){

     // produto pizza
     if($item['tipo']=='pizza'){
        // verificar tipo de valor de sabores
        $tipoValorSaborPizza = Config::getVariavelConfigHash('configValorPizza')['valor'];

        // maior valor entre os sabores
        if($tipoValorSaborPizza=='maior'){
          $valorItem = 0;
          foreach($item['sabores'] as $sabor){
            if($sabor['valor'] > $valorItem){
              $valorItem = $sabor['valor'];
            }
          }
          return $valorItem;
        }

        // valor medio dos sabores
        else if($tipoValorSaborPizza=='media'){
          $valorItem = 0;
          foreach($item['sabores'] as $sabor){
            $valorItem += $sabor['valor'];
          }
          return ($valorItem / $item['quantidade']);
        }
    }

    // se não for pizza
    else{
      return $item['produto']['valor'];
    }
  }


  /**
   * calcula valor dos adicionais de uma venda de celular
   */
  private static function calcularValorAdicionais($item){
    $valorTotalAdicionais = 0;
    if(isset($item['adicionais'])){
      foreach($item['adicionais'] as $adicional){
        $valorTotalAdicionais += $adicional['valor'];
      }
    }
    return $valorTotalAdicionais;
  }


  // utilizado na venda celular
  public static function calcularTotalVendaCelular(array $itens){
    $valorTotal = 0;
    foreach($itens as $item){
      $valorItem  = self::calcularValorSabores($item);
      $valorItem += self::calcularValorAdicionais($item);

      $valorTotal += ($valorItem * $item['quantidade']);
    }
    return $valorTotal;
  }


  //grava os itens do pedido da venda-celular
  public static function gravarPedidoItensVendaCelular($produtos,$idPedido){
    foreach ($produtos as $produto){

      if($produto['produto']['valor']>='999.00'){
        $produto['produto']['valor']=0;
      }

      $sql  = "INSERT INTO pedido_item(id_pedido,id_produto,nome_produto,qtd,valor,observacoes) VALUES(?,?,?,?,?,?)";
      $link = getConnection();
      $stmt = $link->prepare($sql);
      $stmt->execute([
        $idPedido,
        $produto['produto']['id'],
        $produto['produto']['nome'],
        $produto['produto']['quantidade'],
        0,
        $produto['produto']['observacoes'],
      ]);
      $idPedidoItemInserido = $link->lastInsertId();

      $valorItem = 0;
      $valorItem = self::calcularValorSabores($produto);
      $valorItem += self::calcularValorAdicionais($produto);

      self::atualizarValorPedidoItem($valorItem,$idPedidoItemInserido);
      self::gravarAdicionaisESaboresArray($idPedidoItemInserido,$produto);
    }
  }

  public static function atualizarValorPedidoItem($valor,$idPedidoItem){
    $sql  = "UPDATE pedido_item SET valor=? WHERE id=?";
    $link = getConnection();
    $stmt = $link->prepare($sql);
    $stmt->execute([$valor,$idPedidoItem]);
  }

  //grava adicionais e sabores da venda-celular
  public static function gravarAdicionaisESaboresArray($idPedidoItem,$produto){
    //salvar adicionais
    if(isset($produto['adicionais']) && !empty($produto['adicionais'])){
      foreach ($produto['adicionais'] as $key => $adicional){
        PedidoItemAdicional::gravarPedidoItemAdicional($idPedidoItem,$adicional['id'],$adicional['valor']);
      }
    }
    //salvar sabores
    if(isset($produto['sabores']) && !empty($produto['sabores'])){
      foreach ($produto['sabores'] as $key => $sabor){
        PedidoItemSabor::gravarPedidoItemSabor($idPedidoItem,$sabor['id'],$sabor['valor']);
      }
    }
  }


  public static function gravarPedidoItens($produtos,$idPedido){
    $cupom = Cupom::temCupomNaSessao();
    foreach ($produtos as $value) {
      $produto = !is_object($value) ? unserialize($value) : $value;
      if(!empty($produto)){
        $link = getConnection();
        // caso houver cupom salva o valor do produto com o desconto
        $produto->valor = Cupom::calcularValorProduto($produto->valor);
        $sql  = "INSERT INTO pedido_item (id_pedido,id_produto,nome_produto,qtd,valor,observacoes) VALUES (?,?,?,?,?,?)";
        $stmt = $link->prepare($sql);
        $stmt->execute([$idPedido,$produto->id,$produto->nome,$produto->quantidade,$produto->valor,$produto->observacao]);
        $idPedidoItemInserido = $link->lastInsertId();
        self::verificarSaboresEAdicionais($idPedidoItemInserido,$produto);
      }
    }
  }


  /**
   * método resopnsável por identificar sabores e adicionais no objeto do produtos
   * e caso encontrar adicionalos aos seus respectivos pedido item
   */
  public static function verificarSaboresEAdicionais($idPedidoItem,$obProduto){
    // verifica os adicionais
    if(isset($obProduto->adicionais) && !empty($obProduto->adicionais)){
      foreach ($obProduto->adicionais as $key => $value) {
        PedidoItemAdicional::gravarPedidoItemAdicional($idPedidoItem,$key,$value);
      }
    }
    if(isset($obProduto->sabores) && !empty($obProduto->sabores)){
      foreach ($obProduto->sabores as $key => $value) {
        PedidoItemSabor::gravarPedidoItemSabor($idPedidoItem,$key,$value);
      }
    }
  }


  public static function gravarPedidoItem($dados,$idPedido){
    if(empty($dados)) return false;
    $link = getConnection();
    $sql  = "INSERT INTO pedido_item (id_pedido,id_produto,nome_produto,qtd,valor)
            VALUES (?,?,?,?,?)";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedido,$dados->id,$dados->nome,$dados->quantidade,$dados->valor]);

    return $link->lastInsertId();
  }


  public static function gravarPedidoItemArray($dados,$idPedido){
    if(empty($dados)) return false;
    $link = getConnection();
    $sql  = "INSERT INTO pedido_item (id_pedido,id_produto,nome_produto,qtd,valor)
            VALUES (?,?,?,?,?)";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedido,$dados['id'],$dados['nome'],$dados['quantidade'],$dados['valor']]);

    return $link->lastInsertId();
  }


  public static function getItensPedido($idPedido){
    if(empty($idPedido) || !is_numeric($idPedido)) return false;
    $link = getConnection();

    $sql  = "SELECT * FROM pedido_item
             WHERE id_pedido = :idPedido";

    $stmt = $link->prepare($sql);
    $stmt->bindParam(':idPedido', $idPedido);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PedidoItem');
  }


  public static function getItensPedidoArray($idPedido){
    if(empty($idPedido) || !is_numeric($idPedido)) return false;
    $link = getConnection();

    $sql  = "SELECT * FROM pedido_item
             WHERE id_pedido = ?";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idPedido]);
    $pedidoItensArray = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //monta adicionais do item
    foreach($pedidoItensArray as $key => $itens){
      $pedidoItensArray[$key]['adicionais'] = PedidoItemAdicional::getAdicionaisItemPedidoArray($itens['id']);
      $pedidoItensArray[$key]['sabores']    = PedidoItemSabor::getSaboresItemPedido($itens['id']);
    }
    return $pedidoItensArray;
  }


  public static function calcularSubtotalPedido(array $produtos){
    $total = 0;
    foreach ($produtos as $key => $value) {
      $total += $value->valor * $value->quantidade;
    }
    return number_format($total,2);
  }


  public static function calcularSubtotal($produtos, $idsJaFormatados=null){

    $idsFormatados = '';
    if($idsJaFormatados==null){
      $ids=[];
      foreach($produtos as $produto){
        $ids[] = $produto->id;
      }
      $idsFormatados = implode(',',$ids);
    }else{
      $idsFormatados = $idsJaFormatados;
    }

    $link = getConnection();
    $sql  = "SELECT sum(valor) as valor FROM produto
             WHERE id IN($idsFormatados)";

    $stmt = $link->prepare($sql);
    $stmt->execute();
    $subTotal = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(isset($subTotal[0]['valor'])){
      return intval($subTotal[0]['valor']);
    }
    return 0;
  }
}
