<?php

class Agendamento{

  public static function formatarAgendamentoSession(){
    $agendamento     = 'n';
    $horaAgendamento = date('Y-m-d H:i:s');
    if(isset($_SESSION['carrinho']['agendamento'])){
      $agendamento     = 's';
      $dataFormatada   = date("Y-m-d", strtotime($_SESSION['carrinho']['agendamento']['dataAgendamento']));
      $horaAgendamento = $dataFormatada.' '.$_SESSION['carrinho']['agendamento']['horario'];
    }

    return
      [
        'agendado' =>$agendamento,
        'data-hora'=>$horaAgendamento
      ];
  }

  public static function getLayoutHorariosAgendamento($idDia,$data){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    $horariosAgendamento = Agendamento::getHorariosAgendamentoPorDia($idDia);
    $itens               = '';
    foreach ($horariosAgendamento as $key => $value) {
      $dataInicio = new DateTime($value->abre);
      $dataFim    = new DateTime($value->fecha);
      $dataInicio = $dataInicio->format('H:i:00');
      $dataFim    = $dataFim->format('H:i:50');
      $intervalo  = [];
      $period = new DatePeriod(
        DateTime::createFromFormat('H:i:s',$dataInicio),
        new DateInterval('PT'.INTERVALOTEMPOAGENDAMENTO.'M'),
        DateTime::createFromFormat('H:i:s',$dataFim)
      );
      $itensIntervalo = '';
      foreach ($period as $date) {
        $intervalo    = $date->format("H:i");
        $dataAgora    = DateTime::createFromFormat ('d/m/Y H:i:s', date('d/m/Y H:i:s'));
        $dataAgendado = DateTime::createFromFormat ('d/m/Y H:i:s', $data.' '.$intervalo.':00');
        $ativo        = $dataAgendado < $dataAgora ? 'disabled' : '';
        $itensIntervalo .= Sistema::getLayout(['data'=>$intervalo, 'ativo' => $ativo],'layout/agendamento','agendamento-horario-item-intervalo.html');
      }
      $dadosItem = ['itens'   => $itensIntervalo,
                    'inicia'  => $value->abre,
                    'termina' => $value->fecha];
      $itens .= Sistema::getLayout($dadosItem,'layout/agendamento','agendamento-horario-item.html');;
    }
    return Sistema::getLayout(['itens' => $itens],'layout/agendamento','agendamento-horario-estrutura.html');
  }

  public static function getHorariosAgendamentoPorDia($idDia){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    $link = getConnection();
    $sql  = "SELECT * FROM agendamento WHERE id_dia = ?";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idDia]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Agendamento');
  }

  public static function getDiasAPartirDeHojeFormatado($qtdDias){
    $days   = [];
    $period = new DatePeriod(
      new DateTime(),          // Começando da data de hoje
      new DateInterval('P1D'), // Define o intervalo entre os períodos
      $qtdDias                 // Aplica o intervalo 6 vezes, a partir da data inicial
    );
    foreach ($period as $day){
      $numeroDiaDaSemana = $day->format('w');
      // validação caso for domingo que é 0 eu passo para 7
      $numeroDiaDaSemana = $numeroDiaDaSemana == 0 ? 7 : $numeroDiaDaSemana;
      // formatação no array
      $days[$numeroDiaDaSemana] = [
        'diaSemana'       => $numeroDiaDaSemana,
        'data'            => $day->format('d/m/Y'),
        'dataSemFormatar' => $day->format('Y/m/d')
      ];
    }
    return $days;
  }

  public static function getLayoutDiasAgendamento(){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');

    $diasAgendamento = Agendamento::getDiasAgendamentoAgrupadoPorDia();
    $proximosDias    = Agendamento::getDiasAPartirDeHojeFormatado(6);
    $itens           = '';
    foreach ($proximosDias as $key => $value) {
      $dadosItens = (array)$value;
      $dadosItens['label'] = ucwords($diasAgendamento[$value['diaSemana']]->label);
      // validacao indisponivel
      $dadosItens['ativo']           = 'ativo';
      $dadosItens['indisponivel']    = '';
      $dadosItens['dataSemFormatar'] = $value['dataSemFormatar'];
      if($diasAgendamento[$value['diaSemana']]->fechado == 's'){
        $dadosItens['ativo']        = 'disabled';
        $dadosItens['indisponivel'] = '<span class="indisponivel">Indisponível</span>';
      }
      $itens .= Sistema::getLayout($dadosItens,'layout/agendamento','agendamento-data-item.html');;
    }
    return Sistema::getLayout(['itens' => $itens],'layout/agendamento','agendamento-data-estrutura.html');
  }

  public static function validarQuantidadeDeAgendamentosPorDataEHora($dataAgendamento, $horaAgendamento){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    $link = getConnection();
    $sql  = "SELECT count(id) as qtdAgendamentos FROM pedido WHERE agendamento = 's' AND id_status = 2 AND data_agendamento = ?";
    $stmt = $link->prepare($sql);
    $dataAgendamento = $dataAgendamento.' '.$horaAgendamento.':00';
    $stmt->execute([$dataAgendamento]);
    $quantidadeAgendamentos = $stmt->fetch(PDO::FETCH_ASSOC);
    return $quantidadeAgendamentos['qtdAgendamentos'];
  }

  public static function getDiasAgendamento(){
    $link = getConnection();
    $sql  = "SELECT * FROM agendamento ORDER BY id_dia ASC, id ASC";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'Agendamento');
  }

  public static function getDiasAgendamentoAgrupadoPorDia(){
    $link = getConnection();
    $sql  = "SELECT * FROM agendamento GROUP BY id_dia ORDER BY id_dia ASC, id ASC";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    $diasAgendamento = $stmt->fetchAll(PDO::FETCH_CLASS,'Agendamento');
    // formata o retorno
    $retorno = [];
    foreach ($diasAgendamento as $key => $value) {
      $retorno[$value->id_dia] = $value;
    }
    return $retorno;
  }

  public static function agruparDiasAgendamento($diasAgendamento){
    $retorno = [];
    foreach ($diasAgendamento as $key => $value) {
      $idDia = $value->id_dia;
      $retorno[$idDia][] = $value;
    }
    return $retorno;
  }

  public static function montarLayoutDiasAgendamento($diasAgendamento){
    $diasAgendamento = self::agruparDiasAgendamento($diasAgendamento);
    $retorno         = '';
    foreach ($diasAgendamento as $key => $value) {
      if(count($value) > 1){
        foreach ($value as $chave => $valor) {
          if($chave == 0){
            $layoutHorarioAdicionalAbreA  = '';
            $layoutHorarioAdicionalFechaA = '';
            $layoutHorarioAdicionalAbreB  = '';
            $layoutHorarioAdicionalFechaB = '';
            if(isset($value[1])){
              $layoutHorarioAdicionalAbreA = '<br><input maxlength="8" class="horarioAgendamento" name="abre['.$value[1]->id_dia.']['.$value[1]->id.']" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'" value="'.$value[1]->abre.'">';
              $layoutHorarioAdicionalFechaA = '<br><input maxlength="8" class="horarioAgendamento" name="abre['.$value[1]->id_dia.']['.$value[1]->id.']" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'" value="'.$value[1]->fecha.'"><span class="removerLinhaHorario" data-id-dia="'.$value[1]->id_dia.'" data-id="'.$value[1]->id.'"><i class="fas fa-minus-circle"></i><span></span></span>';
            }
            if(isset($value[2])){
              $layoutHorarioAdicionalAbreB = '<br><input maxlength="8" class="horarioAgendamento" name="abre['.$value[2]->id_dia.']['.$value[2]->id.']" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'" value="'.$value[2]->abre.'">';
              $layoutHorarioAdicionalFechaB = '<br><input maxlength="8" class="horarioAgendamento" name="abre['.$value[2]->id_dia.']['.$value[2]->id.']" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'" value="'.$value[2]->fecha.'"><span class="removerLinhaHorario" data-id-dia="'.$value[2]->id_dia.'" data-id="'.$value[2]->id.'"><i class="fas fa-minus-circle"></i><span></span></span>';
            }
            $checked           = $valor->fechado == 's' ? 'checked' : '';
            $mostrarBotaSalvar = $valor->fechado == 's' ? 'd-none' : '';
            $classFechado      = $valor->fechado == 's' ? 'disabledbutton' : '';
            $retorno .= '<tr>
            <th scope="row">'.$valor->label.'<br> <span data-id-dia="'.$valor->id_dia.'" class="addSubHorario">+ horário</span> </th>

            <td class="abre '.$classFechado.'">
            <input maxlength="8" data-id-dia="'.$valor->id_dia.'" data-id="'.$valor->id.'" class="horarioAgendamento" name="abre['.$valor->id.']['.$valor->id_dia.']" value="'.$valor->abre.'">
            '.$layoutHorarioAdicionalAbreA.'
            '.$layoutHorarioAdicionalAbreB.'
            </td>

            <td class="fecha '.$classFechado.'">
            <input maxlength="8" data-id-dia="'.$valor->id_dia.'" data-id="'.$valor->id.'" class="horarioAgendamento" name="fecha['.$valor->id.']['.$valor->id_dia.']" value="'.$valor->fecha.'">
            '.$layoutHorarioAdicionalFechaA.'
            '.$layoutHorarioAdicionalFechaB.'
            </td>

            <td class="fechado"><input class="inativarDataAgendamento" data-id="'.$valor->id.'" data-id-dia="'.$valor->id_dia.'" name="fechado['.$valor->id.']['.$valor->id_dia.']" '.$checked.' type="checkbox"></td>
            <td class="salvar salvarAgendamento'.$mostrarBotaSalvar.'" data-id="'.$valor->id.'" data-id-dia="'.$valor->id_dia.'"><button class="botaoSalvar">salvar</button></td>
            </tr>';
            break;
          }
        }

      }else {
        $value             = $value[0];
        $checked           = $value->fechado == 's' ? 'checked' : '';
        $mostrarBotaSalvar = $value->fechado == 's' ? 'd-none' : '';
        $classFechado      = $value->fechado == 's' ? 'disabledbutton' : '';
        $retorno .= '<tr>
        <th scope="row">'.$value->label.'<br> <span data-id-dia="'.$value->id_dia.'" class="addSubHorario">+ horário</span> </th>

        <td class="abre '.$classFechado.'">
        <input maxlength="8" data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" class="horarioAgendamento" name="abre['.$value->id.']['.$value->id_dia.']" value="'.$value->abre.'">
        </td>

        <td class="fecha '.$classFechado.'">
        <input maxlength="8" data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" class="horarioAgendamento" name="fecha['.$value->id.']['.$value->id_dia.']" value="'.$value->fecha.'">
        </td>

        <td class="fechado"><input class="inativarDataAgendamento" data-id-dia="'.$value->id_dia.'" data-id="'.$value->id.'" name="fechado['.$value->id.']['.$value->id_dia.']" '.$checked.' type="checkbox"></td>
        <td class="salvar salvarAgendamento'.$mostrarBotaSalvar.'" data-id="'.$value->id.'" data-id-dia="'.$value->id_dia.'"><button class="botaoSalvar">salvar</button></td>
        </tr>';
      }
    }
    return $retorno;
  }

  public static function excluirHorariosPorIdDia($idDia){
    $link = getConnection();
    $sql  = "DELETE FROM agendamento WHERE id_dia = ".$idDia;
    $stmt = $link->prepare($sql);
    $stmt->execute();
  }

  public static function getLabelPorIdDia($idDia){
    if(!is_numeric($idDia)) return false;
    $relacaoIdDiaLabel = [
      1 => 'segunda',
      2 => 'terca',
      3 => 'quarta',
      4 => 'quinta',
      5 => 'sexta',
      6 => 'sabado',
      7 => 'domingo',
    ];
    return $relacaoIdDiaLabel[$idDia];
  }

  public static function updateHorarioAgendamento($dadosPost){
    if(!isset($dadosPost['abre']) || !isset($dadosPost['fecha']) || !isset($dadosPost['id_dia'])) return false;

    $dadosAbre  = $dadosPost['abre'];
    $dadosFecha = $dadosPost['fecha'];
    self::excluirHorariosPorIdDia($dadosPost['id_dia']);
    $link  = getConnection();
    $label = self::getLabelPorIdDia($dadosPost['id_dia']);
    foreach ($dadosAbre as $key => $value) {
      $sql  = "INSERT INTO agendamento (id_dia,label,abre,fecha) VALUES (?,?,?,?)";
      $stmt = $link->prepare($sql);
      $stmt->execute([$value['idDia'],$label,$value['abre'],$dadosFecha[$key]['fecha']]);
    }
    return true;
  }

  public static function inativarDataAgendamento($dadosPost){
    $link = getConnection();
    $sql  = "UPDATE agendamento SET fechado=? WHERE id_dia = ?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['fechado'],$dadosPost['id_dia']]);
    return true;
  }

  public static function getAgendamentoLoja(){
    // time zone SP
    date_default_timezone_set('America/Sao_Paulo');
    // inicia algumas variaveis
    $retorno = ['aberta' => false, 'horarioFechamento' => ''];
    // link conexao
    $link = getConnection();
    // recupero o id do dia da semana
    $idDataHoje = date('N');
    // monto a sql
    $sql  = "SELECT * FROM agendamento WHERE id_dia = ?";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idDataHoje]);
    $dadosAgendamento  = $stmt->fetchAll(PDO::FETCH_CLASS,'Agendamento');
    $lojaAberta        = self::verificarLojaAberta($dadosAgendamento);
    $retorno['aberta'] = $lojaAberta['aberta'];
    $retorno['horarioFechamento'] = $lojaAberta['horarioFechamento'];
    return $retorno;
  }

  public static function verificarAgendamentoSessao(){
    $retorno = false;
    // caso estiver setado carrinho e agendamento for preenchido
    if(isset($_SESSION['carrinho']) &&
       isset($_SESSION['carrinho']['agendamento']) &&
       !empty($_SESSION['carrinho']['agendamento'])) return true;
    return $retorno;
  }

  public static function formatarAgendamentoArray(array $agendamentoArray){
    $agendamento     = 'n';
    $horaAgendamento = date('Y-m-d H:i:s');
    if(isset($agendamentoArray['agendamento'])){
      $agendamento     = 's';
      $dataFormatada   = date("Y-m-d", strtotime($agendamentoArray['agendamento']['data']));
      $horaAgendamento = $dataFormatada.' '.$agendamentoArray['agendamento']['hora'];
    }

    return
      [
        'agendado' =>$agendamento,
        'data-hora'=>$horaAgendamento
      ];
  }
}
