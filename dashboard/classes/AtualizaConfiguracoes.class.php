<?php

class AtualizaConfiguracoes{
    private $NOME_BANCO          = "DEFINE('NOME_BANCO','{{NOME_BANCO}}');";
    private $USUARIO_BANCO       = "DEFINE('USUARIO_BANCO','{{USUARIO_BANCO}}');";
    private $SENHA_BANCO         = "DEFINE('SENHA_BANCO','{{SENHA_BANCO}}');";
    private $PASTA_SISTEMA       = '{{PASTA_SISTEMA}}';
    private $CAMINHO             = "DEFINE('CAMINHO','{{CAMINHO}}');";
    private $CAMINHO_DASHBOARD   = 'DEFINE("CAMINHO_DASHBOARD",CAMINHO."dashboard/");';
    private $CAMINHO_IMAGEM      = 'DEFINE("CAMINHO_IMAGEM",CAMINHO."dashboard/images/");';
    private $CAMINHO_SISTEMA     = 'DEFINE("CAMINHO_SISTEMA",$_SERVER["DOCUMENT_ROOT"]."/{{PASTA_SISTEMA}}");';
    private $CAMINHO_UPLOAD      = 'DEFINE("CAMINHO_UPLOAD",$_SERVER["DOCUMENT_ROOT"]."/{{PASTA_SISTEMA}}dashboard/images/");';
    private $CAMINHO_COMPONENTES = 'DEFINE("CAMINHO_COMPONENTES",$_SERVER["DOCUMENT_ROOT"]."/{{PASTA_SISTEMA}}componentes/");';

    public function definirConfiguracoes(){
        $br  = chr(10);
        $br2 = $br.$br;

        $arquivo = fopen('../configuracao.php','w');
        $linhas = [
            '<?php'.$br2,
            '// CONFIGURAÇÕES DO BANCO'.$br,
            $this->NOME_BANCO.$br,
            $this->USUARIO_BANCO.$br,
            $this->SENHA_BANCO.$br2,
            '// URL DO SISTEMA'.$br,
            $this->CAMINHO.$br2,
            '// OUTRAS CONFIGURAÇÕES'.$br,
            $this->CAMINHO_DASHBOARD.$br,
            $this->CAMINHO_IMAGEM.$br,
            $this->CAMINHO_SISTEMA.$br,
            $this->CAMINHO_UPLOAD.$br,
            $this->CAMINHO_COMPONENTES
        ];

        foreach($linhas as $linha){
            fwrite($arquivo, $linha);
        }
        fclose($arquivo);
    }

    /**
     * NOME_BANCO, USUARIO_BANCO, SENHA_BANCO, CAMINHO, PASTA_SISTEMA
     */
    public function setParametros(array $parametros){
        foreach($parametros as $key => $p){
            $this->$key = str_replace('{{'.$key.'}}',$p,$this->$key);
        }
        $this->CAMINHO_SISTEMA     = str_replace('{{PASTA_SISTEMA}}',$this->PASTA_SISTEMA,$this->CAMINHO_SISTEMA);
        $this->CAMINHO_UPLOAD      = str_replace('{{PASTA_SISTEMA}}',$this->PASTA_SISTEMA,$this->CAMINHO_UPLOAD);
        $this->CAMINHO_COMPONENTES = str_replace('{{PASTA_SISTEMA}}',$this->PASTA_SISTEMA,$this->CAMINHO_COMPONENTES);
    }

    public function verificarConfiguracao(){
        if(file_exists('../configuracao.php')){
            $this->carregarConfiguracoes();
            return defined('NOME_BANCO');
        }else{
            return false;
        }
    }

    public function carregarConfiguracoes(){
        include_once '../configuracao.php';
    }
}