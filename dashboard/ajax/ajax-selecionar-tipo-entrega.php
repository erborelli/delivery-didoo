<?php

include_once('../includes.php');

$retorno = ['boxRetorno' => '', 'tipoEndereco' => '', 'sucesso' => false];

// verifica se a loja está aberta e se não está setado nenhum agendamento
if(Atendimento::ALojaEstaFechadaESemAgendamento()){
  echo json_encode(['lojaFechada' => true]);exit;
}

// caso passar por aqui quer dizer que está tudo ok
if(isset($_POST) && isset($_POST['opcaoEscolhida'])){
  $opcaoEscolhida     = $_POST['opcaoEscolhida'];
  $retorno['sucesso'] = true;

  // aqui eu vou 3 possíveis escolhas
  // 1: O CLIENTE ESCOLHEU RETIRADA, RETORNO O ENDEREÇO DO LUGAR
  if($opcaoEscolhida == 'retirada'){
    $enderecoRuaNumeroBairro = RUA.', '.NUMERO.' - '.BAIRRO;
    $dadosEndereco = ['endereco' => $enderecoRuaNumeroBairro, 'cidade' => CIDADE, 'uf' => UF, 'cep' => CEP];
    $retorno['boxRetorno'] = Sistema::getLayout($dadosEndereco,'layout/endereco','endereco-retirada.html');
    echo json_encode($retorno);
    exit;
  }

  // 2: O CLIENTE ESCOLHEU ENTREGA, E A CONFIG DO RESTAURANTE É POR CEP
  if($opcaoEscolhida == 'entrega' && INFORMARENDERECO == 'cep'){
    $retorno['tipoEndereco'] = 'cep';
  }elseif ($opcaoEscolhida == 'entrega' && INFORMARENDERECO == 'digitando') {
  // 3: O CLIENTE ESCOLHEU ENTREGA, E A CONFIG DO RESTAURANTE É DIGITAR O ENDEREÇO MANUAL
    $retorno['tipoEndereco'] = 'digitando';
  }

}

echo json_encode($retorno);
