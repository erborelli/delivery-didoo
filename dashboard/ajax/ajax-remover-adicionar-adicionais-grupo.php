<?php

include_once('../includes.php');

if(!isset($_GET['tipo']) || !isset($_GET['idAdicional'])) return;

$idAdicional = $_GET['idAdicional'];
$idGrupo     = $_GET['idAdicionalGrupo'];

if($_GET['tipo']=='adicionar'){
    AdicionalGrupo::inserirAdicionalNoGrupo($idAdicional,$idGrupo);
}else if($_GET['tipo']=='remover'){
    AdicionalGrupo::removerAdicionalDoGrupo($idAdicional,$idGrupo);
}
