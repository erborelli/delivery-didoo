<?php
include_once('../../../includes.php');

$idCupom = $_GET['id'];
$cupom   = Cupom::getCupomPorId($idCupom);
$cupom   = $cupom[0];

// caso houver o post da ação da edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  Cupom::updateCupom($_POST);
  $cupom = Cupom::getCupomPorId($idCupom);
  $cupom = $cupom[0];

  // caso tenha atualizado redireciona para a listagem
  if(isset($cupom)){
    header('location: '.CAMINHO_DASHBOARD.'painel/pages/cupom/');
  }
}

if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/cupom/');
}

// caso tenha retornado false
if(!$cupom){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/cupom/');
}

?>


<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Cupom</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Cupom</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Editar Produto</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                  <div class="form-group col-md-6" style="float:left;">
                    <label for="nome">Nome Cupom</label>
                    <input type="text" name="nome" id="nome" value="<?=$cupom->nome_cupom?>" class="form-control" value="">
                  </div>
                  <div class="form-group col-md-6" style="float:left;">
                    <label for="hash">Hash Cupom</label>
                    <input type="text" name="hash" id="hash" value="<?=$cupom->hash_cupom?>" class="form-control"></input>
                  </div>
                  <div class="form-group col-md-6" style="float:left;">
                    <label for="desconto">% Desconto</label>
                    <input type="text" name="desconto" id="desconto" value="<?=$cupom->valor_desconto?>" class="form-control"></input>
                  </div>
                  <div class="form-group col-md-6" style="float:left;">
                    <label for="qtd_disponivel">Qtd Disponível</label>
                    <input type="text" name="qtd_disponivel" id="qtd_disponivel" value="<?=$cupom->qtd_disponivel?>" class="form-control"></input>
                  </div>

              </div>

              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>

          </div>

        </form>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <script type="text/javascript">
      $('#desconto').mask("#.##0.00", {reverse: true});
    </script>
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
