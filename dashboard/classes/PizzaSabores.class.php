<?php

class PizzaSabores {

  public static function getVinculosSaboresPizzaPorIdProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT *, a.id as idSabor FROM pizza_sabores a
             LEFT JOIN produto_pizza_sabores b ON a.id = b.id_sabor
             AND b.id_produto = ".$idProduto.' ORDER BY a.id';
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function getVinculosSaboresPizzaPorIdProdutoBuscaSabor($idProduto,$sabor){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT *, a.id as idSabor
             FROM pizza_sabores a
             LEFT JOIN produto_pizza_sabores b
             ON a.id = b.id_sabor
             AND b.id_produto = ?
             WHERE a.label LIKE '%$sabor%'
             ORDER BY a.id";

    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function getLabelSaboresPorIds(array $idsSabores){
    $idsSabores = implode(',',$idsSabores);
    $link = getConnection();
    $sql  = "SELECT b.label FROM pizza_sabores_valores a
             INNER JOIN pizza_sabores b on a.id_sabor = b.id
             WHERE a.id IN (".$idsSabores.")";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function getSaboresPizzaPorIdProduto($idProduto){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT ps.label as sabor, p.qtd_sabores, psv.preco, psv.id as idSaborValor
            FROM produto_pizza_sabores pps
            INNER JOIN pizza_sabores as ps
            ON pps.id_sabor = ps.id
            INNER JOIN produto as p
            ON p.id = pps.id_produto
            INNER JOIN pizza_sabores_valores as psv
            ON psv.id_sabor = ps.id
            AND psv.id_tamanho = p.id_tamanho
            WHERE pps.id_produto = ?";
    $stmt = $link->prepare($sql);
    $stmt->execute([$idProduto]);
    return $stmt->fetchAll();
  }

  public static function getSaboresPizza(){
    $link = getConnection();
    $sql  = "SELECT * FROM pizza_sabores";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function gravarSaborPizza($dadosPost){
    if(!isset($dadosPost) || empty($dadosPost)) return false;
    $link = getConnection();
    $sql = "INSERT INTO pizza_sabores (label,descricao) VALUES (?,?)";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['label'],$dadosPost['descricao']]);
    return $link->lastInsertId();
  }

  public static function alterarSaborPizza($dadosPost){
    if(!isset($dadosPost) || empty($dadosPost)) return false;
    $link = getConnection();
    $sql = "UPDATE pizza_sabores SET label=?,descricao=? WHERE id =?";
    $stmt= $link->prepare($sql);
    $stmt->execute([$dadosPost['label'],$dadosPost['descricao'],$_GET['id']]);
    return true;
  }

  public static function getVinculoPizzaTamanhoSaboresValores($idTamanho,$idSabor){
    if(!is_numeric($idTamanho) || !is_numeric($idSabor)) return false;
    $link = getConnection();
    $sql  = 'SELECT id FROM pizza_sabores_valores WHERE id_tamanho=? AND id_sabor=?';
    $stmt = $link->prepare($sql);
    $stmt->execute([$idTamanho,$idSabor]);
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function updateVinculosPizzaTamanhoSaboresValores($tamanhosValores,$idSabor){
    if(!isset($tamanhosValores) || empty($tamanhosValores) || !is_numeric($idSabor)) return false;
    $link = getConnection();
    foreach ($tamanhosValores as $key => $value) {
      $vinculo = self::getVinculoPizzaTamanhoSaboresValores($key,$idSabor);
      if(empty($vinculo)){
        $sql  = 'INSERT INTO pizza_sabores_valores (id_tamanho,id_sabor,preco) VALUES (?,?,?)';
        $stmt = $link->prepare($sql);
        $stmt->execute([$key,$idSabor,$value]);
      }else {
        $sql  = 'UPDATE pizza_sabores_valores SET preco=? WHERE id_tamanho=? AND id_sabor=?';
        $stmt = $link->prepare($sql);
        $stmt->execute([$value,$key,$idSabor]);
      }
    }
    return true;
  }

  public static function gravarVinculosPizzaTamanhoSaboresValores($tamanhosValores,$idSabor){
    if(!isset($tamanhosValores) || empty($tamanhosValores) || !is_numeric($idSabor)) return false;
    $link = getConnection();
    $sql = "INSERT INTO pizza_sabores_valores (id_tamanho,id_sabor,preco) VALUES (?,?,?)";
    $stmt= $link->prepare($sql);
    foreach ($tamanhosValores as $key => $value) {
      $stmt->execute([$key,$idSabor,$value]);
    }
    return true;
  }

  public static function getSaborPizzaPorIdSabor($idSabor){
    if(!is_numeric($idSabor)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM pizza_sabores
             WHERE id = ".$idSabor;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getTamanhosValoresPizzaPorIdSabor($idSabor){
    if(!is_numeric($idSabor)) return false;
    $link = getConnection();
    $sql  = "SELECT * FROM pizza_sabores_valores a
             INNER JOIN pizza_tamanhos b on a.id_tamanho = b.id
             WHERE a.id_sabor = ".$idSabor;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function formatarArraySabores($saboresValores){
    if(!isset($saboresValores) || empty($saboresValores)) return [];
    $aux = [];
    foreach ($saboresValores as $key => $value) {
      $aux[$value->id] = $saboresValores[$key];
    }
    return $aux;
  }

  public static function getValorPizzaVendaInterna($saboresPizza){
    $valorPizza = 0;
    $configValorPizza = Config::getConfigEspecifico('configValorPizza');

    //retorna o valor do sabor mais caro
    if($configValorPizza=='maior'){
      forEach($saboresPizza as $sabores){
        if(floatval($sabores['valor']) > $valorPizza){
          $valorPizza=floatval($sabores['valor']);
        }
      }
    }

    // retorna o valor médio dos sabores
    if($configValorPizza=='media'){
      $qtdSabores = 0;
      $valorTotal = 0;
      forEach($saboresPizza as $sabores){
        $valorTotal += floatval($sabores['valor']);
        $qtdSabores++;
      }
      $valorPizza = ($valorTotal/$qtdSabores);
    }

    return $valorPizza;
  }

  public static function getValorPizzaAPartirDoIdProdutoESabor($idProduto,$idSabor){
    if(!is_numeric($idProduto) || !is_numeric($idSabor)) return '';
    $link = getConnection();
    $sql  = "SELECT b.preco FROM produto a
             INNER JOIN pizza_sabores_valores b ON a.id_tamanho = b.id_tamanho
             AND a.id       = ".$idProduto."
             AND b.id_sabor = ".$idSabor;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

  public static function getValorPizzaAPartirDoIdSaborValor($idVinculoSaborValorPizza){
    if(!is_numeric($idVinculoSaborValorPizza)) return '';
    $link = getConnection();
    $sql  = "SELECT preco FROM pizza_sabores_valores
             WHERE id =".$idVinculoSaborValorPizza;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getSaborPizzaAPartirDoIdSaborValor($idVinculoSaborValorPizza){
    if(!is_numeric($idVinculoSaborValorPizza)) return '';
    $link = getConnection();
    $sql  = "SELECT b.id,b.label
             FROM pizza_sabores_valores a
             INNER JOIN pizza_sabores b on a.id_sabor = b.id
             WHERE a.id = ".$idVinculoSaborValorPizza;
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public static function getSaboresPizzaHome($idProduto,$idTamanho){
    if(!is_numeric($idProduto)) return false;
    $link = getConnection();
    $sql  = "SELECT *, (SELECT c.qtd_sabores FROM produto c WHERE c.id = ".$idProduto.") as qtdSabores
             FROM pizza_sabores a
             INNER JOIN produto_pizza_sabores b ON a.id = b.id_sabor AND b.id_produto = ".$idProduto."
             INNER JOIN pizza_sabores_valores d ON d.id_sabor = b.id_sabor AND d.id_tamanho = ".$idTamanho."
             ORDER BY d.preco";
    $stmt = $link->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS,'PizzaSabores');
  }

}
