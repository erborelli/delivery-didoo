<?php
include_once('../../../includes.php');

$entregadores = Entregador::getEntregadores();

?>
<!DOCTYPE html>
<html>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Entregadores</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Entregadores</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Modal -->
    <div class="modal fade" id="modalEntregas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div style="max-width:700px;" class="modal-dialog" role="document">
        <div class="modal-content" style="width:700px;margin:0 auto;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Entregas realizadas:</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <div style="margin-top:20px;margin-left:20px;">
              <label>Filtrar Por Data:</label>
              <input id="date" class="filtrarDataEntregador" type="text" value="<?=date('d-m-Y - d-m-Y')?>">
              <button data-id-entregador="" class="filtraPorDataEntregador" style="display:none;" type="button" name="button">Filtrar</button>
            </div>

          <div id="conteudoEntregas">
            <style>
            /* css etiqueta */
            html,body{
              margin:0px;
              padding:0px;
            }
            #modalEntregas .modal-content{
              max-width:700px;
              margin:0 auto;
            }
            #modalEntregas .modal-dialog{
              max-width:700px;
            }
            .modal-body.text-center .table td, .table th {
              padding: 0px;
              vertical-align: middle;
              border-top: 1px solid #dee2e6;
            }
            .text-center {
              text-align: center!important;
            }
            .modal-body {
              position: relative;
              -ms-flex: 1 1 auto;
              flex: 1 1 auto;
              padding: 1rem;
            }
            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }
            </style>
            <div style="width: 680px;" class="modal-body text-center">
              <img width="200" src="<?=CAMINHO_DASHBOARD?>images/preloader/preloader.gif">
            </div>
          </div>
          <div class="modal-footer">
            <div class="containerImpressaoEntregadores">
              <i class="fas fa-print">&nbsp;&nbsp;</i>Imprimir
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/entregadores/criar.php">
        <i class="fas fa-plus"></i> Novo Entregador
      </a>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Entregadores</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 5%">
                          #
                      </th>
                      <th style="width: 35%">
                          Nome Entregador
                      </th>
                      <th style="width: 35%">
                          Entregas Feitas
                      </th>
                      <th>
                          Data criação
                      </th>
                  </tr>
              </thead>
              <tbody>

                <?php foreach ($entregadores as $key => $value) {

             echo '<tr>
                      <td>#</td>
                      <td><a>'.$value->nome.'</a><br></td>
                      <td data-toggle="modal" data-target="#modalEntregas" data-id-entregador="'.$value->id.'" class="verEntregas">Ver Entregas</td>
                      <td class="project_progress">
                          <small>'.$value->data_criacao.'</small>
                      </td>

                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="'.CAMINHO_DASHBOARD.'painel/pages/entregadores/editar.php?id='.$value->id.'">
                              <i class="fas fa-pencil-alt"></i>Editar
                          </a>
                          <a data-id="'.$value->id.'" data-modulo="entregador" class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash"></i>Excluir
                          </a>
                      </td>
                  </tr>';
                }
                  ?>

              </tbody>
          </table>
        </div>
      </div>
      <a class="btn btn-primary btn-sm" href="<?=CAMINHO_DASHBOARD?>painel/pages/entregadores/criar.php">
          <i class="fas fa-plus"></i> Novo Entregador
      </a>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- date range picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- Page Script -->
<?php
$hoje = date('d/m/Y');
 ?>
<script>

  $(function() {
    $('#date').daterangepicker({
      opens: 'left',
      minYear: 2020,
      maxYear: parseInt(moment().format('YYYY'),10),
      startDate: '<?=$hoje?>',
      endDate  : '<?=$hoje?>',
      locale: {
        format: 'DD/MM/YYYY'
      }
    }, function(start, end, label) {
      // $(document).on('click','.filtraPorDataEntregador',function(){
        var idEntregador = $('.filtraPorDataEntregador').attr('data-id-entregador');
        var dataFiltrar  = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        dataFiltrar = dataFiltrar.split("/").join("-");;
        $('.filtraPorDataEntregador').attr('data-id-entregador',idEntregador);
        $.ajax({
          url     : CAMINHO_DASHBOARD+'ajax/ajax-get-entregas-entregador.php',
          type    : 'POST',
          data    : {
            idEntregador : idEntregador,
            dataFiltrar  : dataFiltrar
          },
          success:function(data){
            console.log(data);
            $('.modal-body.text-center').html(data);
          },
          error:function(data){
            console.log(data);
          }
        });
      // });
    });
  });

  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>
