<?php

$adicionaisModal = Adicional::getAdicionais(true);

?>

<div class="modal fade" id="modalAdicionais" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Adicionar adicionais ao grupo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Selecionar</th>
                    <th>Nome</th>
                    <th>Preço</th>
                </tr>
            </tbody>
            <tbody id='tbodyAdicionaisSelecionados'>
                <?php foreach($adicionaisModal as $adicionalMd){ ?>
                    <tr>
                        <td>
                            <input type="checkbox">
                        </td>
                        <td style="display: none;"><?=$adicionalMd['id']?></td>
                        <td><?=$adicionalMd['nome']?></td>
                        <td><?=$adicionalMd['preco']?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button onclick="adicionarModal()" type="button" class="btn btn-primary">Adicionar</button>
      </div>
    </div>
  </div>
</div>
<script>
    function desmarcarTodos(){
        let selecionados = document.getElementById('tbodyAdicionaisSelecionados')
        let trs = selecionados.getElementsByTagName('tr')

        // percorre todas as linhas da tabela de adicionais
        for(let l=0;l<trs.length;l++){
            let td = trs[l].getElementsByTagName('td')
            td[0].getElementsByTagName('input')[0].checked=false
        }
    }

    function verificaSeJaTemAdicional(idAdicional){
        let tbodyAdicionais = document.getElementById('tbodyAdicionais')
        let trJaAdicionados = tbodyAdicionais.getElementsByTagName('tr')
        for(let s=0;s<trJaAdicionados.length;s++){
            // verifica se já existe id
            let tdId = trJaAdicionados[s].getElementsByTagName('td')[0].textContent
            if(tdId==idAdicional){
                return true
            }
        }
        return false
    }

    // fecha modal e adiciona adicionais na tabela da pagina 'grupos de adicionais'
    function adicionarModal(){
        $('#modalAdicionais').modal('hide')
        let idAdicionalGrupo = document.getElementById('idAdicionalGrupo').value
        let tbodyAdicionais  = document.getElementById('tbodyAdicionais')
        let selecionados     = document.getElementById('tbodyAdicionaisSelecionados')
        let trs = selecionados.getElementsByTagName('tr')

        // percorre todas as linhas da tabela de adicionais
        for(let l=0;l<trs.length;l++){
            let td = trs[l].getElementsByTagName('td')

            // se adicional estiver marcado
            if(td[0].getElementsByTagName('input')[0].checked==true){

                // se ainda não tem o adicional, adiciona linha
                if(verificaSeJaTemAdicional(td[1].textContent)==false){

                    let tr      = document.createElement('tr')
                    let tdId    = document.createElement('td')
                    let tdNome  = document.createElement('td')
                    let tdPreco = document.createElement('td')
                    let tdBtn   = document.createElement('td')
                    let btn     = document.createElement('button')
                    let i       = document.createElement('i')

                    tdId.style.display = 'none'
                    i.className   = 'fas fa-trash'
                    btn.className = 'btn btn-danger btn-sm'
                    btn.style     = 'color: white;'
                    btn.setAttribute('onclick','removerAdicional(this)')
                    btn.append(i)
                    btn.append(' Remover')
                    tdBtn.append(btn)

                    tdId.textContent    = td[1].textContent
                    tdNome.textContent  = td[2].textContent
                    tdPreco.textContent = td[3].textContent

                    tr.append(tdId)
                    tr.append(tdNome)
                    tr.append(tdPreco)
                    tr.append(tdBtn)
                    
                    tbodyAdicionais.append(tr)

                    $.ajax({
                        url     : CAMINHO_DASHBOARD+'ajax/ajax-remover-adicionar-adicionais-grupo.php?tipo=adicionar&idAdicional='+td[1].textContent+'&idAdicionalGrupo='+idAdicionalGrupo,
                        type    : 'GET',
                        dataType: 'JSON',
                    })
                }
            }
        }
    }
</script>