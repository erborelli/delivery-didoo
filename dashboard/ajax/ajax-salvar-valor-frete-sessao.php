<?php

include_once('../includes.php');

if(!isset($_POST['valorSalvarSessao']) || !isset($_POST['keyValorSessao'])){
  echo json_encode([false]);exit;
}

$_SESSION['venda_interna']['frete'][$_POST['keyValorSessao']] = $_POST['valorSalvarSessao'];

echo json_encode($_POST['valorSalvarSessao']);
