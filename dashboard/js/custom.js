$(document).on('change','.filtrarCategoria',function(){
  var valorCategoria = $(this).val();
  window.location    = CAMINHO_DASHBOARD+'painel/pages/produtos?filtrarCategoria='+valorCategoria;
});

$(document).on('change','.selectTipoProduto',function(){
  var valorSelecionado = $(this).val();
  if(valorSelecionado == 'pizza'){
    $('.formPreco').hide();
    $('.formPreco input').val(999);
    $('.avisoTipoProduto').show();
  }else {
    $('.formPreco').show();
    $('.formPreco input').val('');
    $('.avisoTipoProduto').hide();
  }
});

$(document).on('change','.selectStatusPedido',function(){
  var elementoClicado = $(this);
  var novoStatus      = $(this).val();
  var idPedido        = $(this).parents('.tr-container-linha-pedido').attr('id-pedido');
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-alterar-status-pedido.php',
    type :  'POST',
    async: false,
    data : {idPedido  : idPedido,
            novoStatus: novoStatus},
    success: function(data){
      // mensagem pedido atualizado
      toastr.options.positionClass = 'toast-top-right';
      toastr.options.progressBar   = false;
      toastr.success('Status Pedido Atualizado!')

      $(elementoClicado).parents('.tr-container-linha-pedido').removeClass('tr-pedido-negado tr-pedido-aceito tr-pedido-pendente tr-pedido-cancelado');
      switch (novoStatus) {
        case '1':
          $(elementoClicado).parents('.tr-container-linha-pedido').addClass('tr-pedido-pendente');
          break;
        case '2':
        console.log('aceito');
          $(elementoClicado).parents('.tr-container-linha-pedido').addClass('tr-pedido-aceito');
          break;
        case '3':
          $(elementoClicado).parents('.tr-container-linha-pedido').addClass('tr-pedido-negado');
          break;
        case '4':
          $(elementoClicado).parents('.tr-container-linha-pedido').addClass('tr-pedido-cancelado');
          break;
      }
    },
    error: function(data){
      console.log(data);
    },
  });
});

$(document).on('click','.deixarObsAoCliente',function(){
  var idPedido = $(this).attr('data-id-pedido');
  $('#containerCamposPedido'+idPedido).slideToggle();
});

// notificação dashboard novos pedidos
setInterval(function(){
  if($('#modalAvisoNovoPedido').is(':visible') || $('.wrap-login100').length) return false;

  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-buscar-novos-pedidos.php',
    type    : 'GET',
    dataType: 'JSON',
    success: function(data){
      if(data.novosPedidos){
        alertaNovosPedidos(data);
      }
    },
    error: function(data){
      console.log(data);
    },
  });
},3000);

function getDataHoraAtual(){
  // Obtém a data/hora atual
  var data = new Date();
  // Guarda cada pedaço em uma variável
  var dia     = data.getDate();           // 1-31
  var mes     = data.getMonth();          // 0-11 (zero=janeiro)
  var ano     = data.getFullYear();       // 4 dígitos
  var hora    = data.getHours();          // 0-23
  var min     = data.getMinutes();        // 0-59
  var seg     = data.getSeconds();        // 0-59
  // Formata a data e a hora (note o mês + 1)
  var str_data = ano + '-' + (mes+1) + '-' + dia;
  var str_hora = hora + ':' + min + ':' + seg;
  return str_data + ' ' + str_hora;
}

function alertaNovosPedidos(dadosNovosPedidos){
  // abre o modal
  $('.abrirModalAvisoNovoPedido').trigger('click');
  var dataHoraAbertura = getDataHoraAtual();
  $('#modalAvisoNovoPedido').attr('data-hora-abertura',dataHoraAbertura);
  tocarAudioPedido();
  $('#modalAvisoNovoPedido .qtdPedidosPendentes').html(dadosNovosPedidos.qtdPedidos);
  $('.conteudoModalPedido .layoutPedidos').html(dadosNovosPedidos.layoutPedidos);
}

// botao negar todos os pedidos
$(document).on('click','.botaoNegarTodosPedidos',function(){
  if (confirm('Tem certeza que deseja cancelar TODOS os pedidos?')) {
    cancelarTodosPedidos($('#modalAvisoNovoPedido').attr('data-hora-abertura'));
  } else {
    alert('ufa... quase!');
  }
});

// botao negar o pedido unico
$(document).on('click','.botaoNegarPedido',function(){
  if (confirm('Tem certeza que deseja cancelar o pedido?')) {
    cancelarPedido($(this).attr('data-id-pedido'));
  }
});

// botao visualizar o pedido
$(document).on('click','.botaoVisualizarPedido',function(){
  var idPedido  = $(this).attr('data-id-pedido');
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-visualizar-pedido.php',
    type :  'POST',
    data : {idPedido : idPedido},
    success: function(data){
      $('.containerPedido'+idPedido).find('.botaoVisualizarPedido').replaceWith('');
      var qtdPedidosPendentes = $( ".layoutPedidos .conteudoPedido" ).length;
      // decresce a qtd de itens no pedido
      $('.qtdPedidosPendentes').html(qtdPedidosPendentes);
      // fecha o modal
      // if(qtdPedidosPendentes == 0){
      //   $('#modalAvisoNovoPedido .close').trigger('click');
      // }
    },
    error: function(date){
      console.log(data);
    }
  });
});

// botao aceitar o pedido
$(document).on('click','.botaoAceitarPedido',function(){
  var idPedido  = $(this).attr('data-id-pedido');
  var obsPedido = $('#obsAoCliente'+idPedido).val();
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-alterar-status-pedido-unico.php',
    type :  'POST',
    data : {idPedido : idPedido,
            acao     : 'aprovar',
            obsPedido: obsPedido},
    success: function(data){
      $('.containerPedido'+idPedido).find('.botaoAceitarPedido').replaceWith('');
      $('.containerPedido'+idPedido).find('.botaoNegarPedido').replaceWith('');
      var qtdPedidosPendentes = $( ".layoutPedidos .conteudoPedido" ).length;
      // decresce a qtd de itens no pedido
      $('.qtdPedidosPendentes').html(qtdPedidosPendentes);
      // fecha o modal
      if(qtdPedidosPendentes == 0){
        $('#modalAvisoNovoPedido .close').trigger('click');
      }
    },
    error: function(date){
      console.log(data);
    }
  });
});

// botao aceitar todos os pedidos
$(document).on('click','.botaoAceitarTodosPedidos',function(){
  aceitarTodosPedidos($('#modalAvisoNovoPedido').attr('data-hora-abertura'));
});

function cancelarTodosPedidos(dataHoraPedidos){
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-alterar-status-pedidos-em-massa.php',
    type :  'POST',
    data : {dataHoraPedidos : dataHoraPedidos,
            acao            : 'cancelar'},
    success: function(data){
      // fecha o modal
      $('#modalAvisoNovoPedido .close').trigger('click');
    },
    error: function(date){
      console.log(data);
    }
  });
}

function aceitarTodosPedidos(dataHoraPedidos){
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-alterar-status-pedidos-em-massa.php',
    type :  'POST',
    data : {dataHoraPedidos : dataHoraPedidos,
            acao            : 'aprovar'},
    success: function(data){
      // fecha o modal
      $('#modalAvisoNovoPedido .close').trigger('click');
    },
    error: function(date){
      console.log(data);
    }
  });
}

function cancelarPedido(idPedido){
  var obsPedido = $('#obsAoCliente'+idPedido).val();
  $.ajax({
    url  : CAMINHO_DASHBOARD+'ajax/ajax-alterar-status-pedido-unico.php',
    type :  'POST',
    data : {idPedido : idPedido,
            acao     : 'cancelar',
            obsPedido: obsPedido},
    success: function(data){
      $('.containerPedido'+idPedido).replaceWith('');
      var qtdPedidosPendentes = $( ".layoutPedidos .conteudoPedido" ).length;
      // decresce a qtd de itens no pedido
      $('.qtdPedidosPendentes').html(qtdPedidosPendentes);
      // fecha o modal
      if(qtdPedidosPendentes == 0){
        $('#modalAvisoNovoPedido .close').trigger('click');
      }
    },
    error: function(date){
      console.log(data);
    }
  });
}

$(document).on('click','.botaoAbrirDetalhesPedido',function(){
  var target = $(this).attr('data-target');
  $(target).slideToggle();
});

function tocarAudioPedido(){
  var audio = new Audio(CAMINHO_DASHBOARD+'sound/alertaPedido.wav');
  audio.addEventListener('canplaythrough', function() {
    audio.play();
  });

  // var sound = new Howl({
  //   src: [CAMINHO_DASHBOARD+'sound/alertaPedido.wav']
  // });
  // sound.play();

}

$(document).on('click','.etiquetaAncora',function(){
  var idPedido = $(this).attr('data-id-pedido');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-dados-pedido-etiqueta.php',
    type    : 'POST',
    data    : {idPedido:idPedido},
    success: function(data){
      $('#conteudoEtiqueta').html(data);
    },
    error: function(data){
      console.log(data);
    },
  });
});

function montarAdicionaisItemPedidoEtiqueta(adicionais){
  //formata adicionais para string
  let adicionaisStr = ''
  adicionais.forEach(a=>{
    adicionaisStr += a.nome+', '
  })
  if(adicionaisStr!=''){
    adicionaisStr = '<li><b>Adicionais:</b> '+adicionaisStr+'</li>'
  }
  return adicionaisStr
}

function montarItensPedidoEtiqueta(itens){
  let divItens = document.createElement('div')
  itens.forEach(item=>{
    adicionaisStr = montarAdicionaisItemPedidoEtiqueta(item.adicionais)
    divItens.innerHTML = item.qtd +'un - '+item.nome_produto+adicionaisStr

  })
  return divItens;
}

function montarAdicionaisItensPedidoEtiqueta(adicionais){
  let div = document.createElement('div')
  itens.forEach(item=>{
    div.innerHTML = item.qtd +'un - '+item.nome_produto
  })
  return div;
}

$(document).on('click','.filtraPorData',function(){
  var dataFiltrar = $('.dataFiltrar').val();
  window.location = '?filtrarData='+dataFiltrar;
});

// evento realizar login
$(document).on('submit','.login100-form',function(){
    var email  = $('.emailLogin').val();
    var senha  = $('.passLogin').val();
    event.preventDefault();
    $.ajax({
        url     : CAMINHO_DASHBOARD+'ajax/ajax-login.php',
        type    : 'POST',
        data    : {'email': email, 'senha': senha},
        success : function (data) {
          if(!data){
            $('.retornoAjaxLogin').html('Email e password não conferem!');
            return false;
          }
          // Recarrega a página atual sem usar o cache
          document.location.reload(true);
        },
        error: function(data){
          console.log('ERR');
          console.log(data);
          return false;
        }
      });
    return false;
});

// evento logout
$(document).on('click','.logoutDashboard',function(){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-logout.php',
    type    : 'GET',
    success : function (data) {
      // Recarrega a página atual sem usar o cache
      document.location.reload(true);
    },
    error: function(data){
      console.log(data);
    }
  });
});

function exibirWarning(mensagem,submensagem){
  swal({
   title: mensagem,
   text: submensagem,
   html: true,
   type: "warning",
   customClass: 'swal-wide',
   confirmButtonColor: '#CACACA',
   showCancelButton: false,
   showConfirmButton:true
  });
}

// evento salvar horario atendimento
$(document).on('click','.salvar',function(){
  var erro   = false;
  var id_dia = $(this).attr('data-id-dia');
  var abre   = [];
  var fecha  = [];

  // dados abre
  $(this).siblings('.abre').find('input').each(function(index,element){
    // valida campos preenchidos
    if($(element).val() == ''){
      exibirWarning('Os campos não podem estar em branco!');
      erro = true;
    }
    abre[index] = {};
    abre[index].abre  = $(element).val();
    abre[index].id    = $(element).attr('data-id');
    abre[index].idDia = $(element).attr('data-id-dia');
  });

  // dados fecha
  $(this).siblings('.fecha').find('input').each(function(index,element){
    // valida campos preenchidos
    if($(element).val() == ''){
      exibirWarning('Os campos não podem estar em branco!');
      erro = true;
    }
    fecha[index]       = {};
    fecha[index].fecha = $(element).val();
    fecha[index].id    = $(element).attr('data-id');
    fecha[index].idDia = $(element).attr('data-id-dia');
  });

  // caso houver algum erro não deixa passar
  if(erro) return false;

  var localClick = $(this).hasClass('salvarAgendamento') ? 'agendamento' : 'atendimento';
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-salvar-atendimento.php',
    type    : 'POST',
    data    : {
      id_dia    : id_dia,
      abre      : abre,
      fecha     : fecha,
      localClick:localClick
    },
    success : function (data) {
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar   = false;
      toastr.success('Horário de atendimento atualizado!')
    },
    error: function(data){
      console.log('erro');
    }
  });
});

$(document).on('click','.addSubHorario',function(){
  var qtdHorarios = $(this).parent().siblings('.abre').find('input').length;
  if(qtdHorarios == 3){
    exibirWarning('Máximo de 3 horários por dia da semana!','Somente é permitido 3 horários por dia da semana!');
    return false;
  }
  var idDia       = $(this).attr('data-id-dia');
  var inputAbre   = '<br><input maxlength="8" class="horarioAtendimento" name="abre['+idDia+'][x]" data-id-dia="'+idDia+'" data-id="x" value="">';
  var inputFecha  = '<br><input maxlength="8" class="horarioAtendimento" name="fecha['+idDia+'][x]" data-id-dia="'+idDia+'" data-id="x" value=""><span class="removerLinhaHorario" data-id-dia="'+idDia+'" data-id="x"><i class="fas fa-minus-circle"></i><span>';
  $(this).parent().siblings('.abre').append(inputAbre);
  $(this).parent().siblings('.fecha').append(inputFecha);
});

$(document).on('click','.removerLinhaHorario',function(){
  var idDia     = $(this).attr('data-id-dia');
  var id        = $(this).attr('data-id');
  var container = $(this).parents('tr');

  $(container).find('input[name="abre['+idDia+']['+id+']"]').replaceWith('');
  $(container).find('input[name="fecha['+idDia+']['+id+']"]').replaceWith('');
  $(this).replaceWith('');
})

function replaceBadInputs(val,element) {
  // Replace impossible inputs as they appear
  val = val.replace(/[^\dh:]/, "");
  val = val.replace(/^[^0-2]/, "");
  val = val.replace(/^([2-9])[4-9]/, "$1");
  val = val.replace(/^\d[:h]/, "");
  val = val.replace(/^([01][0-9])[^:h]/, "$1");
  val = val.replace(/^(2[0-3])[^:h]/, "$1");
  val = val.replace(/^(\d{2}[:h])[^0-5]/, "$1");
  val = val.replace(/^(\d{2}h)./, "$1");
  val = val.replace(/^(\d{2}:[0-5])[^0-9]/, "$1");
  val = val.replace(/^(\d{2}:\d[0-9]:\d[0-9])./, "$1");

  if(val.length == 2 || val.length == 5){
    var valor = val + ':';
    $(element).val(val);
    val = valor;
  }

  return val;
}

// Apply input rules as the user types or pastes input
$(document).on('keyup','.horarioAtendimento,.horarioAgendamento',function(){
  var val = this.value;
  var lastLength;
  do {
    // Loop over the input to apply rules repeately to pasted inputs
    lastLength = val.length;
    val = replaceBadInputs(val,$(this));
  } while(val.length > 0 && lastLength !== val.length);
  this.value = val;
});

// evento fechar loja
$(document).on('click','.fechado input',function(){
  var  id_dia = $(this).attr('data-id-dia');
  var fechado = $(this).is(':checked') ? 's' : 'n';
  if(fechado == 's'){
    $(this).parent().siblings('.salvar').hide();
    $(this).parent().siblings('.abre').addClass("disabledbutton");
    $(this).parent().siblings('.fecha').addClass("disabledbutton");
  }else {
    $(this).parent().siblings('.salvar').show().removeClass('d-none');
    $(this).parent().siblings('.abre').removeClass("disabledbutton");
    $(this).parent().siblings('.fecha').removeClass("disabledbutton");
  }
  var localClick = $(this).hasClass('inativarDataAgendamento') ? 'agendamento' : 'atendimento';
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-fechar-loja.php',
    type    : 'POST',
    data    : {
      id_dia     : id_dia,
      fechado    : fechado,
      localClick : localClick
    }
  });
});


$(document).on('click','.containerImgEditar svg',function(){
  let imgProduto = $(this).parent().attr('imagem');
  let idProduto  = $(this).parent().attr('id-produto');
  exibirWarningOpcoesSimNao('Tem certeza que deseja excluir a imagem?', 'Não será possível recupera-la após a exclusão!', 'confirmarExcluirImagem');
  $(document).on('click','.confirmarExcluirImagem',function(){
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-excluir-imagem-produto.php',
      type    : 'POST',
      data    : {
        imgProduto : imgProduto,
        idProduto  : idProduto
      },
      success: function(data){
        document.location.reload(true);
      },
      error: function(data){
        console.log(data);
      }
    });
  })
})

function exibirWarningOpcoesSimNao(title, text, classBtnConfirm, classBtnCancel){
  swal({
    title: title,
    text: text,
    icon: "warning",
    buttons: true,
    dangerMode: true,
    buttons: {
      confirm: {
        text: "Sim",
        value: true,
        visible: true,
        className: classBtnConfirm,
        closeModal: true
      },
      cancel: {
        text: "Cancelar",
        value: false,
        visible: true,
        className: classBtnCancel,
        closeModal: true,
      }
    }
  });
}

function exibirWarningExclusao(){
  swal({
    title: "Tem certeza que deseja excluir?",
    text: "Após excluir, não será possível recuperar o registro!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    buttons: {
      confirm: {
        text: "Sim",
        value: true,
        visible: true,
        className: "tenhoCertezaExcluir",
        closeModal: true
      },
      cancel: {
        text: "Cancelar",
        value: false,
        visible: true,
        className: "cancelarExclusao",
        closeModal: true,
      }
    }
  });
}

// evento excluir item
$(document).on('click','.project-actions .btn-danger',function(){
  var id     = $(this).attr('data-id');
  var modulo = $(this).attr('data-modulo');
  exibirWarningExclusao();
  $(document).on('click','.tenhoCertezaExcluir',function(){
    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-excluir-item.php',
      type    : 'POST',
      data    : {
        id     : id,
        modulo : modulo
      },
      success : function (data) {
        document.location.reload(true);
      },
      error: function(data){
        console.log('erro');
        console.log(data);
      }
    });
  });
});

$(document).on('click','.verSenha',function(){
  $('.passwordUser').attr('type','text');
  $(this).html('<small>esconder senha</small>').addClass('esconderSenha').removeClass('verSenha');
});
$(document).on('click','.esconderSenha',function(){
  $('.passwordUser').attr('type','password');
  $(this).html('<small>ver senha</small>').removeClass('esconderSenha').addClass('verSenha');
});

$(document).on('click','.checkInativarProduto',function(){
  var inativo   = $(this).is(':checked') ? 's' : 'n';
  var idProduto = $(this).attr('data-id');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-ativar-inativar-produto.php',
    type    : 'POST',
    data    : {
      inativo  : inativo,
      idProduto:idProduto
    }
  });
});

$(document).on('click','.cuponsUtilizados',function(){
  var idCliente = $(this).attr('data-id-cliente');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-cupons-utilizado-cliente.php',
    type    : 'POST',
    data    : {
      idCliente : idCliente
    },
    success:function(data){
      $('.modal-body.text-center').html(data);
    },
    error:function(data){
      console.log(data);
    }
  });
});

$(document).on('click','.retirarRelatorioPedidos',function(){
  atualizarRelatorioProtudos();
});

function atualizarRelatorioProtudos(dataFiltrar = ''){
  var dataPedidos = dataFiltrar.length > 0 ? dataFiltrar : $('.filtrarDataRelatorioPedidos').val();
  console.log(dataPedidos);
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-relatorio-pedidos.php',
    type    : 'POST',
    data    : {
      dataPedidos : dataPedidos
    },
    success:function(data){
      $('.modal-body.text-center.relatorioPedidos').html(data);
    },
    error:function(data){
      console.log(data);
    }
  });
}

$(document).on('click','.verQuemUsou',function(){
  var hashCupom = $(this).attr('data-id-cupom');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-ver-quem-usou-cupom.php',
    type    : 'POST',
    data    : {
      hashCupom : hashCupom
    },
    success:function(data){
      $('.modal-body.text-center').html(data);
    },
    error:function(data){
      console.log(data);
    }
  });
});

// ver entregas feita pelos entregadores
$(document).on('click','.verEntregas',function(){
  now = new Date;
  var dia  = ("0" + now.getDate()).slice(-2);
  var mes  = ("0" + (now.getMonth() + 1)).slice(-2);
  var hoje = dia + '/' + mes + '/' + now.getFullYear();
  var idEntregador = $(this).attr('data-id-entregador');
  $('.filtraPorDataEntregador').attr('data-id-entregador',idEntregador);
  $('.filtrarDataEntregador').val(hoje + ' - ' + hoje);
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-entregas-entregador.php',
    type    : 'POST',
    data    : {
      idEntregador : idEntregador
    },
    success:function(data){
      $('.modal-body.text-center').html(data);
    },
    error:function(data){
      console.log(data);
    }
  });
});

// selecao do entregador para o pedido
$(document).on('change','.selectEntregadores',function(){
  var idEntregador = $(this).val();
  var idPedido     = $(this).attr('data-id-pedido');
  if(idEntregador == 0){
    alert('Selecione um entregador válido!');
    return false;
  }
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-atualizar-pedido-entregador.php',
    type    : 'POST',
    data    : {
      idEntregador : idEntregador,
      idPedido     : idPedido
    }
  });
});


// funcao print
function printData(idElemento){
    var mywindow = window.open('', 'PRINT', 'height=auto,width=400');
    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById(idElemento).innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
}

$(document).on('click','.containerImpressaoEntregadores',function(){
  printData('conteudoEntregas');
});

$(document).on('click','.imprimirConteudoProdutos',function(){
  printData('containerProdutos');
});

$(document).on('click','.imprimirConteudoClientes',function(){
  printData('containerClientes');
});

$(document).on('click','.impressora',function(){
  printData('conteudoEtiqueta');
});

$(document).on('click','.impressoraalerta',function(){
  printData('conteudoEtiquetaAlerta');
});

// ação 'addVendaInterna' para produtos cadastrados
$(document).on('click','.addVendaInterna',function(){
  verificarTipoProdutoAddCarrinho()
})

// verifica o tipo de um produto
function verificarTipoProdutoAddCarrinho(){
  var idProduto  = $('#selectProdutos').val()

  //consulta ajax, retorna string do tipo
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-verificar-tipo-produto-venda-interna.php',
    type    : 'GET',
    data    : {
      idProduto : idProduto
    },
    success:function(data){
      if(data=='pizza'){
        $('#txtTipoProduto').val('pizza')
        mostrarSaboresPizza()
      }else{
        mostrarAdicionais()
      }
    }
  })
}

function adicionarProdutoCarrinho(){
  var idProduto  = $('#selectProdutos').val()
  var quantidade = $('.quantidadeVendaInterna').val()

  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-carrinho-venda-interna.php',
    type    : 'POST',
    data    : {
      idProduto : idProduto,
      quantidade: quantidade,
      adicionais: montarJsonAdicionais(),
      tipo:'comum'
    },
    success:function(retorno){
      $('.quantidadeVendaInterna').html(1);
      location.reload();
      return false;
    }
  })
}

$(document).on('click','.salvarVinculoSabores',function(){
  var idProduto  = $('.tabelaVinculoSaboresProduto').attr('id-produto');
  var sabores = [];
  $('.conteudoSabores tbody tr td input').each(function(index,element){
    var idSabor      = $(element).attr('id-sabor');
    var vinculado    = $(element).is(':checked');
    sabores[idSabor] = vinculado;
  });
  salvarVinculoSabores(sabores,idProduto);
});

function salvarVinculoSabores(dadosVinculo,idProduto){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-salvar-vinculos-sabores-produto.php',
    type    : 'POST',
    data    : {
      dadosVinculo : dadosVinculo,
      idProduto    : idProduto
    },
    success:function(data){
      $('#modalSabores').modal('toggle');
      toastr.options.positionClass = 'toast-top-right';
      toastr.options.progressBar   = false;
      toastr.success('Atualizado!')
    }
  });
}

//salvar vinculos adicionais produto
$(document).on('click','.salvarVinculoAdicionais',function(){
  var idProduto  = $('.tabelaVinculoAdicionais').attr('id-produto')
  var adicionais = []
  $('.conteudoAdicionais tbody tr td input').each(function(index,element){
    var idAdicional         = $(element).attr('id-adicional')
    var vinculado           = $(element).is(':checked')
    adicionais[idAdicional] = vinculado
  })
  salvarVinculoAdicionais(adicionais,idProduto)
})

function salvarVinculoAdicionais(dadosVinculo,idProduto){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-salvar-vinculos-adicionais-produto.php',
    type    : 'POST',
    data    : {
      dadosVinculo : dadosVinculo,
      idProduto    : idProduto
    },
    success:function(data){
      $('#modalAdicionais').modal('toggle')
      toastr.options.positionClass = 'toast-top-right'
      toastr.options.progressBar   = false
      toastr.success('Atualizado!')
    }
  })
}

// modal vincular adicional ao produto
$(document).on('click','.modalAdicionaisProduto',function(){
  var idProduto  = $(this).attr('data-id-produto')
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-vinculos-adicional-produto.php',
    type    : 'POST',
    dataType: 'json',
    data    : {
      idProduto : idProduto
    },
    success:function(data){
      $('#txtBuscaAdicionaisProdutos').val('');
      $('#chkModalAdicionaisSelecionarTodos').prop('checked',false);
      $('.conteudoAdicionais').html(data)
      return false;
    }
  })
})

function buscaItensNaLista(classBusca, classConatinerLista, classItemLista, classeSeraEscondida, ev){
  // define a busca
  var busca = $(classBusca).val().toLowerCase();
  // caso estiver apagando a busca, mostra todos os itens
  if(ev.keyCode == 8 && busca.length <= 1){
    $(classeSeraEscondida).show(300);
  }
  // busca maior de 2 caracteres
  if(busca.length >= 2){
    // foreach das categorias
    $(classConatinerLista).each(function(index,element){
      var qtdResultadosEncontrados = 0;
      // foreach produtos
      $(element).find(classItemLista).each(function(indice,elemento){
        var labelProcurada = $(elemento).text().toLowerCase();
        var match          = labelProcurada.indexOf(busca) !== -1;
        if(!match){
          $(elemento).parents(classeSeraEscondida).hide(300);
        }else {
          qtdResultadosEncontrados++;
          $(elemento).parents(classeSeraEscondida).show(300);
        }
      });
      // caso nao achou nenhum produto ele esconde a categoria
      if(qtdResultadosEncontrados == 0){
        $(element).hide(300);
      }else {
        $(element).show(300);
      }
    });
  }
}

//busca por adicionais
$(document).on('keyup','#txtBuscaAdicionaisProdutos',function(e){
  buscaItensNaLista('#txtBuscaAdicionaisProdutos', '.tabelaVinculoAdicionais', '.labelAdicionalProdutoListagem', '.containerSelectLabelAdicionais', e);
})

//selecionar todos os adicionais
$(document).on('click','#chkModalAdicionaisSelecionarTodos',function(){
  let tbl = document.getElementsByClassName('conteudoAdicionais')[0].getElementsByTagName('table')[0]
  let trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr')

  //loop pelos trs marcando o check
  for(let l=0;l<trs.length;l++){
    trs[l].getElementsByTagName('td')[0].getElementsByTagName('input')[0].checked = this.checked
  }
})



//selecionar todos os sabores
$(document).on('click','#chkSelecionarTodosSabores',function(){
  let tbl = document.getElementsByClassName('conteudoSabores')[0].getElementsByTagName('table')[0]
  let trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr')

  //loop pelos trs marcando o check
  for(let l=0;l<trs.length;l++){
    trs[l].getElementsByTagName('td')[0].getElementsByTagName('input')[0].checked = this.checked
  }
})

//busca por sabores
$(document).on('keyup','#txtBuscaSaboresPizza',function(e){
  buscaItensNaLista('#txtBuscaSaboresPizza', '.tabelaVinculoSaboresProduto', '.labelSaborProduto', '.containerSelectSabores', e);
})

// vincular sabores ao produto pizza
$(document).on('click','.vincularSaboresModal',function(){
  var idProduto  = $(this).attr('id-produto');
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-vinculos-sabores-pizza.php',
    type    : 'POST',
    dataType: 'json',
    data    : {
      idProduto : idProduto
    },
    success:function(data){
      $('#txtBuscaSaboresPizza').val('');
      $('#chkSelecionarTodosSabores').prop('checked',false);
      $('.conteudoSabores').html(data)
      return false;
    }
  })
})

//produto custom
$(document).on('click','#addVendaInternaCustom',function(){
  let jsonProdutosCustom = montarJsonCustom()
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-carrinho-venda-interna.php',
    type    : 'POST',
    data    : {
      produtosCustom:jsonProdutosCustom
    },
    success:function(data){
      $('.quantidadeVendaInterna').html(1)
      location.reload()
      return false
    }
  })
})

function montarJsonCustom(){
  let unindexed_array = $('#dvProdutoCustom :input').serializeArray()
  return JSON.stringify(unindexed_array)
}

// valor entrega para o bairro
function getValorEntregaBairro(dados,cep,semCep = false){
  $.ajax({
    async   : false,
    url     : CAMINHO_DASHBOARD+'ajax/ajax-calcular-valor-entrega-venda-interna.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {dados :dados,
               cep   :cep,
               semCep: semCep},
    success : function (data) {
      console.log(data);
      var subTotal   = $('.subtotalCarrinho').html();
      var valorFrete = data.valor;
      var total      = parseFloat(subTotal) + parseFloat(valorFrete);
      $('.freteVendaInterna').html(valorFrete);
      $('.totalCarrinho').html(total);
      if(!semCep){
        $('.enderecoVendaInterna').html(data.endereco);
      }
    },
    error: function(data){
      console.log(data);
    }
  });
}

// selecionar forma de pagamento venda interna
$(document).on('change','#slcFormaPagamento',function(){

  // mostrar troco
  if($(this).find(":selected").data('troco')=='s'){
    $('.boxMetodoPagamento').show(400);
  }

  // ocultar troco
  else{
    $('.boxMetodoPagamento').hide(400);
  }
})

$(document).on('blur','.itemCep input',function(e){
  var valorSalvarSessao = $(this).val();
  var keyValorSessao    = $(this).attr('name');
  if(valorSalvarSessao.length == 0) return false;
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-salvar-valor-frete-sessao.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : { valorSalvarSessao : valorSalvarSessao,
                keyValorSessao    : keyValorSessao},
    success: function(data){
      $('.enderecoVendaInterna').append(', '+valorSalvarSessao);
    },error:function(data){
      console.log(data);
    }
  });
});

// busca o endereco da venda interna de acordo com o cep digitado
// $(document).on('blur','.cepVendaInterna',function(e){
//   var cepVendaInterna = $('.cepVendaInterna').val();
//   if(cepVendaInterna.length <= 6) return false;
//   //Consulta o webservice viacep.com.br/
//   $.getJSON("https://viacep.com.br/ws/"+ cepVendaInterna +"/json/?callback=?", function(dados) {
//       if(dados.uf == 'SP' && dados.localidade == "Caraguatatuba"){
//       getValorEntregaBairro(dados,cepVendaInterna);
//       var valorFrete = $('.freteVendaInterna').html();
//       // caso nao houver bairro cadastrado para entrega
//       if(!valorFrete || valorFrete == 0){
//         alert('entrega indisponível para esta localidade');
//         location.reload();
//         return false;
//       }
//     }else {
//       // caso cair aqui é por que não está na cidade do cliente
//       alert('entrega indisponível para sua localidade');
//       location.reload();
//       return false;
//     }
//     location.reload();
//   });
// });

$(document).on('blur','.bairroVendaIntera',function(e){
  var bairroVendaInterna = $(this).val();
  if(bairroVendaInterna.length == 0) return false;
  //Consulta o webservice viacep.com.br/
    var dados = {'bairro' : bairroVendaInterna};
    getValorEntregaBairro(dados,'',true);
    var valorFrete = $('.freteVendaInterna').html();
    // caso nao houver bairro cadastrado para entrega
    if(!valorFrete || valorFrete == 0){
      alert('entrega indisponível para esta localidade');
      location.reload();
      return false;
    }
});

$(document).on('blur','#whatsCliente',function(){
  var whatsCliente = $(this).val();
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-get-dados-cliente-por-contato.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {whatsCliente:whatsCliente},
    success: function(data){
      if(data.sucesso){
        $('#nomeCliente').val(data.dadosCliente.nome);
      }else {
        $('#nomeCliente').val('');
      }
    },error:function(data){
      console.log(data);
    }
  });
});

$(document).on('blur','.numeroVendaIntera',function(e){
  var numero = $('input[name="numero"]').val();
  if(numero.length == 0) return false;
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-numero-endereco-venda-interna.php',
    type    : 'POST',
    dataType: 'JSON',
    data    : {numero:numero},
    success: function(data){
      $('.numeroCarrinho').html(data);
      location.reload();
    },error:function(data){
      console.log(data);
    }
  });
});

$(document).on('click','.slideDown',function(){
  var idPedido = $(this).attr('data-id-pedido');
  $(this).find('i').toggleClass('fa-angle-right');
  $('.containerInfosExtras'+idPedido).toggle(300);
});

// limpar venda interna
$(document).on('click','.limparVendaInterna',function(){
  $.ajax({
    url     : CAMINHO_DASHBOARD+'ajax/ajax-limpar-venda-interna.php',
  });
  location.reload();
});

/* Máscaras ER */
function mascaraTel(o, f) {
  v_obj = o
  v_fun = f
  setTimeout(execmascara(), 1)
}

function execmascara() {
  v_obj.value = v_fun(v_obj.value)
}

function mtel(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/^(\d{2})(\d)/g, '($1) $2'); //Coloca parênteses em volta dos dois primeiros dígitos
  v = v.replace(/(\d)(\d{4})$/, '$1 - $2'); //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}

function id(el) {
  return document.getElementById(el);
}

$(document).on('keyup','#whatsCliente',function() {
  mascaraTel(this, mtel);
});

$(document).on('change','#origemVenda',function(){
  var valorOrigem = $(this).val();
  $.ajax({
    dataType : 'GET',
    url      : CAMINHO_DASHBOARD+'ajax/ajax-salvar-origem-sessao.php?origem='+valorOrigem,
  });
});
