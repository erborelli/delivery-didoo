<?php
    require_once('../../../../includes.php');

    //submit de salvar loja
    if(isset($_POST['botao']) && $_POST['botao']=='salvar'){

        //tratamento de URL
        $_POST['url'] = str_replace('http://','',$_POST['url']);
        $_POST['url'] = str_replace('https://','',$_POST['url']);
        $_POST['url'] = substr($_POST['url'],-1) == '/' ? $_POST['url'] : $_POST['url'].'/';

        if(isset($_POST['id']) && empty($_POST['id'])){
            Lojas::gravarLoja($_POST);
        }else{
            Lojas::updateLoja($_POST);
        }
        unset($_POST);
        $mensagem = 'alert("Loja salva!")';
    }else{
        $mensagem = '';
    }

    $titulo = 'Nova Loja';
    $loja=null;
    if(isset($_GET['id'])){
        $id     = $_GET['id'];
        $loja   = Lojas::getLojaPorId($id);
        $titulo = 'Editar Loja';
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$titulo?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div style="text-align: center;">
        <h4><?=$titulo?></h4>
    </div>
    <form action="" method="POST">
        <div class="form-group" style="padding: 15px;">
            <div class="row">
                <input type="hidden" name="id" value="<?=isset($loja['id'])?$loja['id']:''?>">
                <div class="col-md-12">
                    <label>Nome</label>
                    <input class="form-control" name="nome" value="<?=isset($loja['nome'])?$loja['nome']:''?>" required>
                </div>
                <div class="col-md-12">
                    <label>URL</label>
                    <input class="form-control" name="url" value="<?=isset($loja['url'])?$loja['url']:''?>" required>
                </div>
                <div class="col-md-12">
                    <br>
                    <button class="btn btn-primary" type="submit" name="botao" value="salvar">Salvar</button>
                    <a class="btn btn-warning" type="button" href="<?=CAMINHO?>/integracoes/painel/lojas/">Voltar</a>
                </div>
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
    <script>
        <?=$mensagem?>
    </script>
</body>
</html>