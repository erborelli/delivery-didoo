<?php
include_once('../../../includes.php');

$idProduto = $_GET['id'];
$produto   = Produto::getProdutoMesmoInativado($idProduto);
$produto   = $produto[0];

// caso houver o post da ação da edição
if(isset($_POST) && isset($_POST['btnEnviar'])){
  $_POST['imagensProduto'] = [];
  if(!empty($_FILES) && isset($_FILES['imagensProduto'])){
    foreach ($_FILES['imagensProduto']['tmp_name'] as $key => $tmpName) {
      if($_FILES['imagensProduto']['error'][$key] == 0){
        $destinoArquivo = CAMINHO_UPLOAD . 'produtos/' . basename($_FILES['imagensProduto']['name'][$key]);
        move_uploaded_file($tmpName, $destinoArquivo);
        $_POST['imagensProduto'][] = $_FILES['imagensProduto']['name'][$key];
        //redimensionamento de imagem para celular
        // $origemImagem        = CAMINHO_UPLOAD.'produtos/'.$_POST['imagensProduto'];
        // $destinoImagemMobile = CAMINHO_UPLOAD.'produtos/'.'mobile-'.$_POST['imagensProduto'];
        // Imagem::redimensionarPadrao($origemImagem,$destinoImagemMobile);
      }
    }
  }
  // recupero do banco as fotos já existentes
  $fotosExistentes = Produto::getFotosProdutoPorId($idProduto);
  // explode para virar array
  $fotosExistentes = array_filter(explode(',',$fotosExistentes));
  // merge das novas com as existentes
  $fotosProduto    = array_merge($fotosExistentes, $_POST['imagensProduto']);
  // implode para virar string para salvar no banco
  $_POST['imagensProduto'] = implode(',',$fotosProduto);

  // conta quantos pontos tem no preço
  // if(substr_count($_POST['preco'], '.') >= 2){
    // echo "<pre>"; print_r($_POST['preco']); echo "</pre>";
    // troca o ultimo ponto por virgula
    // $_POST['preco'] = substr_replace($_POST['preco'], ',', strrpos($_POST['preco'], '.'), 1);
    // remove tudo depois da virgula
    // $_POST['preco'] = substr($_POST['preco'], 0, strpos($_POST['preco'], ","));
    // echo "<pre>"; print_r($_POST['preco']); echo "</pre>";exit;
  // }

  Produto::updateProduto($_POST);
  $produto   = Produto::getProduto($idProduto);
  $produto   = $produto[0];

  // unset no post após realizar as ações de salvar
  header("Refresh:0");
  exit;
}

$idTamanho     = $produto->id_tamanho;
$layoutPizza   = '';
$modalSabores  = '';
$tamanhosPizza = PizzaTamanhos::getSelectTamanhosPizza($idTamanho);
$boxPreco     = '<div class="form-group">
                  <label for="preco">Preço</label>
                  <input type="text" name="preco" id="preco" value="'.$produto->valor.'" class="form-control"></input>
                </div>';
if($produto->tipo == 'pizza'){
  $boxPreco     = '<div class="form-group d-none">
                    <label for="preco">Preço</label>
                    <input type="text" name="preco" id="preco" value="999" class="form-control"></input>
                  </div>';
  $modalSabores = Sistema::getLayout([],'dashboard/layout/produtos/pizza','modal-produto-pizza-estrutura-sabores.php');
  $layoutPizza  = Sistema::getLayout(['tamanhosPizza' => $tamanhosPizza,'qtdSabores' => $produto->qtd_sabores,'idProduto' => $produto->id],'dashboard/layout/produtos/pizza','produto-pizza-estrutura-formulario.html');
}

if(!isset($_GET['id'])){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/');
}

// caso tenha retornado false
if(!$produto){
  header('location: '.CAMINHO_DASHBOARD.'painel/pages/produtos/');
}

$categorias = Categoria::getCategorias();
$adicionais = Adicional::getTodosAdicionaisPorProduto($_GET['id'])

?>


<!DOCTYPE html>
<html>
  <style>
    #div{
      border: 1px solid;
      padding: 5px;
      border-radius: 10px;
      border-color: #d2d4d7
    }
    .containerImgEditar{
      margin-left: 10px;
    }
    .previewImgs{
      display: none;
      font-weight: bold;
    }
    .gallery img{
      margin-left: 10px;
    }
    .containerImgEditar svg{
      position: absolute;
      background: black;
      fill: white;
      display: block;
      cursor: pointer;
    }
  </style>

<?php include_once('../../head/head-estrutura.php'); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- NAVEGAÇÃO -->
  <?php include_once('../../navegacao/navegacao-estrutura.php'); ?>

  <!-- Main Sidebar Container -->
  <?php include_once('../../sidebar/sidebar-estrutura.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Editar Produto</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produto</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <?=$modalSabores?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <form action="" enctype="multipart/form-data" style="display:contents;" method="post">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Editar Produto</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="nome">Nome Produto</label>
                  <input type="text" name="nome" id="nome" class="form-control" value="<?=$produto->nome?>">
                </div>
                <?=$boxPreco?>
                <div class="form-group">
                  <label for="categoria">Categoria</label>
                  <select name="categoria" class="form-control custom-select">
                    <option selected="" disabled="">Selecione Categoria</option>
                    <?php
                      foreach ($categorias as $key => $value) {
                        $selected = $value->id == $produto->id_categoria ? 'selected' : '';
                        echo '<option '.$selected.' value="'.$value->id.'">'.$value->nome.'</option>';
                      }
                     ?>
                  </select>
                </div>
                <?=$layoutPizza?>
                <div class="form-group">
                  <label for="descricao">Descrição</label>
                  <textarea name="descricao" id="descricao" class="form-control" rows="4"><?=$produto->descricao?></textarea>
                </div>

                <?php
                  $imagensProduto = '';
                  foreach (explode(',',$produto->foto_produto) as $key => $value) {
                    if(empty($value)) continue;
                    $imagensProduto .= '<div class="containerImgEditar" id-produto="'.$produto->id.'" imagem="'.$value.'">
                                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
                                          <img alt="Foto Produto" style="width:65px;" class="table-avatar" src="'.CAMINHO_DASHBOARD.'/images/produtos/'.$value.'">
                                        </div>';
                  }
                 ?>
                <div class="form-group">
                  <li class="list-inline-item" style="float:left;display:flex;">
                      <?=$imagensProduto?>
                  </li>
                  <span class="previewImgs">Preview das Imagens Selecionadas:</span>
                  <div class="gallery"></div>
                  <label for="imagensProduto">Foto Produto (300x300)</label><br>
                  <!-- <input type="image" id="inputEstimatedBudget" class="form-control"> -->
                  <input type="file" name="imagensProduto[]" id="imagensProduto" multiple>
                  <!-- <input type="submit" value="Upload Image" name="submit"> -->
                </div>

              </div>

              <input type="submit" style="margin:10px;" name="btnEnviar" value="Salvar" class="btn btn-success float-left">
            </div>
          </div>
        </form>
      </div>
      <br>
      <!-- tabela de adicionais do produto -->
      <div id="div" style="background-color: white;">
        <div style="text-align: center;">
          <h4>Adicionais vinculados ao produto</h4>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Vinculado a</th>
                <th>Adicional / Grupo Adicional</th>
                <th>Nome</th>
              </tr>
            </thead>
            <tbody>

                <?php
                  foreach($adicionais as $adicional){
                    $adicionaisStr = '';
                    $idAdicional   = 0;
                    $dados = json_decode($adicional['dados']);

                    if($adicional['tipo']=='adicionais'){
                      $adicional['tipo']='produto';
                    }

                    if($adicional['tipoAdicional']=='adicional'){
                      if(isset($dados->adicionais)){
                        $adicionalObj  = json_decode($dados->adicionais[0]);
                        $adicionaisStr = $adicionalObj->nome_adicional;
                        $idAdicional   = $adicionalObj->id_adicional;
                      }
                    }else{
                      $adicionaisStr = $dados->nome_grupo;
                      $idAdicional   = $dados->id_grupo;
                    }
                  ?>
                  <tr>
                    <td style="display: none;"><?=$idAdicional?></td>
                    <td><?=$adicional['tipo']?></td>
                    <td><?=$adicional['tipoAdicional']?></td>
                    <td><?=$adicionaisStr?></td>
                  </tr>
                <?php } ?>
            </tbody>
        </table>
      </div>
      <br>

    </section>
  </div>

  <footer class="main-footer">
    <strong><?=NOMELOJA?></strong>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script type="text/javascript">
  $('#preco').mask("#.##0.00", {reverse: true});

  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            if(filesAmount > 3){
              alert('Você pode realizar no máximo upload de 3 imagens!');
              return false;
            }
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<img width="100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#imagensProduto').on('change', function() {
        imagesPreview(this, 'div.gallery');
        $('.previewImgs').show();
    });
  });
</script>

<?php include_once('../../includes-scripts/includes-scripts-estrutura.php'); ?>

</body>
</html>
