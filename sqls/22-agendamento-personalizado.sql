CREATE TABLE IF NOT EXISTS `agendamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dia` int(11) NOT NULL DEFAULT 0,
  `label` varchar(50) NOT NULL DEFAULT '0',
  `abre` time DEFAULT NULL,
  `fecha` time DEFAULT NULL,
  `fechado` enum('s','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

INSERT INTO `agendamento` (`id`, `id_dia`, `label`, `abre`, `fecha`, `fechado`) VALUES
	(93, 7, 'domingo', '11:00:00', '16:00:00', 's'),
	(94, 5, 'sexta', '11:00:00', '16:00:00', 'n'),
	(95, 4, 'quinta', '11:00:00', '16:00:00', 'n'),
	(96, 3, 'quarta', '11:00:00', '15:00:00', 'n'),
	(98, 1, 'segunda', '11:00:00', '15:00:00', 'n'),
	(99, 6, 'sabado', '11:00:00', '16:00:00', 'n'),
	(100, 2, 'terca', '11:00:00', '15:00:00', 'n');


INSERT INTO `config` (`hash`, `valor`, `obs`) VALUES ('intervaloTempoAgendamento', '10', 'define o intervalo entre os horarios de agendamento em minutos');
