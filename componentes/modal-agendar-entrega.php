<div data-id-produto="" id="modalAgendarEntrega" class="modal w920 fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; justify-content: center; padding-right: 16px;">
  <div class="modal-dialog w920" role="document" data-cy="modal-dialog">
    <div class="modal-content w920" style="top: 0px;">
      <div class="modal-close-content">
        <i class="material-icons close-button js-modal-close" data-dismiss="modal" data-ga="modal-close" data-cy="modal-dialog__close">close</i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Agendar entrega</h4>
      </div>
      <div class="modal-header-mobile">
        <button class="modal-header-content-back fecharModalAgendarPedidoMobile">
          <i class="material-icons icon">keyboard_arrow_left</i>
          <span class="text"> Voltar</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="schedule-order-modal">
          <div class="row">
            <div class="col-xs-6">
              <div class="input-box">
                <label for="scheduleDate">Data</label>
                <input data-selecionada="" id="escolhaData" value="Selecionar a data" class="escolhaData js-schedule-hour-input input-field enabled" type="button">
                <input disabled id="date" id-dia-agendado="" class="escolhaData js-schedule-hour-input input-field enabled campoInputData" type="text">
                <span class="alterarData escolhaData">Alterar Data</span>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="input-box">
                <label for="scheduleHour">Horário</label>
                <input id="escolhaHorario" value="Selecionar o Horário" class="escolhaHorario js-schedule-hour-input input-field enabled" type="button">
                <input disabled type="text" data-horario="" value="" required id="horarioAgendamento" class="js-schedule-hour-input input-field enabled horarioAgendamento">
                <span class="alterarHorario escolhaHorario">Alterar Horário</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 error is-hidden">
              Para agendar um pedido é preciso informar a data e a hora para efetuar a entrega.
            </div>
            <div class="col-xs-12 save-container js-save-container with-remove">
              <button aria-label="close" id="scheduleSubmit" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent btn btn-iconed btn-iconed-action btn-schedule-order agendarPedido" data-ga-action="cart__schedule_submit" data-ga-category="cart">
                <i class="material-icons icon">check</i>
                Agendar </button>
            </div>
            <div class="col-xs-12 boxalert">
              * Os horários de entrega são aproximados
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer is-hidden"></div>
    </div>
  </div>
</div>
