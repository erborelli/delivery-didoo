<?php

include_once('includes.php');

// redireciona o usuário caso não houver a sessao logada
if(Login::verificarUsuarioLogado()){
    header('location: painel');
}else{
    header('location: login.php');
}