
var carrinho = []

//botão modal pedido
function adicionaProdutoPedido(){

    // verificar se tem itens selecionados
    if(verificarSaboresAdicionaisSelecionados()){

        let item = {
            produto:    getProduto(),
            adicionais: getArrayAdicionais(),
            sabores:    getArraySabores(),
            tipo:       getTipo(),
            quantidade: document.getElementById('txtQuantidade').value
        }

        carrinho.push(item)
        montarCarrinho(item)
        $('#pedidoModal').modal('toggle')
    }
}

// retorna json com os sabores selecionados
function getArraySabores(){

    if(getTipo()=='pizza'){
        let sabores = document.getElementsByName('theadSabores')[0]
        let trs     = sabores.getElementsByTagName('tr')
    
        let saboresArray = []
        for(let s=0;s<trs.length;s++){
            
            let tds = trs[s].getElementsByTagName('td')
            
            // verifica se linha esta selecionada
            if(tds[3].getElementsByTagName('input')[0].checked){
                let saboresSelecionados   = new Object()
                saboresSelecionados.id    = tds[0].textContent
                saboresSelecionados.nome  = tds[1].textContent
                saboresSelecionados.valor = tds[2].textContent
                saboresArray.push(saboresSelecionados)
            }
        }
        return saboresArray
    }else{
        return []
    }
}

// retorna json com ids dos adicionais selecionados
function getArrayAdicionais(){
    let adicionais = document.getElementsByName('theadAdicionais')

    // percorre os tbody
    let adicionaisArray = []
    for(let t=0;t<adicionais.length;t++){

        // percorre os tr do tbory atual
        let trs = adicionais[t].getElementsByTagName('tr')
        for(let s=0;s<trs.length;s++){
            let tds = trs[s].getElementsByTagName('td')

            // verifica se linha esta selecionada
            if(tds[3].getElementsByTagName('input')[0].checked){
                let adicionaisSelecionados   = new Object()
                adicionaisSelecionados.id    = tds[0].textContent
                adicionaisSelecionados.nome  = tds[1].textContent
                adicionaisSelecionados.valor = tds[2].textContent
                adicionaisArray.push(adicionaisSelecionados)
            }
        }
    }
    return adicionaisArray
}

function getProduto(){
    return {
        id          : document.getElementById('txtIdProduto').value,
        nome        : document.getElementById('txtNomeProduto').textContent,
        valor       : Number(document.getElementById('lblValor').textContent),
        quantidade  : document.getElementById('txtQuantidade').value,
        observacoes : document.getElementById('txtObservacoes').value,
    }
}

function getTipo(){
    return document.getElementById('txtTipo').value
}


function finalizarPedido(){

    if(document.getElementById('txtNomeCliente').value==''){
        alert('Preencha o seu nome!')
        return
    }else if(document.getElementById('txtWathsappCliente').value==''){
        alert('Preencha o seu WathsApp!')
        return
    }else{

        let pedido = new Object()
    
        pedido.itens           = carrinho
        pedido.clienteNome     = document.getElementById('txtNomeCliente').value
        pedido.clienteWathsapp = document.getElementById('txtWathsappCliente').value
        pedido.mesa            = document.getElementById('txtMesaCliente').value

        console.log(pedido)

        $.ajax({
            url      : URL_API_MOBILE+'pedido/?inserir-pedido',
            type     : 'POST',
            data     : pedido,
            headers: {
                'chave-acesso': CHAVE_API_MOBILE
            },
            success:function(data){
                console.log(data)
                $('#finalizarModal').modal('toggle')
                alert('Pedido realizado com sucesso!')
                document.location.reload(true)
            },
            error: function(data){
                console.log(data)
            }
        })
    }
}

function verificarSaboresAdicionaisSelecionados(){

    let tabelas = document.getElementById('dvPedido').getElementsByTagName('table')

    for(let t=0;t<tabelas.length;t++){
        let min   = tabelas[t].getElementsByClassName('min')[0].value
        let tbody = tabelas[t].getElementsByTagName('tbody')[0]

        let qtdSelecionado = getQuantidadeSelecionados(tbody)

        if(qtdSelecionado < min){
            let nomeGrupo = tabelas[t].previousElementSibling.getElementsByTagName('h5')[0].textContent
            alert(nomeGrupo + ' deve ter ao menos '+min+' itens selecionados!')
            return
        }
    }

    return true
}

function getQuantidadeSelecionados(tbody){
    let qtd = 0
    let trs = tbody.getElementsByTagName('tr')
    for(let i=0;i<trs.length;i++){
        let tdChk = trs[i].getElementsByTagName('td')[3].getElementsByTagName('input')[0]
        if(tdChk.checked){
            qtd++
        }
    }
    return qtd
}

// funções pra aumentar e diminuir quatidade de produto no modal de pedido
function diminuirQtd(){
    let qtd = document.getElementById('txtQuantidade')
    if(Number(qtd.value)>1){
        qtd.value = (Number(qtd.value)-1)
    }
}

function aumentarQtd(){
    let qtd = document.getElementById('txtQuantidade')
    qtd.value = (Number(qtd.value)+1)
}