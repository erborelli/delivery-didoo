
//Ação ao confirmar modal de sabores da pizza
$(document).on('click','#btnConfirmarSaborPizza',function(){
    if(quantidadeSaboresSelecionados()==0){
        alert('Selecione um sabor de pizza!')
    }else{
        //fecha modal sabores da pizza
        $('#saboresPizzaModal').modal('hide')
        mostrarAdicionais()
    }
})

//metodo monta sabores da pizza e abre modal
function mostrarSaboresPizza(){
    var idProduto  = $('#selectProdutos').val()

    //ajax retorna sabores
    consultarSaboresPizza(idProduto)
    
    //modal com os sabores da pizza
    $('#saboresPizzaModal').modal()
}

function consultarSaboresPizza(idProduto){
    $.ajax({
        url  : CAMINHO_DASHBOARD+'ajax/ajax-consultar-sabores-pizza.php',
        type : 'GET',
        data : {
          idProduto : idProduto
        },
        success:function(data){
            montarTabelaSaboresPizza(data)
        },
        error: function(data){
            console.log(data);
        }
    })
}

function montarTabelaSaboresPizza(dados){
    let sabores = JSON.parse(dados)
    document.getElementById('lblQtdSaboresPizza').textContent = sabores[0].qtd_sabores

    let tbl = document.getElementById('tbodySaboresPizza')
    tbl.innerHTML = null

    sabores.forEach(e => {
        let tr             = document.createElement('tr')
        let tdSabor        = document.createElement('td')
        let tdPreco        = document.createElement('td')
        let tdIdSaborValor = document.createElement('td')
        let tdSelecionar   = document.createElement('td')

        tdIdSaborValor.style.display = 'none'
        tdSelecionar.style.textAlign = 'center'

        let chkSelecionado       = document.createElement('input')
        chkSelecionado.className = 'form-check-input'
        chkSelecionado.type      = 'checkbox'
        chkSelecionado.setAttribute('onclick','selecionarSabor(this)')
        tdSelecionar.append(chkSelecionado)

        tdSabor.textContent        = e.sabor
        tdPreco.textContent        = e.preco
        tdIdSaborValor.textContent = e.idSaborValor

        tr.append(tdSabor)
        tr.append(tdPreco)
        tr.append(tdIdSaborValor)
        tr.append(tdSelecionar)
        tbl.append(tr)
    })
}

function selecionarSabor(elemento){
    let lblQtdSaboresPizza  = document.getElementById('lblQtdSaboresPizza')
    let saboresSelecionados = quantidadeSaboresSelecionados()

    if(saboresSelecionados > lblQtdSaboresPizza.textContent){
        alert('Maximo de '+lblQtdSaboresPizza.textContent+' sabores!')
        elemento.checked = false
    }
}

function quantidadeSaboresSelecionados(){
    let tbodySaboresPizza = document.getElementById('tbodySaboresPizza')
    let tds = tbodySaboresPizza.getElementsByClassName('form-check-input')

    let saboresSelecionados = 0
    for(let i=0;i<tds.length;i++){
        if(tds[i].checked){
            saboresSelecionados++
        }
    }
    return saboresSelecionados
}

//percorre tabela de sabores e monta json apenas com sabores selecionados
function montarJsonSaboresPizza(){
    let tbodySaboresPizza = document.getElementById('tbodySaboresPizza')
    let tds = tbodySaboresPizza.getElementsByClassName('form-check-input')

    let saboresSelecionados = []
    for(let i=0;i<tds.length;i++){
        if(tds[i].checked){
            let saborAtual = new Object()
            saborAtual.nome         = tds[i].parentElement.parentElement.getElementsByTagName('td')[0].textContent
            saborAtual.valor        = tds[i].parentElement.parentElement.getElementsByTagName('td')[1].textContent
            saborAtual.idSaborValor = tds[i].parentElement.parentElement.getElementsByTagName('td')[2].textContent
            saboresSelecionados.push(saborAtual)
        }
    }
    return JSON.stringify(saboresSelecionados)
}

function adicionarPizzaCarrinho(){
    var idProduto  = $('#selectProdutos').val()
    var quantidade = $('.quantidadeVendaInterna').val()
    let sabores    = montarJsonSaboresPizza()
    let adicionais = montarJsonAdicionais()

    $.ajax({
      url     : CAMINHO_DASHBOARD+'ajax/ajax-adicionar-carrinho-venda-interna.php',
      type    : 'POST',
      data    : {
        idProduto : idProduto,
        quantidade: quantidade,
        adicionais: adicionais,
        sabores   : sabores,
        tipo      : 'pizza'
      },
      success:function(data){
        $('.quantidadeVendaInterna').html(1)
        location.reload()
        return false
      }
    })
  }