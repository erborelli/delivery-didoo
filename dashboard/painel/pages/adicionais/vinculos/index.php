<?php include_once('../../../../includes.php'); ?>
<!DOCTYPE html>
<html>
<?php include_once('../../../head/head-estrutura.php'); ?>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include_once('../../../navegacao/navegacao-estrutura.php'); ?>
        <?php include_once('../../../sidebar/sidebar-estrutura.php'); ?>
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Associação em Massa</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Associação em Massa</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="row">
                    <div style="margin-top: 5px; margin-left: 10px; text-align: end;">
                        <label>Vincular</label>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" id='slcFiltragem'>
                            <option value="produto">Produtos</option>
                            <option value="categoria">Categoria</option>
                        </select>
                    </div>
                    <div style="margin-top:5px; text-align:center;">
                        <label>ao</label>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" id='slcTipoAdicional'>
                            <option value="adicionais">Adicional</option>
                            <option value="gruposAdicionais">Grupo de Adicionais</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" id='slcAdicional'></select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary" type="button" id="btnAssociar">Associar</button>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-4">
                        <label>Busca por item</label>
                        <input id="txtBusca" class="form-control" placeholder="buscar">
                    </div>
                </div>
                <br>
                <div style="text-align: center;">
                    <h3 id='h3Titulo'>teste</h3>
                </div>
                <div style="margin-left: 18px;">
                    <input type="checkbox" onclick="selecionarTodosItens(this)">
                    <label>Selecionar todos</label>
                </div>
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th style="width:20px">
                                        Selecionado
                                    </th>
                                    <th style="width:20px">
                                        Id
                                    </th>
                                    <th>
                                        Nome
                                    </th>
                                </tr>
                            </thead>
                            <tbody id='tbody'></tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
        <footer class="main-footer">
            <strong><?=NOMELOJA?></strong>
        </footer>
        <aside class="control-sidebar control-sidebar-dark"></aside>
    </div>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/jquery/jquery.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/adminlte.min.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/dist/js/demo.js"></script>
    <script src="<?=CAMINHO_DASHBOARD.'painel'?>/plugins/summernote/summernote-bs4.min.js"></script>
    <script>
        document.getElementById('slcFiltragem').setAttribute('onChange','selecionar(this)')
        document.getElementById('slcAdicional').setAttribute('onChange','atualizarItens()')
        document.getElementById('txtBusca').setAttribute('oninput','buscarPorItem(this)')
        document.getElementById('slcTipoAdicional').setAttribute('oninput','getAdicionais()')
        document.getElementById('btnAssociar').setAttribute('onclick','associar()')

        var produtosSelecionados   = []
        var produtosDesmarcados    = []
        var categoriasSelecionados = []
        var categoriasDesmarcados  = []

        // inicia o 'slcAdicionais'
        getAdicionais()
        
        // delay para bug de listagem
        setTimeout(
            function(){
                // inicia a página com listagem de produtos
                get('produtos')                
            },
            100
        )

        function selecionarTodosItens(chk){
            let tbody = document.getElementById('tbody')
            let trs = tbody.getElementsByTagName('tr')

            for(let l=0;l<trs.length;l++){
                let td    = trs[l].getElementsByTagName('td')[0]
                let input = td.getElementsByTagName('input')[0]

                input.click()
            }
        }

        // atualiza listagem de itens para o adicional selecionado
        function atualizarItens(){
            produtosSelecionados   = []
            categoriasSelecionados = []
            buscarPorItem()
        }

        // executa ao mudar valor da filtragem de itens
        function selecionar(slc){
            produtosSelecionados   = []
            categoriasSelecionados = []
            document.getElementById('txtBusca').value = ''

            if(slc.value=='produto'){
                document.getElementById('h3Titulo').textContent = 'Produtos'
                get('produtos')
            }else{
                document.getElementById('h3Titulo').textContent = 'Categorias'
                get('categorias')
            }
        }

        // execução ao digitar no campo de busca 'txtBusca'
        function buscarPorItem(){
            // verifica a filtragem e faz a requisição para receber os dados
            slcFiltragem = document.getElementById('slcFiltragem').value
            if(slcFiltragem=='produto'){
                document.getElementById('h3Titulo').textContent = 'Produtos'
                get('produtos')
            }else{
                document.getElementById('h3Titulo').textContent = 'Categorias'
                get('categorias')
            }
        }

        // ação executada ao marcar/desmarcar checkbox na listagem de itens
        function selecionarElemento(tipo,id,elemento){
            // se listagem atual for de produtos
            if(tipo=='produtos'){

                // elemento foi desmarcado
                if(elemento.checked==false){
                    // adiciona no array de desmarcados
                    produtosDesmarcados.push(id)

                    // verifica se elemento esta no array de produtos selecionados
                    let index = produtosSelecionados.indexOf(id)
                    if(index > -1){
                        // remove elemento do array de selecionado
                        produtosSelecionados.splice(index, 1)
                    }
                }
                // elemento marcado
                else{
                    // adiciona no array de selecionados
                    produtosSelecionados.push(id)

                    // verificar se elemento esta no array de desmarcados
                    let index = produtosDesmarcados.indexOf(id)
                    if(index > -1){
                        // remove elemento do array de desmarcados
                        produtosDesmarcados.splice(index, 1)
                    }
                }
            }
            
            // se listagem atual for de categorias
            else if(tipo=='categorias'){
                if(elemento.checked==false){
                    categoriasDesmarcados.push(id)
                    let index = categoriasSelecionados.indexOf(id)
                    if(index > -1){
                        categoriasSelecionados.splice(index, 1)
                    }
                }
                else{
                    categoriasSelecionados.push(id)
                    let index = categoriasDesmarcados.indexOf(id)
                    if(index > -1){
                        categoriasDesmarcados.splice(index, 1)
                    }
                }
            }
        }

        // monta tabela de itens com 'produtos' ou 'categorias'
        function montarTabela(tipo,data){
            tbody = document.getElementById('tbody')
            tbody.innerHTML = ''

            data.forEach(e => {
                let tr     = document.createElement('tr')
                let tdChk  = document.createElement('td')
                let tdId   = document.createElement('td')
                let tdNome = document.createElement('td')

                let chk = document.createElement('input')
                chk.type      = 'checkbox'
                chk.className = 'form-check-input'
                chk.onchange = function(){selecionarElemento(tipo,e.id,this)}

                if(tipo=='produtos'){
                    // checka com dados do banco
                    if(e.adicional=='s' && produtosDesmarcados.indexOf(e.id)==-1){
                        chk.checked = true
                    }

                    // checka com o que o usuario marcou, mas ainda não associou
                    if(produtosSelecionados.indexOf(e.id)>-1){
                        chk.checked = true
                    }
                }else if(tipo=='categorias'){
                    if(e.adicional=='s' && categoriasDesmarcados.indexOf(e.id)==-1){
                        chk.checked = true
                    }

                    if(categoriasSelecionados.indexOf(e.id)>-1){
                        chk.checked = true
                    }
                }

                tdChk.style.textAlign = 'center'
                tdChk.append(chk)
                tdId.textContent   = e.id
                tdNome.textContent = e.nome

                tr.append(tdChk)
                tr.append(tdId)
                tr.append(tdNome)
                tbody.append(tr)
            })
        }

        // monta select de adicionais com os dados recebidos na requisição
        function montarSelectAdicionais(data){
            let slcAdicional = document.getElementById('slcAdicional')
            slcAdicional.innerHTML = ''

            data.forEach(e => {
                let option = document.createElement('option')
                option.value       = e.id
                option.textContent = e.nome
                slcAdicional.append(option)
            })

            // corrigi 'bug' causado pela função assincrona do ajax
            let primeiroAdicional = slcAdicional.getElementsByTagName('option')[0].value
            slcAdicional.value    = primeiroAdicional

            atualizarItens()
        }

        // faz requisição ajax pra receber dados de 'produtos' ou 'categorias'
        function get(tipo){
            let busca         = document.getElementById('txtBusca').value
            let idAdicional   = document.getElementById('slcAdicional').value
            let tipoAdicional = document.getElementById('slcTipoAdicional').value

            $.ajax({
                url     : CAMINHO_DASHBOARD+'ajax/ajax-get-dados-adicionais.php?tipo='+tipo+'&busca='+busca+'&idAdicional='+idAdicional+'&tipoAdicional='+tipoAdicional,
                type    : 'GET',
                dataType: 'JSON',
                success: function(data){
                    montarTabela(tipo, data)
                },
                error: function(data){
                    console.log(data);
                },
            })
        }

        // faz requisição pra receber 'adicionais' ou 'grupo de adicionais'
        function getAdicionais(){
            let slcTipoAdicional = document.getElementById('slcTipoAdicional').value
            $.ajax({
                url     : CAMINHO_DASHBOARD+'ajax/ajax-get-dados-adicionais.php?tipo='+slcTipoAdicional,
                type    : 'GET',
                dataType: 'JSON',
                success: function(data){
                    montarSelectAdicionais(data)
                },
                error: function(data){
                    console.log(data);
                },
            })
        }
        
        // requisição post pra vincular itens a adicionais/ grupos de adicionais
        function associar(){
            let itens = []
            let itensDesmarcados = [] 
            if(document.getElementById('slcFiltragem').value == 'produto'){
                itens = produtosSelecionados
                itensDesmarcados = produtosDesmarcados
            }else{
                itens = categoriasSelecionados
                itensDesmarcados = categoriasDesmarcados
            }

            if(confirm("Deseja mesmo vincular os adicionais selecionados?")){
                $.ajax({
                    url     : CAMINHO_DASHBOARD+'ajax/ajax-post-vincular-adicionais.php',
                    type    : 'POST',
                    data    : {
                        itens            : itens,
                        itensDesmarcados : itensDesmarcados,
                        tipoItens        : document.getElementById('slcFiltragem').value,
                        adicional        : document.getElementById('slcAdicional').value,
                        tipoAdicional    : document.getElementById('slcTipoAdicional').value
                    },
                    success: function(recebido){
                        if(recebido=='1'){
                            alert('Associado com sucesso!')
                        }else{
                            console.log(recebido)
                            alert('Erro ao associar!')
                        }
                    },
                    error: function(recebido){
                        console.log(recebido)
                        alert('Erro ao associar!')
                        return
                    },
                })
            }
        }
    </script>
</body>
</html>