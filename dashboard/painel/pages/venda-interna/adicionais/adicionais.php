
<div class="modal fade" id="adicionaisModal" tabindex="-1" role="dialog" aria-labelledby="adicionaisModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="adicionaisModalLabel">Adicionais disponiveis</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
            <label>Valor total dos adicionais: R$</label>
            <label id="lblValorTotalAdicionais">0</label>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">Adicional</th>
                        <th style="text-align: center;">Valor</th>
                        <th style="text-align: center;">Selecionar</th>
                    </tr>
                </thead>
                <tbody id="tbodyAdicionais">
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnConfirmarAdicional" class="btn btn-primary">Continuar</button>
      </div>
    </div>
  </div>
</div>