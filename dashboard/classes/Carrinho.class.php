<?php

class Carrinho {

  public static function adicionarProduto($dadosPost){
    // iniciar variaveis
    $retorno = ['boxProduto'          => '',
                'boxProdutoMobile'    => '',
                'labelCarrinhoMobile' => '',
                'sucesso'             => false,
                'subtotalCarrinho'    => false,
                'totalCarrinho'       => 0];
    // faz validações iniciais
    if(!isset($dadosPost['idProduto']) || empty($dadosPost['idProduto']) || !is_numeric($dadosPost['idProduto'])) return false;
      $produto = Produto::getProduto($dadosPost['idProduto']);
      // verifica se realmente o id passado é referente a um produto existente
      if(!empty($produto)){
        $produto = $produto[0];
        $produto->quantidade = $dadosPost['qtdProduto'];
        // caso ainda não houver itens então quer dizer que será acrescentado na posição 0
        if(empty($_SESSION['carrinho']['itens'])){
          $produto->idSessaoCarrinho = 0;
        }else {
          $keyArray = array_keys($_SESSION['carrinho']['itens']);
          $keyArray = end($keyArray);
          $produto->idSessaoCarrinho = $keyArray + 1;
        }
        self::verificarAdicionaisCarrinho($produto);
        self::verificarValoresPizzaCarrinho($produto);
        $produto->valor = Produto::calcularValorProduto($produto->id, true);
        $produto->observacao = $dadosPost['observacao'];
        // adiciona o objeto do produto na sessao do cliente
        $_SESSION['carrinho']['itens'][] = serialize($produto);

        $retorno['sucesso']     = true;
        $produtosSessao         = $_SESSION['carrinho']['itens'];
        $subtotalCarrinho       = 0;

        // percorre os produtos da sessao
        foreach ($produtosSessao as $key => $value) {
          // unserialize no produto que estava na sessao
          $produto = unserialize($value);
          $produto->valor    = number_format($produto->valor,2);
          $subtotalCarrinho += $produto->valor * $produto->quantidade;
          $total             = $subtotalCarrinho + $_SESSION['carrinho']['frete'];
          $valorProduto      = $produto->valor * $produto->quantidade;
          $valorProduto      = number_format($valorProduto,2);
          $boxSabores        = self::montarBoxSabores($produto);
          $labelObservacao   = self::getLabelObservacao($produto->observacao);
          $boxAdicionais     = self::montarBoxAdicionais($produto);
          $retorno['boxProduto']   .= '
          <li class="row list-item js-checkout-details-items-list js-checkout-details-items-list-'.$produto->id.'" data-id="'.$produto->id.'" data-iid="1060098676" data-ga="newcheckout-edit-item">
            <div class="col-xs-1 list-item-content edit-content js-edit-checkout-details-items-list" data-ga-action="cart__edit_item" data-ga-category="cart" data-id="'.$produto->id.'" data-iid="1060098676" data-cy="cart__edit-item--'.$produto->id.'">
              <i class="material-icons icon action-color">mode_edit</i>
            </div>
            <div class="col-xs-6 list-item-content description-content" data-ga-action="cart__edit_item" data-ga-category="cart" data-cy="cart__item-description">
              <span class="title">'.$produto->nome.'</span><br>
              '.$boxSabores.'
              '.$boxAdicionais.'
              '.$labelObservacao.'
            </div>
            <div class="col-xs-2 list-item-content amount-content">
              <span class="amount js-amount">'.$produto->quantidade.'</span>
            </div>
            <div class="col-xs-3 list-item-content price-content">
              <span class="text js-price" data-price="'.$produto->valor.'" data-total="'.$valorProduto.'">R$'.$valorProduto.'</span>
              <span class="delete js-delete-item" data-id-sess="'.$produto->idSessaoCarrinho.'" data-id="'.$produto->id.'" data-iid="1060098676">
                <i class="material-icons list-item-delete" data-ga-action="cart__remove_item" data-ga-category="cart">highlight_off</i>
              </span>
            </div>
            <div class="col-xs-12 list-item-observations js-observations"></div>
          </li>';
          $retorno['boxProdutoMobile']   .= '
          <ul class="col-xs-12 checkout-details-items-list checkout-separator-bottom">
            <li class="checkout-details-items-list-item checkout-details-items-list-item-'.$produto->id.'" data-id="'.$produto->id.'" data-status=""
              data-numeric_price="'.$produto->valor.'" data-price="0,00" data-view_order="0" data-ref="items-list-item-628149" data-base_price="0" data-cover_photo="">

              <div class="item-checkout js-item row" data-id-sess="'.$produto->idSessaoCarrinho.'">
                <div class="col-xs-7 js-edit-checkout-details-items-list" data-id="'.$produto->id.'" data-status=""
                  data-numeric_price="'.$produto->valor.'" data-price="0,00" data-view_order="0" data-ref="items-list-item-628149" data-base_price="0" data-cover_photo="">
                  <div class="item-checkout-name-box">
                    <span class="title">'.$produto->nome.'</span><br>
                    '.$boxSabores.'
                    '.$boxAdicionais.'
                    '.$labelObservacao.'
                  </div>
                </div>
                <div class="col-xs-2">
                  <div class="item-checkout-amount text-align-right" data-id="'.$produto->id.'" data-price="0" data-properties_price="'.$produto->valor.'">
                    '.$produto->quantidade.'
                  </div>
                </div>
                <div class="col-xs-3">
                  <div class="item-checkout-price js-price text-align-right" data-price="0">
                    R$'.$produto->valor.'
                    <i class="material-icons item-checkout-delete js-delete-item" data-id="'.$produto->id.'">highlight_off</i>
                    <i class="input-amount" data-id="'.$produto->valor.'"></i>
                  </div>
                </div>
              </div>
            </li>
          </ul>';
        }
        $_SESSION['carrinho']['subtotal'] = number_format($subtotalCarrinho,2);
        $_SESSION['carrinho']['total']    = number_format($total,2);
        if(isset($_SESSION['carrinho']['cupom'])){
          Cupom::atualizarSessaoValoresCupom();
        }
        // label carrinho mobile
        $qtdItensCarrinho = count($_SESSION['carrinho']['itens']);
        $retorno['labelCarrinhoMobile'] = 'Seu carrinho está vazio.';
        if($qtdItensCarrinho > 0){
          $retorno['labelCarrinhoMobile'] = $qtdItensCarrinho > 1 ? $qtdItensCarrinho.' ITENS NO SEU CARRINHO' : $qtdItensCarrinho.' ITEM NO SEU CARRINHO';
        }
        $retorno['totalCarrinho']         = $_SESSION['carrinho']['total'];
        $retorno['subtotalCarrinho']      = $_SESSION['carrinho']['subtotal'];
      }
      return $retorno;
    }

    /**
     * Método responsável por receber um objeto de produto e validar se na sessao
     * se existe adicionais para incluir no ob do produto do carrinho
     */
    public static function verificarAdicionaisCarrinho(&$obProduto){
      if(!$obProduto instanceof Produto) return false;
      if(isset($_SESSION['valoresProduto']['valoresAdicional']) && isset($_SESSION['valoresProduto']['valoresAdicional'][$obProduto->id])){
        $obProduto->adicionais = (object)$_SESSION['valoresProduto']['valoresAdicional'][$obProduto->id];
      }
    }

    public static function montarBoxSabores($obProduto){
      if(!isset($obProduto->sabores) || empty($obProduto->sabores)) return '';
      $listaSabores = array_keys((array)$obProduto->sabores);
      $labelSabores = PizzaSabores::getLabelSaboresPorIds($listaSabores);
      $retorno = '<b>Sabores:</b><br>';
      foreach ($labelSabores as $key => $value) {
        $retorno .= '<span class="saboresCart">'.$value->label.'</span>';
      }
      return $retorno;
    }

    public static function getLabelObservacao($observacao){
      if(!isset($observacao) || empty($observacao)) return '';
      $retorno  = '<b>Observação:</b><br>';
      $retorno .= '<span class="saboresCart">'.$observacao.'</span>';
      return $retorno;
    }

    public static function montarBoxAdicionais($obProduto){
      if(!isset($obProduto->adicionais) || empty($obProduto->adicionais)) return '';
      $listaAdicionais = [];
      foreach ($obProduto->adicionais as $key => $value) {
        $listaAdicionais[] = $key;
      }
      $labelAdicionais = Adicional::getLabelAdicionaisPorIds($listaAdicionais);
      $retorno = '<b>Adicionais:</b><br>';
      foreach ($labelAdicionais as $key => $value) {
        $retorno .= '<span class="adicionaisCart">'.$value->nome.'</span>';
      }
      return $retorno;
    }

    public static function verificarValoresPizzaCarrinho(&$obProduto){
      if(!$obProduto instanceof Produto) return false;
      if(isset($_SESSION['valoresProduto']['valoresPizza']) && isset($_SESSION['valoresProduto']['valoresPizza'][$obProduto->id])){
        $obProduto->sabores = (object)$_SESSION['valoresProduto']['valoresPizza'][$obProduto->id];
      }
    }

    public static function getValoresCarrinho(){
      $produtosSessao = $_SESSION['carrinho']['itens'];
      $retorno = ['total'    => 0,
                  'subtotal' => 0,
                  'frete'    => $_SESSION['carrinho']['frete']];
      foreach ($produtosSessao as $key => $value) {
        $produto = unserialize($value);
        $retorno['subtotal'] += $produto->valor * $produto->quantidade;
      }
      $retorno['total']    = number_format($retorno['subtotal'] + $retorno['frete'],2);
      $retorno['subtotal'] = number_format($retorno['subtotal'],2);
      return $retorno;
    }

    public static function calculaEAtualizarValoresSessaoCarrinho(){
      $valores = self::getValoresCarrinho();
      $_SESSION['carrinho']['total']    = $valores['total'];
      $_SESSION['carrinho']['subtotal'] = $valores['subtotal'];
      if(isset($_SESSION['carrinho']['cupom'])){
        $subtotalCarrinho = $_SESSION['carrinho']['subtotal'];
        $valorDesconto    = $subtotalCarrinho * $_SESSION['carrinho']['valorDescontoCupom'] / 100;

        $_SESSION['carrinho']['subtotal'] = $subtotalCarrinho - $valorDesconto;
        $_SESSION['carrinho']['total']    = $_SESSION['carrinho']['subtotal'] + $_SESSION['carrinho']['frete'];
      }
    }

}
